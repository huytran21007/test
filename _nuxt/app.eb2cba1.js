/*! For license information please see LICENSES */
(window.webpackJsonp = window.webpackJsonp || []).push([
  [0],
  [
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    function (t, e, n) {
      "use strict";
      n.d(e, "k", function () {
        return y;
      }),
        n.d(e, "l", function () {
          return w;
        }),
        n.d(e, "e", function () {
          return x;
        }),
        n.d(e, "b", function () {
          return C;
        }),
        n.d(e, "q", function () {
          return _;
        }),
        n.d(e, "g", function () {
          return O;
        }),
        n.d(e, "h", function () {
          return P;
        }),
        n.d(e, "d", function () {
          return L;
        }),
        n.d(e, "p", function () {
          return k;
        }),
        n.d(e, "j", function () {
          return S;
        }),
        n.d(e, "r", function () {
          return j;
        }),
        n.d(e, "m", function () {
          return $;
        }),
        n.d(e, "o", function () {
          return E;
        }),
        n.d(e, "f", function () {
          return A;
        }),
        n.d(e, "c", function () {
          return M;
        }),
        n.d(e, "i", function () {
          return D;
        }),
        n.d(e, "n", function () {
          return W;
        }),
        n.d(e, "a", function () {
          return N;
        });
      n(23), n(91), n(19), n(83), n(117), n(142);
      var r = n(90),
        o = (n(55), n(308), n(107)),
        l = (n(71), n(72), n(143), n(482), n(484), n(485), n(40), n(30), n(11)),
        c = (n(54), n(17), n(14), n(20), n(38), n(13)),
        d = n(7);

      function h(object, t) {
        var e = Object.keys(object);
        if (Object.getOwnPropertySymbols) {
          var n = Object.getOwnPropertySymbols(object);
          t &&
            (n = n.filter(function (t) {
              return Object.getOwnPropertyDescriptor(object, t).enumerable;
            })),
            e.push.apply(e, n);
        }
        return e;
      }

      function f(t) {
        for (var i = 1; i < arguments.length; i++) {
          var source = null != arguments[i] ? arguments[i] : {};
          i % 2
            ? h(Object(source), !0).forEach(function (e) {
                Object(c.a)(t, e, source[e]);
              })
            : Object.getOwnPropertyDescriptors
            ? Object.defineProperties(
                t,
                Object.getOwnPropertyDescriptors(source)
              )
            : h(Object(source)).forEach(function (e) {
                Object.defineProperty(
                  t,
                  e,
                  Object.getOwnPropertyDescriptor(source, e)
                );
              });
        }
        return t;
      }

      function m(t, e) {
        var n;
        if ("undefined" == typeof Symbol || null == t[Symbol.iterator]) {
          if (
            Array.isArray(t) ||
            (n = (function (t, e) {
              if (!t) return;
              if ("string" == typeof t) return v(t, e);
              var n = Object.prototype.toString.call(t).slice(8, -1);
              "Object" === n && t.constructor && (n = t.constructor.name);
              if ("Map" === n || "Set" === n) return Array.from(t);
              if (
                "Arguments" === n ||
                /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)
              )
                return v(t, e);
            })(t)) ||
            (e && t && "number" == typeof t.length)
          ) {
            n && (t = n);
            var i = 0,
              r = function () {};
            return {
              s: r,
              n: function () {
                return i >= t.length
                  ? {
                      done: !0,
                    }
                  : {
                      done: !1,
                      value: t[i++],
                    };
              },
              e: function (t) {
                throw t;
              },
              f: r,
            };
          }
          throw new TypeError(
            "Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
          );
        }
        var o,
          l = !0,
          c = !1;
        return {
          s: function () {
            n = t[Symbol.iterator]();
          },
          n: function () {
            var t = n.next();
            return (l = t.done), t;
          },
          e: function (t) {
            (c = !0), (o = t);
          },
          f: function () {
            try {
              l || null == n.return || n.return();
            } finally {
              if (c) throw o;
            }
          },
        };
      }

      function v(t, e) {
        (null == e || e > t.length) && (e = t.length);
        for (var i = 0, n = new Array(e); i < e; i++) n[i] = t[i];
        return n;
      }

      function y(t) {
        d.default.config.errorHandler && d.default.config.errorHandler(t);
      }

      function w(t) {
        return (
          t.$options &&
          "function" == typeof t.$options.fetch &&
          !t.$options.fetch.length
        );
      }

      function x(t) {
        var e,
          n =
            arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [],
          r = t.$children || [],
          o = m(r);
        try {
          for (o.s(); !(e = o.n()).done; ) {
            var l = e.value;
            l.$fetch ? n.push(l) : l.$children && x(l, n);
          }
        } catch (t) {
          o.e(t);
        } finally {
          o.f();
        }
        return n;
      }

      function C(t, e) {
        if (e || !t.options.__hasNuxtData) {
          var n =
            t.options._originDataFn ||
            t.options.data ||
            function () {
              return {};
            };
          (t.options._originDataFn = n),
            (t.options.data = function () {
              var data = n.call(this, this);
              return (
                this.$ssrContext && (e = this.$ssrContext.asyncData[t.cid]),
                f(f({}, data), e)
              );
            }),
            (t.options.__hasNuxtData = !0),
            t._Ctor &&
              t._Ctor.options &&
              (t._Ctor.options.data = t.options.data);
        }
      }

      function _(t) {
        return (
          (t.options && t._Ctor === t) ||
            (t.options
              ? ((t._Ctor = t), (t.extendOptions = t.options))
              : ((t = d.default.extend(t))._Ctor = t),
            !t.options.name &&
              t.options.__file &&
              (t.options.name = t.options.__file)),
          t
        );
      }

      function O(t) {
        var e = arguments.length > 1 && void 0 !== arguments[1] && arguments[1],
          n =
            arguments.length > 2 && void 0 !== arguments[2]
              ? arguments[2]
              : "components";
        return Array.prototype.concat.apply(
          [],
          t.matched.map(function (t, r) {
            return Object.keys(t[n]).map(function (o) {
              return e && e.push(r), t[n][o];
            });
          })
        );
      }

      function P(t) {
        var e = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
        return O(t, e, "instances");
      }

      function L(t, e) {
        return Array.prototype.concat.apply(
          [],
          t.matched.map(function (t, n) {
            return Object.keys(t.components).reduce(function (r, o) {
              return (
                t.components[o]
                  ? r.push(e(t.components[o], t.instances[o], t, o, n))
                  : delete t.components[o],
                r
              );
            }, []);
          })
        );
      }

      function k(t, e) {
        return Promise.all(
          L(
            t,
            (function () {
              var t = Object(l.a)(
                regeneratorRuntime.mark(function t(n, r, o, l) {
                  return regeneratorRuntime.wrap(function (t) {
                    for (;;)
                      switch ((t.prev = t.next)) {
                        case 0:
                          if ("function" != typeof n || n.options) {
                            t.next = 4;
                            break;
                          }
                          return (t.next = 3), n();
                        case 3:
                          n = t.sent;
                        case 4:
                          return (
                            (o.components[l] = n = _(n)),
                            t.abrupt(
                              "return",
                              "function" == typeof e ? e(n, r, o, l) : n
                            )
                          );
                        case 6:
                        case "end":
                          return t.stop();
                      }
                  }, t);
                })
              );
              return function (e, n, r, o) {
                return t.apply(this, arguments);
              };
            })()
          )
        );
      }

      function S(t) {
        return I.apply(this, arguments);
      }

      function I() {
        return (I = Object(l.a)(
          regeneratorRuntime.mark(function t(e) {
            return regeneratorRuntime.wrap(function (t) {
              for (;;)
                switch ((t.prev = t.next)) {
                  case 0:
                    if (e) {
                      t.next = 2;
                      break;
                    }
                    return t.abrupt("return");
                  case 2:
                    return (t.next = 4), k(e);
                  case 4:
                    return t.abrupt(
                      "return",
                      f(
                        f({}, e),
                        {},
                        {
                          meta: O(e).map(function (t, n) {
                            return f(
                              f({}, t.options.meta),
                              (e.matched[n] || {}).meta
                            );
                          }),
                        }
                      )
                    );
                  case 5:
                  case "end":
                    return t.stop();
                }
            }, t);
          })
        )).apply(this, arguments);
      }

      function j(t, e) {
        return T.apply(this, arguments);
      }

      function T() {
        return (T = Object(l.a)(
          regeneratorRuntime.mark(function t(e, n) {
            var l, c, d, h;
            return regeneratorRuntime.wrap(function (t) {
              for (;;)
                switch ((t.prev = t.next)) {
                  case 0:
                    return (
                      e.context ||
                        ((e.context = {
                          isStatic: !1,
                          isDev: !1,
                          isHMR: !1,
                          app: e,
                          store: e.store,
                          payload: n.payload,
                          error: n.error,
                          base: "/",
                          env: {
                            defaultLocale: "en",
                          },
                        }),
                        n.req && (e.context.req = n.req),
                        n.res && (e.context.res = n.res),
                        n.ssrContext && (e.context.ssrContext = n.ssrContext),
                        (e.context.redirect = function (t, path, n) {
                          if (t) {
                            e.context._redirected = !0;
                            var r = Object(o.a)(path);
                            if (
                              ("number" == typeof t ||
                                ("undefined" !== r && "object" !== r) ||
                                ((n = path || {}),
                                (path = t),
                                (r = Object(o.a)(path)),
                                (t = 302)),
                              "object" === r &&
                                (path = e.router.resolve(path).route.fullPath),
                              !/(^[.]{1,2}\/)|(^\/(?!\/))/.test(path))
                            )
                              throw (
                                ((path = B(path, n)),
                                window.location.replace(path),
                                new Error("ERR_REDIRECT"))
                              );
                            e.context.next({
                              path: path,
                              query: n,
                              status: t,
                            });
                          }
                        }),
                        (e.context.nuxtState = window.__NUXT__)),
                      (t.next = 3),
                      Promise.all([S(n.route), S(n.from)])
                    );
                  case 3:
                    (l = t.sent),
                      (c = Object(r.a)(l, 2)),
                      (d = c[0]),
                      (h = c[1]),
                      n.route && (e.context.route = d),
                      n.from && (e.context.from = h),
                      (e.context.next = n.next),
                      (e.context._redirected = !1),
                      (e.context._errored = !1),
                      (e.context.isHMR = !1),
                      (e.context.params = e.context.route.params || {}),
                      (e.context.query = e.context.route.query || {});
                  case 15:
                  case "end":
                    return t.stop();
                }
            }, t);
          })
        )).apply(this, arguments);
      }

      function $(t, e) {
        return !t.length || e._redirected || e._errored
          ? Promise.resolve()
          : E(t[0], e).then(function () {
              return $(t.slice(1), e);
            });
      }

      function E(t, e) {
        var n;
        return (n =
          2 === t.length
            ? new Promise(function (n) {
                t(e, function (t, data) {
                  t && e.error(t), n((data = data || {}));
                });
              })
            : t(e)) &&
          n instanceof Promise &&
          "function" == typeof n.then
          ? n
          : Promise.resolve(n);
      }

      function A(base, t) {
        var path = decodeURI(window.location.pathname);
        return "hash" === t
          ? window.location.hash.replace(/^#\//, "")
          : (base &&
              (path.endsWith("/") ? path : path + "/").startsWith(base) &&
              (path = path.slice(base.length)),
            (path || "/") + window.location.search + window.location.hash);
      }

      function M(t, e) {
        return (function (t, e) {
          for (var n = new Array(t.length), i = 0; i < t.length; i++)
            "object" === Object(o.a)(t[i]) &&
              (n[i] = new RegExp("^(?:" + t[i].pattern + ")$", X(e)));
          return function (e, r) {
            for (
              var path = "",
                data = e || {},
                o = (r || {}).pretty ? R : encodeURIComponent,
                l = 0;
              l < t.length;
              l++
            ) {
              var c = t[l];
              if ("string" != typeof c) {
                var d = data[c.name || "pathMatch"],
                  h = void 0;
                if (null == d) {
                  if (c.optional) {
                    c.partial && (path += c.prefix);
                    continue;
                  }
                  throw new TypeError(
                    'Expected "' + c.name + '" to be defined'
                  );
                }
                if (Array.isArray(d)) {
                  if (!c.repeat)
                    throw new TypeError(
                      'Expected "' +
                        c.name +
                        '" to not repeat, but received `' +
                        JSON.stringify(d) +
                        "`"
                    );
                  if (0 === d.length) {
                    if (c.optional) continue;
                    throw new TypeError(
                      'Expected "' + c.name + '" to not be empty'
                    );
                  }
                  for (var f = 0; f < d.length; f++) {
                    if (((h = o(d[f])), !n[l].test(h)))
                      throw new TypeError(
                        'Expected all "' +
                          c.name +
                          '" to match "' +
                          c.pattern +
                          '", but received `' +
                          JSON.stringify(h) +
                          "`"
                      );
                    path += (0 === f ? c.prefix : c.delimiter) + h;
                  }
                } else {
                  if (((h = c.asterisk ? R(d, !0) : o(d)), !n[l].test(h)))
                    throw new TypeError(
                      'Expected "' +
                        c.name +
                        '" to match "' +
                        c.pattern +
                        '", but received "' +
                        h +
                        '"'
                    );
                  path += c.prefix + h;
                }
              } else path += c;
            }
            return path;
          };
        })(
          (function (t, e) {
            var n,
              r = [],
              o = 0,
              l = 0,
              path = "",
              c = (e && e.delimiter) || "/";
            for (; null != (n = H.exec(t)); ) {
              var d = n[0],
                h = n[1],
                f = n.index;
              if (((path += t.slice(l, f)), (l = f + d.length), h))
                path += h[1];
              else {
                var m = t[l],
                  v = n[2],
                  y = n[3],
                  w = n[4],
                  x = n[5],
                  C = n[6],
                  _ = n[7];
                path && (r.push(path), (path = ""));
                var O = null != v && null != m && m !== v,
                  P = "+" === C || "*" === C,
                  L = "?" === C || "*" === C,
                  k = n[2] || c,
                  pattern = w || x;
                r.push({
                  name: y || o++,
                  prefix: v || "",
                  delimiter: k,
                  optional: L,
                  repeat: P,
                  partial: O,
                  asterisk: Boolean(_),
                  pattern: pattern
                    ? F(pattern)
                    : _
                    ? ".*"
                    : "[^" + z(k) + "]+?",
                });
              }
            }
            l < t.length && (path += t.substr(l));
            path && r.push(path);
            return r;
          })(t, e),
          e
        );
      }

      function D(t, e) {
        var n = {},
          r = f(f({}, t), e);
        for (var o in r) String(t[o]) !== String(e[o]) && (n[o] = !0);
        return n;
      }

      function W(t) {
        var e;
        if (t.message || "string" == typeof t) e = t.message || t;
        else
          try {
            e = JSON.stringify(t, null, 2);
          } catch (n) {
            e = "[".concat(t.constructor.name, "]");
          }
        return f(
          f({}, t),
          {},
          {
            message: e,
            statusCode:
              t.statusCode ||
              t.status ||
              (t.response && t.response.status) ||
              500,
          }
        );
      }
      (window.onNuxtReadyCbs = []),
        (window.onNuxtReady = function (t) {
          window.onNuxtReadyCbs.push(t);
        });
      var H = new RegExp(
        [
          "(\\\\.)",
          "([\\/.])?(?:(?:\\:(\\w+)(?:\\(((?:\\\\.|[^\\\\()])+)\\))?|\\(((?:\\\\.|[^\\\\()])+)\\))([+*?])?|(\\*))",
        ].join("|"),
        "g"
      );

      function R(t, e) {
        var n = e ? /[?#]/g : /[/?#]/g;
        return encodeURI(t).replace(n, function (t) {
          return "%" + t.charCodeAt(0).toString(16).toUpperCase();
        });
      }

      function z(t) {
        return t.replace(/([.+*?=^!:${}()[\]|/\\])/g, "\\$1");
      }

      function F(t) {
        return t.replace(/([=!:$/()])/g, "\\$1");
      }

      function X(t) {
        return t && t.sensitive ? "" : "i";
      }

      function B(t, e) {
        var n,
          o = t.indexOf("://");
        -1 !== o
          ? ((n = t.substring(0, o)), (t = t.substring(o + 3)))
          : t.startsWith("//") && (t = t.substring(2));
        var l,
          c = t.split("/"),
          d = (n ? n + "://" : "//") + c.shift(),
          path = c.join("/");
        if (
          ("" === path && 1 === c.length && (d += "/"),
          2 === (c = path.split("#")).length)
        ) {
          var h = c,
            f = Object(r.a)(h, 2);
          (path = f[0]), (l = f[1]);
        }
        return (
          (d += path ? "/" + path : ""),
          e &&
            "{}" !== JSON.stringify(e) &&
            (d +=
              (2 === t.split("?").length ? "&" : "?") +
              (function (t) {
                return Object.keys(t)
                  .sort()
                  .map(function (e) {
                    var n = t[e];
                    return null == n
                      ? ""
                      : Array.isArray(n)
                      ? n
                          .slice()
                          .map(function (t) {
                            return [e, "=", t].join("");
                          })
                          .join("&")
                      : e + "=" + n;
                  })
                  .filter(Boolean)
                  .join("&");
              })(e)),
          (d += l ? "#" + l : "")
        );
      }

      function N(t, e, n) {
        t.$options[e] || (t.$options[e] = []),
          t.$options[e].includes(n) || t.$options[e].push(n);
      }
    },
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    function (t, e, n) {
      "use strict";
      (function (t) {
        n(30);
        var r = n(11),
          o = (n(54), n(90)),
          l = (n(487), n(38), n(17), n(14), n(20), n(92), n(40), n(79)),
          c = n(128),
          d = n(63),
          h = n(421),
          f = n(78),
          m = n(5),
          v = n.n(m),
          y = n(279),
          w = n.n(y),
          x = n(59),
          C = n.n(x),
          _ = n(42),
          O = n.n(_),
          P = n(270),
          L = n.n(P),
          k = n(129),
          S = n.n(k),
          I = n(22),
          j = n.n(I),
          T = n(24),
          $ = n.n(T),
          E = n(76),
          A = n.n(E),
          M = n(130),
          D = n(273),
          W = n(601),
          H = Object(M.createClient)(),
          R = {
            stripHtml: l.d,
            isLocale: c.c,
            getLocale: function (t) {
              return Object(c.b)(t, "en");
            },
            getMasterParent: function (t, e) {
              if (t)
                return t.fields ||
                  (t = e.getters["pages/getByAttr"]("sys.id", t.sys.id, "urls"))
                    .fields.parentPage
                  ? t.fields.parentPage
                    ? R.getMasterParent(t.fields.parentPage, e)
                    : e.getters["pages/getByAttr"](
                        "fields.slug",
                        t.fields.slug,
                        "urls"
                      )
                  : t;
            },
            reducePage: function (t) {
              var e = w()(t, [
                "fields",
                "sys.id",
                "sys.type",
                "sys.contentType",
                "sys.linkType",
                "url",
                "parentPage",
              ]);
              return C()(e, function (t, n) {
                return "fields" !== n
                  ? t
                  : C()(e.fields, function (t, e) {
                      return $()(t)
                        ? t.sys && "Asset" === t.sys.type
                          ? w()(t, ["fields", "sys.type"])
                          : C()(t, function (t, e) {
                              return t;
                            })
                        : t;
                    });
              });
            },
            parsePage: function (t) {
              var e = R.parseFields(t.page);
              return (
                (e = t.assign
                  ? R.assignUrl(t.page, t.collection, !0)
                  : R.parseUrl(e, null, t.collection, t.debug)),
                (e = R.reducePage(e)),
                (e = R.parseRelations(e, t.collection, !0, 0)),
                (e = R.parseMeta(e))
              );
            },
            parsePages: function (t, e) {
              return A()(t, function (p) {
                return R.parsePage({
                  page: p,
                  collection: e,
                  assign: !0,
                });
              });
            },
            parseFields: function (t) {
              if (t)
                return (
                  (t.fields = C()(t.fields, function (e, n) {
                    var r = d.dateFormat;
                    return O()(d.dateFields, n) && !t.dateParsed
                      ? D(new Date(e), r)
                      : O()(d.richFields, n)
                      ? W(e)
                      : e;
                  })),
                  (t.dateParsed = !0),
                  t
                );
            },
            parseMeta: function (t) {
              var e = v()(t, "sys.contentType.sys.id");
              return (
                (t.seo = {}),
                t.fields.title &&
                  (t.slug = t.fields.title.replace("  ", " ").sanitize()),
                j()(h[e], function (object) {
                  var e = object.prependTitle ? f.title + " - " : "";
                  (e +=
                    v()(t, object.key) ||
                    v()(t, object.fallback_key) ||
                    v()(f, object.default_key)),
                    (e =
                      "og:image" == object.value
                        ? void 0 !== e || "undefined" !== e || e
                          ? "https:" + e
                          : f.defaultImage
                        : R.stripHtml(e).replace(/(\r\n|\n|\r)/gm, " ")),
                    (t.seo[object.value] = {
                      value: R.stripHtml(e),
                      attribute: object.attribute,
                    });
                }),
                t
              );
            },
            parseRelations: function (t, e, n, r, o) {
              if (4 !== (r += 1)) {
                var l = R.reducePage(t);
                return C()(l, function (t, n) {
                  return "fields" !== n
                    ? t
                    : C()(l.fields, function (t, n) {
                        if (!$()(t)) return t;
                        if (
                          (t.sys && t.sys.contentType) ||
                          (t.sys &&
                            !t.sys.contentType &&
                            "Entry" === t.sys.linkType)
                        )
                          return R.parsePage({
                            page: t,
                            collection: e,
                            assign: !0,
                          });
                        var r = C()(t, function (t, n) {
                          return t.sys
                            ? R.parsePage({
                                page: t,
                                collection: e,
                                assign: !0,
                              })
                            : t;
                        });
                        return r[0] && r[0].sys ? L()(r) : r;
                      });
                });
              }
            },
            parseUrl: function (e, n, r, o) {
              if (!e || e.url || e.parsed || !e.sys) return e;
              if (
                (e.sys.contentType ||
                  (e = S()(r, function (p) {
                    return p.id === e.sys.id;
                  })),
                e.fields.isHp)
              )
                return (e.url = "/"), e;
              var c = t.env.multilanguage ? "/" + e.sys.locale : "",
                h = e.sys.contentType.sys.id,
                f = e.fields.parentPage;
              for (var m in (f &&
                f.sys &&
                ((f = r.find(function (p) {
                  return p.sys.id === f.sys.id;
                })) &&
                  !f.url &&
                  (f = R.parseUrl(f, n, r)),
                f && ((e.parentPage = f), (c = f.url))),
              d.irregularEntities))
                h === m &&
                  (function () {
                    var t = d.irregularEntities[m],
                      o = Object.keys(t)[0],
                      l = t[o];
                    !f &&
                      r &&
                      (f = r.find(function (t) {
                        return v()(t, o) === l;
                      })),
                      f && !f.url && (f = R.parseUrl(f, n, r)),
                      f && ((e.parentPage = f), (c = f.url));
                  })();
              var y = d.entities_with_url[h];
              if (y) {
                var w = e.fields.slug || e.fields.title;
                (w = Object(l.c)(w)),
                  "query" === y.type
                    ? (e.url = "".concat(c, "?").concat(h, "=").concat(w))
                    : (e.url = "".concat(c, "/").concat(w));
              }
              return (e.parsed = !0), e;
            },
            assignUrl: function (t, e, n) {
              if (!t || t.url || !t.sys) return t;
              var p = e.find(function (p) {
                return (p.name || p.id) === t.sys.id;
              });
              return p && p.url && (t.url = p.url), t;
            },
            getPageTemplate: function (t) {
              if (!t || !t.fields) return "NotFound";
              var template =
                Object(l.a)(t.fields.pageTemplate) ||
                d.pageTemplates[t.sys.contentType.sys.id];
              return template || (template = "NotFound"), template;
            },
            loadUrls: function (t) {
              for (
                var e = [], n = 0, r = Object.entries(d.entities_with_url);
                n < r.length;
                n++
              ) {
                var l = Object(o.a)(r[n], 2),
                  c = l[0],
                  h = l[1];
                e.push(
                  H.getEntries({
                    locale: t,
                    include: 2,
                    content_type: c,
                    select: h.fields,
                    limit: 1e3,
                    order: "sys.createdAt",
                  })
                );
              }
              return Promise.all(e).then(function (t) {
                var e = [];
                return (
                  t.forEach(function (t) {
                    t.items.forEach(function (t) {
                      e.push(t);
                    });
                  }),
                  j()(e, function (t) {
                    return R.parseUrl(t, null, e);
                  }),
                  e
                );
              });
            },
            loadByType: function (t, e) {
              return Object(r.a)(
                regeneratorRuntime.mark(function n() {
                  var r;
                  return regeneratorRuntime.wrap(function (n) {
                    for (;;)
                      switch ((n.prev = n.next)) {
                        case 0:
                          return (n.next = 2), H.getEntries(t);
                        case 2:
                          if (((r = n.sent), v()(r, "items"))) {
                            n.next = 5;
                            break;
                          }
                          return n.abrupt("return");
                        case 5:
                          if (v()(e, "parse") && v()(e, "urls")) {
                            n.next = 7;
                            break;
                          }
                          return n.abrupt("return", r.items);
                        case 7:
                          return n.abrupt(
                            "return",
                            R.parsePages(r.items, e.urls)
                          );
                        case 8:
                        case "end":
                          return n.stop();
                      }
                  }, n);
                })
              )();
            },
            loadPage: function (t) {
              if (t.id)
                return H.getEntry(t.id, {
                  locale: t.locale,
                  include: t.include || 2,
                }).catch(function (e) {
                  console.error("[framework] loadPage", t, e);
                });
            },
            loadActivePage: function (t) {
              return Object(r.a)(
                regeneratorRuntime.mark(function e() {
                  var n, r;
                  return regeneratorRuntime.wrap(function (e) {
                    for (;;)
                      switch ((e.prev = e.next)) {
                        case 0:
                          return (
                            (n = R.getLocale(t.route)),
                            (e.next = 3),
                            R.loadPage({
                              id: t.id,
                              locale: n,
                              include: t.include,
                            })
                          );
                        case 3:
                          return (
                            (r = e.sent),
                            e.abrupt(
                              "return",
                              R.parsePage({
                                page: r,
                                collection: t.urls,
                                debug: !0,
                                assign: !0,
                              })
                            )
                          );
                        case 5:
                        case "end":
                          return e.stop();
                      }
                  }, e);
                })
              )();
            },
          };
        e.a = R;
      }.call(this, n(26)));
    },
    ,
    ,
    ,
    ,
    ,
    function (t, e, n) {
      n(23), n(19), n(17), n(14), n(20);
      var r = n(96),
        o = n(97);

      function l(object, t) {
        var e = Object.keys(object);
        if (Object.getOwnPropertySymbols) {
          var n = Object.getOwnPropertySymbols(object);
          t &&
            (n = n.filter(function (t) {
              return Object.getOwnPropertyDescriptor(object, t).enumerable;
            })),
            e.push.apply(e, n);
        }
        return e;
      }
      t.exports = {
        functional: !0,
        render: function (t, e) {
          var n = e._c,
            data = (e._v, e.data),
            c = e.children,
            d = void 0 === c ? [] : c,
            h = data.class,
            f = data.staticClass,
            style = data.style,
            m = data.staticStyle,
            v = data.attrs,
            y = void 0 === v ? {} : v,
            w = o(data, [
              "class",
              "staticClass",
              "style",
              "staticStyle",
              "attrs",
            ]);
          return n(
            "svg",
            (function (t) {
              for (var i = 1; i < arguments.length; i++) {
                var source = null != arguments[i] ? arguments[i] : {};
                i % 2
                  ? l(Object(source), !0).forEach(function (e) {
                      r(t, e, source[e]);
                    })
                  : Object.getOwnPropertyDescriptors
                  ? Object.defineProperties(
                      t,
                      Object.getOwnPropertyDescriptors(source)
                    )
                  : l(Object(source)).forEach(function (e) {
                      Object.defineProperty(
                        t,
                        e,
                        Object.getOwnPropertyDescriptor(source, e)
                      );
                    });
              }
              return t;
            })(
              {
                class: [h, f],
                style: [style, m],
                attrs: Object.assign(
                  {
                    viewBox: "0 0 103 103",
                    version: "1.1",
                    xmlns: "http://www.w3.org/2000/svg",
                    "xmlns:xlink": "http://www.w3.org/1999/xlink",
                  },
                  y
                ),
              },
              w
            ),
            d.concat([
              n(
                "g",
                {
                  attrs: {
                    id: "Page-1",
                    stroke: "none",
                    "stroke-width": "1",
                    fill: "none",
                    "fill-rule": "evenodd",
                  },
                },
                [
                  n(
                    "g",
                    {
                      attrs: {
                        id: "Folio-Inner",
                        transform: "translate(-909.000000, -3363.000000)",
                      },
                    },
                    [
                      n(
                        "g",
                        {
                          attrs: {
                            id: "star_icon",
                            transform: "translate(908.000000, 3362.000000)",
                          },
                        },
                        [
                          n("path", {
                            attrs: {
                              d: "M52.5,1 C52.5,46 46,52.5 1,52.5 C46,52.5 52.5,59 52.5,104 C52.5,59 59.0836874,52.5 104,52.5 C59,52.5 52.5,46 52.5,1 Z",
                              id: "Fill-1",
                            },
                          }),
                        ]
                      ),
                    ]
                  ),
                ]
              ),
            ])
          );
        },
      };
    },
    ,
    ,
    ,
    ,
    function (t) {
      t.exports = JSON.parse(
        '{"entities_with_url":{"page":{"fields":"fields.slug,fields.title,fields.isHp,fields.pageTemplate","type":"default"},"project":{"fields":"fields.slug,fields.title","type":"default"}},"irregularEntities":{"project":{"fields.pageTemplate":"Portfolio"}},"query_urls":[],"richFields":["content"],"dateFormat":"mmmm dd, yyyy","dateFields":["date"],"excludeFromFetch":[""],"pageTemplates":{"page":"BasicPage","project":"Project"},"_comment":"{\'page\': (name of the page that has to be reduced), \'fieldsToReduce\' : [{\'path\' : (path to the field that needs to be reduced), \'fieldsToKeep\' : [array of fields(keys) that should be kept]}]}","pagesToReduce":[]}'
      );
    },
    function (t, e, n) {
      "use strict";
      n.d(e, "b", function () {
        return sa;
      }),
        n.d(e, "a", function () {
          return fn;
        });
      n(30), n(23), n(19), n(17), n(14), n(20);
      var r = n(11),
        o = n(13),
        l = (n(38), n(7)),
        c = n(419),
        d = n(278),
        h = n.n(d),
        f = n(109),
        m = n.n(f),
        v = (n(92), n(281)),
        y = n(213),
        w = n(52),
        x = (n(117), n(142), n(55), n(9)),
        C = n(22),
        O = n.n(C),
        P = n(5),
        L = n.n(P),
        k = n(104),
        S = n.n(k),
        I = n(35),
        j = n.n(I),
        T = n(423),
        $ = n.n(T),
        E = n(216),
        A = n.n(E),
        M = {
          data: function () {
            return {
              tweens: [],
              defaultEase: "Power4.easeOut",
              defaults: {
                opacity: {
                  from: 0,
                  to: 1,
                  dur: 1,
                },
                y: {
                  from: 50,
                  to: 0,
                  dur: 1.6,
                  ease: "Power3.easeOut",
                },
                x: {
                  from: 200,
                  to: 0,
                  dur: 1.8,
                  ease: "Power4.easeOut",
                },
                scale: {
                  from: 1.4,
                  to: 1,
                  dur: 1.6,
                  ease: "Power2.easeOut",
                },
                rotation: {
                  from: 10,
                  to: 0,
                  dur: 1.6,
                  ease: "Power2.easeOut",
                },
              },
              presets: {
                "opacity,y": {
                  fromopacity: 0,
                  toopacity: 1,
                  fromy: 90,
                  toy: 0,
                  dur: 1.4,
                  ease: "Power3.easeOut",
                },
                "opacity,y,rotate": {
                  fromopacity: 0,
                  toopacity: 1,
                  fromy: 90,
                  toy: 0,
                  fromrotate: 4,
                  torotate: 0,
                  dur: 1.4,
                  ease: "Power3.easeOut",
                },
                "scale,y": {
                  fromscale: 3,
                  toscale: 1,
                  fromy: -100,
                  toy: 0,
                  dur: 1,
                },
                "opacity,x": {
                  fromopacity: 0,
                  toopacity: 1,
                  fromx: 150,
                  tox: 0,
                  dur: 1.8,
                  ease: "Power3.easeOut",
                },
                "opacity,scale": {
                  fromscale: 1.2,
                  toscale: 1,
                  fromopacity: 0,
                  toopacity: 1,
                  dur: 1.4,
                  ease: "Power3.easeOut",
                },
              },
            };
          },
          computed: {
            animationDelay: function () {
              return this.$store.getters["app/getState"]("animationDelay");
            },
          },
          methods: {
            toggleAnimations: function (t) {
              var e = (t.el || this.$el).querySelectorAll(".should-animate");
              this.runAnimation(e, t.visible);
            },
            setAnimationObjects: function (t, e, n, r) {
              var o,
                l,
                c = this.defaults[e],
                d = n || r ? 1 * L()(t, "dur") || L()(c, "dur") : 0;
              if ("clipPath" == e) {
                var h = t["from" + e],
                  f = t["to" + e];
                (o = h || L()(c, "from")), (l = f || L()(c, "to"));
              } else {
                var m = 1 * t["from" + e],
                  v = 1 * t["to" + e];
                (o = j()(m) && !S()(m) ? m : L()(c, "from")),
                  (l = j()(v) && !S()(v) ? v : L()(c, "to"));
              }
              return {
                from: n ? o : l,
                to: n ? l : o,
                dur: d,
                ease: t.ease,
              };
            },
            runAnimation: function (t, e, n) {
              var r = this;
              t &&
                t.length &&
                (x.b.set(t, {
                  opacity: 1,
                }),
                O()(t, function (t, o) {
                  if (t.dataset) {
                    var l = 0,
                      c = t.dataset.preset,
                      d = $()(r.presets, c),
                      h = e ? 1 * t.dataset.delay : 0,
                      f = t.dataset.ease,
                      m = {},
                      v = {},
                      y = d ? r.presets[c] : t.dataset;
                    d && (y = A()({}, y, t.dataset));
                    var w = c || t.dataset.props,
                      C = w ? w.split(",") : [];
                    O()(C, function (o, c) {
                      n = n || t.dataset.notInstant;
                      var d = r.setAnimationObjects(y, o, e, n);
                      (l = d.dur),
                        (f = d.ease || f),
                        (m[o] = d.from),
                        (v[o] = d.to);
                    }),
                      (v.delay = h),
                      (v.ease = f || r.defaultEase),
                      (v.overwrite = !0),
                      (m.transformOrigin = t.dataset.origin || "center center"),
                      y.notDelayed || (v.delay += r.animationDelay || 0),
                      e &&
                        !y.keepProps &&
                        (v.clearProps =
                          "transform,clipPath,transform-origin,will-change"),
                      x.b.fromTo(t, l, m, v);
                  }
                }));
            },
          },
        },
        D = {
          data: function () {
            return {
              elementY: 0,
            };
          },
          mounted: function () {
            this.$bus.$on("resize", this.resizeScrollingElement);
          },
          destroyed: function () {
            this.$bus.$off("resize", this.resizeScrollingElement);
          },
          methods: {
            resizeScrollingElement: function () {
              var t = this;
              setTimeout(function () {
                var e = (t.$el.$el || t.$el).style.transform,
                  n = parseInt(
                    e
                      .substr(e.indexOf("(") + 1, e.indexOf(")") - 1)
                      .split(",")[13]
                  );
                t.elementY = S()(n) ? 0 : n;
              });
            },
          },
        },
        W = {
          data: function () {
            return {
              lines: [],
              visibleWaypoint: !1,
              elementY: 0,
            };
          },
          mixins: [M, D],
          watch: {
            lines: function () {
              this.ignoreLines ||
                this.toggleWaypointWithLines({
                  el: this.$el,
                  visible: this.visibleWaypoint,
                });
            },
          },
          computed: {
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
          },
          methods: {
            onSplited: function (t) {
              this.lines = t;
            },
            toggleWaypoint: function (t) {
              t.visible;
              var e = t.el.querySelectorAll(".should-animate");
              this.runAnimation(e, t.visible),
                (this.visibleWaypoint = t.visible);
            },
            toggleWaypointWithLines: function (t) {
              t.visible;
              this.visibleWaypoint = t.visible;
              var e = t.el.querySelectorAll(".should-animate"),
                n = t.el.querySelectorAll(".split-text");
              if (
                (this.runAnimation(e, t.visible, t.instant),
                n && this.lines && this.lines.length)
              ) {
                n.length &&
                  x.b.set(n, {
                    opacity: 1,
                  });
                var r = {
                  opacity: 0,
                };
                r[this.textDir || "y"] = this.textMove || -40;
                var o = {
                  opacity: 1,
                  clearProps: "transform",
                  overwrite: !0,
                };
                (o[this.textDir || "y"] = 0),
                  (o.stagger = this.textStagger || 0.1);
                var l = t.visible ? r : o,
                  c = t.visible ? o : r,
                  d = t.instant ? 0 : t.visible ? 1 : 0,
                  h = t.instant ? 0 : t.visible ? 0.05 : 0;
                t.instant || (t.visible && this.delayAddition);
                (c.delay = t.instant ? 0 : t.visible ? this.textDelay : 0),
                  (c.ease = "Power3.easeOut"),
                  x.b.fromTo(this.lines, d, l, c, h);
              }
            },
          },
        },
        H = {
          name: "fancy-cursor",
          data: function () {
            return {
              initialized: !1,
            };
          },
          props: ["shrinked", "model"],
          watch: {
            shrinked: function () {
              this.changeSize();
            },
            cursorBounds: function () {
              this.translate();
            },
          },
          mounted: function () {
            var t = this;
            x.b.set(this.$el, {
              x: this.computedMouseX,
              y: this.computedMouseY,
            }),
              this.$watch(
                function (e) {
                  return [t.mouseX, t.mouseY, t.scrollTop].join();
                },
                function (e) {
                  t.translate();
                }
              );
          },
          methods: {
            changeSize: function () {
              x.b.to(this.$refs.circle, 0.6, {
                scale: this.shrinked ? 0.5 : 1,
                overwrite: !0,
                ease: "Power4.easeOut",
              });
            },
            translate: function () {
              (this.initialized = !0),
                (this.tween = x.b.to(this.$el, 0.5, {
                  x: this.computedMouseX,
                  y: this.computedMouseY,
                  overwrite: !0,
                  ease: "Power4.easeOut",
                }));
            },
          },
          computed: {
            mouseDown: function () {
              return this.$store.getters["app/getState"]("mouseDown");
            },
            mouseX: function () {
              return this.$store.getters["app/getState"]("mouseX");
            },
            mouseY: function () {
              return this.$store.getters["app/getState"]("mouseY");
            },
            scrollTop: function () {
              return this.$store.getters["app/getState"]("scrollTop");
            },
            computedMouseX: function () {
              return this.mouseX;
            },
            computedMouseY: function () {
              return this.mouseY + this.scrollTop;
            },
            cursorBounds: function () {
              return this.$store.getters["app/getState"]("cursorBounds");
            },
            size: function () {
              return this.cursorBounds
                ? Math.round(this.cursorBounds.width)
                : this.mouseDown
                ? 60
                : 14;
            },
          },
          components: {},
        },
        R = (n(609), n(3)),
        z = Object(R.a)(
          H,
          function () {
            var t = this.$createElement,
              e = this._self._c || t;
            return e(
              "div",
              {
                directives: [
                  {
                    name: "show",
                    rawName: "v-show",
                    value: this.initialized,
                    expression: "initialized",
                  },
                ],
                staticClass: "fancy-cursor d-none d-md-block",
                class: {
                  "d-none": this.$device.isMobileOrTablet,
                },
              },
              [
                e(
                  "div",
                  {
                    ref: "outer",
                    staticClass: "position-relative outer-wrapper w-100 h-100",
                  },
                  [
                    e("div", {
                      ref: "circle",
                      staticClass: "circle",
                    }),
                  ]
                ),
              ]
            );
          },
          [],
          !1,
          null,
          "9a827ae6",
          null
        ).exports,
        F = n(42),
        X = n.n(F),
        B = n(18),
        N = {
          data: function () {
            return {};
          },
          inject: ["PIXIWrapper"],
          provide: function () {
            return {
              PIXIWrapper: this.PIXIWrapper,
            };
          },
          props: {
            resolution: {
              default: 1,
            },
          },
          created: function () {
            var t = this;
            PIXI.utils.skipHello(),
              this.$bus.$on("destroy:renderer", function (e) {
                t.$bus.$off("destroy:renderer"), t.destroy();
              });
          },
          destroyed: function () {
            var t = this;
            this.PIXIWrapper.PIXIApp.stage.off("mousemove").off("touchmove"),
              setTimeout(function () {
                t.PIXIWrapper.PIXIApp.destroy(!0, {
                  children: !0,
                  texture: !0,
                  baseTexture: !0,
                });
              }, 1200),
              this.$bus.$off("resize", this.resizeHandler);
          },
          watch: {},
          methods: {
            resizeHandler: function () {
              if (this.PIXIWrapper.PIXIApp) {
                this.resizeTo && clearTimeout(this.resizeTo);
                var t = this.$el.getBoundingClientRect();
                (this.PIXIWrapper.PIXIApp.offsetTop = t.top),
                  (this.PIXIWrapper.PIXIApp.offsetLeft = t.left),
                  (this.PIXIWrapper.PIXIApp.height = t.height),
                  (this.PIXIWrapper.PIXIApp.width = t.width);
                try {
                  this.PIXIWrapper.PIXIApp.renderer.resize(
                    this.$el.clientWidth,
                    this.$el.clientHeight
                  );
                } catch (t) {
                  console.log("err:", t);
                }
              }
            },
            destroy: function () {
              this.PIXIWrapper.PIXIApp.ticker.stop(),
                this.PIXIWrapper.PIXIApp.ticker.destroy(),
                this.PIXIWrapper.PIXIApp.stage.destroy(!0, !0, !0),
                (this.PIXIWrapper.PIXIApp.stage = null),
                this.PIXIWrapper.PIXIApp.renderer.destroy(!0),
                (this.PIXIWrapper.PIXIApp.renderer = null);
            },
          },
          mounted: function () {
            var t = this;
            return Object(r.a)(
              regeneratorRuntime.mark(function e() {
                var n, r, o, l;
                return regeneratorRuntime.wrap(function (e) {
                  for (;;)
                    switch ((e.prev = e.next)) {
                      case 0:
                        (n = t),
                          (t.engineLoaded = !0),
                          t.$bus.$on("resize", n.resizeHandler),
                          (r = t.$refs.renderCanvas),
                          (o = t.$el.offsetWidth),
                          (l = t.$el.offsetHeight),
                          (t.PIXIWrapper.PIXIApp = new PIXI.Application({
                            width: o,
                            height: l,
                            view: r,
                            transparent: !0,
                            resolution: t.$device.isMobile ? 2 : t.resolution,
                            antialias: !0,
                            powerPreference: "high-performance",
                            sharedTicker: !1,
                            sharedLoader: !0,
                          })),
                          t.$nextTick(function () {
                            t.resizeHandler(), t.$bus.$emit("ready");
                          });
                      case 8:
                      case "end":
                        return e.stop();
                    }
                }, e);
              })
            )();
          },
        },
        Y =
          (n(610),
          Object(R.a)(
            N,
            function () {
              var t = this.$createElement,
                e = this._self._c || t;
              return e(
                "div",
                {
                  staticClass: "pixi-renderer w-100 h-100",
                },
                [
                  e("canvas", {
                    ref: "renderCanvas",
                  }),
                  this._v(" "),
                  this._t("default"),
                ],
                2
              );
            },
            [],
            !1,
            null,
            "106442b7",
            null
          ).exports),
        V = n(62),
        U = n.n(V),
        Z = {
          inject: ["PIXIWrapper"],
          props: [
            "model",
            "domElement",
            "zIndex",
            "interactive",
            "debug",
            "hidden",
            "id",
            "propKeepLive",
            "renderable",
            "alpha",
            "scale",
          ],
          provide: function () {
            return {
              PIXIWrapper: this.PIXIWrapper,
            };
          },
          data: function () {
            return {
              container: null,
              topBound: 0,
              filters: [],
              domObject: null,
              containerBounds: {},
              stretch: !0,
            };
          },
          computed: {
            scrollTop: function () {
              return this.$store.getters["app/getState"]("scrollTop");
            },
            windowWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
            windowHeight: function () {
              return this.$store.getters["app/getState"]("height");
            },
          },
          watch: {
            alpha: function (a) {
              this.container.alpha = a;
            },
          },
          methods: {
            destroyContainer: function (t) {
              var e = this;
              try {
                setTimeout(function () {
                  (e.container.renderable = !1),
                    (e.container.filters = null),
                    e.parentContainer &&
                      e.parentContainer.removeChild(e.container),
                    e.container.destroy({
                      children: t,
                      texture: !0,
                      baseTexture: !0,
                    });
                }, 1200);
              } catch (t) {
                console.log(t);
              }
            },
            resizeHandler: function (t) {
              this.scale &&
                this.container &&
                ((this.container.scale.x = this.container.scale.y =
                  this.scale || 1),
                (this.container.pivot.x =
                  (this.container.width - this.container.width / this.scale) /
                  2),
                (this.container.pivot.y =
                  (this.container.height - this.container.height / this.scale) /
                  2));
            },
            initContainer: function () {
              var t = this;
              this.$bus.$off("ready", this.initContainer),
                (this.container = new PIXI.Container()),
                (this.container.interactive = this.interactive),
                (this.container.renderable = this.container.visible =
                  !this.hidden),
                (this.container.filters = []),
                this.domObject &&
                  (this.container.filterArea = new PIXI.Rectangle(
                    0,
                    0,
                    this.container.width,
                    this.container.height
                  )),
                (this.container.identifier = this.id + this._uid),
                (this.container.filterArea = this.PIXIWrapper.PIXIApp.screen),
                (this.container.alpha = this.alpha || 1),
                (this.container.sortableChildren = !0),
                (this.container._zIndex =
                  this.container.zIndex =
                  this.container.zOrder =
                    this.zIndex),
                (this.parentContainer =
                  this.$parent.container || this.PIXIWrapper.PIXIApp.stage),
                this.parentContainer.addChild(this.container),
                this.parentContainer.children.sort(function (t, e) {
                  return t.zIndex - e.zIndex;
                }),
                this.asBitmap &&
                  setTimeout(function () {
                    t.container.cacheAsBitmap = t.asBitmap;
                  }, 40),
                U()(this.onInitContainer) && this.onInitContainer();
            },
          },
          created: function () {
            var t = this;
            this.domElement &&
              (this.domObject = document.getElementById(this.domElement)),
              this.resizeHandler &&
                setTimeout(function () {
                  t.$bus.$on("resize", t.resizeHandler);
                }, 20),
              this.PIXIWrapper.PIXIApp
                ? this.initContainer()
                : this.$bus.$on("ready", this.initContainer);
          },
          destroyed: function () {
            this.destroyContainer(!0);
          },
          beforeDestroy: function () {
            this.$bus.$off("ready", this.initContainer),
              this.$bus.$off("resize", this.resizeHandler);
          },
          render: function (t) {
            return t("template", this.$slots.default);
          },
        },
        G = {
          mixins: [Z],
        },
        K = Object(R.a)(G, void 0, void 0, !1, null, null, null).exports,
        J =
          (n(358),
          {
            mixins: [
              {
                inject: ["PIXIWrapper"],
                props: [
                  "imagePath",
                  "alpha",
                  "onClick",
                  "scale",
                  "noPreload",
                  "debug",
                ],
                render: function (t) {
                  return t();
                },
                methods: {
                  addSprite: function () {
                    this.$parent.container
                      ? this.$parent.container.addChild(this.sprite)
                      : this.PIXIWrapper.PIXIApp.stage.addChild(this.sprite),
                      this.onClick &&
                        ((this.sprite.interactive = !0),
                        this.sprite.on("mouseup", this.onClick));
                  },
                },
                mounted: function () {
                  var t = this;
                  this.PIXIWrapper.PIXIApp
                    ? this.initSprite()
                    : this.$bus.$on("ready", function () {
                        t.$nextTick(function () {
                          t.initSprite();
                        });
                      });
                },
              },
            ],
            methods: {
              onLoaded: function (t) {
                var e = this;
                this.imagePath &&
                  ((this.sprite = PIXI.Sprite.from(this.imagePath)),
                  (this.sprite.alpha = j()(this.alpha) ? this.alpha : 1),
                  (this.sprite.scale.x = this.sprite.scale.y = this.scale || 1),
                  this.addSprite(),
                  this.$nextTick(this.resizeHandler),
                  setTimeout(function () {
                    e.$emit("loaded", e.sprite);
                  }, 0));
              },
              initSprite: function () {
                var t = this;
                PIXI.utils.TextureCache[this.imagePath] ||
                this.noPreload ||
                !this.imagePath
                  ? this.onLoaded()
                  : ((this.loader = new PIXI.Loader()),
                    this.loader.add(this.imagePath, this.imagePath),
                    this.loader.load(function (e, n) {
                      t.onLoaded();
                    }));
              },
            },
          }),
        Q = Object(R.a)(J, void 0, void 0, !1, null, null, null).exports,
        tt = {
          inject: ["PIXIWrapper"],
          mixins: [
            {
              data: function () {
                return {
                  y: 0,
                };
              },
              computed: {
                scrollTop: function () {
                  return this.$store.getters["app/getState"]("scrollTop");
                },
                windowHeight: function () {
                  return this.$store.getters["app/getState"]("height");
                },
                move: function () {
                  return this.windowHeight * this.ratio;
                },
                elTop: function () {
                  return this.getOffset(this.horizontal);
                },
                computedProgress: function () {
                  var t = (this.end - this.start) / 100,
                    e = (this.scrollTop - this.start) / t;
                  return this.scrollTop >= this.start &&
                    this.scrollTop <= this.end
                    ? e / 100
                    : this.scrollTop > this.start
                    ? 1
                    : 0;
                },
                progress: function () {
                  if (!this.PIXIWrapper.PIXIApp) return 0;
                  var t =
                      this.start ||
                      (this.noResize
                        ? 0
                        : this.PIXIWrapper.PIXIApp.offsetTop -
                          this.windowHeight),
                    e =
                      this.end ||
                      t + this.windowHeight + this.PIXIWrapper.PIXIApp.height;
                  return this.scrollProgress(t, e, this.scrollTop);
                },
              },
              methods: {
                getOffset: function (t) {
                  var e = -this.move;
                  return this.noResize || (this.wrapped && e > 0)
                    ? 0
                    : this.wrapped
                    ? e
                    : Math.round(e / 2);
                },
                scrollProgress: function (t, e, n) {
                  return n >= t && n <= e
                    ? (n - t) / ((e - t) / 100) / 100
                    : n > t
                    ? 1
                    : 0;
                },
              },
            },
          ],
          provide: function () {
            return {
              PIXIWrapper: this.PIXIWrapper,
            };
          },
          data: function () {
            return {};
          },
          watch: {
            mouseX: function () {
              this.moveFilter();
            },
            mouseY: function () {
              this.moveFilter();
            },
            mouseDown: function (t) {
              this.onMouseDown(t);
            },
            menuOpen: function (t) {
              U()(this.onMenuOpen) && this.onMenuOpen(t);
            },
            stageVisible: function (t) {},
          },
          computed: {
            scrolling: function () {
              return this.$store.getters["app/getState"]("scrolling");
            },
            scrollTop: function () {
              return this.$store.getters["app/getState"]("scrollTop");
            },
            mouseX: function () {
              return this.$store.getters["app/getState"]("mouseX");
            },
            mouseY: function () {
              return this.$store.getters["app/getState"]("mouseY");
            },
            windowWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
            windowHeight: function () {
              return this.$store.getters["app/getState"]("height");
            },
            mouseDown: function () {
              return this.$store.getters["app/getState"]("mouseDown");
            },
            inActiveArea: function () {
              return this.$store.getters["app/getState"]("inActiveArea");
            },
            transitioning: function () {
              return this.$store.getters["app/getState"]("stageTransitioning");
            },
            stageVisible: function () {
              return this.$store.getters["app/getState"]("stageVisible");
            },
            menuOpen: function () {
              return this.$store.getters["app/getState"]("menuOpen");
            },
          },
          render: function (t) {
            return t("template", this.$slots.default);
          },
          methods: {
            transition: function () {},
            onMouseDown: function () {},
            moveFilter: function () {},
            addFilter: function () {
              var t = [this.filter];
              L()(this.$parent, "container.filters") &&
                O()(this.$parent.container.filters, function (filter) {
                  t.push(filter);
                }),
                this.$parent.container &&
                  this.$parent.container.renderable &&
                  ((this.$parent.container.filters = t),
                  this.$parent.filters.push(this));
            },
            setFilter: function () {
              (this.filter = new filters.ZoomBlurFilter()),
                (this.filter.strength = this.strength);
            },
            initFilter: function (text, t) {
              var e = this;
              this.setFilter(),
                this.addFilter(),
                setTimeout(function () {
                  e.resizeHandler();
                }, 50),
                this.animateFilter &&
                  this.PIXIWrapper.PIXIApp.ticker.add(this.animateFilter, this);
            },
            resizeHandler: function (t) {
              var e = this;
              (this.active = !1),
                this.to && clearTimeout(this.to),
                (this.to = setTimeout(function () {
                  e.active = !0;
                }, 500));
            },
          },
          beforeDestroy: function () {
            (this.filter.enabled = !1),
              this.animateFilter &&
                ((this.active = !1),
                this.PIXIWrapper.PIXIApp.ticker.remove(
                  this.animateFilter,
                  this
                ));
          },
          created: function () {
            var t = this;
            this.PIXIWrapper.PIXIApp
              ? this.initFilter()
              : this.$bus.$on("ready", function () {
                  t.$bus.$off("ready"), t.initFilter();
                });
          },
        },
        et = {
          mixins: [tt],
          data: function () {
            return {
              defaultRadius: 0,
              defaultAngle: 1.4,
            };
          },
          props: [
            "customRadius",
            "customAngle",
            "targetAngle",
            "targetRadius",
            "static",
          ],
          created: function () {},
          watch: {
            customRadius: function (t) {
              x.a.set(this.filter, {
                radius: t * this.windowHeight,
              });
            },
            customAngle: function (a) {
              x.a.set(this.filter, {
                angle: a,
              });
            },
          },
          computed: {
            radius: function () {
              return this.customRadius
                ? this.customRadius * this.windowHeight
                : this.defaultRadius;
            },
            angle: function () {
              return this.customAngle || this.defaultAngle;
            },
          },
          methods: {
            fadeIn: function () {
              this.twistTween = x.a.fromTo(
                this.filter,
                1.4,
                {
                  radius: 4 * this.radius,
                  angle: -25 * this.angle,
                },
                {
                  radius: this.radius,
                  angle: this.angle,
                  ease: "Power4.easeOut",
                }
              );
            },
            twistIt: function (t) {
              this.filter &&
                (this.twistTween = x.a.to(this.filter, t.duration, {
                  radius: 2 * this.radius,
                  angle: -25 * this.angle,
                  delay: t.delay,
                  ease: "Power4.easeOut",
                }));
            },
            unTwistIt: function (t) {
              this.filter &&
                (this.twistTween = x.a.from(this.filter, 1, {
                  radius: 2 * this.radius,
                  angle: 50 * this.angle,
                  delay: t.delay,
                  ease: "Power4.easeOut",
                }));
            },
            moveFilter: function () {
              if (this.filter) {
                this.static;
                var t =
                    this.filterX ||
                    this.mouseX - this.PIXIWrapper.PIXIApp.offsetLeft,
                  e = this.filterY || this.mouseY;
                x.a.to(this.filter.offset, 1, {
                  0: t,
                  1: e,
                  ease: "Power4.easeOut",
                  overwrite: !0,
                });
              }
            },
            setFilter: function (text, t) {
              this.filter = new filters.TwistFilter();
              var e = this.static
                  ? this.windowWidth / 2 - this.radius / 3
                  : this.mouseX,
                n = this.static ? this.windowHeight / 2 : this.mouseY;
              x.a.set(this.filter, {
                radius: this.radius,
                angle: this.angle,
                padding: 0,
                offset: {
                  0: e,
                  1: n,
                },
              });
            },
          },
        },
        it = Object(R.a)(et, void 0, void 0, !1, null, null, null).exports;

      function nt(object, t) {
        var e = Object.keys(object);
        if (Object.getOwnPropertySymbols) {
          var n = Object.getOwnPropertySymbols(object);
          t &&
            (n = n.filter(function (t) {
              return Object.getOwnPropertyDescriptor(object, t).enumerable;
            })),
            e.push.apply(e, n);
        }
        return e;
      }
      var at = {
          mixins: [tt],
          data: function () {
            return {};
          },
          props: ["matrix", "hasBlend"],
          computed: (function (t) {
            for (var i = 1; i < arguments.length; i++) {
              var source = null != arguments[i] ? arguments[i] : {};
              i % 2
                ? nt(Object(source), !0).forEach(function (e) {
                    Object(o.a)(t, e, source[e]);
                  })
                : Object.getOwnPropertyDescriptors
                ? Object.defineProperties(
                    t,
                    Object.getOwnPropertyDescriptors(source)
                  )
                : nt(Object(source)).forEach(function (e) {
                    Object.defineProperty(
                      t,
                      e,
                      Object.getOwnPropertyDescriptor(source, e)
                    );
                  });
            }
            return t;
          })(
            {},
            Object(B.c)("stage", [
              "brightness",
              "contrast",
              "contrastMultiplier",
              "reversedBrightness",
            ])
          ),
          watch: {
            brightness: function (b) {
              this.hasBlend &&
                (this.filter.brightness(b), console.log("BB", b));
            },
            reversedBrightness: function (b) {
              this.hasBlend ||
                (this.filter.brightness(b), console.log("BB1", b));
            },
          },
          methods: {
            onMouseDown: function (t) {},
            animateFilter: function (t) {},
            setFilter: function (text, t) {
              (this.filter = new PIXI.filters.ColorMatrixFilter()),
                this.hasBlend
                  ? (this.filter.brightness(this.brightness),
                    (this.filter.blendMode = PIXI.BLEND_MODES.SCREEN))
                  : this.filter.brightness(this.reversedBrightness),
                console.log("BLACK", this.filter);
            },
          },
        },
        st = Object(R.a)(at, void 0, void 0, !1, null, null, null).exports,
        ot =
          (n(612),
          {
            mixins: [tt],
            props: {
              scale: {
                default: 16,
              },
              filterScale: {
                default: 0.03,
              },
              debug: !1,
              anchor: !1,
              containerScale: !1,
              topOffset: null,
              leftOffset: null,
              interactive: !1,
              displacementImage: {
                default: "../clouds.jpg",
              },
            },
            watch: {
              paused: function () {},
              scrollTop: function () {},
              filterScale: function (t) {
                var e = this.PIXIWrapper.PIXIApp.screen.width * t;
                (this.filter.scale.x = this.filter.scale.y = e),
                  (this.filter.autoFit = !0);
              },
              scale: function (t) {
                this.displacementSprite &&
                  (this.displacementSprite.scale.x =
                    this.displacementSprite.scale.y =
                      t);
              },
            },
            data: function () {
              return {};
            },
            methods: {
              moveFilter: function () {
                this.displacementSprite &&
                  this.interactive &&
                  x.b.to(this.displacementSprite.position, 1, {
                    x: (this.mouseX - this.leftOffset) / this.containerScale,
                    y: (this.mouseY - this.topOffset) / this.containerScale,
                    ease: "Power4.easeOut",
                    overwrite: !0,
                  });
              },
              initFilter: function (text, t) {
                this.setFilter(),
                  this.addFilter(),
                  this.resizeHandler(),
                  this.interactive ||
                    this.PIXIWrapper.PIXIApp.ticker.add(
                      this.animateFilter,
                      this
                    );
              },
              setFilter: function (text, t) {
                this.initDisplacementSprite(),
                  this.$parent.container
                    ? this.$parent.container.addChild(this.displacementSprite)
                    : this.PIXIWrapper.PIXIApp.stage.addChild(
                        this.displacementSprite
                      ),
                  this.initDisplacementFilter();
              },
              animateFilter: function (t) {
                try {
                  this.displacementSprite.rotation += 0.008;
                } catch (t) {
                  console.warn("err:", t);
                }
              },
              initDisplacementSprite: function () {
                var t = this.scale;
                return (
                  (this.displacementSprite = PIXI.Sprite.from(
                    this.displacementImage
                  )),
                  (this.displacementSprite.scale.x =
                    this.displacementSprite.scale.y =
                      t),
                  this.displacementSprite.anchor.set(this.anchor),
                  this.interactive ||
                    (this.displacementSprite.texture.baseTexture.wrapMode =
                      PIXI.WRAP_MODES.REPEAT),
                  this.displacementSprite.position.set(500, 400),
                  this.displacementSprite
                );
              },
              initDisplacementFilter: function () {
                try {
                  var t =
                    this.PIXIWrapper.PIXIApp.screen.width * this.filterScale;
                  return (
                    (this.filter = new PIXI.filters.DisplacementFilter(
                      this.displacementSprite
                    )),
                    (this.filter.scale.x = this.filter.scale.y = t),
                    (this.filter.autoFit = !0),
                    this.filter
                  );
                } catch (t) {
                  console.log("init displacement filter error", t);
                }
              },
            },
          }),
        lt = Object(R.a)(ot, void 0, void 0, !1, null, null, null).exports,
        ct = {
          mixins: [Z],
          props: {
            ratio: !1,
            scaleFactor: null,
            y: {
              default: 0,
            },
          },
          data: function () {
            return {
              containerHeight: 0,
              resizing: !1,
              bounds: {},
              container: null,
              outerContainer: null,
            };
          },
          watch: {
            currentProgress: function (t) {
              this.timeline && this.timeline.progress(t);
            },
          },
          computed: {
            noOffseting: function () {
              return 0 == this.topBound && !this.$device.isMobileOrTablet;
            },
            containerRenderable: function () {
              return this.scrollTop <= this.end && this.scrollTop >= this.start;
            },
            start: function () {
              var t = this.topOffset || this.topBound;
              return j()(this.offset) || 0 === t ? 0 : t - this.pageHeight;
            },
            end: function () {
              var t = this.start + this.bounds.height;
              return (t += this.noOffseting ? 0 : this.pageHeight), t;
            },
            pageHeight: function () {
              return this.$store.getters["app/getState"]("height");
            },
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
            scrollTop: function () {
              return this.$store.getters["app/getState"]("scrollTop");
            },
            currentProgress: function () {
              return this.scrollProgress(this.start, this.end, this.scrollTop);
            },
            move: function () {
              return this.pageWidth * this.ratio || 0;
            },
            elTop: function () {
              return this.getOffset();
            },
          },
          methods: {
            getOffset: function () {
              var t = -this.move;
              return j()(this.offset)
                ? this.offset
                : (t > 0 && this.wrapped) ||
                  (t > 0 && !this.wrapped) ||
                  this.noOffseting
                ? 0
                : this.wrapped && this.noScale
                ? t / 2
                : t;
            },
            scrollProgress: function (t, e, n) {
              return (n = n || this.scrollTop) >= t && n <= e
                ? (n - t) / ((e - t) / 100) / 100
                : n > t
                ? 1
                : 0;
            },
            initContainer: function () {
              (this.outerContainer = new PIXI.Container()),
                (this.container = new PIXI.Container()),
                this.outerContainer.addChild(this.container),
                (this.container.filters = []),
                this.$parent.container
                  ? this.$parent.container.addChild(this.outerContainer)
                  : this.PIXIWrapper.PIXIApp.stage.addChild(
                      this.outerContainer
                    ),
                U()(this.onInitContainer) && this.onInitContainer();
            },
            initParallaxTween2: function () {
              this.timeline && this.timeline.seek(0).clear(),
                (this.timeline = x.b.timeline({
                  paused: !0,
                })),
                this.timeline.fromTo(
                  this.container,
                  1,
                  {
                    y: this.elTop,
                  },
                  {
                    y: this.elTop + this.move,
                    ease: "Power1.easeOut",
                  }
                ),
                this.timeline.progress(this.currentProgress);
            },
            initParallaxTween: function () {
              this.timeline && this.timeline.seek(0).clear();
              var t = this.scaleFactor < 0 ? 1 - this.scaleFactor : 1,
                e = this.scaleFactor < 0 ? 1 : 1 + this.scaleFactor;
              (this.container.pivot.x = this.container.width / 2),
                (this.container.pivot.y = this.container.height / 2),
                (this.outerContainer.position.x = this.container.width / 2),
                (this.outerContainer.position.y = this.container.height / 2),
                (this.timeline = x.b.timeline({
                  paused: !0,
                })),
                this.timeline.fromTo(
                  this.container,
                  1,
                  {
                    y: this.elTop,
                  },
                  {
                    y: this.elTop + this.move,
                    ease: "Power1.easeOut",
                  }
                ),
                this.timeline.fromTo(
                  this.container.scale,
                  1,
                  {
                    x: t,
                    y: t,
                  },
                  {
                    x: e,
                    y: e,
                    ease: "Power1.easeOut",
                  },
                  0
                ),
                this.timeline.progress(this.currentProgress);
            },
            initMask: function () {
              this.mask ||
                ((this.mask = new PIXI.Graphics()),
                this.container.addChild(this.mask),
                (this.container.mask = this.mask)),
                this.mask.clear(),
                this.mask.beginFill("0x000000"),
                this.mask.drawRect(0, 0, this.bounds.width, this.bounds.height),
                (this.mask.renderable = !0);
            },
            onInitContainer: function () {
              var t = this;
              setTimeout(function () {
                t.resizeHandler();
              });
            },
            resizeParallax: function () {
              var t = this;
              if (
                (this.domObject &&
                  ((this.bounds = this.domObject.getBoundingClientRect()),
                  (this.topBound =
                    0 != this.y || this.$device.isMobileOrTablet
                      ? this.scrollTop + this.bounds.top
                      : this.bounds.top),
                  (this.topBound = Math.ceil(this.topBound))),
                this.container && this.container.height)
              ) {
                var e =
                  (this.bounds.height + Math.abs(this.move)) /
                  (L()(this.container, "height") /
                    L()(this.container, "scale.y"));
                (this.container.scale.x = this.container.scale.y = e),
                  (this.container.position.y = this.elTop),
                  setTimeout(function () {
                    t.initParallaxTween();
                  }, 100);
              }
            },
            resizeHandler: function () {
              var t = this;
              this.domObject &&
                (this.resizeParallax(),
                setTimeout(function () {
                  t.resizeParallax();
                }, 20));
            },
          },
        },
        ut = Object(R.a)(ct, void 0, void 0, !1, null, null, null).exports,
        pt = {
          mixins: [Z],
          props: ["ratio", "model", "noScale", "hasLights", "debug"],
          watch: {
            mouseX: function () {
              this.moveContainer();
            },
            mouseY: function () {
              this.moveContainer();
            },
            containerPosition: {
              handler: function (t, e) {
                _.isObject(this.container) &&
                  !this.container._destroyed &&
                  _.assign(this.container.position, t);
              },
              deep: !0,
            },
            outerScale: function (s) {
              _.isObject(this.outerContainer) &&
                !this.outerContainer._destroyed &&
                (this.outerContainer.scale.x = this.outerContainer.scale.y = s);
            },
            outerContainerPosition: function (t) {
              _.isObject(this.outerContainer) &&
                !this.outerContainer._destroyed &&
                _.assign(this.outerContainer.position, t);
            },
          },
          data: function () {
            return {
              yRatio: 0.5,
              containerPosition: {
                x: 0,
                y: 0,
              },
              currentX: 0,
              currentY: 0,
            };
          },
          computed: {
            mouseX: function () {
              return this.$store.getters["app/getState"]("mouseX");
            },
            mouseY: function () {
              return this.$store.getters["app/getState"]("mouseY");
            },
            windowWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
            windowHeight: function () {
              return this.$store.getters["app/getState"]("height");
            },
            move: function () {
              return this.ratio * this.windowWidth * 0.8;
            },
            x: function () {
              return -(this.mouseX / this.windowWidth - 0.5) * this.move;
            },
            y: function () {
              return (
                -(this.mouseY / this.windowHeight - 0.5) *
                this.move *
                this.yRatio
              );
            },
            outerScale: function () {
              return this.noScale ? 1 : 1 + this.ratio;
            },
            outerContainerPosition: function () {
              var t = this.windowWidth - this.windowWidth * this.outerScale;
              return {
                x: t / 2,
                y: t / (2 / this.yRatio),
              };
            },
          },
          mounted: function () {
            this.$bus.$on("resize", this.resizeHandler), this.moveContainer();
          },
          beforeDestroy: function () {
            this.tween && this.tween.pause(),
              this.$bus.$off("resize", this.resizeHandler);
          },
          methods: {
            moveContainer: function () {
              this.container &&
                (this.tween = x.a.to(this.containerPosition, 1.5, {
                  x: Math.round(this.x),
                  y: Math.round(this.y),
                  overwrite: !0,
                  ease: "Power4.easeOut",
                }));
            },
            initContainer: function () {
              (this.outerContainer = new PIXI.Container()),
                (this.container = new PIXI.Container()),
                this.outerContainer.addChild(this.container),
                (this.container.filters = []),
                (this.currentX = this.x),
                (this.currentY = this.y),
                x.a.set(this.container, {
                  x: this.x,
                  y: this.y,
                  ease: "Power4.easeOut",
                  onUpdate: function () {},
                }),
                _.assign(
                  this.outerContainer.position,
                  this.outerContainerPosition
                ),
                (this.outerContainer.scale.x = this.outerContainer.scale.y =
                  this.outerScale),
                this.$parent.container
                  ? this.$parent.container.addChild(this.outerContainer)
                  : this.PIXIWrapper.PIXIApp.stage.addChild(
                      this.outerContainer
                    );
            },
          },
        },
        ht = Object(R.a)(pt, void 0, void 0, !1, null, null, null).exports,
        ft = n(24),
        mt = n.n(ft),
        gt = n(105),
        vt = n.n(gt);

      function bt(object, t) {
        var e = Object.keys(object);
        if (Object.getOwnPropertySymbols) {
          var n = Object.getOwnPropertySymbols(object);
          t &&
            (n = n.filter(function (t) {
              return Object.getOwnPropertyDescriptor(object, t).enumerable;
            })),
            e.push.apply(e, n);
        }
        return e;
      }

      function yt(t) {
        for (var i = 1; i < arguments.length; i++) {
          var source = null != arguments[i] ? arguments[i] : {};
          i % 2
            ? bt(Object(source), !0).forEach(function (e) {
                Object(o.a)(t, e, source[e]);
              })
            : Object.getOwnPropertyDescriptors
            ? Object.defineProperties(
                t,
                Object.getOwnPropertyDescriptors(source)
              )
            : bt(Object(source)).forEach(function (e) {
                Object.defineProperty(
                  t,
                  e,
                  Object.getOwnPropertyDescriptor(source, e)
                );
              });
        }
        return t;
      }
      var wt = {
        mixins: [Z],
        components: {
          Container: K,
          Sprite: Q,
          ScrollingParallaxContainer: ut,
          ParallaxContainer: ht,
          TwistFilter: it,
          ColorMatrixFilter: st,
          DisplacementFilter: lt,
        },
        props: {
          domElement: !1,
          fill: !1,
          fixed: !1,
          sprite: !1,
          image: !1,
          y: {
            default: 0,
          },
        },
        data: function () {
          return {
            container: null,
            top: 0,
            bounds: {},
            containerSize: {},
            loaded: !1,
            spriteSize: {
              width: 0,
              height: 0,
            },
            anchors: [0.5, 0.6, 0.4],
            containerPosition: {
              x: 0,
              y: 0,
            },
            matrixes: [
              [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
              [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0],
            ],
          };
        },
        watch: {
          loaded: function () {
            this.resizeHandler();
          },
          containerPosition: function (t) {
            mt()(this.container) && vt()(this.container.position, t);
          },
          containerScale: function (s) {
            this.$refs.wrapper.container &&
              (this.$refs.wrapper.container.scale.x =
                this.$refs.wrapper.container.scale.y =
                  s);
          },
          isVisible: function (t) {
            this.container.renderable = this.container.visible = t;
          },
          image: function () {
            (this.loaded = !1), this.preload();
          },
        },
        created: function () {
          this.preload();
        },
        methods: {
          preload: function () {
            var t = this,
              e = PIXI.utils.TextureCache[this.image];
            !e && this.image
              ? ((this.loader = new PIXI.Loader()),
                this.loader.add(this.image, this.image),
                this.loader.load(function (e, n) {
                  var r = n[t.image].texture;
                  setTimeout(function () {
                    t.onSpriteLoaded(r);
                  }, 50);
                }))
              : setTimeout(function () {
                  t.onSpriteLoaded(e);
                }, 50);
          },
          initMask: function () {
            this.mask ||
              ((this.mask = new PIXI.Graphics()),
              this.container.addChild(this.mask),
              (this.container.mask = this.mask)),
              this.mask.clear(),
              this.mask.beginFill("0x000000"),
              this.mask.drawRect(0, 0, this.bounds.width, this.bounds.height),
              (this.mask.renderable = !0);
          },
          onSpriteLoaded: function (t) {
            var e = this;
            (this.loaded = !0),
              (this.spriteSize = {
                width: L()(t, "baseTexture.realWidth"),
                height: L()(t, "baseTexture.realHeight"),
              }),
              this.resizeHandler(),
              setTimeout(function () {
                e.$refs.parallax && e.$refs.parallax.resizeHandler();
              });
          },
          resizeHandler: function () {
            this.domObject &&
              ((this.bounds = this.domObject.getBoundingClientRect()),
              (this.top =
                0 != this.y
                  ? this.scrollTop + this.bounds.top
                  : this.bounds.top),
              (this.top = Math.ceil(this.top)),
              console.log("TOP!!!", this.top, this.isVisible, this.y),
              (this.containerPosition = {
                x:
                  (this.bounds.width -
                    this.spriteSize.width * this.containerScale) /
                  2,
                y:
                  (this.bounds.height -
                    this.spriteSize.height * this.containerScale) /
                  2,
              }),
              (this.container.renderable = this.container.visible =
                this.isVisible));
          },
        },
        computed: yt(
          yt({}, Object(B.c)("stage", ["spriteScale", "filterScale"])),
          {},
          {
            pageHeight: function () {
              return this.$store.getters["app/getState"]("height");
            },
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
            isVisible: function () {
              var t = this.top - this.pageHeight,
                e = this.top + this.bounds.height;
              return this.scrollTop >= t && this.scrollTop < e;
            },
            containerScale: function () {
              return (
                (this.spriteSize &&
                  0 !== this.spriteSize.width &&
                  Math[this.fill ? "max" : "min"](
                    this.bounds.width / this.spriteSize.width,
                    this.bounds.height / this.spriteSize.height
                  )) ||
                1
              );
            },
            topOffset: function () {
              return this.top - this.scrollTop;
            },
            leftOffset: function () {
              return this.bounds.left + this.containerPosition.x;
            },
          }
        ),
      };

      function xt(object, t) {
        var e = Object.keys(object);
        if (Object.getOwnPropertySymbols) {
          var n = Object.getOwnPropertySymbols(object);
          t &&
            (n = n.filter(function (t) {
              return Object.getOwnPropertyDescriptor(object, t).enumerable;
            })),
            e.push.apply(e, n);
        }
        return e;
      }
      var Ct,
        _t = {
          name: "stage",
          data: function () {
            return {
              PIXIWrapper: {
                PIXI: null,
                PIXIApp: null,
              },
            };
          },
          components: {
            PixiRenderer: Y,
            TracingContainer: Object(R.a)(
              wt,
              function () {
                var t = this,
                  e = t.$createElement,
                  n = t._self._c || e;
                return n(
                  "container",
                  {
                    key: t.image,
                    ref: "wrapper",
                  },
                  [
                    n(
                      "scrolling-parallax-container",
                      {
                        ref: "parallax",
                        attrs: {
                          domElement: t.domElement,
                          ratio: 0.15,
                          scaleFactor: 0.35,
                          y: t.y,
                          wrapped: !0,
                        },
                      },
                      t._l(t.matrixes, function (e, r) {
                        return n(
                          "container",
                          {
                            key: r,
                            attrs: {
                              ratio: 0.02,
                            },
                          },
                          [
                            n("color-matrix-filter", {
                              attrs: {
                                hasBlend: 0 != r,
                                matrix: e,
                              },
                            }),
                            t._v(" "),
                            n("displacement-filter", {
                              attrs: {
                                filterScale: t.filterScale,
                                scale: t.spriteScale,
                                interactive: !0,
                                displacementImage: "../displacement.png",
                                topOffset: t.topOffset,
                                leftOffset: t.leftOffset,
                                containerScale: t.containerScale,
                                anchor: t.anchors[r],
                              },
                            }),
                            t._v(" "),
                            t.loaded
                              ? n("sprite", {
                                  attrs: {
                                    noPreload: !0,
                                    imagePath: t.image,
                                  },
                                })
                              : t._e(),
                          ],
                          1
                        );
                      }),
                      1
                    ),
                  ],
                  1
                );
              },
              [],
              !1,
              null,
              null,
              null
            ).exports,
            Container: K,
            Sprite: Q,
            TwistFilter: it,
          },
          provide: function () {
            return {
              PIXIWrapper: this.PIXIWrapper,
            };
          },
          props: {
            model: !1,
            image: !1,
            domElement: !1,
            y: {
              default: 0,
            },
          },
          created: function () {
            PIXI.utils.skipHello();
          },
          methods: {
            onLoaded: function () {},
          },
          computed: (function (t) {
            for (var i = 1; i < arguments.length; i++) {
              var source = null != arguments[i] ? arguments[i] : {};
              i % 2
                ? xt(Object(source), !0).forEach(function (e) {
                    Object(o.a)(t, e, source[e]);
                  })
                : Object.getOwnPropertyDescriptors
                ? Object.defineProperties(
                    t,
                    Object.getOwnPropertyDescriptors(source)
                  )
                : xt(Object(source)).forEach(function (e) {
                    Object.defineProperty(
                      t,
                      e,
                      Object.getOwnPropertyDescriptor(source, e)
                    );
                  });
            }
            return t;
          })({}, Object(B.c)("stage", ["containers"])),
        },
        Ot =
          (n(613),
          Object(R.a)(
            _t,
            function () {
              var t = this.$createElement,
                e = this._self._c || t;
              return e(
                "div",
                {
                  staticClass: "stage",
                },
                [
                  e(
                    "client-only",
                    [
                      e(
                        "pixi-renderer",
                        {
                          staticClass: "renderer",
                        },
                        [
                          e("tracing-container", {
                            attrs: {
                              domElement: this.domElement,
                              fill: !0,
                              y: this.y,
                              image: this.image,
                            },
                          }),
                        ],
                        1
                      ),
                    ],
                    1
                  ),
                ],
                1
              );
            },
            [],
            !1,
            null,
            "2cdf4e22",
            null
          ).exports),
        Pt = {
          name: "hero-header",
          data: function () {
            return {
              tl: null,
              aniDelay: 0.45,
              showCursor: !1,
              image1Visible: !1,
              image2Visible: !1,
              shrinked: !1,
              loading: !0,
              letterX: [
                -100, 80, 120, 150, 140, -60, -100, -80, -20, 60, 90, -40, -80,
                90,
              ],
              letterY: [
                -100, -80, -140, -180, -10, 40, -50, -40, -10, -60, 100, -40,
                -90, 30,
              ],
              filterScale: 0.01,
              spriteScale: 5,
              brightness: 0.6,
              contrast: 0.5,
              contrastMultiplier: 4,
              reversedBrightness: 0.2,
            };
          },
          props: ["model"],
          mixins: [W, n(106).a],
          watch: {
            currentProgress: function (t) {
              this.tl.pause(), this.tl && this.tl.progress(1 - t);
            },
            filterScale: function (t) {
              this.$store.dispatch("stage/SET_STATE", {
                filterScale: t,
              });
            },
            spriteScale: function (t) {
              this.$store.dispatch("stage/SET_STATE", {
                spriteScale: t,
              });
            },
            brightness: function (t) {
              this.$store.dispatch("stage/SET_STATE", {
                brightness: t,
              });
            },
            contrast: function (t) {
              this.$store.dispatch("stage/SET_STATE", {
                contrast: t,
              });
            },
            reversedBrightness: function (t) {
              this.$store.dispatch("stage/SET_STATE", {
                reversedBrightness: t,
              });
            },
          },
          mounted: function () {
            var t = this,
              e = this;
            this.initLettersTimeline(),
              setTimeout(function () {
                t.tl.play();
              }, 1e3 * this.animationDelay + 1200),
              setTimeout(function () {
                t.loading = !1;
              }, 1e3 * (this.aniDelay + this.animationDelay + 1.5)),
              setTimeout(function () {
                t.$refs.image &&
                  t.isWebkit &&
                  (x.b.set(t.$refs.image.$el, {
                    "-webkit-filter": "grayscale(100%)",
                    filter: "grayscale(100%)",
                  }),
                  x.b.to({}, 1.4, {
                    onUpdateParams: ["{self}"],
                    ease: "Power4.easeInOut",
                    delay: t.aniDelay + t.animationDelay + 1.4,
                    onUpdate: function (t) {
                      var n = (100 - 100 * this.progress()) >> 0;
                      e.$refs.image &&
                        x.b.set(e.$refs.image.$el, {
                          "-webkit-filter": "grayscale(".concat(n, "%)"),
                          filter: "grayscale(".concat(n, "%)"),
                        });
                    },
                  }));
              });
          },
          methods: {
            initLettersTimeline: function () {
              var t = this,
                e = this,
                n = this.$el.querySelectorAll(".hero-headline");
              (this.tl = x.b.timeline({
                paused: !0,
              })),
                O()(n, function (n, r) {
                  t.tl.fromTo(
                    n,
                    1,
                    {
                      opacity: 0,
                      x: e.letterX[r],
                      y: e.letterY[r],
                    },
                    {
                      opacity: 1,
                      x: 0,
                      y: 0,
                    },
                    0
                  );
                });
            },
            onLeave: function (t) {
              this.shrinked = !1;
            },
            showImage: function (t) {
              this.loading ||
                (X()([4, 9], 1 * t) &&
                  ((this.shrinked = !0), clearTimeout(this.t)),
                4 == t
                  ? (this.image1Visible = !0)
                  : 9 == t && (this.image2Visible = !0));
            },
            isBreak: function (t) {
              return t.includes("br");
            },
            mobileOnlyBreak: function (t) {
              return t.includes("d-md-none");
            },
            desktopOnlyBreak: function (t) {
              return t.includes("d-none") && !this.mobileOnlyBreak(t);
            },
            image: function (t) {
              return this.getImage(this.images[t], "img", "?w=320");
            },
            item: function (t) {
              return this.headlineList && this.headlineList[t];
            },
          },
          computed:
            ((Ct = {
              controls: function () {
                return this.$store.state.route.query.controls;
              },
              animationDelay: function () {
                return this.$store.getters["app/getState"]("animationDelay");
              },
              pageWidth: function () {
                return this.$store.getters["app/getState"]("width");
              },
              isWebkit: function () {
                return this.$store.getters["app/getState"]("isWebkit");
              },
              images: function () {
                return L()(this.model, "fields.images");
              },
              headlineList: function () {
                return L()(this.model, "fields.headlineList");
              },
            }),
            Object(o.a)(Ct, "animationDelay", function () {
              return this.$store.getters["app/getState"]("animationDelay");
            }),
            Object(o.a)(Ct, "end", function () {
              return this.start + this.pageHeight / 2;
            }),
            Ct),
          components: {
            FancyCursor: z,
            HeaderStage: Ot,
          },
        },
        Lt =
          (n(615),
          Object(R.a)(
            Pt,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "waypoint",
                {
                  staticClass:
                    "hero-header d-flex flex-column flex-center justify-content-md-end pb-15 position-relative overflow-hidden",
                  attrs: {
                    id: "hero-header",
                    "data-scroll-section": "",
                    y: t.elementY,
                  },
                  on: {
                    toggleOneDirectionVisible: t.toggleWaypoint,
                  },
                  nativeOn: {
                    mousemove: function (e) {
                      t.showCursor = !0;
                    },
                    mouseleave: function (e) {
                      t.showCursor = !1;
                    },
                  },
                },
                [
                  t.$device.isDesktop
                    ? t._e()
                    : n(
                        "parallax",
                        {
                          attrs: {
                            offset: 0,
                            absolute: !0,
                            wrapped: !0,
                            speedFactor: 0.15,
                            global: !0,
                            y: t.elementY,
                            scaleFactor: 0.35,
                          },
                        },
                        [
                          n("image-div", {
                            ref: "image",
                            staticClass:
                              "h-100 w-100 image position-absolute l-0 t-0 should-animate",
                            attrs: {
                              innerClasses: "transition-out-image",
                              src: t.getImage(
                                t.model,
                                "backgroundImage",
                                "?w=1920",
                                "?h=735&w=375&fit=fill"
                              ),
                              "data-preset": "opacity,scale",
                              "data-fromscale": "3",
                              "data-delay": t.aniDelay + 0.2,
                              "data-dur": "2",
                            },
                          }),
                        ],
                        1
                      ),
                  t._v(" "),
                  n(
                    "client-only",
                    [
                      t.$device.isDesktop
                        ? n("header-stage", {
                            ref: "image",
                            staticClass:
                              "position-absolute w-100 h-100 t-0 l-0 no-events ",
                            class: {
                              "should-animate":
                                !t.$device.isMobileOrTablet || !0,
                            },
                            attrs: {
                              "data-preset": "opacity,scale",
                              "data-fromscale": "3",
                              "data-delay": t.aniDelay + 1.45,
                              "data-dur": "1.4",
                              "data-ease": "Power4.easeOut",
                              interactive: !0,
                              image: t.getImage(
                                t.model,
                                "backgroundImage",
                                "?w=1920",
                                "?h=735&w=375&fit=fill"
                              ),
                              domElement: "hero-header",
                            },
                          })
                        : t._e(),
                    ],
                    1
                  ),
                  t._v(" "),
                  n(
                    "div",
                    {
                      staticClass:
                        "on-top color-white text-center d-flex flex-column justify-content-between h-100 pt-md-5",
                    },
                    [
                      n("div"),
                      t._v(" "),
                      n(
                        "h1",
                        {
                          staticClass:
                            "px-1 px-md-0 text-center row flex-center",
                        },
                        t._l(t.headlineList, function (e, r) {
                          return n("span", {
                            key: r,
                            staticClass:
                              "antiqua-font hero-headline display-3 lh-09",
                            class: {
                              "d-md-none w-100 h-0": t.mobileOnlyBreak(e),
                              "d-none d-md-block w-100 h-0":
                                t.desktopOnlyBreak(e),
                              "d-inline-block margin-em": !t.isBreak(e),
                              "d-block w-100 h-0":
                                !t.mobileOnlyBreak(e) &&
                                !t.desktopOnlyBreak(e) &&
                                t.isBreak(e),
                            },
                            attrs: {
                              "data-delay": 0.3 + t.aniDelay,
                            },
                            domProps: {
                              innerHTML: t._s(e),
                            },
                          });
                        }),
                        0
                      ),
                      t._v(" "),
                      t.model.fields.caption
                        ? n("p", {
                            staticClass:
                              "antiqua-font medium d-none d-md-block should-animate mt-15 big-letter-spacing h6 lh-12",
                            attrs: {
                              "data-preset": "opacity,y",
                              "data-delay": 0.75 + t.aniDelay,
                              "data-fromy": "-40",
                            },
                            domProps: {
                              innerHTML: t._s(t.model.fields.caption),
                            },
                          })
                        : t._e(),
                      t._v(" "),
                      n("div", [
                        n(
                          "div",
                          {
                            staticClass: "mb-4 my-md-5",
                          },
                          [
                            n("div", {
                              staticClass: "vertical-line should-animate",
                              attrs: {
                                "data-props": "scaleX",
                                "data-delay": 0.95 + t.aniDelay,
                                "data-dur": "1.2",
                                "data-toscale-x": "1",
                                "data-fromscale-x": "0",
                              },
                            }),
                          ]
                        ),
                        t._v(" "),
                        t.model.fields.description
                          ? n("p", {
                              staticClass:
                                "mediumv2 uppercase antiqua-font d-none d-md-block should-animate lh-12",
                              attrs: {
                                "data-props": "opacity,y",
                                "data-delay": 1.1 + t.aniDelay,
                                "data-fromy": "-40",
                              },
                              domProps: {
                                innerHTML: t._s(t.model.fields.description),
                              },
                            })
                          : t._e(),
                        t._v(" "),
                        t.model.fields.caption
                          ? n("h2", {
                              staticClass:
                                "antiqua-font medium font-mobile d-block d-md-none h6",
                              domProps: {
                                innerHTML: t._s(t.model.fields.caption),
                              },
                            })
                          : t._e(),
                      ]),
                    ]
                  ),
                  t._v(" "),
                  t.$device.isDesktop
                    ? n("fancy-cursor", {
                        class: {
                          "fade-out": !t.showCursor,
                        },
                        attrs: {
                          shrinked: t.shrinked,
                        },
                      })
                    : t._e(),
                ],
                1
              );
            },
            [],
            !1,
            null,
            "093c996a",
            null
          ).exports),
        kt = {
          name: "page-header",
          props: ["model"],
        },
        St =
          (n(616),
          Object(R.a)(
            kt,
            function () {
              var t = this.$createElement,
                e = this._self._c || t;
              return e(
                "div",
                {
                  staticClass: "page-header",
                },
                [e("h1", [this._v(this._s(this.model.fields.title))])]
              );
            },
            [],
            !1,
            null,
            "d863529e",
            null
          ).exports),
        It = {
          name: "contact-header",
          data: function () {
            return {
              textDelay: 0.2,
            };
          },
          props: ["model"],
          mixins: [W],
          watch: {
            lines: function () {
              this.toggleWaypointWithLines({
                el: this.$refs.waypoint1.$el,
                visible: !0,
              });
            },
          },
          mounted: function () {},
          methods: {
            image: function (t) {
              return this.getImage(this.images[t], "img", "?w=1024");
            },
          },
          computed: {
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
            images: function () {
              return L()(this.model, "fields.images");
            },
          },
          components: {},
        },
        jt =
          (n(617),
          Object(R.a)(
            It,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "div",
                {
                  staticClass:
                    "contact-header bg-black color-white text-center pt-4 pb-4 pt-md-6 pb-md-5 overflow-hidden position-relative",
                  attrs: {
                    "data-scroll-section": "",
                  },
                },
                [
                  n(
                    "waypoint",
                    {
                      ref: "waypoint1",
                      on: {
                        toggleOneDirectionVisible: t.toggleWaypointWithLines,
                      },
                    },
                    [
                      t.model.fields.caption
                        ? n("h2", {
                            staticClass:
                              "small lh-1 mb-2 founders-regular on-top position-relative pt-2 pt-md-0 should-animate",
                            attrs: {
                              "data-preset": "opacity,y",
                              "data-fromy": "-40",
                              "data-delay": ".1",
                            },
                            domProps: {
                              innerHTML: t._s(t.model.fields.caption),
                            },
                          })
                        : t._e(),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass: "d-flex flex-center",
                        },
                        [
                          t.model.fields.headline
                            ? n(
                                "split-text",
                                {
                                  key: t.pageWidth,
                                  staticClass:
                                    "col-md-5  px-2 px-md-0 mb-2 mb-md-0  on-top",
                                  attrs: {
                                    tag: "h2",
                                  },
                                  on: {
                                    splited: t.onSplited,
                                  },
                                },
                                [
                                  n("h2", {
                                    staticClass:
                                      "display-4 antiqua-font headline",
                                    domProps: {
                                      innerHTML: t._s(t.model.fields.headline),
                                    },
                                  }),
                                ]
                              )
                            : t._e(),
                        ],
                        1
                      ),
                      t._v(" "),
                      n(
                        "parallax",
                        {
                          staticClass: "image-one pl-md-2 d-none d-md-block",
                          attrs: {
                            speedFactor: -0.2,
                            global: !0,
                            startOffset: 0,
                            offset: 0,
                          },
                        },
                        [
                          n(
                            "mouse-parallax",
                            {
                              attrs: {
                                global: !0,
                                ratioX: 0.2,
                                ratioY: 0.2,
                              },
                            },
                            [
                              n("image-div", {
                                attrs: {
                                  innerClasses:
                                    "transition-in-image transition-out-image",
                                  proportion: t.pageWidth < 768 ? 1 : 0.7,
                                  src: t.image(0),
                                },
                              }),
                            ],
                            1
                          ),
                        ],
                        1
                      ),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass:
                            "d-flex image-wrapper flex-row-reverse flex-md-row",
                        },
                        [
                          n(
                            "parallax",
                            {
                              staticClass: "image-two offset-md-2",
                              attrs: {
                                speedFactor: -0.3,
                                global: !0,
                                startOffset: 0,
                                offset: 0,
                              },
                            },
                            [
                              n(
                                "mouse-parallax",
                                {
                                  attrs: {
                                    global: !0,
                                    ratioX: 0.3,
                                    ratioY: 0.3,
                                  },
                                },
                                [
                                  n("image-div", {
                                    attrs: {
                                      innerClasses:
                                        "transition-in-image transition-out-image",
                                      proportion: t.pageWidth < 768 ? 0.9 : 0.7,
                                      src: t.image(1),
                                    },
                                  }),
                                ],
                                1
                              ),
                            ],
                            1
                          ),
                          t._v(" "),
                          n(
                            "parallax",
                            {
                              staticClass: "image-three offset-md-1",
                              attrs: {
                                speedFactor: -0.35,
                                global: !0,
                                startOffset: 0,
                                offset: 0,
                              },
                            },
                            [
                              n(
                                "mouse-parallax",
                                {
                                  attrs: {
                                    global: !0,
                                    ratioX: 0.35,
                                    ratioY: 0.35,
                                  },
                                },
                                [
                                  n("image-div", {
                                    attrs: {
                                      innerClasses:
                                        "transition-in-image transition-out-image",
                                      proportion: t.pageWidth < 768 ? 0.9 : 0.7,
                                      src: t.image(2),
                                    },
                                  }),
                                ],
                                1
                              ),
                            ],
                            1
                          ),
                        ],
                        1
                      ),
                      t._v(" "),
                      n(
                        "parallax",
                        {
                          staticClass:
                            "image-four offset-md-8 d-none d-md-block",
                          attrs: {
                            speedFactor: -0.1,
                            global: !0,
                            startOffset: 0,
                            offset: 0,
                          },
                        },
                        [
                          n(
                            "mouse-parallax",
                            {
                              attrs: {
                                global: !0,
                                ratioX: 0.1,
                                ratioY: 0.1,
                              },
                            },
                            [
                              n("image-div", {
                                attrs: {
                                  innerClasses:
                                    "transition-in-image transition-out-image",
                                  proportion: t.pageWidth < 768 ? 1 : 0.7,
                                  src: t.image(3),
                                },
                              }),
                            ],
                            1
                          ),
                        ],
                        1
                      ),
                      t._v(" "),
                      n(
                        "parallax",
                        {
                          staticClass: "image-five",
                          attrs: {
                            speedFactor: -0.2,
                            global: !0,
                            startOffset: 0,
                            offset: 0,
                          },
                        },
                        [
                          n(
                            "mouse-parallax",
                            {
                              attrs: {
                                global: !0,
                                ratioX: 0.2,
                                ratioY: 0.2,
                              },
                            },
                            [
                              n("image-div", {
                                attrs: {
                                  innerClasses:
                                    "transition-in-image transition-out-image",
                                  proportion: t.pageWidth < 768 ? 1 : 0.9,
                                  src: t.image(4),
                                },
                              }),
                            ],
                            1
                          ),
                        ],
                        1
                      ),
                    ],
                    1
                  ),
                  t._v(" "),
                  n(
                    "waypoint",
                    {
                      staticClass:
                        "d-flex flex-column flex-center mt-3 mt-md-0",
                      on: {
                        toggleOneDirectionVisible: t.toggleWaypoint,
                      },
                    },
                    [
                      n(
                        "span",
                        {
                          staticClass: "uppercase d-block should-animate",
                          attrs: {
                            "data-preset": "opacity,y",
                            "data-delay": ".1",
                          },
                        },
                        [t._v("Email Me")]
                      ),
                      t._v(" "),
                      n(
                        "a",
                        {
                          staticClass:
                            "medium antiqua-font founders-regular link d-flex mb-3 should-animate",
                          attrs: {
                            "data-preset": "opacity,y",
                            "data-delay": ".2",
                            href: "mailto:" + t.model.fields.email,
                          },
                        },
                        [t._v(t._s(t.model.fields.email))]
                      ),
                      t._v(" "),
                      n(
                        "span",
                        {
                          staticClass: "uppercase d-block should-animate",
                          attrs: {
                            "data-preset": "opacity,y",
                            "data-delay": ".3",
                          },
                        },
                        [t._v("Follow me")]
                      ),
                      t._v(" "),
                      n(
                        "a",
                        {
                          staticClass:
                            "medium antiqua-font founders-regular link d-flex should-animate",
                          attrs: {
                            "data-preset": "opacity,y",
                            "data-delay": ".4",
                            rel: "noopener",
                            href: t.model.fields.instagram,
                            target: "_blank",
                          },
                        },
                        [t._v(t._s(t.$t("@nabazabihphotography")))]
                      ),
                    ]
                  ),
                ],
                1
              );
            },
            [],
            !1,
            null,
            "adb2b482",
            null
          ).exports),
        Tt = n(58),
        $t = n.n(Tt),
        Et =
          (n(619),
          {
            mixins: [Z],
            components: {
              Container: K,
              Sprite: Q,
            },
            props: {
              domElement: null,
              y: {
                default: 0,
              },
              squareMask: !1,
              parentOffset: {
                default: 0,
              },
              fill: !1,
              fixed: !1,
              sprite: !1,
              image: !1,
              hasMask: !1,
            },
            data: function () {
              return {
                container: null,
                top: 0,
                bounds: {},
                containerSize: {},
                loaded: !1,
                spriteSize: {
                  width: 0,
                  height: 0,
                },
                containerPosition: {
                  x: 0,
                  y: 0,
                },
              };
            },
            watch: {
              loaded: function () {
                this.resizeHandler();
              },
              parentOffset: function () {
                this.resizeHandler();
              },
              containerPosition: function (t) {
                mt()(this.container) && vt()(this.container.position, t);
              },
              containerScale: function (s) {
                this.$refs.wrapper.container &&
                  (this.$refs.wrapper.container.scale.x =
                    this.$refs.wrapper.container.scale.y =
                      s);
              },
              isVisible: function (t) {},
              image: function () {
                (this.loaded = !1), this.preload();
              },
            },
            mounted: function () {},
            created: function () {
              this.preload();
            },
            methods: {
              preload: function () {
                var t = this,
                  e = PIXI.utils.TextureCache[this.image];
                !e && this.image
                  ? ((this.loader = new PIXI.Loader()),
                    this.loader.add(this.image, this.image),
                    this.loader.load(function (e, n) {
                      var r = n[t.image].texture;
                      setTimeout(function () {
                        t.onSpriteLoaded(r);
                      }, 50);
                    }))
                  : setTimeout(function () {
                      t.onSpriteLoaded(e);
                    }, 50);
              },
              initMask: function () {
                this.t && clearTimeout(this.t);
                try {
                  this.mask ||
                    ((this.mask = new PIXI.Graphics()),
                    this.container.addChild(this.mask),
                    (this.container.mask = this.mask)),
                    this.mask.clear(),
                    this.mask.beginFill("0x000000"),
                    this.squareMask
                      ? this.mask.drawRect(
                          0,
                          0,
                          this.bounds.width,
                          this.bounds.height
                        )
                      : this.mask.drawEllipse(
                          this.bounds.width / 2,
                          this.bounds.height / 2,
                          this.bounds.width / 2,
                          this.bounds.height / 2
                        );
                } catch (t) {
                  console.log("init mask error:", t);
                }
              },
              onSpriteLoaded: function (t) {
                var e = this;
                (this.loaded = !0),
                  (this.spriteSize = {
                    width: L()(t, "baseTexture.realWidth"),
                    height: L()(t, "baseTexture.realHeight"),
                  }),
                  this.resizeHandler(),
                  setTimeout(function () {
                    e.$refs.parallax && e.$refs.parallax.resizeHandler(),
                      console.log("preload image", t),
                      e.$emit("loaded", t);
                  });
              },
              resizeHandler: function () {
                if (this.domObject && this.container) {
                  this.bounds = this.domObject.getBoundingClientRect();
                  var t =
                      0 != this.y || this.$device.isMobileOrTablet
                        ? this.scrollTop + this.bounds.top
                        : this.bounds.top,
                    e = t - this.parentOffset;
                  (this.top = t - this.pageHeight),
                    this.fixed
                      ? (this.containerPosition = {
                          x: this.bounds.x,
                          y: e,
                        })
                      : (this.containerPosition = {
                          x:
                            (this.PIXIWrapper.PIXIApp.width -
                              this.bounds.width) /
                            2,
                          y:
                            (this.PIXIWrapper.PIXIApp.height -
                              this.bounds.height) /
                            2,
                        }),
                    this.hasMask && this.initMask();
                }
              },
            },
            computed: {
              pageHeight: function () {
                return this.$store.getters["app/getState"]("height");
              },
              pageWidth: function () {
                return this.$store.getters["app/getState"]("width");
              },
              isVisible: function () {
                var t = this.top,
                  e = this.top + this.pageHeight + this.bounds.height;
                return this.scrollTop >= t && this.scrollTop < e;
              },
              containerScale: function () {
                return (
                  (this.spriteSize &&
                    0 !== this.spriteSize.width &&
                    Math[this.fill ? "max" : "min"](
                      this.bounds.width / this.spriteSize.width,
                      this.bounds.height / this.spriteSize.height
                    )) ||
                  1
                );
              },
              topOffset: function () {
                return this.top - this.scrollTop;
              },
              leftOffset: function () {
                return this.bounds.left + this.containerPosition.x;
              },
            },
          }),
        At = Object(R.a)(
          Et,
          function () {
            var t = this.$createElement;
            return (this._self._c || t)(
              "container",
              {
                key: this.image,
                ref: "wrapper",
              },
              [this._t("default")],
              2
            );
          },
          [],
          !1,
          null,
          null,
          null
        ).exports,
        Mt =
          (n(308),
          {
            inject: ["PIXIWrapper"],
            props: [
              "options",
              "domElement",
              "rotation",
              "color",
              "text",
              "visible",
              "fontSize",
              "alwaysAnimate",
              "repeat",
              "width",
              "image",
              "letterSpacing",
              "scaleFactor",
              "ratio",
            ],
            data: function () {
              return {
                container: null,
                domObject: null,
                sprite: null,
                rope: null,
                radius: 0,
                points: [],
                bounds: [],
                angle: 1.2219,
                step: 0,
                maxRopePoints: 30,
              };
            },
            render: function (t) {
              return t();
            },
            beforeDestroy: function () {
              this.$bus.$off("resize", this.resizeHandler),
                PIXI.Ticker.shared.remove(
                  this.animate,
                  this.PIXIWrapper.PIXIApp
                ),
                this.t && clearTimeout(this.t),
                this.rope && this.rope.destroy(!0),
                this.container &&
                  this.container.destroy({
                    children: !0,
                    texture: !0,
                    baseTexture: !0,
                  });
            },
            watch: {
              visible: function (t) {
                t
                  ? PIXI.Ticker.shared.add(
                      this.animate,
                      this.PIXIWrapper.PIXIApp
                    )
                  : PIXI.Ticker.shared.remove(
                      this.animate,
                      this.PIXIWrapper.PIXIApp
                    );
              },
            },
            computed: {
              computedFontSize: function () {
                return this.circumference / 180;
              },
              isWebkit: function () {
                return this.$store.getters["app/getState"]("isWebkit");
              },
              circumference: function () {
                var t = (2.033 * this.radius) / 2,
                  e = (2.6725 * this.radius) / 2,
                  n = Math.pow(t - e, 2) / Math.pow(t + e, 2);
                return (
                  Math.PI *
                  (t + e) *
                  (1 + (3 * n) / (10 + Math.sqrt(4 - 3 * n)))
                );
              },
            },
            methods: {
              animate: function () {
                for (var i = 0; i < this.points.length; i++)
                  (this.points[i].y =
                    this.radius *
                    Math.sin(
                      this.step * i + PIXI.Ticker.shared.lastTime / 12e3
                    ) *
                    this.angle),
                    (this.points[i].x =
                      this.radius *
                      Math.cos(
                        this.step * i + PIXI.Ticker.shared.lastTime / 12e3
                      ));
              },
              resizeHandler: function () {
                if (null !== this.rope && this.domObject) {
                  (this.bounds = this.domObject.getBoundingClientRect()),
                    (this.angle = this.bounds.height / this.bounds.width),
                    (this.rope.position.x = this.bounds.width / 2),
                    (this.rope.position.y =
                      (this.bounds.width * this.angle) / 2);
                  var s = this.width + this.fontSize;
                  this.rope.scale.x = this.rope.scale.y =
                    (this.bounds.width / s) * (this.scaleFactor || 1);
                }
              },
              preloadAssets: function () {
                var t = this;
                if (this.image) {
                  var img = PIXI.utils.TextureCache[this.imagePath];
                  console.log("img:", img),
                    (this.loader = new PIXI.Loader()),
                    this.loader.add(this.image, this.image),
                    this.loader.load(function (e, n) {
                      t.initRope();
                    });
                } else this.initRope();
              },
              initRope: function () {
                console.log(this),
                  this.initSprite(),
                  this.addRope(),
                  this.resizeHandler();
              },
              initSprite: function () {
                var t = (this.container = new PIXI.Container());
                console.log("font size:", this.fontSize);
                var e = this.text.repeat(this.repeat).toUpperCase(),
                  n = new PIXI.Text(e, {
                    fontSize: this.fontSize,
                    fontFamily: "AntiquaRoman",
                    fill: this.color,
                    letterSpacing: this.letterSpacing,
                  });
                (n.resolution = 2),
                  (n.style.trim = !0),
                  n.updateText(),
                  (n.cacheAsBitmap = !0),
                  t.addChild(n);
                var r = this.isWebkit ? 1.4 : 1;
                if (this.image) {
                  var o = this.isWebkit ? 1 : 1.4,
                    l = new PIXI.Sprite.from("../star.png");
                  (l.position.x = -270 / o / this.ratio),
                    (l.position.y = -10 / this.ratio),
                    (l.scale.x = l.scale.y = (1 * r) / this.ratio),
                    t.addChild(l);
                }
                if (this.PIXIWrapper.PIXIApp.renderer) {
                  var c = this.PIXIWrapper.PIXIApp.renderer.generateTexture(t);
                  (this.sprite = new PIXI.Sprite(c)),
                    (this.sprite.resolution = 2);
                }
              },
              addRope: function () {
                if (this.domObject && this.sprite) {
                  (this.radius = this.width / 2),
                    (this.step = Math.PI / this.maxRopePoints);
                  var t =
                    this.maxRopePoints -
                    Math.round(
                      (this.sprite.texture.width / (this.radius * Math.PI)) *
                        this.maxRopePoints
                    );
                  t /= 2;
                  for (
                    var e = (this.points = []), i = this.maxRopePoints - t;
                    i > t;
                    i--
                  ) {
                    var n = this.radius * Math.cos(this.step * i),
                      r = this.radius * Math.sin(this.step * i) * this.angle;
                    e.push(new PIXI.Point(n, -r));
                  }
                  (this.rope = new PIXI.SimpleRope(this.sprite.texture, e)),
                    (this.rope.rotation = this.rotation || 0),
                    (
                      this.$parent.container || this.PIXIWrapper.PIXIApp.stage
                    ).addChild(this.rope),
                    this.alwaysAnimate &&
                      PIXI.Ticker.shared.add(
                        this.animate,
                        this.PIXIWrapper.PIXIApp
                      );
                }
              },
            },
            mounted: function () {
              var t = this;
              (this.domObject = document.getElementById(this.domElement)),
                console.log("rope mounted:", this.domObject, this.domElement),
                this.$bus.$on("resize", this.resizeHandler),
                this.PIXIWrapper.PIXIApp
                  ? this.preloadAssets()
                  : this.$bus.$on("ready", function () {
                      t.$nextTick(function () {
                        t.preloadAssets();
                      });
                    });
            },
          });

      function Dt(object, t) {
        var e = Object.keys(object);
        if (Object.getOwnPropertySymbols) {
          var n = Object.getOwnPropertySymbols(object);
          t &&
            (n = n.filter(function (t) {
              return Object.getOwnPropertyDescriptor(object, t).enumerable;
            })),
            e.push.apply(e, n);
        }
        return e;
      }
      var Wt = {
          name: "stage",
          data: function () {
            return {
              PIXIWrapper: {
                PIXI: null,
                PIXIApp: null,
              },
              texture: null,
            };
          },
          components: {
            PixiRenderer: Y,
            TracingContainer: At,
            Container: K,
            Sprite: Q,
            TwistFilter: it,
            PixiRope: Object(R.a)(Mt, void 0, void 0, !1, null, null, null)
              .exports,
            DisplacementFilter: lt,
          },
          provide: function () {
            return {
              PIXIWrapper: this.PIXIWrapper,
            };
          },
          props: {
            elementY: {
              default: 0,
            },
            containers: !1,
            model: !1,
            index: !1,
          },
          destroyed: function () {},
          mounted: function () {
            console.log("render stage", this.index, this.containers);
          },
          methods: {
            onLoadedImage: function (t) {
              this.texture = t;
            },
            getRatio: function (t) {
              return this.isWebkit ? t / this.width : 1;
            },
            getFontSize: function (t, e) {
              return t / this.getRatio(e);
            },
            getLetterSpacing: function (t, e) {
              return t / this.getRatio(e);
            },
            getWidth: function (t) {
              return t / this.getRatio(t);
            },
            onLoaded: function () {
              O()(this.$refs.wrapper, function (t) {
                t.resizeHandler();
              });
            },
          },
          computed: (function (t) {
            for (var i = 1; i < arguments.length; i++) {
              var source = null != arguments[i] ? arguments[i] : {};
              i % 2
                ? Dt(Object(source), !0).forEach(function (e) {
                    Object(o.a)(t, e, source[e]);
                  })
                : Object.getOwnPropertyDescriptors
                ? Object.defineProperties(
                    t,
                    Object.getOwnPropertyDescriptors(source)
                  )
                : Dt(Object(source)).forEach(function (e) {
                    Object.defineProperty(
                      t,
                      e,
                      Object.getOwnPropertyDescriptor(source, e)
                    );
                  });
            }
            return t;
          })({}, Object(B.c)("app", ["width", "fontsLoaded", "isWebkit"])),
        },
        Ht = Object(R.a)(
          Wt,
          function () {
            var t = this,
              e = t.$createElement,
              n = t._self._c || e;
            return n(
              "div",
              {
                staticClass: "stage",
              },
              [
                n(
                  "client-only",
                  [
                    n(
                      "pixi-renderer",
                      t._l(t.containers, function (e, r) {
                        return t.fontsLoaded
                          ? n(
                              "tracing-container",
                              {
                                key: r,
                                attrs: {
                                  y: t.elementY,
                                  hasMask: e.hasMask,
                                  domElement: e.domElement,
                                  image: e.image,
                                  fill: !1,
                                },
                                on: {
                                  loaded: t.onLoadedImage,
                                },
                              },
                              [
                                t.texture || !e.image
                                  ? n(
                                      "container",
                                      {
                                        ref: "wrapper",
                                        refInFor: !0,
                                        attrs: {
                                          scale: e.hasFilter ? 1.075 : 1,
                                        },
                                      },
                                      [
                                        e.hasFilter
                                          ? n("displacement-filter")
                                          : t._e(),
                                        t._v(" "),
                                        n("sprite", {
                                          attrs: {
                                            centered: !0,
                                            imagePath: e.image,
                                          },
                                          on: {
                                            loaded: t.onLoaded,
                                          },
                                        }),
                                        t._v(" "),
                                        n(
                                          "container",
                                          {
                                            key: t.width,
                                          },
                                          [
                                            e.text
                                              ? n("pixi-rope", {
                                                  attrs: {
                                                    letterSpacing:
                                                      t.getLetterSpacing(
                                                        e.letterSpacing,
                                                        e.width
                                                      ),
                                                    fontSize: t.getFontSize(
                                                      e.fontSize,
                                                      e.width
                                                    ),
                                                    width: t.getWidth(e.width),
                                                    color: e.color,
                                                    repeat: 1,
                                                    ratio: t.getRatio(e.width),
                                                    domElement: e.domElement,
                                                    alwaysAnimate: !0,
                                                    text: e.text,
                                                    rotation: e.rotation,
                                                    scaleFactor: e.scaleFactor,
                                                    centered: !0,
                                                    image: e.bulletImage,
                                                  },
                                                  on: {
                                                    loaded: t.onLoaded,
                                                  },
                                                })
                                              : t._e(),
                                          ],
                                          1
                                        ),
                                      ],
                                      1
                                    )
                                  : t._e(),
                              ],
                              1
                            )
                          : t._e();
                      }),
                      1
                    ),
                  ],
                  1
                ),
              ],
              1
            );
          },
          [],
          !1,
          null,
          "198cc955",
          null
        ).exports,
        Rt = {
          name: "about-caption",
          props: ["model"],
          mixins: [W],
        },
        zt = Object(R.a)(
          Rt,
          function () {
            var t = this,
              e = t.$createElement,
              n = t._self._c || e;
            return n(
              "waypoint",
              {
                staticClass:
                  "d-flex flex-center w-100 about-caption mt-4 mt-md-0",
                attrs: {
                  y: t.elementY,
                  debug: !0,
                },
                on: {
                  toggleOneDirectionVisible: t.toggleWaypointWithLines,
                },
              },
              [
                t.model.fields.caption
                  ? n(
                      "split-text",
                      {
                        key: t.pageWidth,
                        staticClass: "col-5 text-center",
                        on: {
                          splited: t.onSplited,
                        },
                      },
                      [
                        n("h4", {
                          staticClass: "uppercase antiqua-font",
                          domProps: {
                            innerHTML: t._s(t.model.fields.caption),
                          },
                        }),
                      ]
                    )
                  : t._e(),
              ],
              1
            );
          },
          [],
          !1,
          null,
          "23ce33b2",
          null
        ).exports,
        Ft = n(89),
        Xt = n.n(Ft),
        Bt = {
          name: "about-header",
          data: function () {
            return {
              textDelay: 0.1,
              containers: [],
              loaded: !1,
            };
          },
          props: ["model"],
          mixins: [W],
          mounted: function () {
            var t = this,
              e = this.isWebkit ? 1 : 0.65;
            (this.containers = [
              {
                text: "And a memory keeper. If the sea is anywhere near me I will feel at home. I believe in Matic. I’m potato waiting for her French fry moment in live. I’m a wild child. A fire move minutes kinda cuddler. An adventurer (a clumsy one). A lover of spontaneity.",
                domElement: this.elementId,
                fontSize: 45.8 * e,
                width: 1800 * e,
                rotation: -0.4,
                scaleFactor: 0.975,
                letterSpacing: 2.4 * e,
                color: "#ffffff",
              },
            ]),
              Xt()(this.$el, function () {
                setTimeout(function () {
                  t.loaded = !0;
                }, 600);
              });
          },
          methods: {
            image: function (t) {
              return this.getImage(
                this.images[t],
                "image",
                "?w=1920",
                "?h=1000",
                "50"
              );
            },
          },
          computed: {
            elementId: function () {
              return "wrapper-".concat(this._uid);
            },
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
            isWebkit: function () {
              return this.$store.getters["app/getState"]("isWebkit");
            },
            images: function () {
              return L()(this.model, "fields.images");
            },
          },
          components: {
            StarIcon: $t.a,
            AboutCaption: zt,
            Stage: Ht,
          },
        },
        Nt =
          (n(620),
          Object(R.a)(
            Bt,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "div",
                {
                  staticClass:
                    "about-header bg-greyv4 color-white position-relative py-3 py-md-5 d-flex flex-center flex-column",
                  attrs: {
                    "data-scroll-section": "",
                  },
                },
                [
                  n("image-div", {
                    staticClass: "h-100 w-100 image position-absolute l-0 t-0",
                    attrs: {
                      src: t.getImage(t.model, "backgroundImage", "?w=1920"),
                    },
                  }),
                  t._v(" "),
                  n(
                    "waypoint",
                    {
                      staticClass:
                        "d-flex flex-center flex-column w-100 px-2 px-md-5",
                      attrs: {
                        debug: !0,
                        y: t.elementY,
                      },
                      on: {
                        toggleOneDirectionVisible: t.toggleWaypoint,
                      },
                    },
                    [
                      n("star-icon", {
                        staticClass: "star-icon should-animate mb-3",
                        attrs: {
                          "data-fromscale": "0",
                          "data-toscale": "1",
                          "data-props": "scale",
                          "data-delay": ".1",
                        },
                      }),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass:
                            "position-relative h-100 w-100 d-flex justify-content-center align-items-end",
                        },
                        [
                          n(
                            "proportion-div",
                            {
                              staticClass: "w-100",
                              attrs: {
                                src: t.image(1),
                              },
                            },
                            [
                              n("client-only", [
                                n(
                                  "div",
                                  {
                                    staticClass:
                                      "position-absolute w-100 h-100 t-0 l-0",
                                  },
                                  [
                                    n("img", {
                                      staticClass:
                                        "position-absolute l-0 t-0 on-top oval-text hidden",
                                      attrs: {
                                        src: "../text_oval.svg",
                                        id: t.elementId,
                                        alt: "Naba Zabih",
                                      },
                                    }),
                                    t._v(" "),
                                    n("stage", {
                                      staticClass:
                                        "position-absolute t-0 l-0 no-events on-top stage",
                                      style: {
                                        opacity: t.loaded ? 1 : 0,
                                      },
                                      attrs: {
                                        index: t.pageWidth,
                                        containers: t.containers,
                                      },
                                    }),
                                  ],
                                  1
                                ),
                              ]),
                              t._v(" "),
                              n(
                                "div",
                                {
                                  staticClass:
                                    "position-absolute w-100 h-100 t-0 l-0 image-part-3",
                                },
                                [
                                  n("img", {
                                    staticClass: "w-100 img2 should-animate",
                                    attrs: {
                                      "data-preset": "y",
                                      "data-delay": ".4",
                                      "data-dur": "2",
                                      "data-fromy": -150,
                                      src: t.image(2),
                                      alt: "Naba Zabih",
                                    },
                                  }),
                                ]
                              ),
                              t._v(" "),
                              n(
                                "div",
                                {
                                  staticClass:
                                    "position-absolute w-100 h-100 l-0 image-part-2",
                                },
                                [
                                  n("img", {
                                    staticClass: "w-100 img1 should-animate",
                                    attrs: {
                                      "data-preset": "y",
                                      "data-delay": ".4",
                                      "data-dur": "2",
                                      "data-fromy": 150,
                                      src: t.image(1),
                                      alt: "Naba Zabih",
                                    },
                                  }),
                                ]
                              ),
                              t._v(" "),
                              n(
                                "div",
                                {
                                  staticClass:
                                    "position-absolute w-100 h-100 t-0 l-0 image-part-1",
                                },
                                [
                                  n("img", {
                                    staticClass: "w-100 img0 should-animate",
                                    attrs: {
                                      "data-preset": "y",
                                      "data-delay": ".4",
                                      "data-dur": "2",
                                      "data-fromy": -150,
                                      src: t.image(0),
                                      alt: "Naba Zabih",
                                    },
                                  }),
                                ]
                              ),
                            ],
                            1
                          ),
                          t._v(" "),
                          t._l(2, function (t) {
                            return n("div", {
                              staticClass:
                                "line position-absolute should-animate",
                              class: "line--" + t,
                              attrs: {
                                "data-props": "scaleY",
                                "data-fromscale-y": "0",
                                "data-toscale-y": "1",
                                "data-delay": 0.6 + t / 5,
                                "data-debug": "true",
                                "data-dur": "2",
                              },
                            });
                          }),
                          t._v(" "),
                          n(
                            "waypoint",
                            {
                              staticClass:
                                "display-1 antiqua-font position-absolute d-none d-md-block on-top lh-09 uppercase headline",
                              attrs: {
                                y: t.elementY,
                                tag: "h1",
                              },
                              on: {
                                toggleOneDirectionVisible: t.toggleWaypoint,
                              },
                            },
                            [
                              n(
                                "span",
                                {
                                  staticClass:
                                    "text-uppercase d-block should-animate",
                                  attrs: {
                                    "data-preset": "opacity,x",
                                    "data-delay": ".1",
                                    "data-fromx": "40",
                                  },
                                },
                                [t._v("This")]
                              ),
                              t._v(" "),
                              n(
                                "span",
                                {
                                  staticClass:
                                    "text-uppercase d-block big-left-margin should-animate",
                                  attrs: {
                                    "data-preset": "opacity,x",
                                    "data-delay": ".1",
                                    "data-fromx": "-40",
                                  },
                                },
                                [t._v("Is Me.")]
                              ),
                            ]
                          ),
                        ],
                        2
                      ),
                    ],
                    1
                  ),
                  t._v(" "),
                  n("about-caption", {
                    staticClass: "mt-md-6",
                    attrs: {
                      model: t.model,
                    },
                  }),
                ],
                1
              );
            },
            [],
            !1,
            null,
            "6d5b4454",
            null
          ).exports);

      function qt(object, t) {
        var e = Object.keys(object);
        if (Object.getOwnPropertySymbols) {
          var n = Object.getOwnPropertySymbols(object);
          t &&
            (n = n.filter(function (t) {
              return Object.getOwnPropertyDescriptor(object, t).enumerable;
            })),
            e.push.apply(e, n);
        }
        return e;
      }

      function Yt(t) {
        for (var i = 1; i < arguments.length; i++) {
          var source = null != arguments[i] ? arguments[i] : {};
          i % 2
            ? qt(Object(source), !0).forEach(function (e) {
                Object(o.a)(t, e, source[e]);
              })
            : Object.getOwnPropertyDescriptors
            ? Object.defineProperties(
                t,
                Object.getOwnPropertyDescriptors(source)
              )
            : qt(Object(source)).forEach(function (e) {
                Object.defineProperty(
                  t,
                  e,
                  Object.getOwnPropertyDescriptor(source, e)
                );
              });
        }
        return t;
      }
      var Vt = {
          name: "page-footer",
          props: ["model", "noPadding"],
          computed: Yt(
            Yt(
              {},
              Object(B.b)({
                getState: "pages/getState",
              })
            ),
            {},
            {
              items: function () {
                return L()(this.navigationModel, "fields.pages");
              },
              navigationModel: function () {
                return this.$store.getters["pages/getState"]("navigation");
              },
            }
          ),
        },
        Ut =
          (n(621),
          Object(R.a)(
            Vt,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "div",
                {
                  staticClass: "footer-part pb-2 pb-md-4",
                },
                [
                  n(
                    "div",
                    {
                      staticClass:
                        "d-md-flex justify-content-between mt-md-6 px-md-5 w-100",
                    },
                    [
                      n(
                        "div",
                        {
                          staticClass:
                            "col-md-4 py-2 py-md-0 d-flex flex-column d-md-none px-2 px-md-0 align-items-center",
                        },
                        t._l(t.items, function (e, r) {
                          return e.url
                            ? n(
                                "nuxt-link",
                                {
                                  key: r,
                                  staticClass:
                                    "small founders-regular py-7 color-white link nav-item",
                                  attrs: {
                                    to: e.url,
                                  },
                                },
                                [t._v(t._s(e.fields.title))]
                              )
                            : t._e();
                        }),
                        1
                      ),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass:
                            "col-md-4 py-2 py-md-0 d-none d-md-flex px-2 px-md-0",
                        },
                        t._l(t.items, function (e, r) {
                          return e.url
                            ? n(
                                "nuxt-link",
                                {
                                  key: r,
                                  staticClass:
                                    "d-none d-md-block small py-7 founders-regular color-white link nav-item",
                                  attrs: {
                                    to: e.url,
                                  },
                                },
                                [t._v(t._s(e.fields.title))]
                              )
                            : t._e();
                        }),
                        1
                      ),
                      t._v(" "),
                      t.model.fields.copywrite
                        ? n("div", {
                            staticClass:
                              "content col-md-4 text-center d-none d-md-flex align-items-center justify-content-center",
                            domProps: {
                              innerHTML: t._s(t.model.fields.copywrite),
                            },
                          })
                        : t._e(),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass:
                            "d-flex flex-column flex-md-row  align-items-center col-md-4 justify-content-between justify-content-md-end px-2 px-md-0",
                        },
                        [
                          n(
                            "a",
                            {
                              staticClass:
                                "color-white py-7 small founders-regular mr-md-3 link",
                              attrs: {
                                rel: "noopener",
                                href: t.model.fields.facebook,
                                target: "_blank",
                              },
                            },
                            [t._v("Facebook")]
                          ),
                          t._v(" "),
                          n(
                            "a",
                            {
                              staticClass:
                                "color-white py-7 small founders-regular link",
                              attrs: {
                                rel: "noopener",
                                href: t.model.fields.instagram,
                                target: "_blank",
                              },
                            },
                            [t._v("Instagram")]
                          ),
                        ]
                      ),
                    ]
                  ),
                  t._v(" "),
                  t.model.fields.copywrite
                    ? n("div", {
                        staticClass:
                          "content col-md-4 text-center mt-3 mt-md-0 d-block d-md-none",
                        domProps: {
                          innerHTML: t._s(t.model.fields.copywrite),
                        },
                      })
                    : t._e(),
                ]
              );
            },
            [],
            !1,
            null,
            "56c4ce04",
            null
          ).exports);

      function Zt(object, t) {
        var e = Object.keys(object);
        if (Object.getOwnPropertySymbols) {
          var n = Object.getOwnPropertySymbols(object);
          t &&
            (n = n.filter(function (t) {
              return Object.getOwnPropertyDescriptor(object, t).enumerable;
            })),
            e.push.apply(e, n);
        }
        return e;
      }

      function Gt(t) {
        for (var i = 1; i < arguments.length; i++) {
          var source = null != arguments[i] ? arguments[i] : {};
          i % 2
            ? Zt(Object(source), !0).forEach(function (e) {
                Object(o.a)(t, e, source[e]);
              })
            : Object.getOwnPropertyDescriptors
            ? Object.defineProperties(
                t,
                Object.getOwnPropertyDescriptors(source)
              )
            : Zt(Object(source)).forEach(function (e) {
                Object.defineProperty(
                  t,
                  e,
                  Object.getOwnPropertyDescriptor(source, e)
                );
              });
        }
        return t;
      }
      var Kt = {
          name: "page-footer",
          props: ["model", "noPadding"],
          mixins: [W, D],
          computed: Gt(
            Gt(
              {},
              Object(B.b)({
                getState: "pages/getState",
              })
            ),
            {},
            {
              bgColor: function () {
                return L()(this.model, "fields.bgColor");
              },
            }
          ),
          components: {
            FooterPart: Ut,
          },
        },
        Jt =
          (n(622),
          Object(R.a)(
            Kt,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "waypoint",
                {
                  staticClass:
                    "page-footer color-white pt-2 position-relative bg-black",
                  class: {
                    "pt-md-6": !t.noPadding,
                  },
                  attrs: {
                    "data-scroll-section": "",
                    y: t.elementY,
                  },
                  on: {
                    toggleOneDirectionVisible: t.toggleWaypointWithLines,
                  },
                },
                [
                  t.model && t.model.fields
                    ? n(
                        "div",
                        {
                          staticClass:
                            "d-flex flex-column flex-center position-relative on-top mb-2",
                        },
                        [
                          n("h3", {
                            staticClass:
                              "antiqua-font text-center mb-1 mb-md-2",
                            domProps: {
                              innerHTML: t._s(t.model.fields.headline),
                            },
                          }),
                          t._v(" "),
                          t.model.fields.content
                            ? n("div", {
                                staticClass:
                                  "content tiny text-center px-2 px-md-0 should-animate",
                                attrs: {
                                  "data-delay": ".4",
                                  "data-preset": "opacity,y",
                                  "data-fromy": "30",
                                },
                                domProps: {
                                  innerHTML: t._s(t.model.fields.content),
                                },
                              })
                            : t._e(),
                        ]
                      )
                    : t._e(),
                  t._v(" "),
                  t.model && t.model.fields
                    ? n("footer-part", {
                        staticClass: "mt-md-6",
                        attrs: {
                          model: t.model,
                        },
                      })
                    : t._e(),
                ],
                1
              );
            },
            [],
            !1,
            null,
            "6db60436",
            null
          ).exports),
        Qt = n(77),
        te = n.n(Qt),
        ee = {
          name: "text-component",
          data: function () {
            return {
              textDelay: 0.2,
            };
          },
          props: ["model"],
          mixins: [W],
          watch: {
            lines: function () {
              this.toggleWaypointWithLines({
                el: this.$el,
                visible: this.visibleWaypoint,
              });
            },
          },
          components: {
            StarIcon: $t.a,
            MemoryIcon: te.a,
          },
        },
        ie =
          (n(623),
          Object(R.a)(
            ee,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "waypoint",
                {
                  staticClass:
                    "text-component bg-whitev2 d-flex flex-column flex-center py-2 py-md-5",
                  attrs: {
                    "data-scroll-section": "",
                    debug: !0,
                    y: t.elementY,
                  },
                  on: {
                    toggleOneDirectionVisible: t.toggleWaypointWithLines,
                  },
                },
                [
                  n(
                    "div",
                    {
                      staticClass:
                        "content-wrapper col-md-9 col-xl-6 text-center w-100",
                    },
                    [
                      n("star-icon", {
                        staticClass: "star-icon mb-2 mb-md-5 should-animate",
                        attrs: {
                          "data-props": "scale",
                          "data-delay": ".1",
                          "data-fromscale": "0",
                          "data-dur": "1",
                        },
                      }),
                      t._v(" "),
                      t.model.fields.headline
                        ? n(
                            "split-text",
                            {
                              key: t.pageWidth,
                              on: {
                                splited: t.onSplited,
                              },
                            },
                            [
                              n("h2", {
                                staticClass:
                                  "antiqua-font uppercase text-center headline px-2 px-md-0 lh-13 h6",
                                domProps: {
                                  innerHTML: t._s(t.model.fields.headline),
                                },
                              }),
                            ]
                          )
                        : t._e(),
                      t._v(" "),
                      n(
                        "h3",
                        {
                          staticClass:
                            "uppercase antiqua-font mt-1 mt-md-3 should-animate h6",
                          attrs: {
                            "data-preset": "opacity,y",
                            "data-delay": ".5",
                            "data-fromy": "-40",
                            "data-dur": "1",
                          },
                        },
                        [t._v(t._s(t.$t("ill-be")))]
                      ),
                      t._v(" "),
                      n("memory-icon", {
                        staticClass: "memory-icon my-2 my-md-5 should-animate",
                        attrs: {
                          "data-props": "opacity,rotate",
                          "data-delay": ".6",
                          "data-fromrotate": "-180",
                          "data-torotate": "0",
                          "data-dur": "2",
                        },
                      }),
                      t._v(" "),
                      t.model.fields.content
                        ? n("h3", {
                            staticClass:
                              "antiqua-font text-center should-animate h5",
                            attrs: {
                              "data-preset": "opacity,y",
                              "data-delay": ".7",
                              "data-fromy": "-40",
                              "data-dur": "1",
                            },
                            domProps: {
                              innerHTML: t._s(t.model.fields.content),
                            },
                          })
                        : t._e(),
                    ],
                    1
                  ),
                ]
              );
            },
            [],
            !1,
            null,
            "66674285",
            null
          ).exports),
        ne = {
          name: "half-screen-image",
          data: function () {
            return {
              textDelay: 0.5,
            };
          },
          props: ["model"],
          mixins: [W],
          methods: {
            image: function (t) {
              return this.getImage(this.images[t], "img", "?w=1024");
            },
          },
          computed: {
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
            images: function () {
              return L()(this.model, "fields.images");
            },
          },
        },
        re =
          (n(624),
          Object(R.a)(
            ne,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "waypoint",
                {
                  staticClass: "half-screen-image d-md-flex bg-olive",
                  attrs: {
                    "data-scroll-section": "",
                    y: t.elementY,
                  },
                  on: {
                    toggleOneDirectionVisible: t.toggleWaypointWithLines,
                  },
                },
                [
                  n(
                    "div",
                    {
                      staticClass:
                        "col-md-6 d-flex flex-column flex-center justify-content-md-between pt-2 pt-md-0",
                    },
                    [
                      n("div"),
                      t._v(" "),
                      n("div", {
                        staticClass:
                          "d-flex d-md-none position-absolute t-0 l-0 bg-whitev2 white-div w-100",
                      }),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass: "col-md-3 mb-5 mb-md-0 px-4 px-md-0",
                        },
                        [
                          n(
                            "div",
                            {
                              staticClass: "overflow-hidden",
                            },
                            [
                              n("image-div", {
                                staticClass: "small-img should-animate",
                                attrs: {
                                  proportion: (t.pageWidth, 1.2),
                                  src: t.image(0),
                                  "data-props": "scale",
                                  "data-fromscale": "1.5",
                                  "data-delay": ".1",
                                  innerClasses: "transition-out-image",
                                },
                              }),
                            ],
                            1
                          ),
                        ]
                      ),
                      t._v(" "),
                      t.model.fields.content
                        ? n("split-text", {
                            key: t.pageWidth,
                            staticClass:
                              "content medium antiqua-font lh-13 text-center px-2 px-md-2 pb-2 pb-md-3 w-100",
                            domProps: {
                              innerHTML: t._s(t.model.fields.content),
                            },
                            on: {
                              splited: t.onSplited,
                            },
                          })
                        : t._e(),
                    ],
                    1
                  ),
                  t._v(" "),
                  n(
                    "div",
                    {
                      staticClass: "col-md-6 overflow-hidden position-relative",
                    },
                    [
                      n("div", {
                        staticClass:
                          "position-absolute should-animate overlay h-100 t-0 l-0 bg-olive z-1",
                        attrs: {
                          "data-fromx-percent": "0",
                          "data-tox-percent": "-100",
                          "data-delay": ".1",
                          "data-dur": "2",
                          "data-keep-props": "true",
                          "data-props": "xPercent",
                        },
                      }),
                      t._v(" "),
                      n("image-div", {
                        staticClass: "should-animate",
                        attrs: {
                          "data-fromscale": "1",
                          "data-toscale": "1.1",
                          "data-delay": ".1",
                          "data-dur": "2",
                          "data-keep-props": "true",
                          "data-props": "scale",
                          innerClasses: "transition-out-image",
                          proportion: (t.pageWidth, 1),
                          src: t.image(1),
                        },
                      }),
                    ],
                    1
                  ),
                ]
              );
            },
            [],
            !1,
            null,
            "39d8ac34",
            null
          ).exports),
        ae = {
          name: "gallery-circle-bg",
          data: function () {
            return {
              textDelay: 0.5,
              textDir: "x",
            };
          },
          props: ["model"],
          mixins: [W],
          methods: {
            image: function (t) {
              return this.getImage(this.images[t], "img", "?w=1200", "?h=1000");
            },
            item: function (t) {
              return this.headlineList && this.headlineList[t];
            },
          },
          computed: {
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
            images: function () {
              return L()(this.model, "fields.images");
            },
            headlineList: function () {
              return L()(this.model, "fields.headlineList");
            },
          },
        },
        se =
          (n(625),
          Object(R.a)(
            ae,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "waypoint",
                {
                  staticClass: "gallery-circle-bg antiqua-font bg-whitev2",
                  attrs: {
                    "data-scroll-section": "",
                    y: t.elementY,
                  },
                  on: {
                    toggleOneDirectionVisible: t.toggleWaypointWithLines,
                  },
                },
                [
                  n(
                    "div",
                    {
                      staticClass:
                        "row position-relative h-100 py-md-6 px-md-2 pl-xl-4 pr-xl-2 py-3",
                    },
                    [
                      n("div", {
                        staticClass:
                          "circle position-absolute bg-light-red should-animate",
                        attrs: {
                          "data-props": "opacity,scale",
                          "data-delay": ".1",
                          "data-fromscale": "0",
                          "data-dur": "1.6",
                        },
                      }),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass: "col-md-6 d-md-flex",
                        },
                        [
                          n(
                            "div",
                            {
                              staticClass:
                                "col-md-4 d-none d-md-flex flex-column caption-wrapper mr-0 mr-md-2 mr-xl-0",
                            },
                            [
                              n(
                                "h2",
                                {
                                  staticClass:
                                    "h1 pl-md-2 no-wrap should-animate lh-09",
                                  attrs: {
                                    "data-props": "opacity,x",
                                    "data-delay": ".1",
                                    "data-fromx": "50",
                                    "data-dur": "1",
                                  },
                                },
                                [t._v(t._s(t.$t("just-feel")))]
                              ),
                              t._v(" "),
                              n(
                                "h2",
                                {
                                  staticClass: "h1 lh-08 should-animate",
                                  attrs: {
                                    "data-props": "opacity,x",
                                    "data-delay": ".2",
                                    "data-fromx": "50",
                                    "data-dur": "1",
                                  },
                                },
                                [t._v(t._s(t.$t("just-be")))]
                              ),
                            ]
                          ),
                          t._v(" "),
                          n(
                            "div",
                            {
                              staticClass: "col-md-8 position-relative",
                            },
                            [
                              n("h2", {
                                staticClass:
                                  "display-3 pl-md-4 lh-08 color-greyv2 text-center text-md-left should-animate lh-08 mt-md-3",
                                attrs: {
                                  "data-props": "opacity,y",
                                  "data-delay": ".1",
                                  "data-fromy": "-50",
                                  "data-dur": "1",
                                },
                                domProps: {
                                  innerHTML: t._s(t.item(0)),
                                },
                              }),
                              t._v(" "),
                              n("h2", {
                                staticClass:
                                  "display-3 headline-two mb-md-1 color-greyv2 text-center text-md-left should-animate lh-09",
                                attrs: {
                                  "data-props": "opacity,y",
                                  "data-delay": ".2",
                                  "data-fromy": "-50",
                                  "data-dur": "1",
                                },
                                domProps: {
                                  innerHTML: t._s(t.item(1)),
                                },
                              }),
                              t._v(" "),
                              n(
                                "div",
                                {
                                  staticClass: "small-image-top",
                                },
                                [
                                  n("image-div", {
                                    staticClass: "should-animate",
                                    attrs: {
                                      "data-props": "x,y,opacity",
                                      "data-delay": ".1",
                                      "data-fromx": "50",
                                      "data-tox": "0",
                                      "data-fromy": "-50",
                                      "data-toy": "0",
                                      "data-dur": "1",
                                      innerClasses: "transition-out-image",
                                      proportion: t.pageWidth < 768 ? 1.3 : 1.1,
                                      src: t.image(1),
                                    },
                                  }),
                                ],
                                1
                              ),
                              t._v(" "),
                              n(
                                "div",
                                {
                                  staticClass: "row px-2 px-md-0",
                                },
                                [
                                  n(
                                    "div",
                                    {
                                      staticClass:
                                        "col-6 d-md-none d-flex justify-content-end pr-1",
                                    },
                                    [
                                      n(
                                        "h2",
                                        {
                                          staticClass:
                                            "h1 pl-md-2 no-wrap d-md-none align-self-end mobile-margin",
                                        },
                                        [t._v(t._s(t.$t("just-feel")))]
                                      ),
                                    ]
                                  ),
                                  t._v(" "),
                                  n(
                                    "div",
                                    {
                                      staticClass:
                                        "col-6 col-md-11 should-animate",
                                      attrs: {
                                        "data-preset": "opacity,y",
                                        "data-delay": ".1",
                                        "data-dur": "1",
                                      },
                                    },
                                    [
                                      n("image-div", {
                                        attrs: {
                                          innerClasses: "transition-out-image",
                                          proportion: (t.pageWidth, 1.2),
                                          src: t.image(0),
                                        },
                                      }),
                                    ],
                                    1
                                  ),
                                  t._v(" "),
                                  n(
                                    "h2",
                                    {
                                      staticClass:
                                        "h1 d-md-none text-center small-word mt-1",
                                    },
                                    [t._v(t._s(t.$t("just-be")))]
                                  ),
                                ]
                              ),
                              t._v(" "),
                              t.model.fields.content
                                ? n("div", {
                                    key: t.pageWidth,
                                    staticClass:
                                      "h1 content lh-09 d-none d-md-block",
                                    domProps: {
                                      innerHTML: t._s(t.model.fields.content),
                                    },
                                    on: {
                                      splited: t.onSplited,
                                    },
                                  })
                                : t._e(),
                              t._v(" "),
                              n(
                                "div",
                                {
                                  staticClass:
                                    "button-wrapper d-none d-md-block should-animate",
                                  attrs: {
                                    "data-preset": "opacity,y",
                                    "data-fromy": "40",
                                    "data-delay": ".65",
                                  },
                                },
                                [
                                  n(
                                    "nuxt-link",
                                    {
                                      staticClass: "d-block align-self-center",
                                      attrs: {
                                        to: "./contact",
                                      },
                                    },
                                    [
                                      n("liquid-button", {
                                        staticClass: "my-2 my-md-0 mediumv2",
                                        attrs: {
                                          black: !1,
                                          blackBg: !0,
                                          text: t.$t("come-say-hi"),
                                        },
                                      }),
                                    ],
                                    1
                                  ),
                                ],
                                1
                              ),
                            ]
                          ),
                        ]
                      ),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass: "col-md-6 d-flex",
                        },
                        [
                          n("div", {
                            staticClass: "col-md d-none d-md-block",
                          }),
                          t._v(" "),
                          n(
                            "div",
                            {
                              staticClass: "col-right align-self-center",
                            },
                            [
                              n(
                                "div",
                                {
                                  staticClass: "row",
                                },
                                [
                                  n(
                                    "div",
                                    {
                                      staticClass:
                                        "col-6 col-md-12 pr-1 pr-md-0 should-animate",
                                      attrs: {
                                        "data-props": "opacity,x",
                                        "data-delay": ".1",
                                        "data-fromx": "-80",
                                        "data-dur": "1",
                                      },
                                    },
                                    [
                                      n("image-div", {
                                        attrs: {
                                          innerClasses: "transition-out-image",
                                          proportion:
                                            t.pageWidth < 768 ? 1.3 : 1.2,
                                          src: t.image(2),
                                        },
                                      }),
                                    ],
                                    1
                                  ),
                                  t._v(" "),
                                  n(
                                    "div",
                                    {
                                      staticClass: "col-6 d-md-none d-flex",
                                    },
                                    [
                                      n(
                                        "h2",
                                        {
                                          staticClass:
                                            "h1 text-right d-block align-self-end lh-08 no-wrap mobile-margin-2",
                                        },
                                        [t._v(t._s(t.$t("More")))]
                                      ),
                                    ]
                                  ),
                                ]
                              ),
                              t._v(" "),
                              n(
                                "h2",
                                {
                                  staticClass:
                                    "h1 text-right mt-md-1 d-none d-md-block should-animate",
                                  attrs: {
                                    "data-props": "opacity,x",
                                    "data-delay": ".2",
                                    "data-fromx": "50",
                                    "data-dur": "1",
                                  },
                                },
                                [t._v(t._s(t.$t("more-than")))]
                              ),
                              t._v(" "),
                              n(
                                "h2",
                                {
                                  staticClass:
                                    "h1 text-md-right text-center lh-04 pr-md-5 mt-1 mt-md-0 mb-3 mb-md-0 d-flex align-items-center pl-3 pl-md-4 lh-08 should-animate",
                                  attrs: {
                                    "data-props": "opacity,x",
                                    "data-delay": ".3",
                                    "data-fromx": "50",
                                    "data-dur": "1",
                                  },
                                },
                                [
                                  n(
                                    "span",
                                    {
                                      staticClass: "d-block d-md-none mr-1",
                                    },
                                    [t._v(t._s(t.$t("than")))]
                                  ),
                                  t._v(" " + t._s(t.$t("snapshots"))),
                                ]
                              ),
                              t._v(" "),
                              t.model.fields.content
                                ? n("h2", {
                                    staticClass:
                                      "h1 content d-block d-md-none text-center text-md-left lh-1 mb-3",
                                    domProps: {
                                      innerHTML: t._s(t.model.fields.content),
                                    },
                                  })
                                : t._e(),
                              t._v(" "),
                              n(
                                "div",
                                {
                                  staticClass:
                                    "text-right button-wrapper d-block d-md-none px-1",
                                },
                                [
                                  n(
                                    "nuxt-link",
                                    {
                                      staticClass:
                                        "btn btn-black mediumv2 align-self-center no-wrap",
                                      attrs: {
                                        to: "",
                                      },
                                    },
                                    [
                                      n("span", [
                                        t._v(t._s(t.$t("come-say-hi"))),
                                      ]),
                                    ]
                                  ),
                                ],
                                1
                              ),
                            ]
                          ),
                        ]
                      ),
                    ]
                  ),
                ]
              );
            },
            [],
            !1,
            null,
            "822e4710",
            null
          ).exports),
        oe = {
          data: function () {
            return {
              texture: !1,
            };
          },
          components: {
            TracingContainer: At,
            Container: K,
            Sprite: Q,
          },
          mixins: [Z],
          props: {
            elementY: {
              default: 0,
            },
            model: {
              default: {},
            },
            parentOffset: {
              default: 0,
            },
          },
          methods: {
            onLoaded: function (t) {
              this.texture = t;
            },
          },
        };

      function le(object, t) {
        var e = Object.keys(object);
        if (Object.getOwnPropertySymbols) {
          var n = Object.getOwnPropertySymbols(object);
          t &&
            (n = n.filter(function (t) {
              return Object.getOwnPropertyDescriptor(object, t).enumerable;
            })),
            e.push.apply(e, n);
        }
        return e;
      }
      var ce = {
          name: "stage",
          mixins: [],
          data: function () {
            return {
              parentOffset: !1,
              filterScale: 0,
              visible: !1,
              PIXIWrapper: {
                PIXI: null,
                PIXIApp: null,
              },
            };
          },
          components: {
            PixiRenderer: Y,
            Container: K,
            DisplacementFilter: lt,
            TracingImage: Object(R.a)(
              oe,
              function () {
                var t = this,
                  e = t.$createElement,
                  n = t._self._c || e;
                return n(
                  "tracing-container",
                  {
                    attrs: {
                      fill: !0,
                      fixed: !0,
                      y: t.elementY,
                      parentOffset: t.parentOffset,
                      hasMask: !0,
                      squareMask: !0,
                      image: t.model.image,
                      domElement: t.model.domElement,
                      renderable: !0,
                    },
                    on: {
                      loaded: t.onLoaded,
                    },
                  },
                  [
                    n(
                      "container",
                      {
                        attrs: {
                          scale: 1.1,
                        },
                      },
                      [
                        t.texture
                          ? n("sprite", {
                              attrs: {
                                centered: !0,
                                imagePath: t.model.image,
                              },
                            })
                          : t._e(),
                      ],
                      1
                    ),
                  ],
                  1
                );
              },
              [],
              !1,
              null,
              null,
              null
            ).exports,
          },
          provide: function () {
            return {
              PIXIWrapper: this.PIXIWrapper,
            };
          },
          props: {
            model: !1,
            elementY: {
              default: 0,
            },
            containers: null,
          },
          mounted: function () {
            this.$bus.$on("resize", this.resizeHandler), this.resizeHandler();
          },
          beforeDestroy: function () {
            this.$bus.$off("resize", this.resizeHandler);
          },
          methods: {
            resizeHandler: function () {
              var t = this;
              setTimeout(function () {
                var e = t.$el.getBoundingClientRect();
                t.parentOffset =
                  0 != t.elementY || t.$device.isMobileOrTablet
                    ? t.scrollTop + e.top
                    : e.top;
              }, 10);
            },
            onLoaded: function () {},
            toggleFilter: function (t) {
              var e = this;
              (this.visible = t.visible),
                t.visible
                  ? (x.b.fromTo(
                      this,
                      4.5,
                      {
                        filterScale: 0.03,
                      },
                      {
                        filterScale: 0,
                        overwrite: !0,
                      }
                    ),
                    O()(this.containers, function (t, n) {
                      var r = L()(
                          e.$refs,
                          "container".concat(n, "[0].container.position")
                        ),
                        o = 0 == n ? "-300" : "300";
                      x.b.fromTo(
                        r,
                        2,
                        {
                          x: o,
                        },
                        {
                          x: 0,
                          ease: "Power4.easeOut",
                          overwrite: !0,
                        }
                      );
                    }))
                  : x.b.set(this, {
                      filterScale: 0.03,
                      overwrite: !0,
                    });
            },
          },
          computed: (function (t) {
            for (var i = 1; i < arguments.length; i++) {
              var source = null != arguments[i] ? arguments[i] : {};
              i % 2
                ? le(Object(source), !0).forEach(function (e) {
                    Object(o.a)(t, e, source[e]);
                  })
                : Object.getOwnPropertyDescriptors
                ? Object.defineProperties(
                    t,
                    Object.getOwnPropertyDescriptors(source)
                  )
                : le(Object(source)).forEach(function (e) {
                    Object.defineProperty(
                      t,
                      e,
                      Object.getOwnPropertyDescriptor(source, e)
                    );
                  });
            }
            return t;
          })({}, Object(B.c)("app", ["width", "fontsLoaded", "scrollTop"])),
        },
        de = {
          name: "gallery-and-text",
          data: function () {
            return {
              containers: [],
            };
          },
          props: ["model"],
          mixins: [W],
          mounted: function () {
            this.containers = [
              {
                image: this.image(0),
                domElement: "image-0",
              },
              {
                image: this.image(3),
                domElement: "image-3",
              },
            ];
          },
          methods: {
            image: function (t) {
              return this.getImage(this.images[t], "img", "?w=1024");
            },
          },
          computed: {
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
            images: function () {
              return L()(this.model, "fields.images");
            },
          },
          components: {
            ImagesStage: Object(R.a)(
              ce,
              function () {
                var t = this,
                  e = t.$createElement,
                  n = t._self._c || e;
                return n(
                  "waypoint",
                  {
                    staticClass: "stage",
                    attrs: {
                      y: t.elementY,
                    },
                    on: {
                      toggleOneDirectionVisible: t.toggleFilter,
                    },
                  },
                  [
                    n(
                      "client-only",
                      [
                        n(
                          "pixi-renderer",
                          {
                            attrs: {
                              resolution: 1,
                            },
                          },
                          [
                            n(
                              "container",
                              [
                                n("displacement-filter", {
                                  ref: "filter",
                                  attrs: {
                                    filterScale: t.filterScale,
                                    scale: 9,
                                  },
                                }),
                                t._v(" "),
                                t._l(t.containers, function (e, r) {
                                  return t.parentOffset
                                    ? n(
                                        "container",
                                        {
                                          key: r,
                                          ref: "container" + r,
                                          refInFor: !0,
                                        },
                                        [
                                          n("tracing-image", {
                                            attrs: {
                                              elementY: t.elementY,
                                              parentOffset: t.parentOffset,
                                              model: e,
                                            },
                                          }),
                                        ],
                                        1
                                      )
                                    : t._e();
                                }),
                              ],
                              2
                            ),
                          ],
                          1
                        ),
                      ],
                      1
                    ),
                  ],
                  1
                );
              },
              [],
              !1,
              null,
              null,
              null
            ).exports,
          },
        },
        ue =
          (n(626),
          Object(R.a)(
            de,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "waypoint",
                {
                  staticClass:
                    "gallery-and-text d-md-flex position-relative bg-olivev2",
                  attrs: {
                    "data-scroll-section": "",
                    y: t.elementY,
                  },
                  on: {
                    toggleOneDirectionVisible: t.toggleWaypoint,
                  },
                },
                [
                  n("div", {
                    staticClass:
                      "bg-brown position-absolute t-0 l-0 w-100 d-block d-md-none py-3",
                  }),
                  t._v(" "),
                  t.$device.isMobile
                    ? t._e()
                    : n("images-stage", {
                        staticClass:
                          "position-absolute w-100 h-100 t-0 l-0 no-events d-none d-md-block",
                        attrs: {
                          elementY: t.elementY,
                          containers: t.containers,
                        },
                      }),
                  t._v(" "),
                  n(
                    "div",
                    {
                      staticClass: "col-left position-relative",
                    },
                    [
                      n(
                        "div",
                        {
                          staticClass:
                            "small-image-top px-4 px-md-0 should-animate",
                          attrs: {
                            "data-preset": "opacity,y",
                            "data-delay": ".1",
                            "data-not-instant": "true",
                            "data-fromy": "-50",
                          },
                        },
                        [
                          n("image-div", {
                            attrs: {
                              innerClasses: "transition-out-image",
                              proportion: 1.2,
                              src: t.image(1),
                            },
                          }),
                        ],
                        1
                      ),
                      t._v(" "),
                      n("proportion-div", {
                        ref: "image0",
                        staticClass: "d-none d-md-block hidden",
                        attrs: {
                          id: "image-0",
                          innerClasses: "transition-out-image",
                          proportion: t.pageWidth < 768 ? 1 : 1.35,
                        },
                      }),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass:
                            "position-absolute small-image-bottom should-animate d-none d-md-block",
                          attrs: {
                            "data-preset": "opacity,y",
                            "data-fromy": "-80",
                            "data-delay": ".4",
                          },
                        },
                        [
                          n("image-div", {
                            attrs: {
                              innerClasses: "transition-out-image",
                              proportion: t.pageWidth < 768 ? 1 : 1.2,
                              src: t.image(2),
                            },
                          }),
                        ],
                        1
                      ),
                    ],
                    1
                  ),
                  t._v(" "),
                  n(
                    "div",
                    {
                      staticClass: "col-middle pt-2 pt-xl-3",
                    },
                    [
                      t.model.fields.headline
                        ? n("h2", {
                            staticClass:
                              "display-4 antiqua-font text-center lh-1",
                            domProps: {
                              innerHTML: t._s(t.model.fields.headline),
                            },
                          })
                        : t._e(),
                      t._v(" "),
                      n(
                        "h3",
                        {
                          staticClass:
                            "medium antiqua-font uppercase text-center mb-2 mb-md-0 h6",
                        },
                        [t._v(t._s(t.$t("photo")))]
                      ),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass:
                            "mb-4 my-md-4 text-center d-none d-md-block",
                        },
                        [
                          n("div", {
                            staticClass: "vertical-line",
                          }),
                        ]
                      ),
                      t._v(" "),
                      t.model.fields.content
                        ? n("div", {
                            staticClass:
                              "text-center px-1 pb-3 pb-md-0 px-md-4",
                            domProps: {
                              innerHTML: t._s(t.model.fields.content),
                            },
                          })
                        : t._e(),
                    ]
                  ),
                  t._v(" "),
                  n(
                    "div",
                    {
                      staticClass: "col-md",
                    },
                    [
                      n("proportion-div", {
                        staticClass: "d-none d-md-block hidden",
                        attrs: {
                          id: "image-3",
                          proportion: t.pageWidth < 768 ? 1 : 1.2,
                        },
                      }),
                      t._v(" "),
                      n("image-div", {
                        staticClass: "d-block d-md-none",
                        attrs: {
                          proportion: t.pageWidth < 768 ? 1 : 1.3,
                          src: t.image(0),
                          innerClasses: "transition-out-image",
                        },
                      }),
                    ],
                    1
                  ),
                ],
                1
              );
            },
            [],
            !1,
            null,
            "36fa3233",
            null
          ).exports),
        pe = {
          name: "list-and-image",
          props: ["model"],
          mixins: [W],
          methods: {
            image: function (t) {
              return this.getImage(this.images[t], "img", "?w=1024");
            },
          },
          computed: {
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
            images: function () {
              return L()(this.model, "fields.images");
            },
            list: function () {
              return L()(this.model, "fields.list");
            },
          },
        },
        he =
          (n(627),
          Object(R.a)(
            pe,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "waypoint",
                {
                  staticClass: "list-and-image d-md-flex bg-brown",
                  attrs: {
                    "data-scroll-section": "",
                    y: t.elementY,
                  },
                  on: {
                    toggleOneDirectionVisible: t.toggleWaypoint,
                  },
                },
                [
                  n(
                    "div",
                    {
                      staticClass: "col-md color-white pt-md-3",
                    },
                    [
                      n(
                        "div",
                        {
                          staticClass: "px-3 px-md-6 text-center text-md-left",
                        },
                        [
                          t.model.fields.headline
                            ? n("h2", {
                                staticClass:
                                  "display-4 antiqua-font lh-09 pr-xl-3 mb-md-2",
                                domProps: {
                                  innerHTML: t._s(t.model.fields.headline),
                                },
                              })
                            : t._e(),
                          t._v(" "),
                          t.model.fields.content
                            ? n("h3", {
                                staticClass:
                                  "antiqua-font lh-1 mt-2 mt-md-0 mb-3 pr-md-3 h6",
                                domProps: {
                                  innerHTML: t._s(t.model.fields.content),
                                },
                              })
                            : t._e(),
                        ]
                      ),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass: "list my-md-5",
                        },
                        t._l(t.list, function (e, r) {
                          return n(
                            "div",
                            {
                              key: r,
                              staticClass:
                                "px-3 px-md-0 uppercase d-md-flex align-items-center justify-content-center justify-content-md-start text-center text-md-left item",
                            },
                            [
                              n(
                                "span",
                                {
                                  staticClass:
                                    "mr-md-2 d-flex d-md-block justify-content-center",
                                },
                                [t._v("0" + t._s(parseInt(r) + 1) + ".")]
                              ),
                              t._v(" "),
                              n(
                                "span",
                                {
                                  staticClass: "mediumv2 text-left",
                                },
                                [t._v(t._s(e))]
                              ),
                            ]
                          );
                        }),
                        0
                      ),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass:
                            "px-1 pl-md-6 pr-xl-6 d-md-flex text-center",
                        },
                        [
                          n(
                            "nuxt-link",
                            {
                              attrs: {
                                to: "../contact",
                              },
                            },
                            [
                              n("liquid-button", {
                                staticClass: "my-2 my-md-0",
                                attrs: {
                                  black: !0,
                                  text: t.$t("contact-us"),
                                },
                              }),
                            ],
                            1
                          ),
                          t._v(" "),
                          n(
                            "div",
                            {
                              staticClass:
                                "content px-3 pl-md-2 pr-md-1 align-self-center text-center text-md-left",
                            },
                            [t._v(t._s(t.$t("contact-us-to-get")))]
                          ),
                        ],
                        1
                      ),
                    ]
                  ),
                  t._v(" "),
                  n(
                    "div",
                    {
                      staticClass: "col-right should-animate",
                      attrs: {
                        "data-props": "scaleX,x",
                        "data-fromx": "100",
                        "data-tox": "0",
                        "data-dur": "1.6",
                        "data-delay": ".1",
                        "data-toscale-x": "1",
                        "data-fromscale-x": "0.85",
                      },
                    },
                    [
                      n("image-div", {
                        staticClass: "d-none d-md-block",
                        attrs: {
                          innerClasses: "transition-out-image",
                          proportion: t.pageWidth < 768 ? 1 : 1.4,
                          src: t.image(0),
                        },
                      }),
                    ],
                    1
                  ),
                ]
              );
            },
            [],
            !1,
            null,
            "2cad7814",
            null
          ).exports),
        fe = {
          name: "two-image-and-text",
          props: ["model"],
          mixins: [W],
          methods: {
            image: function (t) {
              return this.getImage(this.images[t], "image", "?w=980");
            },
          },
          computed: {
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
            images: function () {
              return L()(this.model, "fields.images");
            },
          },
          components: {
            StarIcon: $t.a,
          },
        },
        me =
          (n(628),
          Object(R.a)(
            fe,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "waypoint",
                {
                  staticClass:
                    "two-image-and-text bg-black color-white text-center pt-3 pt-md-0 pb-md-5 px-1 px-md-0 pb-3",
                  attrs: {
                    "data-scroll-section": "",
                  },
                  on: {
                    toggleOneDirectionVisible: t.toggleWaypoint,
                  },
                },
                [
                  n(
                    "div",
                    {
                      staticClass: "d-md-flex",
                    },
                    [
                      n(
                        "div",
                        {
                          staticClass:
                            "col-md-6 d-flex justify-content-md-end align-items-xl-end pr-md-5",
                        },
                        [
                          n(
                            "div",
                            {
                              staticClass:
                                "col-xl-7 px-1 px-md-3 px-xl-0 should-animate",
                              attrs: {
                                "data-fromrotate": "-10",
                                "data-torotate": "0",
                                "data-dur": "1",
                                "data-delay": ".2",
                                "data-fromy": "100",
                                "data-props": "opacity,y,rotate",
                              },
                            },
                            [
                              n("image-div", {
                                attrs: {
                                  proportion: (t.pageWidth, 1),
                                  src: t.image(0),
                                },
                              }),
                            ],
                            1
                          ),
                          t._v(" "),
                          n(
                            "div",
                            {
                              staticClass:
                                "col-md-9 col-xl-5 position-absolute px-2 px-md-3 px-xl-0 should-animate",
                              attrs: {
                                "data-fromrotate": "10",
                                "data-torotate": "0",
                                "data-dur": "1",
                                "data-delay": ".1  ",
                                "data-fromy": "100",
                                "data-props": "opacity,y,rotate",
                              },
                            },
                            [
                              n("image-div", {
                                attrs: {
                                  proportion: (t.pageWidth, 1),
                                  src: t.image(1),
                                },
                              }),
                            ],
                            1
                          ),
                        ]
                      ),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass: "col-md-6",
                        },
                        [
                          n(
                            "div",
                            {
                              staticClass:
                                "col-md-8 col-xl-7 col-xl-5 d-flex flex-column mt-3 mt-md-0",
                            },
                            [
                              n("star-icon", {
                                staticClass:
                                  "star-icon align-self-md-start align-self-center mb-2 should-animate",
                                attrs: {
                                  "data-fromscale": "0",
                                  "data-toscale": "1",
                                  "data-props": "scale",
                                  "data-delay": ".1",
                                },
                              }),
                              t._v(" "),
                              t.model.fields.content
                                ? n("div", {
                                    staticClass:
                                      "content text-center text-md-left content px-2 px-md-0 mb-2 mb-md-3 pr-xlg-3 should-animate",
                                    attrs: {
                                      "data-preset": "opacity,y",
                                      "data-delay": ".2",
                                    },
                                    domProps: {
                                      innerHTML: t._s(t.model.fields.content),
                                    },
                                  })
                                : t._e(),
                              t._v(" "),
                              n(
                                "div",
                                {
                                  staticClass:
                                    "d-flex justify-content-md-start justify-content-center should-animate",
                                  attrs: {
                                    "data-preset": "opacity,y",
                                    "data-delay": ".3",
                                  },
                                },
                                [
                                  n(
                                    "nuxt-link",
                                    {
                                      staticClass:
                                        "link-line uppercase reversed",
                                      attrs: {
                                        to: "../contact",
                                      },
                                    },
                                    [t._v(t._s(t.$t("wanna-be")))]
                                  ),
                                ],
                                1
                              ),
                            ],
                            1
                          ),
                        ]
                      ),
                    ]
                  ),
                ]
              );
            },
            [],
            !1,
            null,
            "1be493fa",
            null
          ).exports),
        ge = {
          mixins: [Z],
          components: {
            Container: K,
          },
          props: ["fill", "fixed", "sprite", "image", "hasMask"],
          data: function () {
            return {
              container: null,
              containerPosition: {
                x: 0,
                y: 0,
              },
            };
          },
          watch: {
            containerPosition: function (t) {
              if (mt()(this.container))
                try {
                  vt()(this.container.position, t);
                } catch (t) {
                  console.log("container resize error:", t);
                }
            },
          },
          mounted: function () {
            this.$bus.$on("resize", this.resizeHandler), this.resizeContainer();
          },
          beforeDestroy: function () {
            this.$bus.$off("resize", this.resizeHandler);
          },
          methods: {
            resizeContainer: function () {
              if (this.container)
                try {
                  this.containerPosition = {
                    x:
                      (this.PIXIWrapper.PIXIApp.width - this.container.width) /
                      2,
                    y: this.PIXIWrapper.PIXIApp.height - this.container.height,
                  };
                } catch (t) {
                  console.log("container resize error:", t);
                }
            },
            resizeHandler: function () {
              var t = this;
              this.resizeContainer(),
                setTimeout(function () {
                  t.resizeContainer();
                }, 20);
            },
          },
          computed: {
            pageHeight: function () {
              return this.$store.getters["app/getState"]("height");
            },
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
          },
        },
        ve = {
          inject: ["PIXIWrapper"],
          props: {
            text: {
              default: "Nothing short of\ncosmic.",
            },
            mobileText: {
              default: "I hug pretty\nfreaking hard.",
            },
          },
          data: function () {
            return {};
          },
          provide: function () {
            return {
              PIXIWrapper: this.PIXIWrapper,
            };
          },
          render: function (t) {
            return t("template", this.$slots.default);
          },
          computed: {
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
          },
          methods: {
            resizeHandler: function () {
              if (this.textSprite) {
                this.textSprite.text =
                  this.pageWidth > 768 ? this.text : this.mobileText;
                var t = this.pageWidth / 8;
                (this.textSprite.style.fontSize = t),
                  (this.textSprite.style.lineHeight = 1 * t);
              }
            },
            initText: function () {
              var text = this.pageWidth > 768 ? this.text : this.mobileText,
                t = this.pageWidth / 8,
                style = new PIXI.TextStyle({
                  fontFamily: "AntiquaRoman",
                  fontSize: t,
                  fill: "#ffffff",
                  align: "center",
                  lineHeight: 1 * t,
                });
              return (
                (this.textSprite = new PIXI.Text(text, style)),
                (this.textSprite.resolution = 2),
                this.resizeHandler(),
                this.$parent.container
                  ? this.$parent.container.addChild(this.textSprite)
                  : this.PIXIWrapper.PIXIApp.stage.addChild(this.textSprite),
                this.textSprite
              );
            },
          },
          beforeDestroy: function () {
            this.$bus.$off("resize", this.resizeHandler);
          },
          created: function () {
            var t = this;
            this.$bus.$on("resize", this.resizeHandler),
              this.PIXIWrapper.PIXIApp
                ? this.initText()
                : this.$bus.$on("ready", function () {
                    t.$bus.$off("ready"), t.initText();
                  });
          },
        };

      function be(object, t) {
        var e = Object.keys(object);
        if (Object.getOwnPropertySymbols) {
          var n = Object.getOwnPropertySymbols(object);
          t &&
            (n = n.filter(function (t) {
              return Object.getOwnPropertyDescriptor(object, t).enumerable;
            })),
            e.push.apply(e, n);
        }
        return e;
      }
      var ye = {
          name: "stage",
          data: function () {
            return {
              filterScale: 0,
              visible: !1,
              PIXIWrapper: {
                PIXI: null,
                PIXIApp: null,
              },
            };
          },
          components: {
            PixiRenderer: Y,
            PositionContainer: Object(R.a)(
              ge,
              function () {
                var t = this.$createElement;
                return (this._self._c || t)(
                  "container",
                  {
                    key: this.image,
                    ref: "wrapper",
                  },
                  [this._t("default")],
                  2
                );
              },
              [],
              !1,
              null,
              null,
              null
            ).exports,
            PixiText: Object(R.a)(ve, void 0, void 0, !1, null, null, null)
              .exports,
            DisplacementFilter: lt,
          },
          provide: function () {
            return {
              PIXIWrapper: this.PIXIWrapper,
            };
          },
          props: ["model", "containers", "text"],
          mixins: [],
          watch: {
            containers: function () {},
          },
          mounted: function () {},
          methods: {
            toggleFilter: function (t) {
              (this.visible = t.visible),
                t.visible
                  ? (x.b.fromTo(
                      this,
                      4.5,
                      {
                        filterScale: 0.08,
                      },
                      {
                        filterScale: 0,
                        overwrite: !0,
                      }
                    ),
                    x.b.to(this.$el, 0.65, {
                      opacity: 1,
                      y: 0,
                      overwrite: !0,
                    }))
                  : (x.b.set(this.$el, {
                      opacity: 0,
                      y: 40,
                      overwrite: !0,
                    }),
                    x.b.set(this, {
                      filterScale: 0.03,
                      overwrite: !0,
                    }));
            },
          },
          computed: (function (t) {
            for (var i = 1; i < arguments.length; i++) {
              var source = null != arguments[i] ? arguments[i] : {};
              i % 2
                ? be(Object(source), !0).forEach(function (e) {
                    Object(o.a)(t, e, source[e]);
                  })
                : Object.getOwnPropertyDescriptors
                ? Object.defineProperties(
                    t,
                    Object.getOwnPropertyDescriptors(source)
                  )
                : be(Object(source)).forEach(function (e) {
                    Object.defineProperty(
                      t,
                      e,
                      Object.getOwnPropertyDescriptor(source, e)
                    );
                  });
            }
            return t;
          })({}, Object(B.c)("app", ["width", "fontsLoaded"])),
        },
        we = {
          name: "fullwidth-image",
          data: function () {
            return {
              textDelay: 0.4,
            };
          },
          props: ["model"],
          mixins: [W],
          watch: {},
          mounted: function () {
            console.log("full width mounted");
          },
          methods: {
            image: function (t) {
              return this.getImage(
                this.images[t],
                "img",
                "?w=1920",
                "?h=735&w=375&fit=fill"
              );
            },
          },
          computed: {
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
            images: function () {
              return L()(this.model, "fields.images");
            },
          },
          components: {
            LettersStage: Object(R.a)(
              ye,
              function () {
                var t = this.$createElement,
                  e = this._self._c || t;
                return e(
                  "waypoint",
                  {
                    staticClass: "stage",
                    on: {
                      toggleOneDirectionVisible: this.toggleFilter,
                    },
                  },
                  [
                    e(
                      "client-only",
                      [
                        e(
                          "pixi-renderer",
                          [
                            this.fontsLoaded
                              ? e(
                                  "position-container",
                                  {
                                    attrs: {
                                      renderable: this.visible,
                                    },
                                  },
                                  [
                                    e("displacement-filter", {
                                      ref: "filter",
                                      attrs: {
                                        filterScale: this.filterScale,
                                        scale: 10,
                                      },
                                    }),
                                    this._v(" "),
                                    e("pixi-text"),
                                  ],
                                  1
                                )
                              : this._e(),
                          ],
                          1
                        ),
                      ],
                      1
                    ),
                  ],
                  1
                );
              },
              [],
              !1,
              null,
              null,
              null
            ).exports,
          },
        },
        xe =
          (n(629),
          Object(R.a)(
            we,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "waypoint",
                {
                  staticClass:
                    "fullwidth-image position-relative d-flex align-items-end",
                  attrs: {
                    "data-scroll-section": "",
                    y: t.elementY,
                  },
                  on: {
                    toggleOneDirectionVisible: t.toggleWaypoint,
                  },
                },
                [
                  n(
                    "custom-parallax",
                    {
                      attrs: {
                        y: t.elementY,
                        speedFactor: 0.1,
                        scaleFactor: 0.2,
                        wrapped: !0,
                        global: !0,
                        absolute: !0,
                      },
                    },
                    [
                      n("image-div", {
                        staticClass: "h-100 w-100",
                        attrs: {
                          innerClasses: "transition-out-image",
                          src: t.image(0),
                        },
                      }),
                    ],
                    1
                  ),
                  t._v(" "),
                  t.$device.isMobile
                    ? t._e()
                    : n("letters-stage", {
                        staticClass:
                          "position-absolute w-100 on-top b-0 l-0 stage mb-2 mb-md-0",
                        attrs: {
                          text: t.stripHtml(t.model.fields.headline),
                        },
                      }),
                  t._v(" "),
                  t.model.fields.headline && t.$device.isMobile
                    ? n("h2", {
                        staticClass:
                          "color-white antiqua-font position-relative on-top display-1 text-center px-3 px-md-0 pb-1 pb-md-0 w-100",
                        attrs: {
                          tag: "h2",
                        },
                        domProps: {
                          innerHTML: t._s(t.model.fields.headline),
                        },
                      })
                    : t._e(),
                ],
                1
              );
            },
            [],
            !1,
            null,
            "4e7b9580",
            null
          ).exports),
        Ce = {
          name: "center-image",
          props: ["model"],
          methods: {
            image: function (t, e) {
              return this.getImage(this.images[t], "img", "?w=1024");
            },
          },
          computed: {
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
            images: function () {
              return L()(this.model, "fields.images");
            },
          },
          components: {},
        },
        _e =
          (n(630),
          Object(R.a)(
            Ce,
            function () {
              var t = this.$createElement,
                e = this._self._c || t;
              return e(
                "div",
                {
                  staticClass:
                    "center-image pt-5 pb-6 d-flex flex-center position-relative",
                  attrs: {
                    "data-scroll-section": "",
                  },
                },
                [
                  e(
                    "div",
                    {
                      staticClass: "position-absolute t-0 l-0 w-100 h-100",
                    },
                    [
                      e("image-div", {
                        staticClass: "h-100",
                        attrs: {
                          src: this.getImage(
                            this.images[0],
                            "img",
                            "?w=1920",
                            "?w=640",
                            99
                          ),
                        },
                      }),
                    ],
                    1
                  ),
                  this._v(" "),
                  e(
                    "div",
                    {
                      staticClass: "px-3 px-md-0 col-md-2",
                    },
                    [
                      e("image-div", {
                        attrs: {
                          innerClasses: "transition-out-image",
                          proportion: (this.pageWidth, 1.2),
                          src: this.image(1),
                        },
                      }),
                    ],
                    1
                  ),
                ]
              );
            },
            [],
            !1,
            null,
            "dca84fe6",
            null
          ).exports),
        Oe = n(424),
        Pe = n.n(Oe);

      function Le(object, t) {
        var e = Object.keys(object);
        if (Object.getOwnPropertySymbols) {
          var n = Object.getOwnPropertySymbols(object);
          t &&
            (n = n.filter(function (t) {
              return Object.getOwnPropertyDescriptor(object, t).enumerable;
            })),
            e.push.apply(e, n);
        }
        return e;
      }
      var ke = {
          name: "elipse-image",
          data: function () {
            return {
              containers: [],
              loaded: !1,
            };
          },
          props: ["model"],
          mixins: [D],
          mounted: function () {
            var t = this,
              e = this.isWebkit ? 1 : 0.65;
            setTimeout(function () {
              t.loaded = !0;
            }, 4800),
              (this.containers = [
                {
                  text: "Let's become real friends on instagram",
                  domElement: this.elementId,
                  fontSize: 164 * e,
                  width: 1200 * e,
                  letterSpacing: 12 * e,
                  color: "#000000",
                  bulletImage: "../star.png",
                },
                {
                  image: this.image(0),
                  hasMask: !0,
                  hasFilter: !0,
                  domElement: this.imageId,
                },
              ]);
          },
          methods: {
            image: function (t) {
              return this.getImage(this.images[t], "img", "?w=1024");
            },
          },
          computed: (function (t) {
            for (var i = 1; i < arguments.length; i++) {
              var source = null != arguments[i] ? arguments[i] : {};
              i % 2
                ? Le(Object(source), !0).forEach(function (e) {
                    Object(o.a)(t, e, source[e]);
                  })
                : Object.getOwnPropertyDescriptors
                ? Object.defineProperties(
                    t,
                    Object.getOwnPropertyDescriptors(source)
                  )
                : Le(Object(source)).forEach(function (e) {
                    Object.defineProperty(
                      t,
                      e,
                      Object.getOwnPropertyDescriptor(source, e)
                    );
                  });
            }
            return t;
          })(
            {
              elementId: function () {
                return "wrapper-".concat(this._uid);
              },
              isWebkit: function () {
                return this.$store.getters["app/getState"]("isWebkit");
              },
              imageId: function () {
                return "image-".concat(this._uid);
              },
              pageWidth: function () {
                return this.$store.getters["app/getState"]("width");
              },
              images: function () {
                return L()(this.model, "fields.images");
              },
              footerModel: function () {
                return this.$store.getters["pages/getState"]("footer");
              },
            },
            Object(B.b)({
              getState: "pages/getState",
            })
          ),
          components: {
            ElipseText: Pe.a,
            Stage: Ht,
          },
        },
        Se =
          (n(631),
          Object(R.a)(
            ke,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "div",
                {
                  staticClass:
                    "elipse-image bg-olivev2 d-flex flex-center flex-column py-2 py-md-6",
                  attrs: {
                    "data-scroll-section": "",
                  },
                },
                [
                  n("client-only", [
                    n(
                      "div",
                      {
                        staticClass: "col-11 col-md-6 img position-relative",
                      },
                      [
                        t.loaded
                          ? n("stage", {
                              staticClass:
                                "position-absolute w-100 h-100 t-0 l-0 no-events ",
                              attrs: {
                                elementY: t.elementY,
                                containers: t.containers,
                              },
                            })
                          : t._e(),
                        t._v(" "),
                        n("elipse-text", {
                          staticClass: "elipse-text opacity-05 hidden",
                          attrs: {
                            id: t.elementId,
                          },
                        }),
                        t._v(" "),
                        n("img", {
                          staticClass:
                            "center elipse-img position-absolute hidden",
                          attrs: {
                            id: t.imageId,
                            src: t.image(0),
                          },
                        }),
                      ],
                      1
                    ),
                  ]),
                  t._v(" "),
                  n(
                    "a",
                    {
                      staticClass:
                        "medium link mobile-font antiqua-font text-center mt-md-4 mt-2",
                      attrs: {
                        href: t.footerModel.fields.instagram,
                        target: "_blank",
                        rel: "noopener",
                      },
                    },
                    [t._v(t._s(t.$t("@nabazabihphotography")))]
                  ),
                ],
                1
              );
            },
            [],
            !1,
            null,
            "80ed8178",
            null
          ).exports),
        Ie = n(425),
        je = n.n(Ie),
        Te = {
          name: "mailchimp",
          data: function () {
            return {
              subscribeEmail: null,
              mailChimpSubmitted: !1,
              sent: !1,
              errorMsg: null,
            };
          },
          props: ["model", "mailChimp", "name"],
          mixins: [],
          mounted: function () {},
          methods: {
            emailCheck: function () {
              /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(
                this.subscribeEmail.toLowerCase()
              )
                ? (this.errorMsg = "")
                : (this.errorMsg = this.subscribeEmail
                    ? this.$t("error-msg")
                    : "");
            },
            submitMailchimp: function () {
              var t = this;
              je()(
                this.mailchimpUrl2,
                {
                  param: "c",
                },
                function (e, data) {
                  if ("success" == data.result)
                    return (
                      (t.sent = !0),
                      (t.errorMsg = ""),
                      (t.subscribeEmail = ""),
                      void setTimeout(function () {
                        (t.subscribeEmail = ""), (t.sent = !1);
                      }, 5e3)
                    );
                  X()(data.msg, "subscribed") || X()(data.msg, "has too many")
                    ? (t.errorMsg = t.$t("already-subscribed"))
                    : (t.errorMsg = t.$t("error-msg"));
                }
              );
            },
          },
          computed: {
            mailchimpUrl: function () {
              return this.model.fields.mailchimpUrl || "";
            },
            mailchimpUrl2: function () {
              return (
                "https://yahoo.us20.list-manage.com/subscribe/post-json?u=7759ade544808e2ccbb601243&id=ce7de71874&MERGE0=" +
                this.subscribeEmail
              );
            },
          },
        },
        $e =
          (n(635),
          {
            name: "newsletter",
            props: ["model"],
            mixins: [W],
            methods: {
              item: function (t) {
                return this.headlineList && this.headlineList[t];
              },
            },
            computed: {
              headlineList: function () {
                return L()(this.model, "fields.headlineList");
              },
            },
            components: {
              Mailchimp: Object(R.a)(
                Te,
                function () {
                  var t = this,
                    e = t.$createElement,
                    n = t._self._c || e;
                  return n(
                    "div",
                    {
                      staticClass: "mailchimp w-100 position-relative",
                    },
                    [
                      n(
                        "div",
                        {
                          staticClass: "newsletter-input",
                          attrs: {
                            id: "mc_embed_signup",
                          },
                        },
                        [
                          n(
                            "form",
                            {
                              ref: "form",
                              staticClass:
                                "validate subscr-form position-relative d-md-flex",
                              class: {
                                "fade-out": t.sent,
                              },
                              attrs: {
                                action: t.mailchimpUrl2,
                                method: "get",
                                id: "mc-embedded-subscribe-form" + t.name,
                                name: "mc-embedded-subscribe-form" + t.name,
                                target: "_blank",
                                rel: "noopener",
                                novalidate: "",
                              },
                              on: {
                                submit: function (e) {
                                  return (
                                    e.preventDefault(), t.submitMailchimp()
                                  );
                                },
                              },
                            },
                            [
                              n(
                                "div",
                                {
                                  staticClass:
                                    "input-holder mb-0 uppercase w-100 d-flex flex-column",
                                },
                                [
                                  n("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: t.subscribeEmail,
                                        expression: "subscribeEmail",
                                      },
                                    ],
                                    ref: "inputValue",
                                    staticClass:
                                      "w-100 text-center mb-2 founders-regular",
                                    attrs: {
                                      type: "text",
                                      placeholder: "Email",
                                      name: "MERGE0",
                                      id: "MERGE0",
                                      required: "",
                                    },
                                    domProps: {
                                      value: t.subscribeEmail,
                                    },
                                    on: {
                                      input: [
                                        function (e) {
                                          e.target.composing ||
                                            (t.subscribeEmail = e.target.value);
                                        },
                                        t.emailCheck,
                                      ],
                                      focus: function (e) {
                                        !t.subscribeEmail &&
                                          (t.subscribeEmail = "");
                                      },
                                      focusout: function (e) {
                                        !t.subscribeEmail &&
                                          (t.subscribeEmail = null);
                                      },
                                    },
                                  }),
                                  t._v(" "),
                                  n(
                                    "label",
                                    {
                                      staticClass: "position-absolute indented",
                                      attrs: {
                                        for: "MERGE0",
                                      },
                                    },
                                    [t._v("Email")]
                                  ),
                                  t._v(" "),
                                  n("liquid-button", {
                                    attrs: {
                                      type: "submit",
                                      text: t.$t("submit"),
                                    },
                                  }),
                                ],
                                1
                              ),
                            ]
                          ),
                          t._v(" "),
                          n(
                            "div",
                            {
                              staticClass:
                                "clear mailchimp-error small position-absolute b-100 mt-small text-center center-x",
                              attrs: {
                                id: "mce-responses",
                              },
                            },
                            [
                              n(
                                "p",
                                {
                                  staticClass:
                                    "response founders-regular tiny text-center no-wrap",
                                  class: {
                                    "fade-out": !t.errorMsg,
                                  },
                                  attrs: {
                                    id: "mce-error-response",
                                  },
                                },
                                [t._v(t._s(t.errorMsg) + "  ")]
                              ),
                            ]
                          ),
                          t._v(" "),
                          n(
                            "div",
                            {
                              staticClass: "thank-you h5 lh-12 antiqua-font",
                              class: {
                                "fade-out": !t.sent,
                              },
                            },
                            [t._v(t._s(t.$t("thanks-message")))]
                          ),
                        ]
                      ),
                    ]
                  );
                },
                [],
                !1,
                null,
                "41ab38ea",
                null
              ).exports,
            },
          }),
        Ee =
          (n(636),
          Object(R.a)(
            $e,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "waypoint",
                {
                  staticClass: "newsletter position-relative py-3",
                  attrs: {
                    "data-scroll-section": "",
                    y: t.elementY,
                  },
                  on: {
                    toggleOneDirectionVisible: t.toggleWaypoint,
                  },
                },
                [
                  n(
                    "custom-parallax",
                    {
                      attrs: {
                        speedFactor: 0.2,
                        scaleFactor: 0.2,
                        wrapped: !0,
                        y: t.elementY,
                        global: !0,
                        absolute: !0,
                      },
                    },
                    [
                      n("image-div", {
                        staticClass:
                          "h-100 w-100 image position-absolute l-0 t-0",
                        attrs: {
                          innerClasses: "transition-out-image",
                          src: t.getImage(
                            t.model,
                            "backgroundImage",
                            "?w=1920",
                            "?h=1000"
                          ),
                        },
                      }),
                    ],
                    1
                  ),
                  t._v(" "),
                  n(
                    "div",
                    {
                      staticClass:
                        "newsletter-content d-flex flex-column justify-content-md-center align-items-md-center color-white position-relative px-1 px-md-5",
                    },
                    [
                      n(
                        "div",
                        {
                          staticClass:
                            "d-flex flex-column justify-content-md-center mb-md-6",
                        },
                        [
                          n(
                            "div",
                            {
                              staticClass: "item-one position-relative",
                            },
                            [
                              t.model.fields.caption
                                ? n("h2", {
                                    staticClass:
                                      "medium uppercase text-right caption d-none d-md-block should-animate",
                                    attrs: {
                                      "data-preset": "opacity,y",
                                      "data-fromy": "-80",
                                      "data-delay": ".1",
                                    },
                                    domProps: {
                                      innerHTML: t._s(t.model.fields.caption),
                                    },
                                  })
                                : t._e(),
                            ]
                          ),
                        ]
                      ),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass:
                            "should-animate d-inline-block text-md-left text-center",
                          attrs: {
                            "data-preset": "opacity,y",
                            "data-fromy": "-80",
                            "data-delay": ".2",
                          },
                        },
                        [
                          n(
                            "span",
                            {
                              staticClass: "display-1 antiqua-font pr-md-5",
                            },
                            [t._v(t._s(t.item(0)))]
                          ),
                        ]
                      ),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass:
                            "item-two text-center no-wrap mt-1 mt-md-0 should-animate",
                          attrs: {
                            "data-preset": "opacity,y",
                            "data-fromy": "-80",
                            "data-delay": ".3",
                          },
                        },
                        [
                          n(
                            "span",
                            {
                              staticClass: "display-1 antiqua-font lh-05",
                            },
                            [t._v(t._s(t.item(1)))]
                          ),
                        ]
                      ),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass:
                            "item-three mt-1 no-wrap text-center should-animate",
                          attrs: {
                            "data-preset": "opacity,y",
                            "data-fromy": "-80",
                            "data-delay": ".4",
                          },
                        },
                        [
                          n(
                            "span",
                            {
                              staticClass:
                                "display-1 lh-1 antiqua-font pr-md-4",
                            },
                            [t._v(t._s(t.item(2)))]
                          ),
                        ]
                      ),
                    ]
                  ),
                  t._v(" "),
                  n(
                    "div",
                    {
                      staticClass: "d-md-flex px-1 px-md-5 color-white",
                    },
                    [
                      n(
                        "div",
                        {
                          staticClass: "col-md-5",
                        },
                        [
                          n(
                            "div",
                            {
                              staticClass:
                                "d-md-flex flex-column justify-content-md-end text-md-left text-center mt-1 mt-md-0",
                            },
                            [
                              n(
                                "span",
                                {
                                  staticClass:
                                    "display-1 lh-1 antiqua-font pl-md-5 no-wrap text-center should-animate d-inline-block item-four",
                                  attrs: {
                                    "data-preset": "opacity,y",
                                    "data-fromy": "-80",
                                    "data-delay": ".5",
                                  },
                                },
                                [t._v(t._s(t.item(3)))]
                              ),
                            ]
                          ),
                        ]
                      ),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass:
                            "col-md-7 pl-md-2 pr-md-6 pb-md-2 d-flex align-items-end",
                        },
                        [
                          n(
                            "div",
                            {
                              staticClass:
                                "newsletter-wrapper w-100 should-animate pr-md-4 mt-2",
                              attrs: {
                                "data-preset": "opacity,y",
                                "data-fromy": "-80",
                                "data-delay": ".7",
                              },
                            },
                            [n("mailchimp")],
                            1
                          ),
                        ]
                      ),
                    ]
                  ),
                ],
                1
              );
            },
            [],
            !1,
            null,
            "60d1b024",
            null
          ).exports),
        Ae = n(6),
        Me = {
          name: "tab-content",
          data: function () {
            return {
              open: !1,
            };
          },
          props: ["model"],
          mixins: [
            {
              data: function () {
                return {
                  expanded: !1,
                };
              },
              mixins: [],
              components: {},
              props: ["index"],
              watch: {
                isVisible: function (t) {
                  var e = this;
                  if (this.timeline) {
                    if (t) {
                      this.expanded = !0;
                      var n = this.$el.getBoundingClientRect();
                      this.$emit("scrollTo", n.top, this.index),
                        this.$nextTick(function () {
                          e.timeline.timeScale(1), e.timeline.play();
                        });
                    } else this.timeline.timeScale(1), this.timeline.reverse();
                  }
                },
                pageWidth: function () {
                  this.initTimeline();
                },
              },
              created: function () {},
              mounted: function () {
                this.initTimeline();
              },
              computed: {
                pageWidth: function () {
                  return this.$store.getters["app/getState"]("width");
                },
                isVisible: function () {
                  return this.open
                    ? this.open
                    : !!this.activeItem &&
                        this.activeItem.sys.id == this.model.sys.id;
                },
              },
              methods: {
                initTimeline: function () {
                  var t = this;
                  if (this.pageWidth >= 768) this.expanded = !0;
                  else {
                    var e = this;
                    (this.timeline = new Ae.c({
                      onReverseComplete: function () {
                        setTimeout(function () {
                          (e.expanded = !1),
                            e.$emit("setActiveHeight", 0),
                            e.$bus.$emit("resize");
                        }, 20);
                      },
                      onComplete: function () {
                        setTimeout(function () {
                          e.$bus.$emit("resize"),
                            e.$emit(
                              "setActiveHeight",
                              e.$refs.contentWrapper.getBoundingClientRect()
                                .height
                            );
                        }, 20);
                      },
                      paused: !0,
                    })),
                      this.timeline.from(this.$refs.contentWrapper, 1, {
                        height: 0,
                        ease: "Power4.easeInOut",
                      }),
                      this.timeline.call(
                        function () {
                          t.$bus.$emit("resize");
                        },
                        null,
                        "-=.5"
                      );
                    var n =
                      L()(this.$refs, "description.$el") ||
                      L()(this.$refs, "description");
                    n &&
                      this.timeline.from(
                        n,
                        0.6,
                        {
                          opacity: 0,
                          delay: 0.6,
                        },
                        0
                      );
                  }
                },
                toggleActive: function (t) {
                  var e = this.isVisible ? null : this.model;
                  this.$emit("update:activeItem", e);
                },
                openActive: function (t) {
                  this.isVisible != t && this.$emit("update:activeItem", t);
                },
              },
            },
          ],
          watch: {
            pageWidth: function (t) {
              this.open = t >= 768;
            },
          },
          created: function () {
            this.open = this.pageWidth >= 768;
          },
          computed: {
            linkText: function () {
              return this.expanded ? "show less" : "read more";
            },
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
          },
          components: {},
        },
        De =
          (n(637),
          Object(R.a)(
            Me,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "div",
                {
                  staticClass: "tab-content-component text-center",
                },
                [
                  n("h2", {
                    staticClass:
                      "h1 antiqua-font no-wrap mb-2 mb-md-3 lh-1 tab-title should-animate",
                    attrs: {
                      "data-delay": ".3",
                      "data-preset": "opacity,y",
                      "data-fromy": "30",
                    },
                    domProps: {
                      innerHTML: t._s(t.model.fields.title),
                    },
                  }),
                  t._v(" "),
                  n("p", {
                    staticClass:
                      "content mb-2 antiqua-font mediumv2 lh-1 should-animate content tab-content",
                    attrs: {
                      "data-delay": ".35",
                      "data-preset": "opacity,y",
                      "data-fromy": "30",
                    },
                    domProps: {
                      innerHTML: t._s(t.model.fields.description),
                    },
                  }),
                  t._v(" "),
                  t.model.fields.content
                    ? n(
                        "div",
                        {
                          directives: [
                            {
                              name: "show",
                              rawName: "v-show",
                              value: t.expanded,
                              expression: "expanded",
                            },
                          ],
                          ref: "contentWrapper",
                          staticClass:
                            "content px-md-2 px-xl-7 tab-content should-animate",
                          class: {
                            "overflow-hidden": t.pageWidth <= 768,
                          },
                          attrs: {
                            "data-delay": ".4",
                            "data-preset": "opacity,y",
                            "data-fromy": "30",
                          },
                        },
                        [
                          n("div", {
                            ref: "description",
                            domProps: {
                              innerHTML: t._s(t.model.fields.content),
                            },
                          }),
                        ]
                      )
                    : t._e(),
                  t._v(" "),
                  n("a", {
                    staticClass:
                      "d-md-none text-uppercase link reversed should-animate content mt-2 d-inline-block",
                    attrs: {
                      "data-delay": ".4",
                      "data-preset": "opacity,y",
                      "data-fromy": "30",
                      href: "#",
                    },
                    domProps: {
                      innerHTML: t._s(t.linkText),
                    },
                    on: {
                      click: function (e) {
                        e.preventDefault(), (t.open = !t.open);
                      },
                    },
                  }),
                ]
              );
            },
            [],
            !1,
            null,
            "6293f6b2",
            null
          ).exports),
        We = n(426),
        He = n.n(We);

      function Re(object, t) {
        var e = Object.keys(object);
        if (Object.getOwnPropertySymbols) {
          var n = Object.getOwnPropertySymbols(object);
          t &&
            (n = n.filter(function (t) {
              return Object.getOwnPropertyDescriptor(object, t).enumerable;
            })),
            e.push.apply(e, n);
        }
        return e;
      }

      function ze(t) {
        for (var i = 1; i < arguments.length; i++) {
          var source = null != arguments[i] ? arguments[i] : {};
          i % 2
            ? Re(Object(source), !0).forEach(function (e) {
                Object(o.a)(t, e, source[e]);
              })
            : Object.getOwnPropertyDescriptors
            ? Object.defineProperties(
                t,
                Object.getOwnPropertyDescriptors(source)
              )
            : Re(Object(source)).forEach(function (e) {
                Object.defineProperty(
                  t,
                  e,
                  Object.getOwnPropertyDescriptor(source, e)
                );
              });
        }
        return t;
      }
      var Fe = {
          name: "tab-component",
          data: function () {
            return {
              currentIndex: 0,
              textDelay: 0.2,
              ignoreLines: !0,
              proportions: [1, 2, 3],
            };
          },
          props: ["model"],
          mixins: [
            W,
            {
              data: function () {
                return {
                  currentIndex: 0,
                  pageSlider: !1,
                };
              },
              props: [],
              watch: {},
              computed: {
                prevItem: function () {
                  return this.items[this.getPreviousIndex()];
                },
                nextItem: function () {
                  return this.items[this.getNextIndex()];
                },
                prevItemLooped: function () {
                  return this.items[this.getPreviousIndex(!0)];
                },
                nextItemLooped: function () {
                  return this.items[this.getNextIndex(!0)];
                },
                numOfSlides: function () {
                  return this.items.length;
                },
                isLastSlide: function () {
                  return this.normalizedIndex == this.numOfSlides - 1;
                },
                isFirstSlide: function () {
                  return 0 == this.normalizedIndex;
                },
                activeItem: function () {
                  return this.confirmedIndex
                    ? this.items[this.confirmedIndex || 0]
                    : this.pageSlider
                    ? this.model
                    : this.items[this.normalizedIndex];
                },
                normalizedIndex: function () {
                  return this.realIndex < this.numOfSlides
                    ? this.realIndex
                    : this.realIndex % this.numOfSlides;
                },
                realIndex: function () {
                  return this.stepSlider
                    ? this.confirmedIndex
                    : this.currentIndex;
                },
              },
              beforeDestroy: function () {},
              methods: {
                goToSlide: function (t) {
                  this.currentIndex = t;
                },
                nextSlide: function (t) {
                  this.currentIndex = this.getNextIndex(t);
                },
                prevSlide: function (t) {
                  this.currentIndex = this.getPreviousIndex(t);
                },
                getPreviousIndex: function (t) {
                  return this.normalizedIndex > 0
                    ? this.normalizedIndex - 1
                    : t
                    ? this.numOfSlides - 1
                    : 0;
                },
                getNextIndex: function (t) {
                  return this.normalizedIndex < this.numOfSlides - 1
                    ? this.normalizedIndex + 1
                    : t
                    ? 0
                    : this.numOfSlides - 1;
                },
              },
            },
          ],
          watch: {
            activeItem: function (t) {
              this.loadImages({
                imgUrls: [this.image(0), this.image(1)],
              });
            },
          },
          methods: {
            onSplited: function (t) {
              this.lines = t;
            },
            enterImages: function (t, e) {
              var n = t.querySelectorAll(".image");
              x.b.fromTo(
                n,
                1,
                {
                  xPercent: 50,
                  rotate: 5,
                  opacity: 0,
                },
                {
                  opacity: 1,
                  xPercent: 0,
                  rotate: 0,
                  onComplete: e,
                  stagger: -0.1,
                  ease: "Power4.easeOut",
                }
              );
            },
            leaveImages: function (t, e) {
              var n = t.querySelectorAll(".image");
              x.b.to(n, 0.8, {
                xPercent: -100,
                opacity: 0,
                rotate: -10,
                onComplete: e,
                stagger: 0.1,
                delay: 0.1,
                ease: "Power4.easeInOut",
              });
            },
            enterContent: function (t, e) {
              setTimeout(function () {
                var title = t.querySelectorAll(".tab-title");
                x.b.set(title, {
                  opacity: 1,
                });
                var content = t.querySelectorAll(".content");
                x.b.fromTo(
                  content,
                  0.6,
                  {
                    opacity: 0,
                  },
                  {
                    opacity: 1,
                    delay: 0.3,
                  }
                ),
                  x.b.fromTo(
                    title,
                    0.6,
                    {
                      xPercent: 25,
                      opacity: 0,
                    },
                    {
                      xPercent: 0,
                      opacity: 1,
                      stagger: 0.06,
                      onComplete: e,
                    }
                  );
              }, 100);
            },
            leaveContent: function (t, e) {
              var title = t.querySelectorAll(".tab-title");
              x.b.to(title, 0.6, {
                xPercent: -50,
                opacity: 0,
                stagger: 0.06,
                delay: 0.2,
                onComplete: e,
              });
              var content = t.querySelectorAll(".content");
              x.b.to(content, 0.6, {
                opacity: 0,
                delay: 0.3,
              });
            },
            image: function (t) {
              return this.getImage(this.images[t], "img", "?w=768");
            },
            getSlideImage: function (t, e) {
              var n = t.fields.images;
              return this.getImage(n[e], "img", "?w=640");
            },
          },
          computed: ze(
            ze({}, Object(B.c)("app", ["width"])),
            {},
            {
              items: function () {
                return L()(this.model, "fields.entities");
              },
              images: function () {
                return L()(this.activeItem, "fields.images");
              },
              swiperOptions: function () {
                return {
                  loop: !0,
                  effect: "drag",
                  grabCursor: !0,
                  autoHeight: !0,
                  slidesPerView: 1,
                  spaceBetween: 20,
                  fadeEffect: {
                    crossFade: !0,
                  },
                  navigation: {
                    nextEl: ".swiper-next",
                    prevEl: ".swiper-prev",
                  },
                };
              },
            }
          ),
          components: {
            Arrow: He.a,
            TabContent: De,
          },
        },
        Xe =
          (n(638),
          Object(R.a)(
            Fe,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "waypoint",
                {
                  staticClass:
                    "tab-component bg-olivev3 pb-2 py-md-6 d-md-flex position-relative",
                  attrs: {
                    "data-scroll-section": "",
                    y: t.elementY,
                  },
                  on: {
                    toggleOneDirectionVisible: t.toggleWaypointWithLines,
                  },
                },
                [
                  n(
                    "section",
                    {
                      staticClass:
                        "images-wrapper col-md-6 d-block px-md-2 pl-md-6 pr-md-4 disabled",
                    },
                    [
                      t.$device.isMobile
                        ? t._e()
                        : n(
                            "transition",
                            {
                              staticClass: "d-none d-md-block",
                              attrs: {
                                css: !1,
                                mode: "out-in",
                              },
                              on: {
                                enter: t.enterImages,
                                leave: t.leaveImages,
                              },
                            },
                            [
                              n(
                                "div",
                                {
                                  key: t.currentIndex,
                                  staticClass:
                                    "position-relative d-none d-md-block",
                                  class: "images-container-" + t.currentIndex,
                                },
                                [
                                  n("img", {
                                    staticClass: "texture-svg image",
                                    attrs: {
                                      loading: "lazy",
                                      alt: "background texture",
                                      src: t.getImage(
                                        t.model,
                                        "image",
                                        "?w=640",
                                        "?w=320"
                                      ),
                                    },
                                  }),
                                  t._v(" "),
                                  n(
                                    "div",
                                    {
                                      staticClass:
                                        "image-one position-relative on-top should-animate image",
                                      attrs: {
                                        "data-fromrotate": "-10",
                                        "data-torotate": "0",
                                        "data-dur": "1",
                                        "data-delay": ".2",
                                        "data-fromy": "100",
                                        "data-props": "opacity,y,rotate",
                                      },
                                    },
                                    [
                                      n("img", {
                                        staticClass: "image-1 contain",
                                        attrs: {
                                          src: t.image(0),
                                          loading: "lazy",
                                          alt: "Naba Zabih",
                                        },
                                      }),
                                    ]
                                  ),
                                  t._v(" "),
                                  n(
                                    "div",
                                    {
                                      staticClass:
                                        "image-two should-animate image mb-1",
                                      attrs: {
                                        "data-props": "opacity,y,rotate",
                                        "data-fromrotate": "10",
                                        "data-torotate": "0",
                                        "data-dur": "1",
                                        "data-delay": ".2",
                                        "data-fromy": "100",
                                      },
                                    },
                                    [
                                      n("img", {
                                        staticClass: "image-1 contain",
                                        attrs: {
                                          src: t.image(1),
                                          loading: "lazy",
                                          alt: "Naba Zabih",
                                        },
                                      }),
                                    ]
                                  ),
                                ]
                              ),
                            ]
                          ),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass: "h-100",
                        },
                        [
                          n(
                            "swiper",
                            {
                              ref: "swiper",
                              staticClass:
                                "slider-wrapper hidden-desktop w-100 h-100 position-relative enabled overflow-visible",
                              attrs: {
                                customOptions: t.swiperOptions,
                                currentIndex: t.currentIndex,
                              },
                              on: {
                                "update:currentIndex": function (e) {
                                  t.currentIndex = e;
                                },
                                "update:current-index": function (e) {
                                  t.currentIndex = e;
                                },
                              },
                            },
                            t._l(t.items, function (e, r) {
                              return n(
                                "div",
                                {
                                  key: r,
                                  staticClass: "swiper-slide",
                                  class: "images-container-" + r,
                                },
                                [
                                  n("img", {
                                    staticClass: "texture-svg image contain",
                                    attrs: {
                                      loading: "lazy",
                                      alt: "background texture",
                                      src: t.getImage(
                                        t.model,
                                        "image",
                                        "?w=640",
                                        "?w=320"
                                      ),
                                    },
                                  }),
                                  t._v(" "),
                                  n(
                                    "div",
                                    {
                                      staticClass:
                                        "image-one position-relative on-top should-animate image px-2 px-md-0",
                                      attrs: {
                                        "data-fromrotate": "-10",
                                        "data-torotate": "0",
                                        "data-dur": "1",
                                        "data-delay": ".2",
                                        "data-fromy": "100",
                                        loading: "lazy",
                                        "data-props": "opacity,y,rotate",
                                      },
                                    },
                                    [
                                      n("img", {
                                        staticClass: "image-1 contain",
                                        attrs: {
                                          loading: "lazy",
                                          src: t.getSlideImage(e, 0),
                                          alt: "Naba Zabih",
                                        },
                                      }),
                                    ]
                                  ),
                                  t._v(" "),
                                  n(
                                    "div",
                                    {
                                      staticClass:
                                        "image-two should-animate image mb-1 px-2 px-md-0",
                                      attrs: {
                                        "data-props": "opacity,y,rotate",
                                        "data-fromrotate": "10",
                                        "data-torotate": "0",
                                        "data-dur": "1",
                                        "data-delay": ".2",
                                        "data-fromy": "100",
                                      },
                                    },
                                    [
                                      n("img", {
                                        staticClass: "image-1 contain",
                                        attrs: {
                                          loading: "lazy",
                                          src: t.getSlideImage(e, 1),
                                          alt: "Naba Zabih",
                                        },
                                      }),
                                    ]
                                  ),
                                ]
                              );
                            }),
                            0
                          ),
                        ],
                        1
                      ),
                    ],
                    1
                  ),
                  t._v(" "),
                  n(
                    "div",
                    {
                      staticClass:
                        "navigation-arrows on-top mb-1 d-flex d-md-none justify-content-center px-1 position-relative z-1",
                    },
                    [
                      n(
                        "div",
                        {
                          staticClass: "swiper-prev",
                        },
                        [
                          n("arrow", {
                            staticClass: "arrow prev mx-7",
                          }),
                        ],
                        1
                      ),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass: "swiper-next",
                        },
                        [
                          n("arrow", {
                            staticClass: "arrow next mx-7",
                          }),
                        ],
                        1
                      ),
                    ]
                  ),
                  t._v(" "),
                  n(
                    "div",
                    {
                      staticClass: "col-md-6 d-block px-1",
                    },
                    [
                      n(
                        "div",
                        {
                          staticClass:
                            "content-wrapper d-flex flex-column justify-content-between",
                        },
                        [
                          n(
                            "transition",
                            {
                              attrs: {
                                css: !1,
                                mode: "out-in",
                                delay: 0.1,
                              },
                              on: {
                                enter: t.enterContent,
                                leave: t.leaveContent,
                              },
                            },
                            [
                              n("tab-content", {
                                key: t.currentIndex,
                                staticClass: "px-md-7 px-xl-5 mb-2",
                                attrs: {
                                  model: t.activeItem,
                                },
                              }),
                            ],
                            1
                          ),
                          t._v(" "),
                          n(
                            "div",
                            {
                              staticClass:
                                "d-none d-md-flex flex-row justify-content-between px-md-2 px-xl-4 text-center",
                            },
                            t._l(t.items, function (e, r) {
                              return n(
                                "div",
                                {
                                  key: r,
                                  staticClass: "should-animate",
                                  attrs: {
                                    "data-delay": 0.4 + r / 10,
                                    "data-fromx": "40",
                                    "data-preset": "opacity,x",
                                  },
                                  on: {
                                    click: function (e) {
                                      return t.goToSlide(r);
                                    },
                                  },
                                },
                                [
                                  n(
                                    "span",
                                    {
                                      staticClass:
                                        "small uppercase cursor-pointer tab-names text-center lh-1",
                                      class: {
                                        "color-black": t.currentIndex == r,
                                        "color-black-opacity":
                                          t.currentIndex != r,
                                      },
                                    },
                                    [t._v(t._s(e.fields.names))]
                                  ),
                                ]
                              );
                            }),
                            0
                          ),
                        ],
                        1
                      ),
                    ]
                  ),
                ]
              );
            },
            [],
            !1,
            null,
            "2a89bf3d",
            null
          ).exports),
        Be = n(427),
        Ne = n.n(Be),
        qe = n(428),
        Ye = n.n(qe),
        Ve = {
          name: "project-preview",
          props: ["model"],
          mixins: [W],
          computed: {
            previewImage: function () {
              return this.getImage(this.model, "previewImage", "?h=1200");
            },
            category: function () {
              return L()(this.model, "fields.category");
            },
            categoryLength: function () {
              return Ye()(this.category);
            },
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
            bgImg: function () {
              return this.getImage(
                this.model,
                "textureBackground",
                "?h=1000",
                "?h=1000",
                "98"
              );
            },
          },
          components: {},
        },
        Ue =
          (n(642),
          Object(R.a)(
            Ve,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "waypoint",
                {
                  staticClass:
                    "project-preview d-flex flex-md-row flex-column flex-column-reverse overflow-hidden",
                  attrs: {
                    y: t.elementY,
                  },
                  on: {
                    toggleOneDirectionVisible: t.toggleWaypoint,
                  },
                },
                [
                  n(
                    "div",
                    {
                      staticClass:
                        "col-md-6 px-2 px-md-6 d-flex pt-2 pb-3 pt-md-6 pb-md-5 flex-column justify-content-between flex-center text-center",
                    },
                    [
                      t.bgImg
                        ? n("image-div", {
                            staticClass:
                              "h-100 w-100 image position-absolute l-0 t-0 should-animate",
                            attrs: {
                              src: t.bgImg,
                              "data-props": "scale",
                              "data-fromscale": "1.2",
                              "data-delay": ".2",
                            },
                          })
                        : t._e(),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass: "category-list d-flex should-animate",
                          attrs: {
                            "data-preset": "opacity,y",
                            "data-delay": ".1",
                          },
                        },
                        t._l(t.category, function (e, r) {
                          return n(
                            "div",
                            {
                              key: r,
                            },
                            [
                              n(
                                "router-link",
                                {
                                  staticClass: "tiny uppercase link-line",
                                  attrs: {
                                    to: "../portfolio?category=" + e,
                                  },
                                },
                                [n("span", [t._v(t._s(e))])]
                              ),
                              t._v(" "),
                              r != t.categoryLength - 1
                                ? n("span", [t._v(", ")])
                                : t._e(),
                            ],
                            1
                          );
                        }),
                        0
                      ),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass: "position-relative w-100",
                        },
                        [
                          n(
                            "nuxt-link",
                            {
                              staticClass: "d-block",
                              attrs: {
                                to: t.model.url,
                              },
                            },
                            [
                              t.model.fields.title
                                ? n("h2", {
                                    staticClass:
                                      "antiqua-font mt-3 mb-2 h4 mt-md-0 mb-md-5 should-animate",
                                    attrs: {
                                      "data-preset": "opacity,y",
                                      "data-delay": ".25",
                                    },
                                    domProps: {
                                      innerHTML: t._s(t.model.fields.title),
                                    },
                                  })
                                : t._e(),
                            ]
                          ),
                          t._v(" "),
                          t.model.fields.names
                            ? n("p", {
                                staticClass: "uppercase should-animate tiny",
                                attrs: {
                                  "data-preset": "opacity,y",
                                  "data-delay": ".4",
                                },
                                domProps: {
                                  innerHTML: t._s(t.model.fields.names),
                                },
                              })
                            : t._e(),
                        ],
                        1
                      ),
                    ],
                    1
                  ),
                  t._v(" "),
                  n(
                    "nuxt-link",
                    {
                      staticClass:
                        "col-md-6 should-animate d-block overflow-hidden",
                      attrs: {
                        to: t.model.url,
                        "data-fromscale": "1.1",
                        "data-props": "scale",
                      },
                    },
                    [
                      n("image-div", {
                        staticClass: "h-100 project-image",
                        attrs: {
                          innerClasses: "transition-out-image",
                          proportion: t.pageWidth < 320 ? 1.2 : 1,
                          src: t.previewImage,
                        },
                      }),
                    ],
                    1
                  ),
                ],
                1
              );
            },
            [],
            !1,
            null,
            "0818242a",
            null
          ).exports),
        Ze = {
          name: "project-wrapper",
          props: ["model"],
          computed: {
            entities: function () {
              return L()(this.model, "fields.entities");
            },
          },
          components: {
            StarIcon: Ne.a,
            ProjectPreview: Ue,
          },
        },
        Ge =
          (n(643),
          Object(R.a)(
            Ze,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "div",
                {
                  staticClass:
                    "project-wrapper d-flex flex-column flex-center color-white",
                },
                [
                  n(
                    "div",
                    {
                      staticClass:
                        "pt-2 pb-3 py-md-6 bg-blackv3 w-100 d-flex flex-column flex-center",
                      attrs: {
                        "data-scroll-section": "",
                      },
                    },
                    [
                      t.model.fields.caption
                        ? n("p", {
                            staticClass: "small text-center lh-12 mb-7",
                            domProps: {
                              innerHTML: t._s(t.model.fields.caption),
                            },
                          })
                        : t._e(),
                      t._v(" "),
                      t.model.fields.headline
                        ? n("h2", {
                            staticClass: "display-4 antiqua-font mb-md-5",
                            domProps: {
                              innerHTML: t._s(t.model.fields.headline),
                            },
                          })
                        : t._e(),
                      t._v(" "),
                      n("star-icon", {
                        staticClass: "star-icon text-center",
                      }),
                    ],
                    1
                  ),
                  t._v(" "),
                  n(
                    "div",
                    {
                      staticClass: "project-wrapper w-100",
                    },
                    t._l(t.entities, function (t, e) {
                      return n("project-preview", {
                        key: e,
                        attrs: {
                          "data-scroll-section": "",
                          model: t,
                        },
                      });
                    }),
                    1
                  ),
                ]
              );
            },
            [],
            !1,
            null,
            "7b28da2a",
            null
          ).exports),
        Ke = n(214),
        Je = n.n(Ke),
        Qe = (n(644), n(429)),
        ti = n.n(Qe),
        ei = {
          data: function () {
            return {
              config: {
                key: "kUojlO-zZCMa_GbPQQM8yA",
                message: {
                  to: [
                    {
                      type: "to",
                      // email: "nabazabihphotography@gmail.com",
                      email: "kuum94@gmail.com",
                    },
                  ],
                  html: "",
                  subject: "Shooting application",
                  autotext: "true",
                  from_name: "Naba Zabih - Website",
                  from_email: "contact@rebellion7.com",
                },
              },
              endpoint: "https://mandrillapp.com/api/1.0/messages/send.json",
            };
          },
          methods: {
            sendEmail: function (t, e) {
              var n = this;
              return Object(r.a)(
                regeneratorRuntime.mark(function e() {
                  var data, html, r;
                  return regeneratorRuntime.wrap(function (e) {
                    for (;;)
                      switch ((e.prev = e.next)) {
                        case 0:
                          return (
                            (data = n.config),
                            (html = ""),
                            O()(t, function (t, e) {
                              html +=
                                "<span>" + ti()(e) + ": " + t + " </span><br/>";
                            }),
                            (data.message.html = html),
                            (e.next = 6),
                            Je.a
                              .post(n.endpoint, data, {
                                headers: {
                                  "Content-Type": "application/json",
                                },
                              })
                              .then(function (t) {
                                200 == t.status && n.onSuccess();
                              })
                              .catch(function (t) {
                                console.warn("error:", t);
                              })
                          );
                        case 6:
                          return (r = e.sent), e.abrupt("return", r);
                        case 8:
                        case "end":
                          return e.stop();
                      }
                  }, e);
                })
              )();
            },
          },
        },
        ii = n(47),
        ni = {
          data: function () {
            return {
              value: null,
              placeholderValue: null,
            };
          },
          components: {
            ValidationProvider: ii.b,
          },
          props: {
            inputId: {
              default: "form-input",
            },
            rules: null,
            model: null,
            placeholder: null,
            errorClasses: null,
            onInput: null,
            label: null,
            labelClasses: null,
          },
          created: function () {
            (this.value = this.model),
              (this.placeholderValue = this.placeholder);
          },
          watch: {
            value: function (t) {
              this.$emit("update:model", t);
            },
          },
          methods: {
            onFocus: function () {
              this.placeholderValue = "";
            },
            onBlur: function () {
              (this.value && "" != this.value) ||
                (this.placeholderValue = this.placeholder);
            },
          },
        },
        ri = {
          name: "form-input",
          mixins: [ni],
          methods: {
            showError: function (t) {
              return t[0] && this.value && "" != this.value;
            },
          },
        },
        ai = Object(R.a)(
          ri,
          function () {
            var t = this,
              e = t.$createElement,
              n = t._self._c || e;
            return n("ValidationProvider", {
              attrs: {
                rules: t.rules,
              },
              scopedSlots: t._u(
                [
                  {
                    key: "default",
                    fn: function (e) {
                      var r = e.errors;
                      e.valid, e.invalid, e.pristine;
                      return [
                        t.label
                          ? n("label", {
                              attrs: {
                                for: t.inputId + t._uid,
                              },
                              domProps: {
                                innerHTML: t._s(t.label),
                              },
                            })
                          : t._e(),
                        t._v(" "),
                        n("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: t.value,
                              expression: "value",
                            },
                          ],
                          staticClass: "w-100",
                          class: {
                            "input--invalid": r[0],
                          },
                          attrs: {
                            type: "text",
                            placeholder: t.placeholderValue,
                            name: t.inputId,
                            id: t.inputId + t._uid,
                          },
                          domProps: {
                            value: t.value,
                          },
                          on: {
                            focus: t.onFocus,
                            blur: t.onBlur,
                            input: function (e) {
                              e.target.composing || (t.value = e.target.value);
                            },
                          },
                        }),
                        t._v(" "),
                        n(
                          "small",
                          {
                            class: t.errorClasses,
                          },
                          [t._v(t._s(r[0]))]
                        ),
                        t._v(" "),
                        t._t("default"),
                      ];
                    },
                  },
                ],
                null,
                !0
              ),
            });
          },
          [],
          !1,
          null,
          null,
          null
        ).exports,
        si = {
          name: "form-input",
          data: function () {
            return {
              OpenIndicator: {
                render: function (t) {
                  return t("img", {
                    attrs: {
                      src: n(756),
                      class: "input-arrow",
                    },
                  });
                },
              },
              Deselect: {
                render: function (t) {
                  return t("span", "");
                },
              },
            };
          },
          methods: {
            onFocus: function () {
              this.$bus.$emit("disableScrollbar");
            },
            onBlur: function () {
              this.$bus.$emit("enableScrollbar");
            },
          },
          mixins: [ni],
          props: {
            options: {
              default: [],
            },
          },
        },
        oi =
          (n(757),
          Object(R.a)(
            si,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n("ValidationProvider", {
                attrs: {
                  rules: t.rules,
                },
                scopedSlots: t._u(
                  [
                    {
                      key: "default",
                      fn: function (e) {
                        var r = e.errors;
                        e.valid, e.invalid, e.pristine;
                        return [
                          n("v-select", {
                            staticClass: "v-select input",
                            class: {
                              "input--invalid": r[0],
                            },
                            attrs: {
                              id: t.inputId + t._uid,
                              placeholder: t.placeholder,
                              components: {
                                OpenIndicator: t.OpenIndicator,
                                Deselect: t.Deselect,
                              },
                              options: t.options,
                            },
                            on: {
                              "search:focus": t.onFocus,
                              "search:input": t.onBlur,
                              "search:blur": t.onBlur,
                            },
                            model: {
                              value: t.value,
                              callback: function (e) {
                                t.value = e;
                              },
                              expression: "value",
                            },
                          }),
                          t._v(" "),
                          n(
                            "small",
                            {
                              class: t.errorClasses,
                            },
                            [t._v(t._s(r[0]))]
                          ),
                          t._v(" "),
                          t._t("default"),
                        ];
                      },
                    },
                  ],
                  null,
                  !0
                ),
              });
            },
            [],
            !1,
            null,
            null,
            null
          ).exports),
        ci = n(273),
        di = n.n(ci),
        ui = {
          name: "form-input",
          mixins: [ni],
          props: {
            dateOptions: {
              default: {},
            },
          },
          filters: {
            date_format: function (t) {
              if (t) return di()(new Date(t), "mmmm dd, yyyy");
            },
          },
        },
        pi = Object(R.a)(
          ui,
          function () {
            var t = this,
              e = t.$createElement,
              n = t._self._c || e;
            return n("ValidationProvider", {
              attrs: {
                rules: t.rules,
              },
              scopedSlots: t._u(
                [
                  {
                    key: "default",
                    fn: function (e) {
                      var r = e.errors;
                      e.valid, e.invalid, e.pristine;
                      return [
                        n(
                          "v-date-picker",
                          {
                            staticClass: "w-100 date-picker",
                            attrs: {
                              id: t.inputId + t._uid,
                              "min-date": new Date(),
                              popover: t.dateOptions,
                              color: "grey",
                            },
                            model: {
                              value: t.value,
                              callback: function (e) {
                                t.value = e;
                              },
                              expression: "value",
                            },
                          },
                          [
                            n("input", {
                              staticClass: "w-100",
                              class: {
                                "input--invalid": r[0],
                              },
                              attrs: {
                                type: "text",
                                placeholder: "Desired date",
                              },
                              domProps: {
                                value: t._f("date_format")(t.value),
                              },
                            }),
                          ]
                        ),
                        t._v(" "),
                        n(
                          "small",
                          {
                            class: t.errorClasses,
                          },
                          [t._v(t._s(r[0]))]
                        ),
                        t._v(" "),
                        t._t("default"),
                      ];
                    },
                  },
                ],
                null,
                !0
              ),
            });
          },
          [],
          !1,
          null,
          "5a7757c0",
          null
        ).exports,
        hi = {
          name: "form-textarea",
          mixins: [ni],
          props: {
            rows: {
              default: 2,
            },
          },
        },
        fi = Object(R.a)(
          hi,
          function () {
            var t = this,
              e = t.$createElement,
              n = t._self._c || e;
            return n("ValidationProvider", {
              attrs: {
                rules: t.rules,
              },
              scopedSlots: t._u(
                [
                  {
                    key: "default",
                    fn: function (e) {
                      var r = e.errors;
                      e.valid, e.invalid, e.pristine;
                      return [
                        t.label
                          ? n("label", {
                              class: t.labelClasses,
                              attrs: {
                                for: t.inputId + t._uid,
                              },
                              domProps: {
                                innerHTML: t._s(t.label),
                              },
                            })
                          : t._e(),
                        t._v(" "),
                        n("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: t.value,
                              expression: "value",
                            },
                          ],
                          staticClass: "w-100",
                          attrs: {
                            rows: t.rows,
                            placeholder: t.placeholderValue,
                            name: t.inputId,
                            id: t.inputId + t._uid,
                          },
                          domProps: {
                            value: t.value,
                          },
                          on: {
                            focus: t.onFocus,
                            blur: t.onBlur,
                            input: function (e) {
                              e.target.composing || (t.value = e.target.value);
                            },
                          },
                        }),
                        t._v(" "),
                        n(
                          "small",
                          {
                            class: t.errorClasses,
                          },
                          [t._v(t._s(r[0]))]
                        ),
                        t._v(" "),
                        t._t("default"),
                      ];
                    },
                  },
                ],
                null,
                !0
              ),
            });
          },
          [],
          !1,
          null,
          null,
          null
        ).exports,
        mi = {
          name: "form-generator",
          data: function () {
            return {};
          },
          props: [
            "schema",
            "defaultErrorClasses",
            "defaultInputClasses",
            "btnText",
            "btnClasses",
          ],
          methods: {
            checkIfActive: function (t) {
              if (!t.activationRule) return !0;
            },
            onSubmit: function () {
              var t = this;
              return Object(r.a)(
                regeneratorRuntime.mark(function e() {
                  return regeneratorRuntime.wrap(function (e) {
                    for (;;)
                      switch ((e.prev = e.next)) {
                        case 0:
                          return (e.next = 2), t.$refs.form.validate();
                        case 2:
                          e.sent && t.$emit("submitForm", t.formData);
                        case 4:
                        case "end":
                          return e.stop();
                      }
                  }, e);
                })
              )();
            },
          },
          computed: {
            formData: function () {
              var t = {};
              return (
                O()(this.schema, function (e, n) {
                  O()(e.fields, function (e, n) {
                    t[n] = e.model;
                  });
                }),
                t
              );
            },
          },
          components: {
            ValidationObserver: ii.a,
            FormInput: ai,
            FormSelect: oi,
            FormDate: pi,
            FormTextarea: fi,
          },
        },
        gi = Object(R.a)(
          mi,
          function () {
            var t = this,
              e = t.$createElement,
              n = t._self._c || e;
            return n(
              "ValidationObserver",
              {
                ref: "form",
                attrs: {
                  tag: "form",
                },
                on: {
                  submit: function (e) {
                    return e.preventDefault(), t.onSubmit(e);
                  },
                },
              },
              [
                t._l(t.schema, function (e, r) {
                  return n(
                    "div",
                    {
                      key: r,
                      class: e.classes,
                    },
                    t._l(e.fields, function (input, i) {
                      return n("form-" + input.type, {
                        key: i,
                        tag: "component",
                        class: t.defaultInputClasses + " " + input.class,
                        attrs: {
                          errorClasses:
                            input.errorClasses || t.defaultErrorClasses,
                          model: e.fields[i].model,
                          rules: input.rules,
                          placeholder: input.placeholder,
                          options: input.options,
                          dateOptions: input.dateOptions,
                          label: input.label,
                          labelClasses: input.labelClasses,
                          rows: input.rows,
                          active: t.checkIfActive(input),
                          inputId: i,
                        },
                        on: {
                          "update:model": function (n) {
                            return t.$set(e.fields[i], "model", n);
                          },
                        },
                      });
                    }),
                    1
                  );
                }),
                t._v(" "),
                n("liquid-button", {
                  class: t.btnClasses,
                  attrs: {
                    type: "submit",
                    pinkBg: !0,
                    text: t.btnText,
                  },
                }),
              ],
              2
            );
          },
          [],
          !1,
          null,
          null,
          null
        ).exports,
        vi = [
          {
            classes: "col-md-6 pr-md-3",
            fields: {
              "Full Names": {
                model: null,
                rules: "",
                placeholder: "You and your Babe’s full names",
                class: "",
                type: "input",
              },
              Email: {
                model: null,
                rules: "required|email",
                placeholder: "Email address",
                class: "",
                type: "input",
              },
              Date: {
                model: null,
                rules: "required",
                placeholder: "Desired date",
                class: "col-md-4 d-inline-block",
                type: "date",
                dateOptions: {
                  placement: "bottom",
                  visibility: "click",
                },
              },
              Location: {
                model: null,
                rules: "",
                placeholder: "Venue or location",
                class: "col-md-8 pl-md-2 d-inline-block",
                type: "input",
              },
              "How did you hear about me": {
                model: null,
                rules: "",
                placeholder: "How did you hear about me?",
                class: "",
                type: "select",
                options: [
                  "Vendor Referral",
                  "Client Referral",
                  "Personal Website",
                  "Google",
                  "Facebook",
                  "Instagram",
                  "Wedding Spot",
                  "The Knot",
                  "Wedding Wire",
                  "Yelp",
                  "Other",
                ],
              },
              Instagram: {
                model: null,
                rules: null,
                placeholder: "Instagram Handles",
                class: "",
                type: "input",
              },
            },
          },
          {
            classes: "col-md-6 pl-md-3 d-flex flex-column",
            fields: {
              "How do you see this day": {
                model: null,
                rules: null,
                class: "aligned-textarea",
                type: "textarea",
                labelClasses: "",
                rows: 2,
                label:
                  "WHAT DO YOU SEE THIS DAY BEING LIKE<br/>& WHAT DREW YOU TO MY WORK?",
              },
              About: {
                model: null,
                rules: null,
                placeholder:
                  "Favorite hobbies, your love languages, dogs or cats (very relevant), how you met, how you see yourselves 80 years from now, gimme all the mushy gushy!",
                class:
                  "d-flex flex-column flex-grow justify-content-between flex-grow",
                label: "tell me about you two!",
                labelClasses: "mb-1",
                rows: 4,
                type: "textarea",
              },
            },
          },
        ];

      function bi(object, t) {
        var e = Object.keys(object);
        if (Object.getOwnPropertySymbols) {
          var n = Object.getOwnPropertySymbols(object);
          t &&
            (n = n.filter(function (t) {
              return Object.getOwnPropertyDescriptor(object, t).enumerable;
            })),
            e.push.apply(e, n);
        }
        return e;
      }

      function yi(t) {
        for (var i = 1; i < arguments.length; i++) {
          var source = null != arguments[i] ? arguments[i] : {};
          i % 2
            ? bi(Object(source), !0).forEach(function (e) {
                Object(o.a)(t, e, source[e]);
              })
            : Object.getOwnPropertyDescriptors
            ? Object.defineProperties(
                t,
                Object.getOwnPropertyDescriptors(source)
              )
            : bi(Object(source)).forEach(function (e) {
                Object.defineProperty(
                  t,
                  e,
                  Object.getOwnPropertyDescriptor(source, e)
                );
              });
        }
        return t;
      }
      var wi = {
          name: "form-component",
          data: function () {
            return {
              formSchema: vi,
              thankYou: !1,
            };
          },
          props: ["model"],
          mixins: [W, ei],
          methods: {
            onSuccess: function (data) {
              this.thankYou = !0;
            },
          },
          computed: yi(
            yi(
              {},
              Object(B.b)({
                getState: "pages/getState",
              })
            ),
            {},
            {
              items: function () {
                return L()(this.navigationModel, "fields.pages");
              },
              navigationModel: function () {
                return this.$store.getters["pages/getState"]("navigation");
              },
              footerModel: function () {
                return this.$store.getters["pages/getState"]("footer");
              },
            }
          ),
          components: {
            ValidationProvider: ii.b,
            ValidationObserver: ii.a,
            FooterPart: Ut,
            FormGenerator: gi,
          },
        },
        xi =
          (n(758),
          {
            mounted: function () {
              this.resizeHandler &&
                (this.$bus.$on("resize", this.resizeHandler),
                this.$nextTick(this.resizeHandler));
            },
            beforeDestroy: function () {
              this.resizeHandler &&
                this.$bus.$off("resize", this.resizeHandler);
            },
            computed: {},
            methods: {
              getComponentTemplate: function (component) {
                var template =
                  L()(component, "fields.template") ||
                  L()(component, "sys.contentType.sys.id");
                if (template)
                  return this.returnComponent(template.capitalize());
              },
              returnComponent: function (component) {
                return l.default.options.components[component] ||
                  this.$options.components[component]
                  ? component
                  : null;
              },
            },
          }),
        Ci = {
          name: "BasicPage",
          components: {},
          mixins: [
            xi,
            {
              components: {
                HeroHeader: Lt,
                ContactHeader: jt,
                AboutHeader: Nt,
                PageHeader: St,
                PageFooter: Jt,
                TextComponent: ie,
                HalfScreenImage: re,
                GalleryCircleBg: se,
                GalleryAndText: ue,
                ListAndImage: he,
                TwoImageAndText: me,
                FullwidthImage: xe,
                CenterImage: _e,
                ElipseImage: Se,
                Newsletter: Ee,
                TabComponent: Xe,
                ProjectWrapper: Ge,
                FormComponent: Object(R.a)(
                  wi,
                  function () {
                    var t = this,
                      e = t.$createElement,
                      n = t._self._c || e;
                    return n(
                      "waypoint",
                      {
                        staticClass:
                          "form-component bg-greyv3 color-white d-flex flex-center flex-column position-relative",
                        attrs: {
                          y: t.elementY,
                          "data-scroll-section": "",
                        },
                        on: {
                          toggleOneDirectionVisible: t.toggleWaypointWithLines,
                        },
                      },
                      [
                        n("img", {
                          staticClass: "w-100 texture bg-black",
                          attrs: {
                            alt: "Background texture",
                            src: "/texture.png",
                          },
                        }),
                        t._v(" "),
                        t.model.fields.headline
                          ? n(
                              "split-text",
                              {
                                key: t.pageWidth,
                                staticClass:
                                  "col-md-5 text-center pt-3 pt-md-0 px-2 px-md-0",
                                on: {
                                  splited: t.onSplited,
                                },
                              },
                              [
                                n("h2", {
                                  staticClass: "antiqua-font",
                                  domProps: {
                                    innerHTML: t._s(t.model.fields.headline),
                                  },
                                }),
                              ]
                            )
                          : t._e(),
                        t._v(" "),
                        n(
                          "div",
                          {
                            staticClass: "position-relative w-100",
                          },
                          [
                            n("form-generator", {
                              staticClass:
                                "pt-3 pt-md-5 d-md-flex w-100 px-1 px-md-5 should-animate row form-generator",
                              class: {
                                "form-sent": t.thankYou,
                              },
                              attrs: {
                                "data-preset": "opacity,y",
                                "data-delay": ".2",
                                defaultInputClasses:
                                  "display-block mb-1 mb-md-2 position-relative",
                                defaultErrorClasses:
                                  "validate-error text-danger tiny position-absolute t-100 l-0",
                                btnText:
                                  "<span>" +
                                  t.$t("apply-for-shooting") +
                                  "</span>",
                                btnClasses: "w-100 mt-md-4 mt-2",
                                schema: t.formSchema,
                              },
                              on: {
                                submitForm: t.sendEmail,
                              },
                            }),
                            t._v(" "),
                            t.thankYou
                              ? n(
                                  "section",
                                  {
                                    staticClass: "position-absolute center",
                                  },
                                  [
                                    n(
                                      "p",
                                      {
                                        staticClass:
                                          "antiqua-font h4 text-center",
                                      },
                                      [
                                        t._v("Thank you,"),
                                        n("br"),
                                        t._v("we will get back to you soon."),
                                      ]
                                    ),
                                  ]
                                )
                              : t._e(),
                          ],
                          1
                        ),
                        t._v(" "),
                        n("footer-part", {
                          staticClass: "w-100",
                          attrs: {
                            model: t.footerModel,
                          },
                        }),
                      ],
                      1
                    );
                  },
                  [],
                  !1,
                  null,
                  "4cfc0620",
                  null
                ).exports,
              },
            },
          ],
          data: function () {
            return {};
          },
          computed: {
            acceptHeader: function () {
              return this.$store.getters["app/getState"]("acceptHeader");
            },
          },
          mounted: function () {
            console.log("header:", this.acceptHeader);
          },
          watch: {},
          asyncData: function (t) {
            return Object(r.a)(
              regeneratorRuntime.mark(function e() {
                var n, r;
                return regeneratorRuntime.wrap(function (e) {
                  for (;;)
                    switch ((e.prev = e.next)) {
                      case 0:
                        return (
                          (n = t.store.getters["pages/getState"]("urls")),
                          (e.next = 3),
                          w.a.loadActivePage({
                            route: t.route.path,
                            id: t.route.name,
                            urls: n,
                            include: 4,
                          })
                        );
                      case 3:
                        return (
                          (r = e.sent),
                          t.store.dispatch("pages/SET_STATE", {
                            activePage: r,
                          }),
                          e.abrupt("return", {
                            model: r,
                          })
                        );
                      case 6:
                      case "end":
                        return e.stop();
                    }
                }, e);
              })
            )();
          },
          methods: {},
        },
        _i = Object(R.a)(
          Ci,
          function () {
            var t = this,
              e = t.$createElement,
              n = t._self._c || e;
            return n(
              "div",
              {
                staticClass: "page page--basic bg-black",
              },
              [
                t.model
                  ? n(
                      "smooth-scroll",
                      {
                        staticClass: "h-100",
                        attrs: {
                          pageId: t.model.sys.id,
                        },
                      },
                      [
                        n(
                          "div",
                          {
                            staticClass: "components-wrapper overflow-hidden",
                          },
                          [
                            t.model.fields.pageHeader
                              ? n(
                                  "div",
                                  [
                                    n(
                                      t.getComponentTemplate(
                                        t.model.fields.pageHeader
                                      ),
                                      {
                                        tag: "component",
                                        attrs: {
                                          model: t.model.fields.pageHeader,
                                        },
                                      }
                                    ),
                                  ],
                                  1
                                )
                              : t._e(),
                            t._v(" "),
                            t.model.fields.components
                              ? n(
                                  "div",
                                  t._l(
                                    t.model.fields.components,
                                    function (component, e) {
                                      return n(
                                        t.getComponentTemplate(component),
                                        {
                                          key: e,
                                          tag: "component",
                                          attrs: {
                                            model: component,
                                          },
                                        }
                                      );
                                    }
                                  ),
                                  1
                                )
                              : t._e(),
                            t._v(" "),
                            t.model.fields.pageFooter
                              ? n(
                                  "div",
                                  [
                                    t.getComponentTemplate(
                                      t.model.fields.pageFooter
                                    )
                                      ? n(
                                          t.getComponentTemplate(
                                            t.model.fields.pageFooter
                                          ),
                                          {
                                            tag: "component",
                                            attrs: {
                                              model: t.model.fields.pageFooter,
                                            },
                                          }
                                        )
                                      : t._e(),
                                  ],
                                  1
                                )
                              : t._e(),
                          ]
                        ),
                      ]
                    )
                  : t._e(),
              ],
              1
            );
          },
          [],
          !1,
          null,
          null,
          null
        ).exports,
        Oi = {
          name: "project-header",
          data: function () {
            return {
              textDelay: 0.1,
            };
          },
          props: ["model"],
          mixins: [W],
          watch: {
            lines: function () {
              this.toggleWaypointWithLines({
                el: this.$el,
                visible: !0,
              });
            },
          },
          computed: Object(o.a)(
            {
              multiplier: function () {
                return this.pageWidth < 1200 ? 1.05 : 1;
              },
              pageWidth: function () {
                return this.$store.getters["app/getState"]("width");
              },
              projectTitle: function () {
                return this.model.fields.headerTitle || this.model.fields.title;
              },
              category: function () {
                return L()(this.model, "fields.category");
              },
            },
            "pageWidth",
            function () {
              return this.$store.getters["app/getState"]("width");
            }
          ),
        },
        Pi =
          (n(759),
          Object(R.a)(
            Oi,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "waypoint",
                {
                  attrs: {
                    "data-scroll-section": "",
                    y: t.elementY,
                  },
                  on: {
                    toggleOneDirectionVisible: t.toggleWaypointWithLines,
                  },
                },
                [
                  n(
                    "proportion-div",
                    {
                      staticClass:
                        "project-header d-md-flex flex-center color-white pt-4 pt-md-5 position-relative h-100 overflow-hidden",
                      attrs: {
                        proportion: t.pageWidth > 500 ? 1.15 : 2.6,
                      },
                    },
                    [
                      n("div", {
                        staticClass:
                          "overlay position-absolute t-0 l-0 w-100 h-100 on-top",
                      }),
                      t._v(" "),
                      n("image-div", {
                        staticClass:
                          "h-100 w-100 image should-animate position-absolute l-0 t-0",
                        attrs: {
                          innerClasses: "transition-out-image",
                          "data-props": "scale",
                          src: t.getImage(
                            t.model,
                            "backgroundImage",
                            "?w=1920",
                            "?h=1200&w=420&fit=fill",
                            "80"
                          ),
                        },
                      }),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass:
                            "position-relative on-top d-flex flex-center flex-column py-5",
                        },
                        [
                          t.projectTitle
                            ? n(
                                "split-text",
                                {
                                  key: t.pageWidth,
                                  staticClass:
                                    "text-center project-headline px-2 px-md-0 w-100",
                                  on: {
                                    splited: t.onSplited,
                                  },
                                },
                                [
                                  n("h1", {
                                    staticClass: "display-2 antiqua-font lh-09",
                                    domProps: {
                                      innerHTML: t._s(t.projectTitle),
                                    },
                                  }),
                                ]
                              )
                            : t._e(),
                          t._v(" "),
                          n(
                            "div",
                            {
                              staticClass:
                                "location-wrapper founders-regular no-wrap text-center text-md-left",
                              style: {
                                left:
                                  t.model.fields.locationPositionX *
                                    t.multiplier +
                                  "vw",
                                top:
                                  t.model.fields.locationPositionY *
                                    t.multiplier +
                                  "vw",
                              },
                            },
                            [
                              t.model.fields.location
                                ? n("h6", {
                                    staticClass:
                                      "small mt-2 mt-md-0 should-animate",
                                    attrs: {
                                      "data-preset": "opacity,x",
                                      "data-delay": ".4",
                                    },
                                    domProps: {
                                      innerHTML: t._s(t.model.fields.location),
                                    },
                                  })
                                : t._e(),
                              t._v(" "),
                              t.model.fields.date
                                ? n("h6", {
                                    staticClass: "small should-animate",
                                    attrs: {
                                      "data-preset": "opacity,x",
                                      "data-delay": ".5",
                                    },
                                    domProps: {
                                      innerHTML: t._s(t.model.fields.date),
                                    },
                                  })
                                : t._e(),
                            ]
                          ),
                          t._v(" "),
                          n(
                            "h6",
                            {
                              staticClass:
                                "tiny mb-1 mt-3 mt-md-4 should-animate",
                              attrs: {
                                "data-preset": "opacity,y",
                                "data-delay": ".6",
                              },
                            },
                            [t._v(t._s(t.$t("categories")))]
                          ),
                          t._v(" "),
                          n(
                            "div",
                            {
                              staticClass:
                                "category-list d-flex should-animate",
                              attrs: {
                                "data-preset": "opacity,y",
                                "data-delay": ".7",
                              },
                            },
                            t._l(t.category, function (e, r) {
                              return n(
                                "h6",
                                {
                                  staticClass:
                                    "mediumv2 founders-regular uppercase link-line",
                                },
                                [t._v(t._s(e)), n("span")]
                              );
                            }),
                            0
                          ),
                        ],
                        1
                      ),
                    ],
                    1
                  ),
                ],
                1
              );
            },
            [],
            !1,
            null,
            "279f9375",
            null
          ).exports),
        Li = n(430),
        ki = n.n(Li),
        Si = {
          name: "headline-text-and-image",
          data: function () {
            return {
              textDelay: 0.2,
            };
          },
          props: ["model", "isBlack"],
          mixins: [W],
          computed: {
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
            images: function () {
              return L()(this.model, "fields.images");
            },
            hasIcon: function () {
              return L()(this.model, "fields.hasIcon");
            },
            swiperOptions: function () {
              return {
                loop: !1,
                effect: "slide",
                autoplay: !1,
                autoHeight: !1,
                grabCursor: !0,
                slidesPerView: "1",
                speed: 1200,
                roundLengths: !0,
                pagination: {
                  el: ".swiper-pagination",
                  type: "fraction",
                },
                navigation: {
                  nextEl: ".swiper-next",
                  prevEl: ".swiper-prev",
                },
                parallax: !0,
              };
            },
          },
          components: {
            StarIcon: $t.a,
            Arrow: ki.a,
          },
        },
        Ii =
          (n(760),
          Object(R.a)(
            Si,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "waypoint",
                {
                  staticClass:
                    "headline-text-and-image d-flex flex-column flex-center position-relative",
                  attrs: {
                    "data-scroll-section": "",
                    y: t.elementY,
                  },
                  on: {
                    toggleOneDirectionVisible: t.toggleWaypointWithLines,
                  },
                },
                [
                  t.images.length > 1
                    ? n(
                        "div",
                        {
                          staticClass: "pb-3 pb-md-5 w-100",
                          class: {
                            "px-md-3": t.images.length > 1,
                          },
                        },
                        [
                          n(
                            "div",
                            {
                              staticClass: "d-flex flex-center",
                            },
                            [
                              t.model.fields.content
                                ? n("div", {
                                    staticClass:
                                      "content medium text-center px-2 px-md-0 py-3 py-md-6",
                                    domProps: {
                                      innerHTML: t._s(t.model.fields.content),
                                    },
                                  })
                                : t._e(),
                            ]
                          ),
                          t._v(" "),
                          n(
                            "div",
                            {
                              staticClass:
                                "d-flex flex-md-column flex-column-reverse w-100",
                            },
                            [
                              n(
                                "div",
                                {
                                  staticClass:
                                    "d-flex justify-content-between position-relative px-1 px-md-4",
                                },
                                [
                                  n("div", {
                                    staticClass:
                                      "swiper-pagination position-relative d-flex medium align-items-end",
                                  }),
                                  t._v(" "),
                                  n(
                                    "div",
                                    {
                                      staticClass: "d-flex",
                                    },
                                    [
                                      n(
                                        "div",
                                        {
                                          staticClass: "swiper-prev",
                                        },
                                        [
                                          n("arrow", {
                                            staticClass:
                                              "arrow left mr-1 mr-md-2",
                                            class: {
                                              black: !t.isBlack,
                                              white: t.isBlack,
                                            },
                                          }),
                                        ],
                                        1
                                      ),
                                      t._v(" "),
                                      n(
                                        "div",
                                        {
                                          staticClass: "swiper-next",
                                        },
                                        [
                                          n("arrow", {
                                            staticClass: "arrow right",
                                            class: {
                                              black: !t.isBlack,
                                              white: t.isBlack,
                                            },
                                          }),
                                        ],
                                        1
                                      ),
                                    ]
                                  ),
                                ]
                              ),
                              t._v(" "),
                              n(
                                "swiper",
                                {
                                  ref: "swiper",
                                  staticClass: "slider-wrapper w-100 px-md-4",
                                  attrs: {
                                    customOptions: t.swiperOptions,
                                    overflowVisible: !1,
                                  },
                                },
                                t._l(t.images, function (image, e) {
                                  return n(
                                    "div",
                                    {
                                      key: e,
                                      staticClass:
                                        "swiper-slide w-100 overflow-hidden",
                                    },
                                    [
                                      n("img", {
                                        staticClass: "slide-bgimg",
                                        attrs: {
                                          src: t.getImage(
                                            image,
                                            "img",
                                            "?w=1900",
                                            "?w=1200"
                                          ),
                                        },
                                      }),
                                    ]
                                  );
                                }),
                                0
                              ),
                            ],
                            1
                          ),
                        ]
                      )
                    : n(
                        "div",
                        {
                          staticClass: "d-flex flex-column flex-center",
                          class: {
                            "pt-2 pt-md-6 pb-md-5": t.hasIcon,
                            "w-100": !t.hasIcon,
                          },
                        },
                        [
                          n("star-icon", {
                            staticClass:
                              "star-icon align-md-self-start align-self-center mb-3 mb-md-5 should-animate",
                            class: {
                              black: !t.isBlack,
                              white: t.isBlack,
                              "d-block": t.hasIcon,
                              "d-none": !t.hasIcon,
                            },
                            attrs: {
                              "data-fromscale": "0",
                              "data-toscale": "1",
                              "data-props": "scale",
                              "data-delay": ".1",
                            },
                          }),
                          t._v(" "),
                          t.model.fields.headline
                            ? n(
                                "split-text",
                                {
                                  key: t.pageWidth,
                                  staticClass:
                                    "antiqua-font uppercase headline text-center px-md-0 px-2 lh-13 should-animate h6",
                                  on: {
                                    splited: t.onSplited,
                                  },
                                },
                                [
                                  n("h6", {
                                    domProps: {
                                      innerHTML: t._s(t.model.fields.headline),
                                    },
                                  }),
                                ]
                              )
                            : t._e(),
                          t._v(" "),
                          t.model.fields.content
                            ? n("div", {
                                staticClass:
                                  "content text-center px-2 px-xl-5 py-3 py-md-6 should-animate",
                                class: {
                                  medium: !t.hasIcon,
                                },
                                attrs: {
                                  "data-preset": "opacity,y",
                                  "data-delay": ".6",
                                },
                                domProps: {
                                  innerHTML: t._s(t.model.fields.content),
                                },
                              })
                            : t._e(),
                          t._v(" "),
                          n(
                            "div",
                            {
                              staticClass:
                                "image-wrapper w-100 px-4 pb-3 px-md-0 py-md-0 overflow-hidden",
                            },
                            t._l(t.images, function (image, e) {
                              return n("image-div", {
                                key: e,
                                staticClass: "should-animate",
                                attrs: {
                                  innerClasses: "transition-out-image",
                                  "data-props": "scale",
                                  "data-dur": "2.5",
                                  delay: 0.4,
                                  proportion: t.pageWidth < 320 ? 1 : 0.65,
                                  src: t.getImage(image, "img", "?w=1920"),
                                },
                              });
                            }),
                            1
                          ),
                        ],
                        1
                      ),
                ]
              );
            },
            [],
            !1,
            null,
            "40c5046b",
            null
          ).exports),
        ji = {
          name: "fullwidth-image",
          props: ["model"],
          mixins: [D, W],
          methods: {
            image: function (t) {
              return this.getImage(this.images[t], "img", "?w=1024");
            },
          },
          computed: {
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
            images: function () {
              return L()(this.model, "fields.images");
            },
            biggerHeight: function () {
              return L()(this.model, "fields.biggerHeight");
            },
            imageBottom: function () {
              return L()(this.model, "fields.imageBottom");
            },
            imageTop: function () {
              return L()(this.model, "fields.imageTop");
            },
          },
        },
        Ti =
          (n(761),
          Object(R.a)(
            ji,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "div",
                {
                  staticClass: "fullwidth-image position-relative h-100",
                  class: {
                    "bigger-height": t.biggerHeight,
                    "normal-height": !t.biggerHeight,
                  },
                  attrs: {
                    "data-scroll-section": "",
                  },
                },
                [
                  t.model.fields.backgroundImage
                    ? n(
                        "waypoint",
                        {
                          staticClass: "overflow-hidden",
                          on: {
                            toggleOneDirectionVisible: t.toggleWaypoint,
                          },
                        },
                        [
                          n("image-div", {
                            staticClass: "w-100 should-animate",
                            attrs: {
                              "data-props": "scale",
                              bgBottom: t.imageBottom,
                              bgTop: t.imageTop,
                              keepProportion: !0,
                              innerClasses: "transition-out-image",
                              src: t.getImage(
                                t.model,
                                "backgroundImage",
                                "?w=1920",
                                "?h=1000"
                              ),
                            },
                          }),
                        ],
                        1
                      )
                    : n(
                        "waypoint",
                        {
                          staticClass: "d-md-flex",
                          on: {
                            toggleOneDirectionVisible: t.toggleWaypoint,
                          },
                        },
                        [
                          n(
                            "div",
                            {
                              staticClass:
                                "col-md-6 overflow-hidden pb-1 pb-md-0",
                            },
                            [
                              t.image
                                ? n("image-div", {
                                    staticClass: "w-100 h-100 should-animate",
                                    attrs: {
                                      innerClasses: "transition-out-image",
                                      proportion: (t.pageWidth, 1.2),
                                      src: t.image(0),
                                      "data-props": "scale",
                                    },
                                  })
                                : t._e(),
                            ],
                            1
                          ),
                          t._v(" "),
                          n(
                            "div",
                            {
                              staticClass: "col-md-6 overflow-hidden",
                            },
                            [
                              t.image
                                ? n("image-div", {
                                    staticClass: "w-100 h-100 should-animate",
                                    attrs: {
                                      innerClasses: "transition-out-image",
                                      proportion: (t.pageWidth, 1.2),
                                      src: t.image(1),
                                      "data-props": "scale",
                                    },
                                  })
                                : t._e(),
                            ],
                            1
                          ),
                        ]
                      ),
                ],
                1
              );
            },
            [],
            !1,
            null,
            "eac81e64",
            null
          ).exports),
        $i = {
          name: "two-image",
          props: ["model", "isBlack"],
          mixins: [W],
          methods: {
            image: function (t) {
              return this.getImage(this.images[t], "img", "?w=1024");
            },
          },
          computed: {
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
            images: function () {
              return L()(this.model, "fields.images");
            },
            isReverse: function () {
              return L()(this.model, "fields.isReverse");
            },
            hasIcon: function () {
              return L()(this.model, "fields.hasIcon");
            },
            imgTop: function () {
              return L()(this.model, "fields.imgTop");
            },
          },
          components: {
            MemoryIcon: te.a,
          },
        },
        Ei =
          (n(762),
          Object(R.a)(
            $i,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "waypoint",
                {
                  staticClass: "two-image mb-md-1",
                  attrs: {
                    "data-scroll-section": "",
                    y: t.elementY,
                  },
                  on: {
                    toggleOneDirectionVisible: t.toggleWaypoint,
                  },
                },
                [
                  n(
                    "div",
                    {
                      staticClass: "text-center",
                      class: {
                        "d-none": !t.hasIcon,
                        "d-flex justify-content-center": t.hasIcon,
                      },
                    },
                    [
                      n("memory-icon", {
                        staticClass: "memory-icon my-3 my-md-6 should-animate",
                        class: {
                          black: !t.isBlack,
                          white: t.isBlack,
                        },
                        attrs: {
                          "data-props": "opacity,rotate",
                          "data-delay": ".1",
                          "data-fromrotate": "-180",
                          "data-torotate": "0",
                          "data-dur": "2",
                        },
                      }),
                    ],
                    1
                  ),
                  t._v(" "),
                  n(
                    "div",
                    {
                      staticClass: "d-flex flex-column",
                      class: {
                        "flex-md-row-reverse": t.isReverse,
                        "flex-md-row flex-column-reverse": !t.isReverse,
                      },
                    },
                    [
                      n(
                        "div",
                        {
                          staticClass: "col-md-6 d-flex",
                          class: {
                            "justify-content-start align-items-end pl-md-1":
                              !t.isReverse && !t.imgTop,
                            "justify-content-end align-items-end pr-md-1":
                              t.isReverse && !t.imgTop,
                            "align-items-start py-1 px-1":
                              !t.isReverse && t.imgTop,
                          },
                        },
                        [
                          n(
                            "div",
                            {
                              staticClass: "col-md-6 mb-md-0 overflow-hidden",
                            },
                            [
                              n("image-div", {
                                staticClass: "should-animate",
                                attrs: {
                                  innerClasses: "transition-out-image",
                                  "data-props": "scale",
                                  keepProportion: !0,
                                  proportion: t.pageWidth < 320 ? 1 : 1.3,
                                  src: t.image(0),
                                },
                              }),
                            ],
                            1
                          ),
                        ]
                      ),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass: "col-md-6 overflow-hidden",
                        },
                        [
                          n("image-div", {
                            staticClass: "should-animate",
                            attrs: {
                              innerClasses: "transition-out-image",
                              "data-props": "scale",
                              keepProportion: !0,
                              proportion: t.pageWidth < 320 ? 1 : 1.3,
                              src: t.image(1),
                            },
                          }),
                        ],
                        1
                      ),
                    ]
                  ),
                ]
              );
            },
            [],
            !1,
            null,
            "52920d21",
            null
          ).exports),
        Ai = {
          name: "two-image-and-text",
          data: function () {
            return {
              debug: !0,
            };
          },
          props: ["model", "isBlack"],
          mixins: [W],
          methods: {
            image: function (t) {
              return (
                !!this.images[t] &&
                this.getImage(this.images[t], "image", "?w=1920")
              );
            },
          },
          computed: {
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
            images: function () {
              return L()(this.model, "fields.images");
            },
            isReverse: function () {
              return L()(this.model, "fields.isReverse");
            },
            hasIcon: function () {
              return L()(this.model, "fields.hasIcon");
            },
          },
          components: {
            MemoryIcon: te.a,
          },
        },
        Mi =
          (n(763),
          Object(R.a)(
            Ai,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "waypoint",
                {
                  staticClass:
                    "two-image-and-text mt-1 mb-1 pt-md-0 pb-4 pb-md-0 position-relative",
                  attrs: {
                    "data-scroll-section": "",
                    y: t.elementY,
                  },
                  on: {
                    toggleOneDirectionVisible: t.toggleWaypoint,
                  },
                },
                [
                  n(
                    "div",
                    {
                      staticClass: "text-center",
                      class: {
                        "d-none": !t.hasIcon,
                        "d-flex justify-content-center": t.hasIcon,
                      },
                    },
                    [
                      n("memory-icon", {
                        staticClass: "memory-icon my-3 my-md-5",
                        class: {
                          black: !t.isBlack,
                          white: t.isBlack,
                        },
                      }),
                    ],
                    1
                  ),
                  t._v(" "),
                  n(
                    "div",
                    {
                      staticClass: "d-md-flex h-100",
                      class: {
                        "flex-row-reverse": t.isReverse,
                        "flex-row": !t.isReverse,
                      },
                    },
                    [
                      n(
                        "div",
                        {
                          staticClass:
                            "col-md-6 d-flex flex-column px-2 pl-md-1 pr-md-1",
                        },
                        [
                          n(
                            "div",
                            {
                              staticClass: "col-xl overflow-hidden mb-1",
                            },
                            [
                              n("image-div", {
                                staticClass: "h-100 image should-animate",
                                attrs: {
                                  innerClasses: "transition-out-image",
                                  "data-props": "scale",
                                  proportion: 0.6,
                                  src: t.image(0),
                                },
                              }),
                            ],
                            1
                          ),
                          t._v(" "),
                          t.image(1) || t.image(2)
                            ? n(
                                "div",
                                {
                                  staticClass: "col-xl overflow-hidden",
                                },
                                [
                                  t.images.length <= 2
                                    ? n(
                                        "div",
                                        {
                                          staticClass: "h-100",
                                        },
                                        [
                                          n("image-div", {
                                            staticClass:
                                              "h-100 should-animate image w-100",
                                            attrs: {
                                              proportion: 0.6,
                                              innerClasses:
                                                "transition-out-image",
                                              "data-props": "scale",
                                              src: t.image(1),
                                            },
                                          }),
                                        ],
                                        1
                                      )
                                    : n(
                                        "div",
                                        {
                                          staticClass: "d-flex h-100 w-100",
                                        },
                                        [
                                          n(
                                            "div",
                                            {
                                              staticClass: "col-md-6",
                                            },
                                            [
                                              n("image-div", {
                                                staticClass:
                                                  "h-100 should-animate image",
                                                attrs: {
                                                  proportion: 1.1,
                                                  innerClasses:
                                                    "transition-out-image",
                                                  "data-props": "scale",
                                                  src: t.image(1),
                                                },
                                              }),
                                            ],
                                            1
                                          ),
                                          t._v(" "),
                                          n(
                                            "div",
                                            {
                                              staticClass: "col-md-6",
                                            },
                                            [
                                              n("image-div", {
                                                staticClass:
                                                  "h-100 should-animate image",
                                                attrs: {
                                                  proportion: 1.1,
                                                  innerClasses:
                                                    "transition-out-image",
                                                  "data-props": "scale",
                                                  src: t.image(2),
                                                },
                                              }),
                                            ],
                                            1
                                          ),
                                        ]
                                      ),
                                ]
                              )
                            : t._e(),
                        ]
                      ),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass:
                            "col-md-6 d-flex flex-center mt-3 mt-md-0 px-2 px-md-0 py-md-3 content-wrapper",
                        },
                        [
                          t.model.fields.content
                            ? n("div", {
                                staticClass:
                                  "medium mobile-font content antiqua-font px-md-1 text-center should-animate",
                                attrs: {
                                  "data-props": "opacity",
                                  "data-dur": "1.4",
                                  "data-ease": "Power2.easeIn",
                                  "data-debug": "true",
                                },
                                domProps: {
                                  innerHTML: t._s(t.model.fields.content),
                                },
                              })
                            : t._e(),
                        ]
                      ),
                    ]
                  ),
                ]
              );
            },
            [],
            !1,
            null,
            "50ff1f3e",
            null
          ).exports),
        Di = {
          name: "three-image",
          props: ["model", "isBlack"],
          mixins: [W],
          methods: {
            image: function (t) {
              return this.getImage(this.images[t], "image", "?w=980");
            },
          },
          computed: {
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
            images: function () {
              return L()(this.model, "fields.images");
            },
            isReverse: function () {
              return L()(this.model, "fields.isReverse");
            },
            hasIcon: function () {
              return L()(this.model, "fields.hasIcon");
            },
          },
          components: {
            MemoryIcon: te.a,
          },
        },
        Wi =
          (n(764),
          Object(R.a)(
            Di,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "waypoint",
                {
                  staticClass: "three-image mb-1 mt-md-1",
                  attrs: {
                    "data-scroll-section": "",
                    y: t.elementY,
                  },
                  on: {
                    toggleOneDirectionVisible: t.toggleWaypoint,
                  },
                },
                [
                  n(
                    "div",
                    {
                      staticClass: "text-center",
                      class: {
                        "d-none": !t.hasIcon,
                        "d-flex justify-content-center": t.hasIcon,
                      },
                    },
                    [
                      n("memory-icon", {
                        staticClass: "memory-icon my-3 my-md-5 should-animate",
                        class: {
                          black: !t.isBlack,
                          white: t.isBlack,
                        },
                        attrs: {
                          "data-props": "opacity,rotate",
                          "data-delay": ".6",
                          "data-fromrotate": "-180",
                          "data-torotate": "0",
                          "data-dur": "2",
                        },
                      }),
                    ],
                    1
                  ),
                  t._v(" "),
                  n(
                    "div",
                    {
                      staticClass: "d-md-flex",
                      class: {
                        "flex-column flex-row-reverse": t.isReverse,
                        "flex-row": !t.isReverse,
                      },
                    },
                    [
                      n(
                        "div",
                        {
                          staticClass:
                            "col-md-6 d-flex px-1 px-md-0 py-1 py-md-0 image-wrapper",
                        },
                        [
                          n(
                            "div",
                            {
                              staticClass:
                                "col-md-6 h-100 mr-7 mr-md-0 pl-md-1",
                            },
                            [
                              n(
                                "div",
                                {
                                  staticClass: "overflow-hidden",
                                },
                                [
                                  n("image-div", {
                                    staticClass: "should-animate",
                                    attrs: {
                                      innerClasses: "transition-out-image",
                                      "data-props": "scale",
                                      proportion:
                                        t.pageWidth < 768 ? 1.36 : 1.3,
                                      src: t.image(0),
                                    },
                                  }),
                                ],
                                1
                              ),
                            ]
                          ),
                          t._v(" "),
                          n(
                            "div",
                            {
                              staticClass: "col-md-6 ml-7 ml-md-0 pl-md-1",
                              class: {
                                "pr-md-1": t.isReverse,
                              },
                            },
                            [
                              n(
                                "div",
                                {
                                  staticClass: "overflow-hidden",
                                },
                                [
                                  n("image-div", {
                                    staticClass: "should-animate",
                                    attrs: {
                                      innerClasses: "transition-out-image",
                                      "data-props": "scale",
                                      proportion: t.isReverse ? 1.36 : 1.3,
                                      src: t.image(1),
                                    },
                                  }),
                                ],
                                1
                              ),
                            ]
                          ),
                        ]
                      ),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass: "col-md-6 pl-md-1",
                          class: {
                            "pr-md-1": !t.isReverse,
                          },
                        },
                        [
                          n(
                            "div",
                            {
                              staticClass: "overflow-hidden",
                            },
                            [
                              n("image-div", {
                                staticClass: "should-animate",
                                attrs: {
                                  innerClasses: "transition-out-image",
                                  "data-props": "scale",
                                  keepProportion: !0,
                                  src: t.image(2),
                                },
                              }),
                            ],
                            1
                          ),
                        ]
                      ),
                    ]
                  ),
                ]
              );
            },
            [],
            !1,
            null,
            "c61a3636",
            null
          ).exports),
        Hi = {
          name: "centered-image",
          props: ["model"],
          mixins: [W],
          methods: {
            image: function (t) {
              return this.getImage(this.images[t], "image", "?w=980");
            },
          },
          computed: {
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
            images: function () {
              return L()(this.model, "fields.images");
            },
            isReverse: function () {
              return L()(this.model, "fields.isReverse");
            },
          },
        },
        Ri =
          (n(765),
          Object(R.a)(
            Hi,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "waypoint",
                {
                  staticClass: "centered-image d-flex px-2 px-md-0 my-1",
                  class: {
                    "flex-row-reverse": t.isReverse,
                    "flex-row": !t.isReverse,
                  },
                  attrs: {
                    "data-scroll-section": "",
                    y: t.elementY,
                  },
                  on: {
                    toggleOneDirectionVisible: t.toggleWaypoint,
                  },
                },
                [
                  n(
                    "div",
                    {
                      staticClass: "col-6 px-7 px-md-0 overflow-hidden pl-md-1",
                      class: {
                        "px-md-1": t.isReverse,
                      },
                    },
                    [
                      n(
                        "div",
                        {
                          staticClass: "overflow-hidden",
                        },
                        [
                          n("image-div", {
                            staticClass: "should-animate",
                            attrs: {
                              "data-props": "scale",
                              keepProportion: !0,
                              src: t.image(0),
                            },
                          }),
                        ],
                        1
                      ),
                    ]
                  ),
                  t._v(" "),
                  n(
                    "div",
                    {
                      staticClass: "col-6 d-flex flex-center px-7 px-md-0",
                    },
                    [
                      n(
                        "div",
                        {
                          staticClass: "col-md-5 overflow-hidden",
                        },
                        [
                          n("img", {
                            staticClass: "should-animate transition-out-image",
                            attrs: {
                              "data-props": "scale",
                              src: t.image(1),
                            },
                          }),
                        ]
                      ),
                    ]
                  ),
                ]
              );
            },
            [],
            !1,
            null,
            "b8ce2914",
            null
          ).exports),
        zi = {
          name: "gallery-three-image",
          props: ["model"],
          mixins: [W],
          methods: {
            image: function (t) {
              return this.getImage(this.images[t], "image", "?w=980");
            },
          },
          computed: {
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
            images: function () {
              return L()(this.model, "fields.images");
            },
            bgColor: function () {
              return L()(this.model, "fields.bgColor");
            },
            topImages: function () {
              return L()(this.model, "fields.topImages");
            },
            smallImageMobile: function () {
              return L()(this.model, "fields.smallImageMobile");
            },
          },
        },
        Fi =
          (n(766),
          Object(R.a)(
            zi,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "waypoint",
                {
                  staticClass: "gallery-three-image position-relative",
                  class: {
                    "bg-greyv6": "LightGrey" == t.bgColor,
                    "bg-greyv5": "Grey" == t.bgColor,
                    "bg-olivev4": "Olive" == t.bgColor,
                    "bg-biege": "Beige" == t.bgColor,
                    "bg-brown": "Brown" == t.bgColor,
                    "bg-black": "Black" == t.bgColor,
                    "bg-whitev2": "White" == t.bgColor,
                  },
                  attrs: {
                    "data-scroll-section": "",
                    y: t.elementY,
                  },
                  on: {
                    toggleOneDirectionVisible: t.toggleWaypoint,
                  },
                },
                [
                  n("image-div", {
                    staticClass:
                      "h-100 w-100 image position-absolute l-0 t-0 d-none d-md-block",
                    attrs: {
                      src: t.getImage(
                        t.model,
                        "bgTexture",
                        "?w=1920",
                        "?h=1000"
                      ),
                    },
                  }),
                  t._v(" "),
                  t.model.fields.content
                    ? n(
                        "div",
                        {
                          staticClass:
                            "d-flex justify-content-center pt-3 pt-md-6",
                        },
                        [
                          n("h6", {
                            staticClass:
                              "content lh-15 text-center medium pb-2 pb-md-0 px-1 px-md-0",
                            domProps: {
                              innerHTML: t._s(t.model.fields.content),
                            },
                          }),
                        ]
                      )
                    : t._e(),
                  t._v(" "),
                  n(
                    "div",
                    {
                      staticClass: "d-md-flex top-wrapper",
                    },
                    [
                      n(
                        "div",
                        {
                          staticClass: "col-md-6 d-flex py-md-0",
                          class: {
                            "p-md-1": t.topImages,
                            "flex-center": !t.topImages,
                            "py-3": t.smallImageMobile,
                            "py-1": !t.smallImageMobile,
                          },
                        },
                        [
                          n(
                            "div",
                            {
                              staticClass: "col-md-4 overflow-hidden",
                              class: {
                                "col-6": t.smallImageMobile,
                              },
                            },
                            [
                              n("image-div", {
                                staticClass: "should-animate",
                                attrs: {
                                  innerClasses: "transition-out-image",
                                  "data-props": "scale",
                                  proportion: (t.pageWidth, 0.7),
                                  src: t.image(0),
                                },
                              }),
                            ],
                            1
                          ),
                        ]
                      ),
                      t._v(" "),
                      n("div", {
                        staticClass: "col-md-6",
                      }),
                    ]
                  ),
                  t._v(" "),
                  n(
                    "div",
                    {
                      staticClass: "d-md-flex",
                    },
                    [
                      n(
                        "div",
                        {
                          staticClass: "col-md-6 pt-md-2 pl-md-1 pb-1",
                        },
                        [
                          n(
                            "div",
                            {
                              staticClass: "overflow-hidden",
                            },
                            [
                              n("image-div", {
                                staticClass: "should-animate",
                                attrs: {
                                  innerClasses: "transition-out-image",
                                  "data-props": "scale",
                                  proportion: t.pageWidth < 768 ? 1.5 : 1.4,
                                  src: t.image(1),
                                },
                              }),
                            ],
                            1
                          ),
                        ]
                      ),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass: "col-md-6 d-flex",
                          class: {
                            "justify-content-end pt-md-2 pb-1 pb-md-0":
                              t.topImages,
                            "justify-content-center align-items-end pb-1 p-md-2 pb-md-0":
                              !t.topImages,
                          },
                        },
                        [
                          n(
                            "div",
                            {
                              staticClass: "col-md-5 pb-md-6 overflow-hidden",
                            },
                            [
                              n("image-div", {
                                staticClass: "should-animate",
                                attrs: {
                                  innerClasses: "transition-out-image",
                                  "data-props": "scale",
                                  proportion: t.pageWidth < 768 ? 0.7 : 1.45,
                                  src: t.image(2),
                                },
                              }),
                              t._v(" "),
                              t.model.fields.imageCaption
                                ? n("h6", {
                                    staticClass:
                                      "medium antiqua-font text-center mt-1 pb-3 pb-md-0",
                                    domProps: {
                                      innerHTML: t._s(
                                        t.model.fields.imageCaption
                                      ),
                                    },
                                  })
                                : t._e(),
                            ],
                            1
                          ),
                        ]
                      ),
                    ]
                  ),
                ],
                1
              );
            },
            [],
            !1,
            null,
            "74ce40ba",
            null
          ).exports),
        Xi = {
          name: "more-projects",
          props: ["projects"],
          mixins: [W],
        },
        Bi =
          (n(767),
          Object(R.a)(
            Xi,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "waypoint",
                {
                  staticClass:
                    "more-projects d-flex flex-center flex-column pt-6 bg-black color-white",
                  attrs: {
                    "data-scroll-section": "",
                    y: t.elementY,
                    "data-scroll-section": "",
                  },
                  on: {
                    toggleOneDirectionVisible: t.toggleWaypoint,
                  },
                },
                [
                  n(
                    "h3",
                    {
                      staticClass:
                        "antiqua-font display-4 mt-md-3 mb-2 mb-md-0 lh-1 text-center",
                    },
                    [t._v("Explore more projects")]
                  ),
                  t._v(" "),
                  n(
                    "p",
                    {
                      staticClass: "text-center lh-12",
                    },
                    [
                      t._v("I want the connection,"),
                      n("br"),
                      t._v("the vulnerability."),
                    ]
                  ),
                  t._v(" "),
                  n(
                    "div",
                    {
                      staticClass: "row w-100 px-2 mt-4",
                    },
                    t._l(t.projects, function (e, r) {
                      return n(
                        "nuxt-link",
                        {
                          key: r,
                          staticClass:
                            "col-md-6 p-small mb-2 mb-md-0 project-link",
                          attrs: {
                            to: e.url,
                          },
                        },
                        [
                          n(
                            "div",
                            {
                              staticClass: "image-wrapper overflow-hidden",
                            },
                            [
                              n("image-div", {
                                staticClass: "image",
                                attrs: {
                                  src: t.getImage(e, "previewImage", "?w=960"),
                                  proportion: 1,
                                },
                              }),
                            ],
                            1
                          ),
                          t._v(" "),
                          n("h5", {
                            staticClass: "antiqua-font mt-1",
                            domProps: {
                              innerHTML: t._s(e.fields.title),
                            },
                          }),
                        ]
                      );
                    }),
                    1
                  ),
                ]
              );
            },
            [],
            !1,
            null,
            "16d5f5fc",
            null
          ).exports),
        filter = n(217),
        Ni = n.n(filter),
        qi = n(271),
        Yi = n.n(qi),
        Vi = {
          name: "Project",
          components: {
            ProjectHeader: Pi,
            HeadlineTextAndImage: Ii,
            FullwidthImage: Ti,
            TwoImage: Ei,
            TwoImageAndText: Mi,
            ThreeImage: Wi,
            GalleryThreeImage: Fi,
            CenteredImage: Ri,
            MoreProjects: Bi,
            PageFooter: Jt,
          },
          mixins: [xi],
          data: function () {
            return {};
          },
          computed: {
            projects: function () {
              return Ni()(this.pages, function (p) {
                return "project" == L()(p, "sys.contentType.sys.id");
              });
            },
            footerModel: function () {
              return this.$store.getters["pages/getState"]("footer");
            },
            isBlack: function () {
              return L()(this.model, "fields.isBlack");
            },
          },
          watch: {},
          mounted: function () {
            console.log("NEXT CASE", this.moreProjects);
          },
          asyncData: function (t) {
            return Object(r.a)(
              regeneratorRuntime.mark(function e() {
                var n, r, o, l, c, d, h, f;
                return regeneratorRuntime.wrap(function (e) {
                  for (;;)
                    switch ((e.prev = e.next)) {
                      case 0:
                        return (
                          (n = t.store.getters["pages/getState"]("urls")),
                          (e.next = 3),
                          w.a.loadActivePage({
                            route: t.route.path,
                            id: t.route.name,
                            urls: n,
                            include: 4,
                          })
                        );
                      case 3:
                        if ((r = e.sent)) {
                          e.next = 7;
                          break;
                        }
                        return (
                          t.error({
                            statusCode: 404,
                            message:
                              "The server successfully processed the request and is not returning any content.",
                          }),
                          e.abrupt("return")
                        );
                      case 7:
                        return (
                          (e.next = 9), t.store.dispatch("pages/LOAD_PROJECTS")
                        );
                      case 9:
                        return (
                          (o = e.sent),
                          (l = Yi()(o, function (t, e) {
                            return t.sys.id == r.sys.id;
                          })),
                          (c = l == o.length - 1 ? 0 : l + 1),
                          (d = l > 0 ? l - 1 : o.length - 1),
                          (h = o[c]),
                          (f = o[d]),
                          t.store.dispatch("pages/SET_STATE", {
                            activePage: r,
                          }),
                          e.abrupt("return", {
                            model: r,
                            moreProjects: {
                              nextCase: h,
                              prevCase: f,
                            },
                          })
                        );
                      case 17:
                      case "end":
                        return e.stop();
                    }
                }, e);
              })
            )();
          },
          methods: {},
        },
        Ui = Object(R.a)(
          Vi,
          function () {
            var t = this,
              e = t.$createElement,
              n = t._self._c || e;
            return n(
              "div",
              {
                staticClass: "page page--project",
                class: {
                  "bg-black color-white": t.isBlack,
                  "bg-whitev2 color-black": !t.isBlack,
                },
              },
              [
                t.model
                  ? n(
                      "smooth-scroll",
                      {
                        staticClass: "h-100",
                        attrs: {
                          pageId: t.model.sys.id,
                        },
                      },
                      [
                        n(
                          "div",
                          {
                            staticClass: "components-wrapper overflow-hidden",
                          },
                          [
                            n("project-header", {
                              attrs: {
                                model: t.model,
                              },
                            }),
                            t._v(" "),
                            t.model.fields.components
                              ? n(
                                  "div",
                                  t._l(
                                    t.model.fields.components,
                                    function (component, e) {
                                      return n(
                                        t.getComponentTemplate(component),
                                        {
                                          key: e,
                                          tag: "component",
                                          attrs: {
                                            model: component,
                                            isBlack: t.isBlack,
                                          },
                                        }
                                      );
                                    }
                                  ),
                                  1
                                )
                              : t._e(),
                            t._v(" "),
                            n("more-projects", {
                              attrs: {
                                projects: t.moreProjects,
                              },
                            }),
                            t._v(" "),
                            n("page-footer", {
                              attrs: {
                                model: t.footerModel,
                              },
                            }),
                          ],
                          1
                        ),
                      ]
                    )
                  : t._e(),
              ],
              1
            );
          },
          [],
          !1,
          null,
          null,
          null
        ).exports,
        Zi = {
          name: "grid-item",
          props: ["model", "index"],
          methods: {
            getProportion: function (t) {
              return X()([1, 2, 3, 7, 13, 15], t) ? 0.7 : 0.8;
            },
            getAlign: function () {
              var t = X()([0, 1, 2, 4, 7, 9, 10, 14], this.index)
                ? "text-md-left"
                : "text-md-right";
              return (t +=
                0 == this.index
                  ? " pr-md-5"
                  : 4 == this.index
                  ? " pr-md-4"
                  : "");
            },
          },
          computed: {
            previewImage: function () {
              return this.getImage(this.model, "previewImage", "?w=1200");
            },
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
          },
        },
        Gi =
          (n(769),
          Object(R.a)(
            Zi,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "div",
                {
                  staticClass: "grid-item",
                },
                [
                  n(
                    "nuxt-link",
                    {
                      staticClass: "project-link d-block",
                      attrs: {
                        draggable: "false",
                        to: t.model.url,
                      },
                    },
                    [
                      n(
                        "div",
                        {
                          staticClass: "overflow-hidden outer-image",
                        },
                        [
                          n("image-div", {
                            staticClass: "img",
                            attrs: {
                              draggable: "false",
                              innerClasses:
                                "transition-in-image transition-out-image",
                              proportion: t.getProportion(t.index),
                              src: t.previewImage,
                            },
                          }),
                        ],
                        1
                      ),
                      t._v(" "),
                      t.model.fields.title
                        ? n("h2", {
                            staticClass:
                              "antiqua-font medium project-title mt-1 pb-3 pb-md-0 px-1 px-md-0",
                            class: t.getAlign(),
                            domProps: {
                              innerHTML: t._s(t.model.fields.title),
                            },
                          })
                        : t._e(),
                    ]
                  ),
                ],
                1
              );
            },
            [],
            !1,
            null,
            "6e67ef06",
            null
          ).exports),
        map = n(76),
        Ki = n.n(map),
        Ji = n(129),
        Qi = n.n(Ji),
        tn = n(218),
        en = n.n(tn),
        nn = n(431),
        rn = n.n(nn),
        an = {
          name: "portfolio-grid",
          props: ["projects"],
          mixins: [W],
          watch: {
            selectedCategory: function () {
              var t = this;
              this.$nextTick(function () {
                t.$bus.$emit("resize");
              });
            },
          },
          methods: {
            computeIndex: function (t) {
              return t < 17 ? t : t % 17;
            },
            gridItem: function (t) {
              return 1 == this.filteredItems.length
                ? "single-item"
                : "item" + t;
            },
            projectVisible: function (t) {
              return Qi()(this.filteredItems, function (e) {
                return t.sys.id == e.sys.id;
              });
            },
            showProject: function (t, e) {
              return !0;
            },
          },
          computed: {
            lastVisibleIndex: function () {
              var t = this,
                e = 0;
              return (
                O()(this.projects, function (p, n) {
                  Qi()(t.filteredItems, function (t, i) {
                    return t.sys.id == p.sys.id;
                  }) && (e = n);
                }),
                e
              );
            },
            speedFactors: function () {
              return Ki()(this.projects, function () {
                return -Math.random() / 2 - 0.02;
              });
            },
            selectedCategory: function () {
              return this.$store.state.route.query.category;
            },
            filteredItems: function () {
              var t = this;
              return this.selectedCategory
                ? Ni()(this.projects, function (p) {
                    var e = !1;
                    return (
                      O()(p.fields.category, function (n) {
                        n == t.selectedCategory && (e = !0);
                      }),
                      e
                    );
                  })
                : this.projects;
            },
            categories: function () {
              var t = [];
              return (
                O()(this.projects, function (e) {
                  O()(L()(e, "fields.category"), function (e) {
                    return t.push(e);
                  });
                }),
                en()(rn()(t))
              );
            },
          },
          components: {
            GridItem: Gi,
          },
        },
        sn =
          (n(775),
          {
            name: "Portfolio",
            components: {
              PortfolioGrid: Object(R.a)(
                an,
                function () {
                  var t = this,
                    e = t.$createElement,
                    n = t._self._c || e;
                  return n(
                    "waypoint",
                    {
                      staticClass:
                        "portfolio-grid bg-black color-white text-center position-relative pt-4 pt-md-6",
                      attrs: {
                        y: t.elementY,
                        "data-scroll-section": "",
                      },
                      on: {
                        toggleOneDirectionVisible: t.toggleWaypoint,
                      },
                    },
                    [
                      n("h6", {
                        staticClass: "lh-1 mb-2 small should-animate",
                        attrs: {
                          "data-preset": "opacity,y",
                          "data-fromy": "-40",
                          "data-delay": ".1",
                        },
                        domProps: {
                          innerHTML: t._s(t.$t("portfolioIntro")),
                        },
                      }),
                      t._v(" "),
                      n(
                        "h1",
                        {
                          staticClass:
                            "display-4 antiqua-font mb-2 mb-md-0 should-animate",
                          attrs: {
                            "data-preset": "opacity,y",
                            "data-fromy": "-40",
                            "data-delay": ".2",
                          },
                        },
                        [t._v(t._s(t.$t("allProjects")))]
                      ),
                      t._v(" "),
                      n(
                        "section",
                        {
                          staticClass:
                            "filters row justify-content-center w-100 mt-2 px-1 mb-2 mb-md-0",
                        },
                        [
                          n("router-link", {
                            staticClass:
                              "mx-small link-line no-hover mb-1 mb-md-0 text-uppercase should-animate",
                            class: {
                              selected: !t.selectedCategory,
                            },
                            attrs: {
                              "data-preset": "opacity,x",
                              "data-delay": 0.3,
                              to: {
                                query: null,
                              },
                            },
                            domProps: {
                              innerHTML: t._s("All"),
                            },
                          }),
                          t._v(" "),
                          t._l(t.categories, function (e, r) {
                            return n("router-link", {
                              key: r,
                              staticClass:
                                "mx-small link-line no-hover mb-1 mb-md-0 text-uppercase should-animate",
                              class: {
                                selected: e == t.selectedCategory,
                              },
                              attrs: {
                                "data-preset": "opacity,x",
                                "data-delay": 0.4 + r / 20,
                                to: {
                                  query: {
                                    category: e,
                                  },
                                },
                              },
                              domProps: {
                                innerHTML: t._s(e),
                              },
                            });
                          }),
                        ],
                        2
                      ),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass: "position-relative",
                        },
                        [
                          n(
                            "custom-transition",
                            {
                              attrs: {
                                mode: "out-in",
                                duration: 0.6,
                              },
                            },
                            [
                              n(
                                "section",
                                {
                                  key: t.selectedCategory || "all",
                                },
                                t._l(t.filteredItems, function (e, r) {
                                  return t.showProject(r, e)
                                    ? n(
                                        "parallax",
                                        {
                                          key: r,
                                          staticClass: "disabled h-auto",
                                          class: [
                                            t.gridItem(t.computeIndex(r)),
                                            {
                                              "hidden d-none d-md-block":
                                                !t.projectVisible(e),
                                            },
                                          ],
                                          attrs: {
                                            y: t.elementY,
                                            speedFactor:
                                              t.speedFactors[
                                                t.computeIndex(r)
                                              ] / 3,
                                            global: !0,
                                            startOffset: 150,
                                            wrapped: !1,
                                          },
                                        },
                                        [
                                          n(
                                            "mouse-parallax",
                                            {
                                              class:
                                                "plax-item-" +
                                                t.computeIndex(r) +
                                                " disabled",
                                              attrs: {
                                                global: !0,
                                                y: t.elementY,
                                                ratioX:
                                                  t.speedFactors[
                                                    t.computeIndex(r)
                                                  ],
                                                ratioY:
                                                  t.speedFactors[
                                                    t.computeIndex(r)
                                                  ],
                                              },
                                            },
                                            [
                                              n("grid-item", {
                                                staticClass: "enabled",
                                                attrs: {
                                                  model: e,
                                                  index: r,
                                                },
                                              }),
                                            ],
                                            1
                                          ),
                                        ],
                                        1
                                      )
                                    : t._e();
                                }),
                                1
                              ),
                            ]
                          ),
                        ],
                        1
                      ),
                    ]
                  );
                },
                [],
                !1,
                null,
                "5ba6d957",
                null
              ).exports,
              PageFooter: Jt,
            },
            mixins: [xi],
            data: function () {
              return {};
            },
            computed: {
              footerModel: function () {
                return this.$store.getters["pages/getState"]("footer");
              },
            },
            watch: {},
            asyncData: function (t) {
              return Object(r.a)(
                regeneratorRuntime.mark(function e() {
                  var n, r, o;
                  return regeneratorRuntime.wrap(function (e) {
                    for (;;)
                      switch ((e.prev = e.next)) {
                        case 0:
                          return (
                            (n = t.store.getters["pages/getState"]("urls")),
                            (e.next = 3),
                            w.a.loadActivePage({
                              route: t.route.path,
                              id: t.route.name,
                              urls: n,
                              include: 4,
                            })
                          );
                        case 3:
                          return (
                            (r = e.sent),
                            (e.next = 6),
                            t.store.dispatch("pages/LOAD_PROJECTS")
                          );
                        case 6:
                          return (
                            (o = e.sent),
                            t.store.dispatch("pages/SET_STATE", {
                              activePage: r,
                            }),
                            e.abrupt("return", {
                              model: r,
                              projects: o,
                            })
                          );
                        case 9:
                        case "end":
                          return e.stop();
                      }
                  }, e);
                })
              )();
            },
            methods: {},
            mounted: function () {},
          }),
        on = Object(R.a)(
          sn,
          function () {
            var t = this.$createElement,
              e = this._self._c || t;
            return e(
              "div",
              {
                staticClass: "page page--portfolio",
              },
              [
                this.model
                  ? e(
                      "smooth-scroll",
                      {
                        staticClass: "h-100",
                        attrs: {
                          pageId: this.model.sys.id,
                        },
                      },
                      [
                        e(
                          "div",
                          {
                            staticClass: "components-wrapper overflow-hidden",
                          },
                          [
                            e("portfolio-grid", {
                              attrs: {
                                projects: this.projects,
                              },
                            }),
                            this._v(" "),
                            e("page-footer", {
                              staticClass: "pt-md-2",
                              attrs: {
                                noPadding: !0,
                                model: this.footerModel,
                              },
                            }),
                          ],
                          1
                        ),
                      ]
                    )
                  : this._e(),
              ],
              1
            );
          },
          [],
          !1,
          null,
          null,
          null
        ).exports,
        ln = [_i, Ui, on];
      l.default.use(v.a);

      function cn(t) {
        return (
          (e = []),
          y.forEach(function (t) {
            var n = ln.find(function (component) {
              return component.name === t.component;
            });
            e.push({
              path: t.url,
              name: t.id,
              component: n,
            });
          }),
          e.push({
            name: "Portfolio",
            path: "../portfolio/:id?",
            component: Ui,
          }),
          new v.a({
            mode: "history",
            routes: e,
          })
        );
        var e;
      }
      var dn = {
          name: "NuxtChild",
          functional: !0,
          props: {
            nuxtChildKey: {
              type: String,
              default: "",
            },
            keepAlive: Boolean,
            keepAliveProps: {
              type: Object,
              default: void 0,
            },
          },
          render: function (t, e) {
            var n = e.parent,
              data = e.data,
              r = e.props,
              o = n.$createElement;
            data.nuxtChild = !0;
            for (
              var l = n,
                c = n.$nuxt.nuxt.transitions,
                d = n.$nuxt.nuxt.defaultTransition,
                h = 0;
              n;

            )
              n.$vnode && n.$vnode.data.nuxtChild && h++, (n = n.$parent);
            data.nuxtChildDepth = h;
            var f = c[h] || d,
              m = {};
            un.forEach(function (t) {
              void 0 !== f[t] && (m[t] = f[t]);
            });
            var v = {};
            pn.forEach(function (t) {
              "function" == typeof f[t] && (v[t] = f[t].bind(l));
            });
            var y = v.beforeEnter;
            if (
              ((v.beforeEnter = function (t) {
                if (
                  (window.$nuxt.$nextTick(function () {
                    window.$nuxt.$emit("triggerScroll");
                  }),
                  y)
                )
                  return y.call(l, t);
              }),
              !1 === f.css)
            ) {
              var w = v.leave;
              (!w || w.length < 2) &&
                (v.leave = function (t, e) {
                  w && w.call(l, t), l.$nextTick(e);
                });
            }
            var x = o("routerView", data);
            return (
              r.keepAlive &&
                (x = o(
                  "keep-alive",
                  {
                    props: r.keepAliveProps,
                  },
                  [x]
                )),
              o(
                "transition",
                {
                  props: m,
                  on: v,
                },
                [x]
              )
            );
          },
        },
        un = [
          "name",
          "mode",
          "appear",
          "css",
          "type",
          "duration",
          "enterClass",
          "leaveClass",
          "appearClass",
          "enterActiveClass",
          "enterActiveClass",
          "leaveActiveClass",
          "appearActiveClass",
          "enterToClass",
          "leaveToClass",
          "appearToClass",
        ],
        pn = [
          "beforeEnter",
          "enter",
          "afterEnter",
          "enterCancelled",
          "beforeLeave",
          "leave",
          "afterLeave",
          "leaveCancelled",
          "beforeAppear",
          "appear",
          "afterAppear",
          "appearCancelled",
        ],
        hn = {
          name: "Error",
          components: {
            StarIcon: $t.a,
            Stage: Ht,
          },
          mixins: [M],
          data: function () {
            return {
              containers: [],
              loaded: !1,
            };
          },
          computed: {
            elementId: function () {
              return "wrapper-".concat(this._uid);
            },
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
          },
          watch: {},
          mounted: function () {
            console.log("WATAFKAAAA!"),
              (this.containers = [
                {
                  text: "Not found Not found Not found Not found Not found Not found Not found Not found Not found Not found Not found Not found Not found Not found Not found Not found Not found Not found ",
                  domElement: this.elementId,
                  fontSize: 53.5,
                  width: 1800,
                  scaleFactor: 0.975,
                  letterSpacing: 2.4,
                  color: "#828F82",
                },
              ]);
            var t = this.$el.querySelectorAll(".should-animate");
            this.runAnimation(t, !0), console.log("WATAFKAAAA!222");
          },
          methods: {},
        },
        fn =
          (n(776),
          Object(R.a)(
            hn,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "div",
                {
                  staticClass:
                    "page page-404 d-flex flex-center position-fixed w-100",
                },
                [
                  n(
                    "section",
                    {
                      staticClass:
                        "wrapper d-flex flex-center flex-column position-relative",
                    },
                    [
                      n(
                        "div",
                        {
                          staticClass:
                            "oval-container position-absolute should-animate",
                          attrs: {
                            "data-props": "opacity",
                            "data-delay": ".65",
                          },
                        },
                        [
                          n(
                            "client-only",
                            [
                              n("img", {
                                staticClass:
                                  "oval-text w-100 h-100 t-0 l-0 hidden",
                                attrs: {
                                  id: t.elementId,
                                  src: "../not-found-oval.svg",
                                },
                              }),
                              t._v(" "),
                              n("stage", {
                                staticClass:
                                  "position-absolute t-0 l-0 no-events stage w-100 h-100",
                                attrs: {
                                  index: t.pageWidth,
                                  containers: t.containers,
                                },
                              }),
                            ],
                            1
                          ),
                        ],
                        1
                      ),
                      t._v(" "),
                      n("star-icon", {
                        staticClass: "star-icon mb-1 should-animate",
                        attrs: {
                          "data-props": "scale",
                          "data-delay": ".1",
                          "data-fromscale": "0",
                          "data-dur": "1",
                        },
                      }),
                      t._v(" "),
                      t._m(0),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass: "mt-2 should-animate button-wrapper p-1",
                          attrs: {
                            "data-preset": "opacity,y",
                            "data-delay": ".3",
                          },
                        },
                        [
                          n(
                            "nuxt-link",
                            {
                              staticClass: "btn btn-white",
                              attrs: {
                                to: "/",
                              },
                            },
                            [n("span", [t._v("Go Back")])]
                          ),
                        ],
                        1
                      ),
                    ],
                    1
                  ),
                ]
              );
            },
            [
              function () {
                var t = this.$createElement,
                  e = this._self._c || t;
                return e(
                  "h1",
                  {
                    staticClass:
                      "color-white antiqua-font should-animate text-center lh-1",
                    attrs: {
                      "data-preset": "opacity,y",
                      "data-delay": ".2",
                    },
                  },
                  [
                    this._v("Page not"),
                    e("br", {
                      staticClass: "d-md-none",
                    }),
                    e(
                      "span",
                      {
                        staticClass: "d-none d-md-inline-block",
                      },
                      [this._v(" ")]
                    ),
                    this._v("found"),
                  ]
                );
              },
            ],
            !1,
            null,
            "173373e4",
            null
          ).exports),
        mn = (n(71), n(72), n(40), n(90)),
        gn = n(12),
        vn = {
          name: "Nuxt",
          components: {
            NuxtChild: dn,
            NuxtError: fn,
          },
          props: {
            nuxtChildKey: {
              type: String,
              default: void 0,
            },
            keepAlive: Boolean,
            keepAliveProps: {
              type: Object,
              default: void 0,
            },
            name: {
              type: String,
              default: "default",
            },
          },
          errorCaptured: function (t) {
            this.displayingNuxtError &&
              ((this.errorFromNuxtError = t), this.$forceUpdate());
          },
          computed: {
            routerViewKey: function () {
              if (
                void 0 !== this.nuxtChildKey ||
                this.$route.matched.length > 1
              )
                return (
                  this.nuxtChildKey ||
                  Object(gn.c)(this.$route.matched[0].path)(this.$route.params)
                );
              var t = Object(mn.a)(this.$route.matched, 1)[0];
              if (!t) return this.$route.path;
              var e = t.components.default;
              if (e && e.options) {
                var n = e.options;
                if (n.key)
                  return "function" == typeof n.key
                    ? n.key(this.$route)
                    : n.key;
              }
              return /\/$/.test(t.path)
                ? this.$route.path
                : this.$route.path.replace(/\/$/, "");
            },
          },
          beforeCreate: function () {
            l.default.util.defineReactive(
              this,
              "nuxt",
              this.$root.$options.nuxt
            );
          },
          render: function (t) {
            var e = this;
            return this.nuxt.err
              ? this.errorFromNuxtError
                ? (this.$nextTick(function () {
                    return (e.errorFromNuxtError = !1);
                  }),
                  t("div", {}, [
                    t("h2", "An error occurred while showing the error page"),
                    t(
                      "p",
                      "Unfortunately an error occurred and while showing the error page another error occurred"
                    ),
                    t(
                      "p",
                      "Error details: ".concat(
                        this.errorFromNuxtError.toString()
                      )
                    ),
                    t(
                      "nuxt-link",
                      {
                        props: {
                          to: "/",
                        },
                      },
                      "Go back to home"
                    ),
                  ]))
                : ((this.displayingNuxtError = !0),
                  this.$nextTick(function () {
                    return (e.displayingNuxtError = !1);
                  }),
                  t(fn, {
                    props: {
                      error: this.nuxt.err,
                    },
                  }))
              : t("NuxtChild", {
                  key: this.routerViewKey,
                  props: this.$props,
                });
          },
        },
        bn =
          (n(91),
          n(83),
          n(54),
          n(777),
          {
            name: "hamburger",
            props: ["closeButton"],
            computed: {
              menuOpen: function () {
                return this.$store.getters["app/getState"]("menuOpen");
              },
            },
          }),
        yn =
          (n(778),
          Object(R.a)(
            bn,
            function () {
              var t = this.$createElement,
                e = this._self._c || t;
              return e(
                "button",
                {
                  staticClass: "hamburger",
                  class: {
                    "is-active": this.menuOpen || this.closeButton,
                  },
                  attrs: {
                    type: "button",
                  },
                },
                [
                  e(
                    "span",
                    {
                      staticClass: "position-absolute indented",
                    },
                    [this._v("Menu")]
                  ),
                  this._v(" "),
                  this._m(0),
                ]
              );
            },
            [
              function () {
                var t = this.$createElement,
                  e = this._self._c || t;
                return e(
                  "span",
                  {
                    staticClass: "hamburger-box",
                  },
                  [
                    e("span", {
                      staticClass: "line line--1",
                    }),
                    this._v(" "),
                    e("span", {
                      staticClass: "line line--3",
                    }),
                  ]
                );
              },
            ],
            !1,
            null,
            "209bb0f5",
            null
          ).exports);

      function wn(object, t) {
        var e = Object.keys(object);
        if (Object.getOwnPropertySymbols) {
          var n = Object.getOwnPropertySymbols(object);
          t &&
            (n = n.filter(function (t) {
              return Object.getOwnPropertyDescriptor(object, t).enumerable;
            })),
            e.push.apply(e, n);
        }
        return e;
      }

      function xn(t) {
        for (var i = 1; i < arguments.length; i++) {
          var source = null != arguments[i] ? arguments[i] : {};
          i % 2
            ? wn(Object(source), !0).forEach(function (e) {
                Object(o.a)(t, e, source[e]);
              })
            : Object.getOwnPropertyDescriptors
            ? Object.defineProperties(
                t,
                Object.getOwnPropertyDescriptors(source)
              )
            : wn(Object(source)).forEach(function (e) {
                Object.defineProperty(
                  t,
                  e,
                  Object.getOwnPropertyDescriptor(source, e)
                );
              });
        }
        return t;
      }
      var Cn = {
          name: "navigation",
          data: function () {
            return {};
          },
          props: ["page"],
          mixins: [],
          watch: {},
          mounted: function () {},
          methods: {
            toggleMenu: function (t) {
              return (
                this.$store.dispatch("app/SET_STATE", {
                  menuOpen: !1 !== t && !this.menuOpen,
                }),
                !0
              );
            },
          },
          computed: xn(
            xn(
              xn({}, Object(B.c)("pages", ["navigation"])),
              Object(B.c)("app", ["menuOpen"])
            ),
            {},
            {
              items: function () {
                return L()(this.navigation, "fields.pages");
              },
            }
          ),
          components: {
            Hamburger: yn,
          },
        },
        _n =
          (n(779),
          Object(R.a)(
            Cn,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "div",
                {
                  staticClass:
                    "navigation position-fixed t-0 l-0 w-100 on-top d-flex justify-content-between",
                },
                [
                  n(
                    "div",
                    [
                      n(
                        "nuxt-link",
                        {
                          staticClass: "d-block",
                          attrs: {
                            to: "/",
                          },
                        },
                        [
                          n(
                            "h5",
                            {
                              staticClass:
                                "uppercase founders-regular color-white logo",
                            },
                            [t._v("Naba Zabih")]
                          ),
                        ]
                      ),
                    ],
                    1
                  ),
                  t._v(" "),
                  n(
                    "div",
                    {
                      staticClass: "color-white d-flex",
                    },
                    [
                      n(
                        "div",
                        {
                          staticClass: "d-flex nav-wrapper align-items-center",
                        },
                        t._l(t.items, function (e, r) {
                          return e.url
                            ? n(
                                "nuxt-link",
                                {
                                  key: r,
                                  staticClass:
                                    "d-none d-md-block small founders-regular color-white link nav-item",
                                  attrs: {
                                    to: e.url,
                                  },
                                },
                                [t._v(t._s(e.fields.title))]
                              )
                            : t._e();
                        }),
                        1
                      ),
                      t._v(" "),
                      n(
                        "div",
                        {
                          staticClass:
                            "hamburger-wrapper d-flex d-md-none align-items-center justify-content-end h-100",
                        },
                        [
                          n(
                            "div",
                            {
                              staticClass:
                                "cursor-pointer d-flex align-items-center no-select enabled",
                              on: {
                                click: function (e) {
                                  return t.toggleMenu();
                                },
                              },
                            },
                            [
                              n("span", {
                                staticClass: "lh-12 transition h6",
                                class: {
                                  "fade-out": t.menuOpen,
                                },
                              }),
                              t._v(" "),
                              n("hamburger"),
                            ],
                            1
                          ),
                        ]
                      ),
                    ]
                  ),
                ]
              );
            },
            [],
            !1,
            null,
            "404d9041",
            null
          ).exports);

      function On(object, t) {
        var e = Object.keys(object);
        if (Object.getOwnPropertySymbols) {
          var n = Object.getOwnPropertySymbols(object);
          t &&
            (n = n.filter(function (t) {
              return Object.getOwnPropertyDescriptor(object, t).enumerable;
            })),
            e.push.apply(e, n);
        }
        return e;
      }
      var Pn = {
          name: "fullscreen-menu",
          props: ["model"],
          mixins: [W],
          mounted: function () {
            this.toggleWaypoint({
              el: this.$el,
              visible: !0,
            });
          },
          methods: {
            closeMenu: function (t) {
              t == this.$route.fullPath &&
                this.$store.dispatch("app/SET_STATE", {
                  menuOpen: !1,
                });
            },
          },
          computed: (function (t) {
            for (var i = 1; i < arguments.length; i++) {
              var source = null != arguments[i] ? arguments[i] : {};
              i % 2
                ? On(Object(source), !0).forEach(function (e) {
                    Object(o.a)(t, e, source[e]);
                  })
                : Object.getOwnPropertyDescriptors
                ? Object.defineProperties(
                    t,
                    Object.getOwnPropertyDescriptors(source)
                  )
                : On(Object(source)).forEach(function (e) {
                    Object.defineProperty(
                      t,
                      e,
                      Object.getOwnPropertyDescriptor(source, e)
                    );
                  });
            }
            return t;
          })(
            {
              items: function () {
                return this.navigationModel.fields.pages;
              },
              navigationModel: function () {
                return this.$store.getters["pages/getState"]("navigation");
              },
            },
            Object(B.b)({
              getState: "pages/getState",
            })
          ),
        },
        Ln =
          (n(780),
          Object(R.a)(
            Pn,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "div",
                {
                  staticClass:
                    "fullscreen-menu bg-blackv2 position-fixed d-flex flex-center px-1 flex-column",
                },
                [
                  n(
                    "div",
                    {
                      staticClass: "color-white",
                    },
                    t._l(t.items, function (e, r) {
                      return e.url
                        ? n(
                            "nuxt-link",
                            {
                              key: r,
                              staticClass:
                                "antiqua-font h4 uppercase text-center color-white d-flex flex-column nav-item should-animate",
                              attrs: {
                                to: e.url,
                                "data-delay": 0.1 + r / 10,
                                "data-preset": "opacity,y",
                              },
                              nativeOn: {
                                click: function (n) {
                                  return t.closeMenu(e.url);
                                },
                              },
                            },
                            [t._v(t._s(e.fields.title))]
                          )
                        : t._e();
                    }),
                    1
                  ),
                ]
              );
            },
            [],
            !1,
            null,
            "3d213c42",
            null
          ).exports),
        kn = {
          computed: {
            isIE: function () {
              return this.$store.getters["app/getState"]("isIE");
            },
            isEdge: function () {
              return this.$store.getters["app/getState"]("isEdge");
            },
          },
          beforeMount: function () {
            this.detectIE(),
              this.detectWebkit(),
              this.isIE && document.documentElement.classList.add("isIE"),
              this.isEdge && document.documentElement.classList.add("isEdge"),
              (this.$device.isMobileOrTablet || this.isIE) &&
                document.documentElement.classList.add("isMobile");
          },
          methods: {
            detectWebkit: function () {
              var t =
                  /Chrome/.test(navigator.userAgent) &&
                  /Google Inc/.test(navigator.vendor),
                e =
                  /Safari/.test(navigator.userAgent) &&
                  /Apple Computer/.test(navigator.vendor);
              this.$store.dispatch("app/SET_STATE", {
                isWebkit: t || e,
              });
            },
            detectIE: function () {
              var t = window.navigator.userAgent,
                e = !1;
              t.indexOf("MSIE ") > 0 && (e = !0),
                t.indexOf("Trident/") > 0 && (e = !0);
              var n = !1;
              return (
                t.indexOf("Edge/") > 0 && (n = !0),
                this.$store.dispatch("app/SET_STATE", {
                  isIE: e,
                  isEdge: n,
                }),
                e
              );
            },
          },
        },
        Sn = {
          name: "loader",
          data: function () {
            return {
              loaded: !1,
              text: "Naba Zabih",
            };
          },
          mounted: function () {
            this.enter();
          },
          methods: {
            enter: function (t) {
              var e = this,
                n = x.b.timeline();
              n.set(this.$refs.text, {
                opacity: 1,
              }),
                n.fromTo(
                  this.$refs.character,
                  0.8,
                  {
                    opacity: 0,
                  },
                  {
                    opacity: 1,
                    delay: 0.65,
                    stagger: 0.1,
                  }
                ),
                n.to(this.$refs.character, 0.8, {
                  opacity: 0,
                  delay: 1,
                  stagger: -0.1,
                  onComplete: function () {
                    e.$emit("update:loaded", !0),
                      e.$store.dispatch("app/SET_STATE", {
                        animationDelay: 0,
                      });
                  },
                });
            },
          },
          computed: {
            letters: function () {
              return this.text.split("");
            },
          },
        },
        In =
          (n(781),
          Object(R.a)(
            Sn,
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "div",
                {
                  staticClass:
                    "loader position-fixed w-100 h-100 t-0 l-0 d-flex flex-center",
                },
                [
                  n(
                    "div",
                    {
                      ref: "text",
                      staticClass: "loader-text",
                    },
                    t._l(t.letters, function (e, r) {
                      return n(
                        "span",
                        {
                          key: r,
                          ref: "character",
                          refInFor: !0,
                          staticClass:
                            "color-greyv7 d-inline-block text-uppercase founders-regular h6",
                          class: {
                            "mx-small": " " == e,
                          },
                        },
                        [t._v(t._s(e))]
                      );
                    }),
                    0
                  ),
                ]
              );
            },
            [],
            !1,
            null,
            "b7046d32",
            null
          ).exports),
        jn = n(432),
        Tn = n.n(jn),
        $n = {
          components: {
            Navigation: _n,
            FullscreenMenu: Ln,
            Loader: In,
          },
          mixins: [kn],
          data: function () {
            return {
              loaded: !1,
            };
          },
          computed: {
            activePage: function () {
              return this.$store.getters["pages/getState"]("activePage");
            },
            pageKey: function () {
              return L()(this.activePage, "sys.id") || "not-found";
            },
            menuOpen: function () {
              return this.$store.getters["app/getState"]("menuOpen");
            },
          },
          head: function () {
            if (!this.activePage || !this.activePage.seo) return {};
            var meta = Ki()(this.activePage.seo, function (t, e) {
              var n = {
                hid: e,
                content: t.value,
              };
              return (n[t.attribute] = e), n;
            });
            return {
              htmlAttrs: {
                lang: "en",
              },
              title: L()(this.activePage, "seo.title.value"),
              meta: meta,
            };
          },
          watch: {},
          created: function () {
            l.default.prototype.$webp =
              this.$store.getters["app/getState"]("webp");
          },
          beforeMount: function () {
            var t = this;
            this.resizeHandler(),
              window.addEventListener("fonts.loaded", function () {
                t.$store.dispatch("app/SET_STATE", {
                  fontsLoaded: !0,
                }),
                  t.$bus.$emit("resize");
              });
          },
          mounted: function () {
            var t = this;
            window.addEventListener(
              "resize",
              Tn()(function () {
                return t.resizeHandler(!0);
              }, 200)
            ),
              window.addEventListener("orientationchange", this.resizeHandler),
              document.addEventListener("touchstart", function () {}, !0);
          },
          methods: {
            onMouseMove: function (t) {
              this.$store.dispatch("app/SET_STATE", {
                mouseX: t.pageX,
                mouseY: t.pageY,
              });
            },
            resizeHandler: function (t) {
              this.$store.dispatch("app/SET_STATE", {
                width: window.innerWidth,
                height: window.innerHeight,
              }),
                this.$bus.$emit("resize", t);
            },
          },
        },
        En = Object(R.a)(
          $n,
          function () {
            var t = this,
              e = t.$createElement,
              n = t._self._c || e;
            return n(
              "div",
              {
                staticClass: "position-relative app-wrapper",
                on: {
                  mousemove: t.onMouseMove,
                },
              },
              [
                n("navigation"),
                t._v(" "),
                n(
                  "custom-transition",
                  {
                    attrs: {
                      duration: 0.6,
                    },
                  },
                  [
                    t.menuOpen
                      ? n("fullscreen-menu", {
                          key: t.menuOpen,
                        })
                      : t._e(),
                  ],
                  1
                ),
                t._v(" "),
                n(
                  "custom-transition",
                  {
                    attrs: {
                      mode: "out-in",
                      delay: 0.15,
                      pageTransition: !0,
                    },
                  },
                  [
                    n("nuxt", {
                      key: t.pageKey,
                    }),
                  ],
                  1
                ),
                t._v(" "),
                n(
                  "custom-transition",
                  {
                    attrs: {
                      noAdditionalClasses: !0,
                    },
                  },
                  [
                    t.loaded
                      ? t._e()
                      : n("loader", {
                          key: t.loaded,
                          attrs: {
                            loaded: t.loaded,
                          },
                          on: {
                            "update:loaded": function (e) {
                              t.loaded = e;
                            },
                          },
                        }),
                  ],
                  1
                ),
              ],
              1
            );
          },
          [],
          !1,
          null,
          null,
          null
        ).exports;

      function An(t, e) {
        var n;
        if ("undefined" == typeof Symbol || null == t[Symbol.iterator]) {
          if (
            Array.isArray(t) ||
            (n = (function (t, e) {
              if (!t) return;
              if ("string" == typeof t) return Mn(t, e);
              var n = Object.prototype.toString.call(t).slice(8, -1);
              "Object" === n && t.constructor && (n = t.constructor.name);
              if ("Map" === n || "Set" === n) return Array.from(t);
              if (
                "Arguments" === n ||
                /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)
              )
                return Mn(t, e);
            })(t)) ||
            (e && t && "number" == typeof t.length)
          ) {
            n && (t = n);
            var i = 0,
              r = function () {};
            return {
              s: r,
              n: function () {
                return i >= t.length
                  ? {
                      done: !0,
                    }
                  : {
                      done: !1,
                      value: t[i++],
                    };
              },
              e: function (t) {
                throw t;
              },
              f: r,
            };
          }
          throw new TypeError(
            "Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
          );
        }
        var o,
          l = !0,
          c = !1;
        return {
          s: function () {
            n = t[Symbol.iterator]();
          },
          n: function () {
            var t = n.next();
            return (l = t.done), t;
          },
          e: function (t) {
            (c = !0), (o = t);
          },
          f: function () {
            try {
              l || null == n.return || n.return();
            } finally {
              if (c) throw o;
            }
          },
        };
      }

      function Mn(t, e) {
        (null == e || e > t.length) && (e = t.length);
        for (var i = 0, n = new Array(e); i < e; i++) n[i] = t[i];
        return n;
      }
      var Dn = {
          _default: Object(gn.q)(En),
        },
        Wn = {
          render: function (t, e) {
            var n = t(this.layout || "nuxt"),
              r = t(
                "div",
                {
                  domProps: {
                    id: "__layout",
                  },
                  key: this.layoutName,
                },
                [n]
              ),
              o = t(
                "transition",
                {
                  props: {
                    name: "layout",
                    mode: "out-in",
                  },
                  on: {
                    beforeEnter: function (t) {
                      window.$nuxt.$nextTick(function () {
                        window.$nuxt.$emit("triggerScroll");
                      });
                    },
                  },
                },
                [r]
              );
            return t(
              "div",
              {
                domProps: {
                  id: "__nuxt",
                },
              },
              [o]
            );
          },
          data: function () {
            return {
              isOnline: !0,
              layout: null,
              layoutName: "",
              nbFetching: 0,
            };
          },
          beforeCreate: function () {
            l.default.util.defineReactive(this, "nuxt", this.$options.nuxt);
          },
          created: function () {
            (l.default.prototype.$nuxt = this),
              (window.$nuxt = this),
              this.refreshOnlineStatus(),
              window.addEventListener("online", this.refreshOnlineStatus),
              window.addEventListener("offline", this.refreshOnlineStatus),
              (this.error = this.nuxt.error),
              (this.context = this.$options.context);
          },
          computed: {
            isOffline: function () {
              return !this.isOnline;
            },
            isFetching: function () {
              return this.nbFetching > 0;
            },
          },
          methods: {
            refreshOnlineStatus: function () {
              void 0 === window.navigator.onLine
                ? (this.isOnline = !0)
                : (this.isOnline = window.navigator.onLine);
            },
            refresh: function () {
              var t = this;
              return Object(r.a)(
                regeneratorRuntime.mark(function e() {
                  var n, r;
                  return regeneratorRuntime.wrap(
                    function (e) {
                      for (;;)
                        switch ((e.prev = e.next)) {
                          case 0:
                            if ((n = Object(gn.h)(t.$route)).length) {
                              e.next = 3;
                              break;
                            }
                            return e.abrupt("return");
                          case 3:
                            return (
                              (r = n.map(function (e) {
                                var p = [];
                                if (
                                  (e.$options.fetch &&
                                    e.$options.fetch.length &&
                                    p.push(
                                      Object(gn.o)(e.$options.fetch, t.context)
                                    ),
                                  e.$fetch)
                                )
                                  p.push(e.$fetch());
                                else {
                                  var n,
                                    r = An(
                                      Object(gn.e)(e.$vnode.componentInstance)
                                    );
                                  try {
                                    for (r.s(); !(n = r.n()).done; ) {
                                      var component = n.value;
                                      p.push(component.$fetch());
                                    }
                                  } catch (t) {
                                    r.e(t);
                                  } finally {
                                    r.f();
                                  }
                                }
                                return (
                                  e.$options.asyncData &&
                                    p.push(
                                      Object(gn.o)(
                                        e.$options.asyncData,
                                        t.context
                                      ).then(function (t) {
                                        for (var n in t)
                                          l.default.set(e.$data, n, t[n]);
                                      })
                                    ),
                                  Promise.all(p)
                                );
                              })),
                              (e.prev = 4),
                              (e.next = 7),
                              Promise.all(r)
                            );
                          case 7:
                            e.next = 13;
                            break;
                          case 9:
                            (e.prev = 9),
                              (e.t0 = e.catch(4)),
                              Object(gn.k)(e.t0),
                              t.error(e.t0);
                          case 13:
                          case "end":
                            return e.stop();
                        }
                    },
                    e,
                    null,
                    [[4, 9]]
                  );
                })
              )();
            },
            setLayout: function (t) {
              return (
                (t && Dn["_" + t]) || (t = "default"),
                (this.layoutName = t),
                (this.layout = Dn["_" + t]),
                this.layout
              );
            },
            loadLayout: function (t) {
              return (
                (t && Dn["_" + t]) || (t = "default"),
                Promise.resolve(Dn["_" + t])
              );
            },
          },
        };

      function Hn(t, e) {
        var n;
        if ("undefined" == typeof Symbol || null == t[Symbol.iterator]) {
          if (
            Array.isArray(t) ||
            (n = (function (t, e) {
              if (!t) return;
              if ("string" == typeof t) return Rn(t, e);
              var n = Object.prototype.toString.call(t).slice(8, -1);
              "Object" === n && t.constructor && (n = t.constructor.name);
              if ("Map" === n || "Set" === n) return Array.from(t);
              if (
                "Arguments" === n ||
                /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)
              )
                return Rn(t, e);
            })(t)) ||
            (e && t && "number" == typeof t.length)
          ) {
            n && (t = n);
            var i = 0,
              r = function () {};
            return {
              s: r,
              n: function () {
                return i >= t.length
                  ? {
                      done: !0,
                    }
                  : {
                      done: !1,
                      value: t[i++],
                    };
              },
              e: function (t) {
                throw t;
              },
              f: r,
            };
          }
          throw new TypeError(
            "Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
          );
        }
        var o,
          l = !0,
          c = !1;
        return {
          s: function () {
            n = t[Symbol.iterator]();
          },
          n: function () {
            var t = n.next();
            return (l = t.done), t;
          },
          e: function (t) {
            (c = !0), (o = t);
          },
          f: function () {
            try {
              l || null == n.return || n.return();
            } finally {
              if (c) throw o;
            }
          },
        };
      }

      function Rn(t, e) {
        (null == e || e > t.length) && (e = t.length);
        for (var i = 0, n = new Array(e); i < e; i++) n[i] = t[i];
        return n;
      }
      l.default.use(B.a);
      var zn = ["state", "getters", "actions", "mutations"],
        Fn = {};
      ((Fn = (function (t, e) {
        if ((t = t.default || t).commit)
          throw new Error(
            "[nuxt] ".concat(
              e,
              " should export a method that returns a Vuex instance."
            )
          );
        return "function" != typeof t && (t = Object.assign({}, t)), Bn(t, e);
      })(n(783), "store/index.js")).modules = Fn.modules || {}),
        Nn(n(274), "modules/index.js"),
        Nn(n(412), "modules/app.store.js"),
        Nn(n(413), "modules/pages.store.js"),
        Nn(n(414), "modules/stage.store.js");
      var Xn =
        Fn instanceof Function
          ? Fn
          : function () {
              return new B.a.Store(
                Object.assign(
                  {
                    strict: !1,
                  },
                  Fn
                )
              );
            };

      function Bn(t, e) {
        if (t.state && "function" != typeof t.state) {
          console.warn(
            "'state' should be a method that returns an object in ".concat(e)
          );
          var n = Object.assign({}, t.state);
          t = Object.assign({}, t, {
            state: function () {
              return n;
            },
          });
        }
        return t;
      }

      function Nn(t, e) {
        t = t.default || t;
        var n = e.replace(/\.(js|mjs)$/, "").split("/"),
          r = n[n.length - 1],
          o = "store/".concat(e);
        if (
          ((t =
            "state" === r
              ? (function (t, e) {
                  if ("function" != typeof t) {
                    console.warn(
                      "".concat(
                        e,
                        " should export a method that returns an object"
                      )
                    );
                    var n = Object.assign({}, t);
                    return function () {
                      return n;
                    };
                  }
                  return Bn(t, e);
                })(t, o)
              : Bn(t, o)),
          zn.includes(r))
        ) {
          var l = r;
          Yn(
            qn(Fn, n, {
              isProperty: !0,
            }),
            t,
            l
          );
        } else {
          "index" === r && (n.pop(), (r = n[n.length - 1]));
          var c,
            d = qn(Fn, n),
            h = Hn(zn);
          try {
            for (h.s(); !(c = h.n()).done; ) {
              var f = c.value;
              Yn(d, t[f], f);
            }
          } catch (t) {
            h.e(t);
          } finally {
            h.f();
          }
          !1 === t.namespaced && delete d.namespaced;
        }
      }

      function qn(t, e) {
        var n =
            arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {},
          r = n.isProperty,
          o = void 0 !== r && r;
        if (!e.length || (o && 1 === e.length)) return t;
        var l = e.shift();
        return (
          (t.modules[l] = t.modules[l] || {}),
          (t.modules[l].namespaced = !0),
          (t.modules[l].modules = t.modules[l].modules || {}),
          qn(t.modules[l], e, {
            isProperty: o,
          })
        );
      }

      function Yn(t, e, n) {
        e &&
          ("state" === n
            ? (t.state = e || t.state)
            : (t[n] = Object.assign({}, t[n], e)));
      }
      var Vn = n(282),
        Un = n.n(Vn),
        Zn = n(434),
        Gn = n(435),
        Kn = n.n(Gn),
        Jn = function (t) {
          var e = t.app;
          Kn()(e.head, Zn);
        },
        Qn = function (t, e) {
          return er.apply(this, arguments);
        };

      function er() {
        return (er = Object(r.a)(
          regeneratorRuntime.mark(function t(e, n) {
            var r;
            return regeneratorRuntime.wrap(function (t) {
              for (;;)
                switch ((t.prev = t.next)) {
                  case 0:
                    (r = {
                      "64x64": "../_nuxt/icons/icon_64x64.01fe1d.png",
                      "120x120": "../_nuxt/icons/icon_120x120.01fe1d.png",
                      "144x144": "../_nuxt/icons/icon_144x144.01fe1d.png",
                      "152x152": "../_nuxt/icons/icon_152x152.01fe1d.png",
                      "192x192": "../_nuxt/icons/icon_192x192.01fe1d.png",
                      "384x384": "../_nuxt/icons/icon_384x384.01fe1d.png",
                      "512x512": "../_nuxt/icons/icon_512x512.01fe1d.png",
                      ipad_1536x2048:
                        "../_nuxt/icons/splash_ipad_1536x2048.01fe1d.png",
                      ipadpro9_1536x2048:
                        "../_nuxt/icons/splash_ipadpro9_1536x2048.01fe1d.png",
                      ipadpro10_1668x2224:
                        "../_nuxt/icons/splash_ipadpro10_1668x2224.01fe1d.png",
                      ipadpro12_2048x2732:
                        "../_nuxt/icons/splash_ipadpro12_2048x2732.01fe1d.png",
                      iphonese_640x1136:
                        "../_nuxt/icons/splash_iphonese_640x1136.01fe1d.png",
                      iphone6_50x1334:
                        "../_nuxt/icons/splash_iphone6_50x1334.01fe1d.png",
                      iphoneplus_1080x1920:
                        "../_nuxt/icons/splash_iphoneplus_1080x1920.01fe1d.png",
                      iphonex_1125x2436:
                        "../_nuxt/icons/splash_iphonex_1125x2436.01fe1d.png",
                      iphonexr_828x1792:
                        "../_nuxt/icons/splash_iphonexr_828x1792.01fe1d.png",
                      iphonexsmax_1242x2688:
                        "../_nuxt/icons/splash_iphonexsmax_1242x2688.01fe1d.png",
                    }),
                      n("icon", function (t) {
                        return r[t + "x" + t] || "";
                      });
                  case 3:
                  case "end":
                    return t.stop();
                }
            }, t);
          })
        )).apply(this, arguments);
      }
      var ir = n(436),
        nr = {
          custom: {
            families: [
              "FoundersGrotesk-Regular",
              "FoundersGrotesk-Light",
              "AntiquaRoman",
            ],
            urls: ["../fonts.css"],
          },
          active: function () {
            window.dispatchEvent(new Event("fonts.loaded")),
              (window._fontsAreLoading = !0);
          },
        };
      n.n(ir).a.load(nr);
      var rr = "dataLayer",
        ar = "0";
      var sr = function (t, e) {
          var n = "0",
            r = ((t.$config && t.$config.gtm) || {}).id,
            l = (function (t, e) {
              return {
                init: function () {
                  var t =
                    arguments.length > 0 && void 0 !== arguments[0]
                      ? arguments[0]
                      : ar;
                  !e[t] &&
                    window._gtm_inject &&
                    (window._gtm_inject(t), (e[t] = !0));
                },
                push: function (t) {
                  window[rr] || (window[rr] = []), window[rr].push(t);
                },
              };
            })(0, Object(o.a)({}, n, !0));
          r && r !== n && l.init(r), (t.$gtm = l), e("gtm", t.$gtm);
        },
        or =
          /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i,
        lr =
          /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i;

      function cr(a) {
        return or.test(a) || lr.test(a.substr(0, 4));
      }
      var dr =
          /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i,
        ur =
          /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i;

      function pr(a) {
        return dr.test(a) || ur.test(a.substr(0, 4));
      }

      function fr(a) {
        return /iPad|iPhone|iPod/.test(a);
      }

      function mr(a) {
        return /android/i.test(a);
      }

      function gr(a) {
        return /Windows/.test(a);
      }

      function vr(a) {
        return /Mac OS X/.test(a);
      }
      var yr =
          "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.39 Safari/537.36",
        wr = function (t, e) {
          return xr.apply(this, arguments);
        };

      function xr() {
        return (xr = Object(r.a)(
          regeneratorRuntime.mark(function t(e, n) {
            var r, o, l, c, d, h, f;
            return regeneratorRuntime.wrap(function (t) {
              for (;;)
                switch ((t.prev = t.next)) {
                  case 0:
                    if (
                      ((r = ""),
                      (r =
                        void 0 !== e.req
                          ? e.req.headers["user-agent"]
                          : "undefined" != typeof navigator
                          ? navigator.userAgent
                          : yr) || (r = yr),
                      (o = null),
                      (l = null),
                      (c = null),
                      (d = null),
                      (h = !1),
                      (f = !0),
                      "Amazon CloudFront" !== r)
                    ) {
                      t.next = 14;
                      break;
                    }
                    "true" === e.req.headers["cloudfront-is-mobile-viewer"] &&
                      ((o = !0), (l = !0)),
                      "true" === e.req.headers["cloudfront-is-tablet-viewer"] &&
                        ((o = !1), (l = !0)),
                      (t.next = 33);
                    break;
                  case 14:
                    if (!e.req || !e.req.headers["cf-device-type"]) {
                      t.next = 29;
                      break;
                    }
                    (t.t0 = e.req.headers["cf-device-type"]),
                      (t.next =
                        "mobile" === t.t0
                          ? 18
                          : "tablet" === t.t0
                          ? 21
                          : "desktop" === t.t0
                          ? 24
                          : 27);
                    break;
                  case 18:
                    return (o = !0), (l = !0), t.abrupt("break", 27);
                  case 21:
                    return (o = !1), (l = !0), t.abrupt("break", 27);
                  case 24:
                    return (o = !1), (l = !1), t.abrupt("break", 27);
                  case 27:
                    t.next = 33;
                    break;
                  case 29:
                    (o = cr(r)), (l = pr(r)), (c = fr(r)), (d = mr(r));
                  case 33:
                    (h = gr(r)),
                      (f = vr(r)),
                      (e.isMobile = o),
                      (e.isMobileOrTablet = l),
                      (e.isTablet = !o && l),
                      (e.isDesktop = !l),
                      (e.isDesktopOrTablet = !o),
                      (e.isIos = c),
                      (e.isAndroid = d),
                      (e.isWindows = h),
                      (e.isMacOS = f),
                      n("device", {
                        isMobile: o,
                        isMobileOrTablet: l,
                        isTablet: !o && l,
                        isDesktop: !l,
                        isIos: c,
                        isAndroid: d,
                        isWindows: h,
                        isMacOS: f,
                        isDesktopOrTablet: !o,
                      });
                  case 45:
                  case "end":
                    return t.stop();
                }
            }, t);
          })
        )).apply(this, arguments);
      }
      var Cr = n(437),
        _r = n.n(Cr);

      function Or(object, t) {
        var e = Object.keys(object);
        if (Object.getOwnPropertySymbols) {
          var n = Object.getOwnPropertySymbols(object);
          t &&
            (n = n.filter(function (t) {
              return Object.getOwnPropertyDescriptor(object, t).enumerable;
            })),
            e.push.apply(e, n);
        }
        return e;
      }

      function Pr(t) {
        for (var i = 1; i < arguments.length; i++) {
          var source = null != arguments[i] ? arguments[i] : {};
          i % 2
            ? Or(Object(source), !0).forEach(function (e) {
                Object(o.a)(t, e, source[e]);
              })
            : Object.getOwnPropertyDescriptors
            ? Object.defineProperties(
                t,
                Object.getOwnPropertyDescriptors(source)
              )
            : Or(Object(source)).forEach(function (e) {
                Object.defineProperty(
                  t,
                  e,
                  Object.getOwnPropertyDescriptor(source, e)
                );
              });
        }
        return t;
      }
      var Lr = (function () {
          var t = Object(r.a)(
            regeneratorRuntime.mark(function t(e, n) {
              var r, o;
              return regeneratorRuntime.wrap(function (t) {
                for (;;)
                  switch ((t.prev = t.next)) {
                    case 0:
                      if (
                        ((r = (e.$config && e.$config.googleAnalytics) || {}),
                        "function" !=
                          typeof (o = Pr(
                            Pr(
                              {},
                              {
                                dev: !0,
                                debug: {
                                  sendHitTask: !0,
                                },
                                id: "UA-183821965-1",
                              }
                            ),
                            r
                          )).asyncID)
                      ) {
                        t.next = 7;
                        break;
                      }
                      return (t.next = 6), o.asyncID(e);
                    case 6:
                      o.id = t.sent;
                    case 7:
                      l.default.use(
                        _r.a,
                        Pr(
                          Pr(
                            {},
                            {
                              router: e.app.router,
                            }
                          ),
                          o
                        )
                      ),
                        (e.$ga = l.default.$ga),
                        n("ga", l.default.$ga);
                    case 10:
                    case "end":
                      return t.stop();
                  }
              }, t);
            })
          );
          return function (e, n) {
            return t.apply(this, arguments);
          };
        })(),
        kr = n(130),
        Sr = n.n(kr),
        Ir = (n(415), n(79)),
        jr = n(128),
        Tr = n(33),
        $r = n.n(Tr),
        Er = n(438),
        Ar = n.n(Er),
        Mr = n(310).remove;
      l.default.mixin({
        methods: {
          getImage: function (t, e, n, r, o, l) {
            if (!t || !mt()(t)) return n ? t + n : t;
            var c = t.sys && "Asset" === t.sys.type;
            if (c || t.fields) {
              var img = c ? t : t.fields[e];
              if (
                !(img = $r()(img)
                  ? L()(img, "[0].fields.file.url")
                  : L()(img, "fields.file.url"))
              )
                return !1;
              var d = Ar()(img.split("."));
              return (
                this.$device && this.$device.isMobile && (n = r || "?w=480"),
                (img = n ? img + n : img),
                this.$webp && !l && "svg" != d
                  ? (img += "&fm=webp")
                  : ("webp" == d && (img += "&fm=jpg"),
                    (img += "&fl=progressive")),
                "https:".concat(img, "&q=").concat(o || 80)
              );
            }
          },
          stripHtml: Ir.d,
          getAttribute: function (t, e) {
            var n = !1,
              r = (t && t.fields) || {};
            return e in r && 0 === (n = r[e]) && (n = !1), n;
          },
          loadImage: function (t) {
            return Object(r.a)(
              regeneratorRuntime.mark(function e() {
                return regeneratorRuntime.wrap(function (e) {
                  for (;;)
                    switch ((e.prev = e.next)) {
                      case 0:
                        return e.abrupt(
                          "return",
                          new Promise(function (e, n) {
                            var img = new Image();
                            (img.onload = function () {
                              e(t);
                            }),
                              (img.onerror = function () {
                                n(t);
                              }),
                              (img.src = t);
                          })
                        );
                      case 1:
                      case "end":
                        return e.stop();
                    }
                }, e);
              })
            )();
          },
          loadImages: function (t) {
            var e = this;
            return Object(r.a)(
              regeneratorRuntime.mark(function n() {
                var r;
                return regeneratorRuntime.wrap(function (n) {
                  for (;;)
                    switch ((n.prev = n.next)) {
                      case 0:
                        if (t.imgUrls) {
                          n.next = 2;
                          break;
                        }
                        return n.abrupt("return");
                      case 2:
                        (r = []),
                          t.imgUrls.forEach(function (t) {
                            return r.push(e.loadImage(t));
                          }),
                          Promise.all(r)
                            .then(function (p) {
                              return t.callback && t.callback(), p;
                            })
                            .catch(function (t) {
                              return !1;
                            });
                      case 5:
                      case "end":
                        return n.stop();
                    }
                }, n);
              })
            )();
          },
        },
      });
      var Dr = {
        install: function (t) {
          (Number.prototype.formatMoney = function (t, e, n) {
            var r = this,
              s =
                ((t = isNaN((t = Math.abs(t))) ? 0 : t),
                (e = null == e ? "." : e),
                (n = null == n ? "," : n),
                r < 0 ? "-" : ""),
              i = String(parseInt((r = Math.abs(Number(r) || 0).toFixed(t)))),
              o = (o = i.length) > 3 ? o % 3 : 0;
            return (
              s +
              (o ? i.substr(0, o) + n : "") +
              i.substr(o).replace(/(\d{3})(?=\d)/g, "$1" + n) +
              (t
                ? e +
                  Math.abs(r - i)
                    .toFixed(t)
                    .slice(2)
                : "")
            );
          }),
            (String.prototype.sanitize = function () {
              if (this)
                return Mr(this)
                  .toLowerCase()
                  .replace(/ /g, "-")
                  .replace(/[^\w-]+/g, "");
            }),
            (String.prototype.capitalize = function () {
              return "string" != typeof this
                ? this
                : this.charAt(0).toUpperCase() + this.slice(1);
            }),
            (String.prototype.stripHtml = function () {
              if ("string" != typeof this) return this;
              var t = document.createElement("DIV");
              return (t.innerHTML = this), t.textContent || t.innerText || "";
            }),
            (l.default.prototype.convertToSlug = Ir.b),
            (l.default.prototype.capitalize = Ir.a),
            (l.default.prototype.getHostName = jr.a),
            (l.default.prototype.getUniqueValues = function (t, e) {
              if ((t = t.filter(Boolean)).length)
                return en()(
                  Ki()(t, function (t, n) {
                    return t.fields[e];
                  }).filter(function (t) {
                    return t || j()(t);
                  })
                ).sort();
            });
        },
      };
      l.default.use(Dr);
      l.default.use({
        install: function (t) {
          t.prototype.$bus = new t();
        },
      });
      var Wr = n(283),
        Hr = n(78);
      l.default.use(Wr.a);
      var Rr = function (t) {
        var e = t.app,
          n = t.store,
          r = {};
        Hr.locales.forEach(function (t, i) {
          r[t] = n.state.pages.translations[t];
        }),
          (e.i18n = new Wr.a({
            locale: n.state.locale,
            fallbackLocale: Hr.defaultLocale,
            silentTranslationWarn: !0,
            messages: r,
          })),
          (e.i18n.path = function (link) {
            return e.i18n.locale === e.i18n.fallbackLocale
              ? "/".concat(link)
              : "/".concat(e.i18n.locale, "/").concat(link);
          });
      };
      try {
        var zr = n(789);
        zr.keys().forEach(function (t) {
          var e = zr(t),
            n = t.split("/").pop().split(".")[0];
          l.default.component(n, e.default || e);
        });
      } catch (t) {
        console.error("ERROR: global-components", t);
      }
      var Fr = n(440),
        Xr = function (t) {
          var e = t.app,
            n = e.store,
            r = e.router;
          Object(Fr.sync)(n, r);
        },
        Br = n(220);
      Object.keys(Br).forEach(function (t) {
        Object(ii.c)(t, Br[t]);
      }),
        Object(ii.c)("required", {
          message: "This field is required",
        }),
        Object(ii.c)("email", {
          message: "This field needs to be email",
        }),
        Object(ii.c)("password", {
          params: ["target"],
          validate: function (t, e) {
            return t === e.target;
          },
          message: "Password confirmation does not match",
        });
      var Nr = n(441),
        qr = n.n(Nr);
      l.default.component("v-select", qr.a);
      var Yr = n(276),
        Vr = n(448);
      (window.PIXI = Yr), (window.filters = Vr);
      var Ur = n(443),
        Zr = n.n(Ur);
      l.default.use(Zr.a, {
        datePickerTintColor: "#F00",
        datePickerShowDayPopover: !1,
      });
      var Gr = n(444),
        Kr = n.n(Gr),
        Jr = {
          install: function (t, e) {
            t.component("split", Kr.a);
          },
        };
      l.default.use(Jr);
      var Qr = Jr,
        ta = n(445),
        ea = n.n(ta);
      l.default.use(ea.a);
      var ia = n(446);

      function na(object, t) {
        var e = Object.keys(object);
        if (Object.getOwnPropertySymbols) {
          var n = Object.getOwnPropertySymbols(object);
          t &&
            (n = n.filter(function (t) {
              return Object.getOwnPropertyDescriptor(object, t).enumerable;
            })),
            e.push.apply(e, n);
        }
        return e;
      }

      function ra(t) {
        for (var i = 1; i < arguments.length; i++) {
          var source = null != arguments[i] ? arguments[i] : {};
          i % 2
            ? na(Object(source), !0).forEach(function (e) {
                Object(o.a)(t, e, source[e]);
              })
            : Object.getOwnPropertyDescriptors
            ? Object.defineProperties(
                t,
                Object.getOwnPropertyDescriptors(source)
              )
            : na(Object(source)).forEach(function (e) {
                Object.defineProperty(
                  t,
                  e,
                  Object.getOwnPropertyDescriptor(source, e)
                );
              });
        }
        return t;
      }
      Object.defineProperty(l.default.prototype, "locomotiveScroll", {
        value: ia.a,
      }),
        l.default.component(h.a.name, h.a),
        l.default.component(
          m.a.name,
          ra(
            ra({}, m.a),
            {},
            {
              render: function (t, e) {
                return (
                  m.a._warned ||
                    ((m.a._warned = !0),
                    console.warn(
                      "<no-ssr> has been deprecated and will be removed in Nuxt 3, please use <client-only> instead"
                    )),
                  m.a.render(t, e)
                );
              },
            }
          )
        ),
        l.default.component(dn.name, dn),
        l.default.component("NChild", dn),
        l.default.component(vn.name, vn),
        l.default.use(c.a, {
          keyName: "head",
          attribute: "data-n-head",
          ssrAttribute: "data-n-head-ssr",
          tagIDKeyName: "hid",
        });
      var aa = {
        name: "page",
        mode: "out-in",
        appear: !1,
        appearClass: "appear",
        appearActiveClass: "appear-active",
        appearToClass: "appear-to",
      };

      function sa(t) {
        return oa.apply(this, arguments);
      }

      function oa() {
        return (oa = Object(r.a)(
          regeneratorRuntime.mark(function t(e) {
            var n,
              r,
              o,
              c,
              d,
              h,
              f,
              path,
              m,
              v = arguments;
            return regeneratorRuntime.wrap(function (t) {
              for (;;)
                switch ((t.prev = t.next)) {
                  case 0:
                    return (
                      (m = function (t, e) {
                        if (!t)
                          throw new Error(
                            "inject(key, value) has no key provided"
                          );
                        if (void 0 === e)
                          throw new Error(
                            "inject('".concat(
                              t,
                              "', value) has no value provided"
                            )
                          );
                        (d[(t = "$" + t)] = e),
                          d.context[t] || (d.context[t] = e),
                          (o[t] = d[t]);
                        var n = "__nuxt_" + t + "_installed__";
                        l.default[n] ||
                          ((l.default[n] = !0),
                          l.default.use(function () {
                            Object.prototype.hasOwnProperty.call(
                              l.default.prototype,
                              t
                            ) ||
                              Object.defineProperty(l.default.prototype, t, {
                                get: function () {
                                  return this.$root.$options[t];
                                },
                              });
                          }));
                      }),
                      (n = v.length > 1 && void 0 !== v[1] ? v[1] : {}),
                      (t.next = 4),
                      cn()
                    );
                  case 4:
                    return (
                      (r = t.sent),
                      ((o = Xn(e)).$router = r),
                      (c = o.registerModule),
                      (o.registerModule = function (path, t, e) {
                        return c.call(
                          o,
                          path,
                          t,
                          Object.assign(
                            {
                              preserveState: !0,
                            },
                            e
                          )
                        );
                      }),
                      (d = ra(
                        {
                          head: {
                            title: "Naba",
                            meta: [
                              {
                                charset: "utf-8",
                              },
                              {
                                name: "viewport",
                                content: "width=device-width, initial-scale=1",
                              },
                              {
                                hid: "description",
                                name: "description",
                                content:
                                  "THE PERSON TO HOLD, TO CARVE, TO RETURN THESE IMAGES TO YOU WHEN YOUR MIND CAN'T SEEM TO RECOLLECT THE ATOMS OF THOSE MOMENTS.",
                              },
                              {
                                "http-equiv": "X-UA-Compatible",
                                content: "IE=edge",
                              },
                            ],

                            link: [
                              {
                                rel: "preload",
                                as: "style",
                                href: "../fonts.css",
                              },
                            ],
                            style: [],
                          },
                          store: o,
                          router: r,
                          nuxt: {
                            defaultTransition: aa,
                            transitions: [aa],
                            setTransitions: function (t) {
                              return (
                                Array.isArray(t) || (t = [t]),
                                (t = t.map(function (t) {
                                  return (t = t
                                    ? "string" == typeof t
                                      ? Object.assign({}, aa, {
                                          name: t,
                                        })
                                      : Object.assign({}, aa, t)
                                    : aa);
                                })),
                                (this.$options.nuxt.transitions = t),
                                t
                              );
                            },
                            err: null,
                            dateErr: null,
                            error: function (t) {
                              (t = t || null),
                                (d.context._errored = Boolean(t)),
                                (t = t ? Object(gn.n)(t) : null);
                              var n = d.nuxt;
                              return (
                                this && (n = this.nuxt || this.$options.nuxt),
                                (n.dateErr = Date.now()),
                                (n.err = t),
                                e && (e.nuxt.error = t),
                                t
                              );
                            },
                          },
                        },
                        Wn
                      )),
                      (o.app = d),
                      (h = e
                        ? e.next
                        : function (t) {
                            return d.router.push(t);
                          }),
                      e
                        ? (f = r.resolve(e.url).route)
                        : ((path = Object(gn.f)(
                            r.options.base,
                            r.options.mode
                          )),
                          (f = r.resolve(path).route)),
                      (t.next = 15),
                      Object(gn.r)(d, {
                        store: o,
                        route: f,
                        next: h,
                        error: d.nuxt.error.bind(d),
                        payload: e ? e.payload : void 0,
                        req: e ? e.req : void 0,
                        res: e ? e.res : void 0,
                        beforeRenderFns: e ? e.beforeRenderFns : void 0,
                        ssrContext: e,
                      })
                    );
                  case 15:
                    if (
                      (m("config", n),
                      window.__NUXT__ &&
                        window.__NUXT__.state &&
                        o.replaceState(window.__NUXT__.state),
                      "function" != typeof Un.a)
                    ) {
                      t.next = 21;
                      break;
                    }
                    return (t.next = 21), Un()(d.context, m);
                  case 21:
                    return (t.next = 24), Jn(d.context);
                  case 24:
                    return (t.next = 27), Qn(d.context, m);
                  case 27:
                    t.next = 30;
                    break;
                  case 30:
                    return (t.next = 33), sr(d.context, m);
                  case 33:
                    t.next = 36;
                    break;
                  case 36:
                    return (t.next = 39), wr(d.context, m);
                  case 39:
                    if ("function" != typeof Lr) {
                      t.next = 42;
                      break;
                    }
                    return (t.next = 42), Lr(d.context, m);
                  case 42:
                    if ("function" != typeof Sr.a) {
                      t.next = 45;
                      break;
                    }
                    return (t.next = 45), Sr()(d.context, m);
                  case 45:
                    t.next = 48;
                    break;
                  case 48:
                    t.next = 51;
                    break;
                  case 51:
                    return (t.next = 54), Rr(d.context);
                  case 54:
                    t.next = 57;
                    break;
                  case 57:
                    return (t.next = 60), Xr(d.context);
                  case 60:
                    return (t.next = 63), void d.context;
                  case 63:
                    t.next = 66;
                    break;
                  case 66:
                    t.next = 69;
                    break;
                  case 69:
                    t.next = 72;
                    break;
                  case 72:
                    if ("function" != typeof Qr) {
                      t.next = 75;
                      break;
                    }
                    return (t.next = 75), Qr(d.context, m);
                  case 75:
                    t.next = 78;
                    break;
                  case 78:
                    t.next = 81;
                    break;
                  case 81:
                    0, (t.next = 85);
                    break;
                  case 85:
                    return t.abrupt("return", {
                      store: o,
                      app: d,
                      router: r,
                    });
                  case 86:
                  case "end":
                    return t.stop();
                }
            }, t);
          })
        )).apply(this, arguments);
      }
    },
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    function (t, e, n) {
      n(23), n(19), n(17), n(14), n(20);
      var r = n(96),
        o = n(97);

      function l(object, t) {
        var e = Object.keys(object);
        if (Object.getOwnPropertySymbols) {
          var n = Object.getOwnPropertySymbols(object);
          t &&
            (n = n.filter(function (t) {
              return Object.getOwnPropertyDescriptor(object, t).enumerable;
            })),
            e.push.apply(e, n);
        }
        return e;
      }
      t.exports = {
        functional: !0,
        render: function (t, e) {
          var n = e._c,
            c = e._v,
            data = e.data,
            d = e.children,
            h = void 0 === d ? [] : d,
            f = data.class,
            m = data.staticClass,
            style = data.style,
            v = data.staticStyle,
            y = data.attrs,
            w = void 0 === y ? {} : y,
            x = o(data, [
              "class",
              "staticClass",
              "style",
              "staticStyle",
              "attrs",
            ]);
          return n(
            "svg",
            (function (t) {
              for (var i = 1; i < arguments.length; i++) {
                var source = null != arguments[i] ? arguments[i] : {};
                i % 2
                  ? l(Object(source), !0).forEach(function (e) {
                      r(t, e, source[e]);
                    })
                  : Object.getOwnPropertyDescriptors
                  ? Object.defineProperties(
                      t,
                      Object.getOwnPropertyDescriptors(source)
                    )
                  : l(Object(source)).forEach(function (e) {
                      Object.defineProperty(
                        t,
                        e,
                        Object.getOwnPropertyDescriptor(source, e)
                      );
                    });
              }
              return t;
            })(
              {
                class: [f, m],
                style: [style, v],
                attrs: Object.assign(
                  {
                    viewBox: "0 0 23 58",
                    version: "1.1",
                    xmlns: "http://www.w3.org/2000/svg",
                    "xmlns:xlink": "http://www.w3.org/1999/xlink",
                  },
                  w
                ),
              },
              x
            ),
            h.concat([
              n("title", [c("memory_icon")]),
              n("desc", [c("Created with Sketch.")]),
              n(
                "g",
                {
                  attrs: {
                    id: "Page-1",
                    stroke: "none",
                    "stroke-width": "1",
                    fill: "none",
                    "fill-rule": "evenodd",
                  },
                },
                [
                  n(
                    "g",
                    {
                      attrs: {
                        id: "Homepage",
                        transform: "translate(-949.000000, -1856.000000)",
                      },
                    },
                    [
                      n(
                        "g",
                        {
                          attrs: {
                            id: "2",
                            transform: "translate(499.000000, 1195.000000)",
                          },
                        },
                        [
                          n(
                            "g",
                            {
                              attrs: {
                                id: "memory_icon",
                                transform: "translate(421.000000, 650.000000)",
                              },
                            },
                            [
                              n("rect", {
                                attrs: {
                                  id: "Rectangle",
                                  x: "0",
                                  y: "0",
                                  width: "80",
                                  height: "80",
                                },
                              }),
                              n("path", {
                                attrs: {
                                  d: "M40.5,39.8740201 C48.1666667,32.5255231 52,26.7341831 52,22.5 C52,16.1487254 46.8512746,11 40.5,11 C34.1487254,11 29,16.1487254 29,22.5 C29,26.7341831 32.8333333,32.5255231 40.5,39.8740201 Z",
                                  id: "Oval",
                                  fill: "#000000",
                                },
                              }),
                              n("path", {
                                attrs: {
                                  d: "M40.5,68.8740201 C48.1666667,61.5255231 52,55.7341831 52,51.5 C52,45.1487254 46.8512746,40 40.5,40 C34.1487254,40 29,45.1487254 29,51.5 C29,55.7341831 32.8333333,61.5255231 40.5,68.8740201 Z",
                                  id: "Oval-Copy",
                                  fill: "#000000",
                                  transform:
                                    "translate(40.500000, 54.437010) rotate(-180.000000) translate(-40.500000, -54.437010) ",
                                },
                              }),
                            ]
                          ),
                        ]
                      ),
                    ]
                  ),
                ]
              ),
            ])
          );
        },
      };
    },
    function (t) {
      t.exports = JSON.parse(
        '{"title":"Naba","description":"THE PERSON TO HOLD, TO CARVE, TO RETURN THESE IMAGES TO YOU WHEN YOUR MIND CAN\'T SEEM TO RECOLLECT THE ATOMS OF THOSE MOMENTS.","image":"https://images.ctfassets.net/tjd3v9sv4r3f/7cwmKvEXZdlq8tCD3uSBrw/1064605d855ae35fd037c860535f813d/mary_and_tyler__brookings_oregon_elopement-183.jpg?w=1024","defaultLocale":"en","locales":["en"],"multilanguage":false,"production_url":"http://boilerplate.com/"}'
      );
    },
    function (t, e, n) {
      "use strict";
      n.d(e, "a", function () {
        return o;
      }),
        n.d(e, "c", function () {
          return l;
        }),
        n.d(e, "d", function () {
          return c;
        }),
        n.d(e, "b", function () {
          return d;
        });
      n(40);
      var r = n(310).remove;

      function o(t) {
        return "string" != typeof t
          ? t
          : t.charAt(0).toUpperCase() + t.slice(1);
      }

      function l(t) {
        if (t)
          return r(t)
            .toLowerCase()
            .replace(/ /g, "-")
            .replace(/[^\w-]+/g, "");
      }

      function c(t) {
        return "string" != typeof t
          ? ""
          : t.replace(/<(?:.|\n)*?>/gm, "").replace(/^\s+|\s+$/gm, "");
      }

      function d(t) {
        return t
          ? t
              .toLowerCase()
              .replace(/ /g, "-")
              .replace(/[^\w-]+/g, "")
          : null;
      }
    },
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    function (t, e, n) {
      "use strict";
      var r = n(89),
        o = n.n(r),
        l = n(35),
        c = n.n(l),
        d = {
          data: function () {
            return {
              height: 0,
              bounds: {},
              topBound: 0,
            };
          },
          props: [
            "speedFactor",
            "scaleFactor",
            "wrapped",
            "absolute",
            "offset",
            "className",
            "global",
            "topOffset",
            "horizontal",
            "debug",
            "progress",
            "startOffset",
            "y",
          ],
          mounted: function () {
            var t = this;
            this.resizeHandler(),
              o()(this.$el, function () {
                setTimeout(function () {
                  t.$nextTick(t.resizeHandler), t.$bus.$emit("resize");
                });
              }),
              this.$device.isDesktop &&
                this.$bus.$on("resize", this.resizeHandler);
          },
          destroyed: function () {
            this.$device.isDesktop &&
              this.$bus.$off("resize", this.resizeHandler);
          },
          computed: {
            elStyle: function () {
              return {
                top: this.elTop + "px",
                height: this.elHeight + "px",
              };
            },
            noScale: function () {
              return this.pageHeight == this.height && this.speedFactor > 0;
            },
            noOffseting: function () {
              return 0 == this.topBound && !this.$device.isMobileOrTablet;
            },
            classObject: function () {
              return {
                wrapped: this.wrapped,
                "position-absolute": this.absolute,
              };
            },
            scrollTop: function () {
              return this.$store.getters["app/getState"]("scrollTop");
            },
            pageHeight: function () {
              return this.$store.getters["app/getState"]("height");
            },
            pageWidth: function () {
              return this.$store.getters["app/getState"]("width");
            },
            currentProgress: function () {
              return this.scrollProgress(this.start, this.end, this.scrollTop);
            },
            currentMove: function () {
              return this.move * this.currentProgress;
            },
            start: function () {
              var t = this.topOffset || this.topBound;
              return c()(this.offset) || 0 === t
                ? 0
                : t - this.pageHeight - (this.startOffset || 0);
            },
            end: function () {
              var t = this.start + this.height;
              return (
                (t += this.noOffseting ? 0 : this.pageHeight),
                this.wrapped ? t : t + this.move / 2
              );
            },
            multiplier: function () {
              return this.pageHeight > this.pageWidth ? 1 : 0.7;
            },
            move: function () {
              return this.global
                ? this.pageWidth * this.speedFactor * this.multiplier
                : this.height * this.speedFactor;
            },
            elHeight: function () {
              return this.getSize(this.horizontal);
            },
            elTop: function () {
              return this.getOffset(this.horizontal);
            },
          },
          watch: {
            currentProgress: function (t) {
              this.$emit("update:progress", t);
            },
            pageWidth: function () {
              this.$device.isDesktop || this.resizeHandler();
            },
            y: function () {
              this.resizeHandler();
            },
          },
          methods: {
            getSize: function (t) {
              if (this.wrapped && !t)
                return (
                  (this.noScale || (0 === this.topBound && this.speedFactor > 0)
                    ? this.height
                    : Math.ceil(this.height + Math.abs(this.move))) || ""
                );
            },
            getOffset: function (t) {
              if (!t) {
                var e = -this.move;
                return c()(this.offset)
                  ? this.offset
                  : (e > 0 && this.wrapped) ||
                    (e > 0 && !this.wrapped) ||
                    this.noOffseting
                  ? 0
                  : this.wrapped && this.noScale
                  ? e / 2
                  : e;
              }
            },
            scrollProgress: function (t, e, n) {
              return (n = n || this.scrollTop) >= t && n <= e
                ? (n - t) / ((e - t) / 100) / 100
                : n > t
                ? 1
                : 0;
            },
            resizeHandler: function () {
              this.$el &&
                ((this.bounds = this.$el.getBoundingClientRect()),
                (this.topBound =
                  0 != this.y || this.$device.isMobileOrTablet
                    ? this.scrollTop + this.bounds.top
                    : this.bounds.top),
                (this.height = this.$el.offsetHeight));
            },
          },
        };
      e.a = d;
    },
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    function (t, e, n) {
      "use strict";
      n.d(e, "c", function () {
        return r;
      }),
        n.d(e, "b", function () {
          return o;
        }),
        n.d(e, "a", function () {
          return l;
        });
      n(301), n(55), n(40), n(143);

      function r(t) {
        return new RegExp(/^[a-z]{2}-[A-Z]{2}$/).test(t);
      }

      function o(t) {
        var e =
            arguments.length > 1 && void 0 !== arguments[1]
              ? arguments[1]
              : "en_US",
          n = e;
        if ("string" != typeof t) return n;
        var o = (t = t.replace(/^\/|\/$/g, "")).split("/"),
          l = o[0];
        return r(l) && (n = l), n;
      }

      function l(t) {
        var e = t.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
        return null != e &&
          e.length > 2 &&
          "string" == typeof e[2] &&
          e[2].length > 0
          ? e[2]
          : null;
      }
    },
    ,
    function (t, e, n) {
      (function (e) {
        var r = n(806),
          o = n(600),
          l = {
            space: o.CTF_SPACE_ID,
            accessToken:
              "development" === e.env.environment
                ? o.CTF_PREVIEW_TOKEN
                : o.CTF_ACCESS_TOKEN,
            host:
              "development" === e.env.environment
                ? o.CTF_DEV_HOST
                : o.CTF_PROD_HOST,
          };
        t.exports = {
          createClient: function () {
            return r.createClient(l);
          },
        };
      }.call(this, n(26)));
    },
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    function (t, e, n) {},
    ,
    function (t) {
      t.exports = JSON.parse(
        '[{"url":"/","id":"5opC3AmRcCK7kkt86BGesE","component":"BasicPage","title":"Home"},{"url":"/about","id":"3wIZCLkeGseweDTx198WUN","component":"BasicPage","title":"About"},{"url":"/contact","id":"4e03MkzpjfCRTdLPAXITCs","component":"BasicPage","title":"Contact"},{"url":"/portfolio","id":"1oR5hmkKR4hMFOETYA7zx6","component":"Portfolio","title":"Portfolio"},{"url":"/wedding-packages","id":"5beLhQVVw1ikUlft7lG3wM","component":"BasicPage","title":"Wedding Packages"},{"url":"/blog","id":"4y1cUBHULSY4I5Puot8yZq","component":"BasicPage","title":"Blog"},{"url":"/elopement-packages","id":"5BGPfmenBqcuyU6V7YyMWs","component":"BasicPage","title":"Elopement Packages"},{"url":"/portland-oregon-wedding-cakes","id":"4Aty5HxU2d4Lo8fxIXrzhI","component":"BasicPage","title":"portland-oregon-wedding-cakes"},{"url":"/placeholder-5","id":"5hUubx0LQII5boryzojzLT","component":"BasicPage","title":"Placeholder (5)"},{"url":"/placeholder-7","id":"5LkCylvOqw0mjnsqYT9JXW","component":"BasicPage","title":"Placeholder (7)"},{"url":"/placeholder-2","id":"4BkF5mfaQG32XSealEqEz1","component":"BasicPage","title":"Placeholder (2)"},{"url":"/best-oregon-elopement-locations","id":"LUiamUJ1qNjguPFYEjOFU","component":"BasicPage","title":"best-oregon-elopement-locations"},{"url":"/best-places-to-elope-in-california","id":"5OSEJ2dZrg8hyrwVn5K9Sd","component":"BasicPage","title":"best-places-to-elope-in-california"},{"url":"/wedding-dress-shops-in-portland","id":"7zUZTCFe450Dre0Dpwp7Rq","component":"BasicPage","title":"wedding-dress-shops-in-portland"},{"url":"/best-places-to-elope-in-washington","id":"WhoUxvNO0lThOcX8qkeGu","component":"BasicPage","title":"best-places-to-elope-in-washington"},{"url":"/oregon-elopement-packages","id":"64NfmO7CeHrJL74YpkcB4u","component":"BasicPage","title":"oregon-elopement-packages"},{"url":"/real-estate-photography-portland","id":"7yMdOQbvFZSsX5swwFuzA","component":"BasicPage","title":"real-estate-photography-portland"},{"url":"/commercial-photographer-portland","id":"1jK919f3iOkJG5p92CnkOY","component":"BasicPage","title":"commercial-photographer-portland"},{"url":"/newborn-photography-portland","id":"4Ss7jZ2nrqOoYsNn2ScTno","component":"BasicPage","title":"newborn-photography-portland"},{"url":"/portland-maternity-photographer","id":"1NceQaAcD4mG1Got3NaHN4","component":"BasicPage","title":"portland-maternity-photographer"},{"url":"/portland-family-photographer","id":"vXgEq3iD5cD7yKOJ9dQSG","component":"BasicPage","title":"portland-family-photographer"},{"url":"/portland-portrait-photographer","id":"7xGpSGMKmOt4IxBcn2J6FI","component":"BasicPage","title":"portland-portrait-photographer"},{"url":"/wedding-photography-ideas","id":"4Kv5nFEVNMSE8mf21O5B3t","component":"BasicPage","title":"wedding-photography-ideas"},{"url":"/portland-wedding-venues","id":"1DvHhgGeNKA1oxvokUZqEz","component":"BasicPage","title":"portland-wedding-venues"},{"url":"/wedding-photography-checklist","id":"4Qp2ZTiQgIGtWlYcFRJfwh","component":"BasicPage","title":"wedding-photography-checklist"},{"url":"/wedding-catering-portland-or","id":"7Fzd644UAmktRrSPJag3Dc","component":"BasicPage","title":"wedding-catering-portland-or"},{"url":"/portland-wedding-dj","id":"1qBTpCoPx4svLpFbzNCHaY","component":"BasicPage","title":"portland-wedding-dj"},{"url":"/wedding-planner-portland-oregon","id":"1whhmCrJh6VyhAIICoSkJP","component":"BasicPage","title":"wedding-planner-portland-oregon"},{"url":"/portfolio/fox-and-the-wolf","id":"AyNCp0I7cxdeRu4mjobnd","component":"Project","title":"The Fox & The Wolf"},{"url":"/portfolio/romantic-southern-oregon-coast-elopement","id":"39gUcwgScSs9VMECwwUSkR","component":"Project","title":"Romantic Southern Oregon Coast Elopement"},{"url":"/portfolio/a-moody-woodland-wedding","id":"2U4Wnfm70GHWbugIPyxqiI","component":"Project","title":"A Moody Woodland Wedding"},{"url":"/portfolio/castaway-portland-wedding","id":"6NHoPKEDXJyn6uLmb2uRm8","component":"Project","title":"Castaway Portland Wedding"},{"url":"/portfolio/cape-kiwanda-elopement","id":"CdNibuBQeWyTxAq5Oh6Sc","component":"Project","title":"Cape Kiwanda Elopement"},{"url":"/portfolio/intimate-sayulita-wedding-villa-del-oso","id":"7e7XuNMqF9RP7g9VB6022H","component":"Project","title":"Intimate Sayulita Wedding at Villa Del Oso"},{"url":"/portfolio/overnight-pacific-city-engagement-session","id":"4NPkg6bckzHBFffRGiteNx","component":"Project","title":"Overnight Pacific City Engagement Session"},{"url":"/portfolio/foggy-mt-hood-wedding","id":"15ezOfVro1EYu3blOQHz9A","component":"Project","title":"A Foggy Mt. Hood Wedding"},{"url":"/portfolio/witch-castle-elopement","id":"3SNywHjzKk1FY7YZvl0950","component":"Project","title":"Witch’s Castle Elopement"},{"url":"/portfolio/cozy-columbia-river-gorge-engagement","id":"6kggV3CpinXAiC0VW3hoMr","component":"Project","title":"Cozy Columbia River Gorge Engagement"},{"url":"/portfolio/portland-engagement-photographer","id":"4J5RnUFZTOYOy1q8rIeTff","component":"Project","title":"Portland Engagement Photographer"},{"url":"/portfolio/victoria-beach-engagement","id":"6vh2LNbZElOzbmwwF6OwT3","component":"Project","title":"Victoria Beach Engagement"},{"url":"/portfolio/oswald-west-state-park","id":"7yD9VT9n8jIRh2n1m1nZkH","component":"Project","title":"Esther x Claire La Faye "},{"url":"/portfolio/beacon-rock-state-park-engagement","id":"22RM7z8Hn10hsS29p7ZpSY","component":"Project","title":"Beacon Rock State Park Engagement"},{"url":"/portfolio/panther-falls-elopement","id":"3RmvBx5D7EtQgRja9xur1M","component":"Project","title":"Panther Falls Elopement "},{"url":"/portfolio/blue-runaway","id":"7M9pVvQF9O4pxlJ5obpeLf","component":"Project","title":"Blue Runaway "},{"url":"/portfolio/astoria-coast-elopement","id":"5t9YmBMCGN0x1HKmpf942r","component":"Project","title":"Astoria Coast Elopement"}]'
      );
    },
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    function (t, e, n) {
      "use strict";
      n(17),
        n(91),
        n(19),
        n(54),
        n(83),
        n(38),
        n(71),
        n(72),
        n(14),
        n(117),
        n(142);
      var r = n(7);

      function o(t, e) {
        var n;
        if ("undefined" == typeof Symbol || null == t[Symbol.iterator]) {
          if (
            Array.isArray(t) ||
            (n = (function (t, e) {
              if (!t) return;
              if ("string" == typeof t) return l(t, e);
              var n = Object.prototype.toString.call(t).slice(8, -1);
              "Object" === n && t.constructor && (n = t.constructor.name);
              if ("Map" === n || "Set" === n) return Array.from(t);
              if (
                "Arguments" === n ||
                /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)
              )
                return l(t, e);
            })(t)) ||
            (e && t && "number" == typeof t.length)
          ) {
            n && (t = n);
            var i = 0,
              r = function () {};
            return {
              s: r,
              n: function () {
                return i >= t.length
                  ? {
                      done: !0,
                    }
                  : {
                      done: !1,
                      value: t[i++],
                    };
              },
              e: function (t) {
                throw t;
              },
              f: r,
            };
          }
          throw new TypeError(
            "Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
          );
        }
        var o,
          c = !0,
          d = !1;
        return {
          s: function () {
            n = t[Symbol.iterator]();
          },
          n: function () {
            var t = n.next();
            return (c = t.done), t;
          },
          e: function (t) {
            (d = !0), (o = t);
          },
          f: function () {
            try {
              c || null == n.return || n.return();
            } finally {
              if (d) throw o;
            }
          },
        };
      }

      function l(t, e) {
        (null == e || e > t.length) && (e = t.length);
        for (var i = 0, n = new Array(e); i < e; i++) n[i] = t[i];
        return n;
      }
      var c =
          window.requestIdleCallback ||
          function (t) {
            var e = Date.now();
            return setTimeout(function () {
              t({
                didTimeout: !1,
                timeRemaining: function () {
                  return Math.max(0, 50 - (Date.now() - e));
                },
              });
            }, 1);
          },
        d =
          window.cancelIdleCallback ||
          function (t) {
            clearTimeout(t);
          },
        h =
          window.IntersectionObserver &&
          new window.IntersectionObserver(function (t) {
            t.forEach(function (t) {
              var e = t.intersectionRatio,
                link = t.target;
              e <= 0 || link.__prefetch();
            });
          });
      e.a = {
        name: "NuxtLink",
        extends: r.default.component("RouterLink"),
        props: {
          prefetch: {
            type: Boolean,
            default: !0,
          },
          noPrefetch: {
            type: Boolean,
            default: !1,
          },
        },
        mounted: function () {
          this.prefetch &&
            !this.noPrefetch &&
            (this.handleId = c(this.observe, {
              timeout: 2e3,
            }));
        },
        beforeDestroy: function () {
          d(this.handleId),
            this.__observed &&
              (h.unobserve(this.$el), delete this.$el.__prefetch);
        },
        methods: {
          observe: function () {
            h &&
              this.shouldPrefetch() &&
              ((this.$el.__prefetch = this.prefetchLink.bind(this)),
              h.observe(this.$el),
              (this.__observed = !0));
          },
          shouldPrefetch: function () {
            return this.getPrefetchComponents().length > 0;
          },
          canPrefetch: function () {
            var t = navigator.connection;
            return !(
              this.$nuxt.isOffline ||
              (t && ((t.effectiveType || "").includes("2g") || t.saveData))
            );
          },
          getPrefetchComponents: function () {
            return this.$router
              .resolve(this.to, this.$route, this.append)
              .resolved.matched.map(function (t) {
                return t.components.default;
              })
              .filter(function (t) {
                return "function" == typeof t && !t.options && !t.__prefetched;
              });
          },
          prefetchLink: function () {
            if (this.canPrefetch()) {
              h.unobserve(this.$el);
              var t,
                e = o(this.getPrefetchComponents());
              try {
                for (e.s(); !(t = e.n()).done; ) {
                  var n = t.value,
                    r = n();
                  r instanceof Promise && r.catch(function () {}),
                    (n.__prefetched = !0);
                }
              } catch (t) {
                e.e(t);
              } finally {
                e.f();
              }
            }
          },
        },
      };
    },
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    function (t, e, n) {
      "use strict";
      n.r(e);
      n(40), n(17), n(14);
      var r = n(784),
        o = {};
      r.keys().forEach(function (t) {
        var e = t
          .replace(/(\.\/|\.store\.js)/g, "")
          .replace(/^\w/, function (t) {
            return t.toLowerCase();
          });
        o[e] = r(t).default || r(t);
      }),
        (e.default = o);
    },
    ,
    ,
    function (t, e, n) {
      "use strict";
      var r = {};
      (r[301] = n(473)),
        (r[301] = r[301].default || r[301]),
        (r.i18n = n(478)),
        (r.i18n = r.i18n.default || r.i18n),
        (e.a = r);
    },
    ,
    ,
    function (t, e) {},
    ,
    function (t, e, n) {
      n(30);
      var r = n(785);

      function o() {
        return (o = r(
          regeneratorRuntime.mark(function t() {
            var e, r, o;
            return regeneratorRuntime.wrap(function (t) {
              for (;;)
                switch ((t.prev = t.next)) {
                  case 0:
                    if (!(!1 in navigator)) {
                      t.next = 2;
                      break;
                    }
                    throw new Error(
                      "serviceWorker is not supported in current browser!"
                    );
                  case 2:
                    return (t.next = 4), n.e(4).then(n.bind(null, 818));
                  case 4:
                    return (
                      (e = t.sent),
                      (r = e.Workbox),
                      (o = new r("../sw.js", {
                        scope: "/",
                      })),
                      (t.next = 9),
                      o.register()
                    );
                  case 9:
                    return t.abrupt("return", o);
                  case 10:
                  case "end":
                    return t.stop();
                }
            }, t);
          })
        )).apply(this, arguments);
      }
      window.$workbox = (function () {
        return o.apply(this, arguments);
      })().catch(function (t) {});
    },
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    function (t, e, n) {
      "use strict";
      n.r(e);
      e.default = {
        namespaced: !0,
        state: function () {
          return {
            defaultTitle: "Nesto nesto",
            scrollTop: 0,
            width: 0,
            height: 0,
            webp: !1,
            fontsLoaded: !1,
            scrolled: !1,
            scrollStart: !1,
            menuOpen: !1,
            myAccountPopUp: !1,
            mouseX: 0,
            mouseY: 0,
            cursorBounds: null,
            mouseDown: !1,
            currentScene: !1,
            whiteNav: !1,
            blackLogo: !1,
            blackNav: !1,
            addedProduct: null,
            notification: null,
            showLoader: !1,
            animationDelay: 4.7,
            acceptHeader: null,
            isWebkit: !1,
          };
        },
        getters: {
          getState: function (t) {
            return function (e) {
              return t[e];
            };
          },
        },
        actions: {
          SET_STATE: function (t, e) {
            var n = t.commit;
            for (var r in e)
              e.hasOwnProperty(r) &&
                n("SET_STATE", {
                  prop: r,
                  value: e[r],
                });
          },
        },
        mutations: {
          SET_STATE: function (t, e) {
            var n = e.prop,
              r = e.value;
            t[n] = r;
          },
        },
      };
    },
    function (t, e, n) {
      "use strict";
      n.r(e);
      n(30);
      var r = n(11),
        o = (n(92), n(215)),
        l = n.n(o),
        c = n(52),
        d = n(5),
        h = n.n(d),
        f = n(59),
        m = n.n(f),
        v = n(433),
        y = n.n(v),
        w = {
          getState: function (t) {
            return function (e) {
              return t[e];
            };
          },
          getParsedPages: function (t) {
            return t.parsedPages;
          },
          getPages: function (t) {
            return t.pages;
          },
          getUrls: function (t) {
            return t.urls;
          },
          getManyByAttr: function (t) {
            return function (e, n) {
              var r =
                arguments.length > 2 && void 0 !== arguments[2]
                  ? arguments[2]
                  : "urls";
              return t[r].filter(function (t) {
                return h()(t, e) === n;
              });
            };
          },
          getByAttr: function (t) {
            return function (e, n) {
              var r =
                arguments.length > 2 && void 0 !== arguments[2]
                  ? arguments[2]
                  : "urls";
              return t[r].find(function (t) {
                return h()(t, e) === n;
              });
            };
          },
        },
        x = {
          SET_STATE: function (t, e) {
            var n = t.commit;
            for (var r in e)
              e.hasOwnProperty(r) &&
                n("SET_STATE", {
                  prop: r,
                  value: e[r],
                });
          },
          LOAD_BY_TYPE: function (t, e) {
            return Object(r.a)(
              regeneratorRuntime.mark(function n() {
                var r, o, d, h;
                return regeneratorRuntime.wrap(function (n) {
                  for (;;)
                    switch ((n.prev = n.next)) {
                      case 0:
                        return (
                          (r = t.commit),
                          (o = t.getters),
                          t.dispatch,
                          (d = o.getState("urls")),
                          (n.next = 4),
                          c.a.loadByType(
                            {
                              content_type: e.type,
                              include: e.include,
                              limit: e.limit,
                              locale: e.locale,
                            },
                            {
                              parse: e.parse,
                              urls: d,
                            }
                          )
                        );
                      case 4:
                        return (
                          (h = n.sent),
                          e.single && (h = h[0]),
                          (h = JSON.parse(l()(h, null, 2))),
                          r("SET_STATE", {
                            prop: e.state,
                            value: h,
                          }),
                          n.abrupt("return", h)
                        );
                      case 9:
                      case "end":
                        return n.stop();
                    }
                }, n);
              })
            )();
          },
          LOAD_PROJECTS: function (t, e) {
            return Object(r.a)(
              regeneratorRuntime.mark(function n() {
                var r, o;
                return regeneratorRuntime.wrap(function (n) {
                  for (;;)
                    switch ((n.prev = n.next)) {
                      case 0:
                        if (
                          (t.commit,
                          t.getters,
                          (r = t.dispatch),
                          !(o = t.state).projects.length)
                        ) {
                          n.next = 3;
                          break;
                        }
                        return n.abrupt("return", o.projects);
                      case 3:
                        return n.abrupt(
                          "return",
                          r("LOAD_BY_TYPE", {
                            type: "project",
                            include: 3,
                            limit: 1e3,
                            parse: !0,
                            state: "projects",
                            locale: e,
                          })
                        );
                      case 4:
                      case "end":
                        return n.stop();
                    }
                }, n);
              })
            )();
          },
          LOAD_FOOTER: function (t, e) {
            return Object(r.a)(
              regeneratorRuntime.mark(function n() {
                var r;
                return regeneratorRuntime.wrap(function (n) {
                  for (;;)
                    switch ((n.prev = n.next)) {
                      case 0:
                        return (
                          t.commit,
                          t.getters,
                          (r = t.dispatch),
                          n.abrupt(
                            "return",
                            r("LOAD_BY_TYPE", {
                              type: "pageFooter",
                              include: 1,
                              limit: 1,
                              parse: !0,
                              single: !0,
                              state: "footer",
                              locale: e,
                            })
                          )
                        );
                      case 2:
                      case "end":
                        return n.stop();
                    }
                }, n);
              })
            )();
          },
          LOAD_NAVIGATION: function (t, e) {
            return Object(r.a)(
              regeneratorRuntime.mark(function n() {
                var r;
                return regeneratorRuntime.wrap(function (n) {
                  for (;;)
                    switch ((n.prev = n.next)) {
                      case 0:
                        return (
                          t.commit,
                          t.getters,
                          (r = t.dispatch),
                          n.abrupt(
                            "return",
                            r("LOAD_BY_TYPE", {
                              type: "navigation",
                              include: 1,
                              limit: 1,
                              parse: !0,
                              single: !0,
                              state: "navigation",
                              locale: e,
                            })
                          )
                        );
                      case 2:
                      case "end":
                        return n.stop();
                    }
                }, n);
              })
            )();
          },
          LOAD_TRANSLATIONS: function (t, e) {
            return Object(r.a)(
              regeneratorRuntime.mark(function n() {
                var r, o, tr;
                return regeneratorRuntime.wrap(function (n) {
                  for (;;)
                    switch ((n.prev = n.next)) {
                      case 0:
                        return (
                          (r = t.commit),
                          t.getters,
                          t.dispatch,
                          (n.next = 3),
                          c.a.loadByType({
                            content_type: "translation",
                            include: 1,
                            limit: 1e3,
                            locale: e,
                          })
                        );
                      case 3:
                        if ((o = n.sent)) {
                          n.next = 6;
                          break;
                        }
                        return n.abrupt("return");
                      case 6:
                        (tr = m()(
                          y()(o, function (t) {
                            return t.fields.title;
                          }),
                          function (t, e) {
                            return t.fields.translation;
                          }
                        )),
                          r("SET_STATE", {
                            prop: "translations",
                            value: tr,
                          });
                      case 8:
                      case "end":
                        return n.stop();
                    }
                }, n);
              })
            )();
          },
        };
      e.default = {
        namespaced: !0,
        state: function () {
          return {
            activePage: null,
            translations: {},
            navigation: {},
            footer: {},
            intro: {},
            projects: [],
            urls: [],
          };
        },
        getters: w,
        actions: x,
        mutations: {
          SET_STATE: function (t, e) {
            var n = e.prop,
              r = e.value;
            t[n] = r;
          },
        },
      };
    },
    function (t, e, n) {
      "use strict";
      n.r(e);
      e.default = {
        namespaced: !0,
        state: function () {
          return {
            containers: [],
            filterScale: 0.005,
            spriteScale: 8,
            contrast: 0.5,
            contrastMultiplier: 4,
            brightness: 0.5,
            reversedBrightness: 0,
          };
        },
        getters: {
          getState: function (t) {
            return function (e) {
              return t[e];
            };
          },
        },
        actions: {
          ADD_CONTAINER: function (t, e) {
            var n = t.commit;
            t.getters, t.dispatch;
            n("ADD_CONTAINER", e);
          },
          SET_STATE: function (t, e) {
            var n = t.commit;
            for (var r in e)
              e.hasOwnProperty(r) &&
                n("SET_STATE", {
                  prop: r,
                  value: e[r],
                });
          },
        },
        mutations: {
          ADD_CONTAINER: function (t, data) {
            t.containers.push({
              domElement: data.domElement,
              image: data.image,
            });
          },
          SET_STATE: function (t, e) {
            var n = e.prop,
              r = e.value;
            t[n] = r;
          },
        },
      };
    },
    ,
    ,
    ,
    ,
    ,
    ,
    function (t) {
      t.exports = JSON.parse(
        '{"page":[{"value":"title","key":"fields.metaTitle","fallback_key":"fields.title","default_key":"title","prependTitle":true,"attribute":"name"},{"value":"description","key":"fields.metaDescription","default_key":"description","attribute":"name"},{"value":"og:title","key":"fields.metaTitle","fallback_key":"fields.title","default_key":"title","prependTitle":true,"attribute":"property"},{"value":"og:description","key":"fields.metaDescription","default_key":"description","attribute":"property"},{"value":"twitter:title","key":"fields.metaTitle","fallback_key":"fields.title","default_key":"title","prependTitle":true,"attribute":"name"},{"value":"twitter:description","key":"fields.metaDescription","default_key":"description","attribute":"name"},{"value":"twitter:card","key":"fields.metaDescription","default_key":"description","attribute":"name"},{"value":"og:image","key":"fields.metaImage.fields.file.url","default_key":"image","attribute":"property"}],"project":[{"value":"title","key":"fields.title","fallback_key":"fields.title","default_key":"title","prependTitle":true,"attribute":"name"},{"value":"description","key":"fields.components[0].fields.content","default_key":"description","attribute":"name"},{"value":"og:title","key":"fields.title","fallback_key":"fields.title","default_key":"title","prependTitle":true,"attribute":"property"},{"value":"og:description","key":"fields.components[0].fields.content","default_key":"description","attribute":"property"},{"value":"twitter:title","key":"fields.title","fallback_key":"fields.title","default_key":"title","prependTitle":true,"attribute":"name"},{"value":"twitter:description","key":"fields.components[0].fields.content","default_key":"description","attribute":"name"},{"value":"twitter:card","key":"fields.components[0].fields.content","default_key":"description","attribute":"name"},{"value":"og:image","key":"fields.previewImage.fields.file.url","default_key":"image","attribute":"property"}]}'
      );
    },
    ,
    ,
    function (t, e, n) {
      n(23), n(19), n(17), n(14), n(20);
      var r = n(96),
        o = n(97);

      function l(object, t) {
        var e = Object.keys(object);
        if (Object.getOwnPropertySymbols) {
          var n = Object.getOwnPropertySymbols(object);
          t &&
            (n = n.filter(function (t) {
              return Object.getOwnPropertyDescriptor(object, t).enumerable;
            })),
            e.push.apply(e, n);
        }
        return e;
      }
      t.exports = {
        functional: !0,
        render: function (t, e) {
          var n = e._c,
            c = e._v,
            data = e.data,
            d = e.children,
            h = void 0 === d ? [] : d,
            f = data.class,
            m = data.staticClass,
            style = data.style,
            v = data.staticStyle,
            y = data.attrs,
            w = void 0 === y ? {} : y,
            x = o(data, [
              "class",
              "staticClass",
              "style",
              "staticStyle",
              "attrs",
            ]);
          return n(
            "svg",
            (function (t) {
              for (var i = 1; i < arguments.length; i++) {
                var source = null != arguments[i] ? arguments[i] : {};
                i % 2
                  ? l(Object(source), !0).forEach(function (e) {
                      r(t, e, source[e]);
                    })
                  : Object.getOwnPropertyDescriptors
                  ? Object.defineProperties(
                      t,
                      Object.getOwnPropertyDescriptors(source)
                    )
                  : l(Object(source)).forEach(function (e) {
                      Object.defineProperty(
                        t,
                        e,
                        Object.getOwnPropertyDescriptor(source, e)
                      );
                    });
              }
              return t;
            })(
              {
                class: [f, m],
                style: [style, v],
                attrs: Object.assign(
                  {
                    viewBox: "0 0 919 1123",
                    version: "1.1",
                    xmlns: "http://www.w3.org/2000/svg",
                    "xmlns:xlink": "http://www.w3.org/1999/xlink",
                  },
                  w
                ),
              },
              x
            ),
            h.concat([
              n("title", [c("Group 2")]),
              n("desc", [c("Created with Sketch.")]),
              n("defs", [
                n("polygon", {
                  attrs: {
                    id: "path-1",
                    points: "0.8552 0.2142 61.8552 0.2142 61.8552 88 0.8552 88",
                  },
                }),
                n("polygon", {
                  attrs: {
                    id: "path-3",
                    points:
                      "0.1207 0.32 73.0917 0.32 73.0917 74.4869 0.1207 74.4869",
                  },
                }),
                n("polygon", {
                  attrs: {
                    id: "path-5",
                    points: "0 0 103 0 103 103 0 103",
                  },
                }),
              ]),
              n(
                "g",
                {
                  attrs: {
                    id: "Page-1",
                    stroke: "none",
                    "stroke-width": "1",
                    fill: "none",
                    "fill-rule": "evenodd",
                  },
                },
                [
                  n(
                    "g",
                    {
                      attrs: {
                        id: "Homepage",
                        transform: "translate(-509.000000, -17308.000000)",
                      },
                    },
                    [
                      n(
                        "g",
                        {
                          attrs: {
                            id: "instagram",
                            transform: "translate(-3.000000, 17085.000000)",
                          },
                        },
                        [
                          n(
                            "g",
                            {
                              attrs: {
                                id: "Group-2",
                                transform: "translate(512.000000, 222.000000)",
                              },
                            },
                            [
                              n(
                                "g",
                                {
                                  attrs: {
                                    id: "text",
                                  },
                                },
                                [
                                  n(
                                    "g",
                                    {
                                      attrs: {
                                        id: "Group-3",
                                        transform:
                                          "translate(293.000000, 1006.680000)",
                                      },
                                    },
                                    [
                                      n(
                                        "mask",
                                        {
                                          attrs: {
                                            id: "mask-2",
                                            fill: "white",
                                          },
                                        },
                                        [
                                          n("use", {
                                            attrs: {
                                              "xlink:href": "#path-1",
                                            },
                                          }),
                                        ]
                                      ),
                                      n("g", {
                                        attrs: {
                                          id: "Clip-2",
                                        },
                                      }),
                                      n("path", {
                                        attrs: {
                                          d: "M55.8752,21.7402 C52.6682,20.6662 51.4782,20.7622 51.1422,21.5022 L24.2852,80.6492 C23.9492,81.3882 24.2762,81.7922 28.4152,84.1802 L32.1792,86.2722 L31.7762,88.0002 L3.3642,75.0992 L4.4002,73.6592 L11.2462,76.2582 C14.4532,77.3322 15.7492,77.2832 16.0852,76.5442 L43.4212,16.3402 L34.1652,11.7552 C18.7442,4.7532 13.6002,8.7872 6.3362,15.8072 L2.7572,20.0432 L0.8552,19.1792 L13.7982,0.2142 L61.8552,22.0352 L60.8192,23.4752 L55.8752,21.7402 Z",
                                          id: "Fill-1",
                                          fill: "#000000",
                                          mask: "url(#mask-2)",
                                        },
                                      }),
                                    ]
                                  ),
                                  n("path", {
                                    attrs: {
                                      d: "M288.1623,998.555 C285.4153,997.012 283.7583,996.536 283.2843,997.196 L245.4133,1049.969 C244.9393,1050.629 245.2753,1051.156 248.8693,1054.305 L252.1523,1057.09 L251.4213,1058.706 L207.5063,1027.193 L218.3183,1009.539 L219.8263,1010.621 L217.9963,1015.16 C215.1163,1024.944 214.9703,1030.121 227.5043,1039.116 L237.0213,1045.946 L255.6193,1020.031 L249.0493,1015.458 C240.2703,1009.587 238.2893,1009.164 233.2653,1012.983 L230.3613,1015.039 L229.1093,1013.998 L244.6543,996.315 L245.9063,997.356 L244.2013,1000.13 C240.7653,1005.516 241.9613,1007.231 250.8053,1014.006 L256.7683,1018.428 L276.7193,990.628 L266.6493,982.974 C252.7023,972.965 245.8733,975.916 238.2033,980.835 L234.4343,983.699 L232.9003,982.455 L248.5483,967.414 L293.7823,999.875 L292.4853,1001.086 L288.1623,998.555 Z",
                                      id: "Fill-4",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M218.3224,941.8753 C215.5864,939.5163 214.4454,939.1623 213.8584,939.7233 L165.7214,985.7233 L169.4884,989.6643 C177.4324,997.4753 184.9924,996.9903 192.2674,994.5293 L196.9164,992.1743 L198.3584,993.6843 L181.1254,1004.6963 L140.4944,962.1783 L153.3194,946.5533 L154.6014,947.8953 L151.8814,952.1003 C147.8204,961.1133 148.0214,966.6973 155.6244,975.1563 L159.3904,979.0983 L207.7794,932.8573 C208.3664,932.2973 208.1334,931.7173 205.1824,927.9583 L201.4274,923.5243 L202.4454,922.0703 L224.6434,945.3013 L223.1454,946.2513 L218.3224,941.8753 Z",
                                      id: "Fill-6",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M129.5646,929.7308 C124.3716,933.6328 127.9036,939.6848 128.9496,941.0758 C127.0256,942.9568 122.9426,943.7038 120.0856,939.9008 C117.2286,936.0988 117.9186,930.6468 122.6486,927.0928 C128.5836,922.6328 137.9336,923.8768 145.5696,927.8598 L145.4556,929.2518 C140.4886,927.4698 134.2936,926.1768 129.5646,929.7308",
                                      id: "Fill-8",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M164.4367,876.8431 L150.0747,887.1511 L149.1047,885.5691 L154.9817,878.2891 C159.2087,871.2051 158.5427,864.1291 154.4177,857.4061 C148.8987,848.4071 139.4487,846.3121 131.7357,851.0431 C123.6277,856.0171 123.2677,863.8591 125.9707,875.8091 C128.4477,886.9451 128.8057,897.0681 119.1157,903.0121 C110.1177,908.5311 97.9397,906.2031 90.6007,894.2391 C87.5067,889.1971 85.9327,884.8561 85.1397,881.1231 L81.2847,877.5001 L97.2357,866.9001 L98.1457,868.3821 L89.3577,881.1211 C89.1767,885.0421 90.0147,889.2911 92.6237,893.5431 C97.4147,901.3551 105.3687,903.0071 111.6967,899.1251 C119.4097,894.3951 118.4557,886.4061 116.4637,876.0611 C113.5787,863.8151 113.4717,852.9921 124.0517,846.5031 C133.9397,840.4381 148.5557,843.8551 156.5007,856.8081 C160.2397,862.6801 163.1007,870.4501 164.4367,876.8431",
                                      id: "Fill-10",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M112.6437,777.4095 C112.2367,776.6975 111.9367,775.9425 112.0657,776.2655 C107.2487,765.6975 98.2097,763.0445 89.6927,766.4235 C79.3417,770.5295 75.5407,781.3955 80.2037,793.1475 C80.8447,794.7645 82.7287,798.8855 83.1997,800.0705 L114.7897,787.5385 L113.3497,778.8765 C113.1357,778.3375 112.9217,777.7985 112.6437,777.4095 L112.6437,777.4095 Z M57.7457,783.0895 C48.3657,786.8105 45.9107,794.1485 50.5727,805.9005 L53.0097,812.0465 L81.0427,800.9265 L78.4767,794.4575 C73.6627,783.2635 65.1847,780.1385 57.7457,783.0895 L57.7457,783.0895 Z M121.3887,799.7705 C119.8257,796.7715 118.8157,795.8005 118.0607,796.0985 L57.7897,820.0085 C57.0357,820.3065 56.8827,820.8665 58.2907,825.6745 L59.4647,829.5765 L57.9757,830.5415 L47.6247,804.4495 C40.7397,787.0905 44.4707,777.6235 52.5577,774.4165 C60.1047,771.4225 68.6897,774.5055 76.0847,787.1705 L76.4087,787.0415 C71.3267,771.7125 76.3537,761.1075 85.5177,757.4725 C96.8387,752.9815 107.9557,759.9275 114.9687,777.6095 L125.4057,803.9175 L123.6597,804.2355 L121.3887,799.7705 Z",
                                      id: "Fill-12",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M96.4972,733.0589 C95.3852,730.1109 94.4222,728.6799 93.6352,728.8809 L30.6922,744.9459 C29.9062,745.1469 29.8242,745.7659 30.5512,750.4889 L31.2752,754.7339 L29.6762,755.5009 L16.3082,703.1239 L35.9412,696.5569 L36.4002,698.3559 L32.0792,700.6559 C23.5822,706.2959 20.1412,710.1669 23.9562,725.1159 L26.8532,736.4679 L57.7622,728.5799 L55.6702,720.8529 C52.7232,710.7099 51.4762,709.1129 45.1732,708.8059 L41.6272,708.5139 L41.3382,706.9129 L64.6152,703.3659 L64.9042,704.9669 L61.8142,705.9959 C55.7192,707.9109 55.5322,709.9939 57.9492,720.8699 L59.6732,728.0909 L92.8302,719.6289 L90.0402,707.2909 C85.7942,690.6569 78.6662,688.5259 69.6292,687.3609 L64.9012,687.1299 L64.5262,685.1909 L86.1832,683.7339 L99.9522,737.6839 L98.1802,737.7779 L96.4972,733.0589 Z",
                                      id: "Fill-14",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M47.0988,671.1341 C24.5258,673.8341 6.7618,659.1371 3.9378,635.5271 C3.0148,627.8101 4.3898,620.7531 5.8518,616.3721 L2.3898,609.8941 L24.0758,606.5991 L24.2948,608.4421 L8.4908,617.9261 C6.4578,621.4401 5.1018,627.6771 6.0518,635.6241 C8.2848,654.2821 25.0678,663.7231 44.7628,661.3661 C66.1848,658.8041 79.3508,644.8461 77.2018,626.8781 C75.8378,615.4761 70.2978,610.1811 61.5608,605.5021 L53.0988,603.1261 L52.8648,601.1681 L72.0918,602.7241 L72.7028,606.8561 C75.1988,611.1131 78.5218,619.3611 79.4588,627.1931 C82.1728,649.8821 69.5578,668.4461 47.0988,671.1341",
                                      id: "Fill-16",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M38.715,518.1898 C17.027,517.7608 2.288,529.6538 1.97,545.7748 C1.65,562.0118 15.109,573.7648 37.262,574.2018 C58.95,574.6298 73.34,562.7308 73.658,546.6098 C73.981,530.2558 60.751,518.6248 38.715,518.1898 M37.412,584.1828 C17.348,583.7878 -0.404,566.1488 0.007,545.2718 C0.433,523.6998 16.297,507.6528 38.566,508.0918 C58.747,508.4908 76.152,526.1218 75.736,547.1148 C75.307,568.9188 59.681,584.6228 37.412,584.1828",
                                      id: "Fill-18",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M74.05,492.9466 C74.573,487.9746 73.694,486.2886 69.071,485.1196 L11.346,473.1666 C9.606,473.6826 8.235,475.4436 7.348,478.4676 L6.044,483.1836 L4.27,483.2246 L7.979,462.2076 L70.031,450.3066 L70.071,450.0786 L15.921,417.2016 L19.57,396.5256 L21.222,397.1716 L20.396,403.8566 C20.195,407.0016 20.853,407.9416 21.632,408.1976 L82.509,417.6436 C86.07,418.1546 86.97,417.7246 88.099,413.3306 L89.268,408.7076 L91.042,408.6676 L85.882,437.9096 L84.229,437.2636 L84.861,431.0156 C85.062,427.8716 84.061,426.8696 80.728,426.3996 L21.317,417.3296 L21.256,417.6716 L82.153,455.0366 L81.83,456.8646 L14.792,469.6516 C13.684,469.9266 12.474,470.7736 11.843,471.0156 L69.628,482.6266 C74.023,483.7556 75.682,481.6916 77.093,475.6986 L78.101,471.9896 L79.875,471.9496 L75.279,497.9936 L73.626,497.3476 L74.05,492.9466 Z",
                                      id: "Fill-20",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M94.3566,392.6019 C95.0236,389.5229 94.9916,387.7979 94.2206,387.5399 L32.6376,366.8609 C31.8686,366.6029 31.4636,367.0789 29.5096,371.4399 L27.8136,375.3979 L26.0526,375.1739 L43.2606,323.9279 L63.3156,329.0709 L62.7246,330.8309 L57.8466,330.4159 C47.6486,330.5399 42.6566,331.9229 37.7456,346.5489 L34.0156,357.6569 L64.2566,367.8109 L66.6956,360.1859 C69.7256,350.0679 69.5466,348.0499 64.4196,344.3709 L61.5996,342.1999 L62.2266,340.6979 L83.7016,350.3559 L83.0746,351.8589 L79.9216,351.0439 C73.7626,349.3429 72.4746,350.9919 68.5996,361.4379 L66.1276,368.4389 L98.5686,379.3319 L102.9226,367.4559 C108.3876,351.1809 103.5586,345.5209 96.6016,339.6359 L92.7556,336.8759 L93.4936,335.0429 L112.4726,345.5769 L94.7486,398.3629 L93.2096,397.4789 L94.3566,392.6019 Z",
                                      id: "Fill-22",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M103.217,242.5111 C93.883,237.3341 84.78,240.1141 78.593,252.4681 C78.536,252.5701 77.862,253.7871 77.119,255.3651 C76.771,256.2341 76.264,257.1471 75.701,258.1611 L103.399,273.5201 C105.392,271.8391 108.645,267.4081 109.939,265.0741 C114.53,256.0791 112.449,247.6301 103.217,242.5111 L103.217,242.5111 Z M128.46,303.2021 C129.224,301.1061 129.899,298.6941 129.189,298.3001 L72.373,266.7951 C71.663,266.4011 71.235,266.6951 68.41,270.8321 L65.573,275.2291 L63.883,274.6891 L78.342,248.6151 C87.062,232.8891 97.391,228.1371 107.536,233.7631 C115.653,238.2641 118.445,248.3011 115.455,258.7151 L145.978,256.5391 C153.121,255.8581 155.036,254.7961 157.422,251.2111 L159.021,249.0471 L160.768,249.4851 L151.99,265.3121 L147.99,265.3491 L111.361,267.0571 C110.043,268.7141 107.151,273.2121 105.326,274.5881 L133.937,290.4531 C134.648,290.8481 136.359,288.4801 137.844,286.5181 L140.737,282.0201 L142.427,282.5601 L127.237,309.9521 L125.884,308.8041 L128.46,303.2021 Z",
                                      id: "Fill-24",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M166.9191,237.5101 C168.5631,234.8221 169.1001,233.1831 168.4581,232.6861 L117.1201,192.8831 C116.4791,192.3861 115.9401,192.7021 112.6591,196.1761 L109.7541,199.3551 L108.1661,198.5641 L141.2871,155.8441 L158.5291,167.3031 L157.3921,168.7701 L152.9221,166.7731 C143.2521,163.5321 138.0831,163.1941 128.6301,175.3871 L121.4511,184.6461 L146.6621,204.1921 L151.4751,197.7951 C157.6671,189.2391 158.1631,187.2761 154.5331,182.1131 L152.5851,179.1341 L153.6721,177.9221 L170.7691,194.1131 L169.6821,195.3251 L166.9731,193.5191 C161.7181,189.8841 159.9591,191.0161 152.8611,199.6041 L148.2211,205.4001 L175.2651,226.3671 L183.2871,216.5881 C193.8061,203.0201 191.1091,196.0851 186.4791,188.2391 L183.7561,184.3661 L185.0561,182.8791 L199.5081,199.0741 L165.3921,243.0781 L164.2301,241.7371 L166.9191,237.5101 Z",
                                      id: "Fill-26",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M188.8176,120.5618 C191.1086,126.1668 193.1786,131.1878 195.1166,135.5448 C198.3086,142.9988 201.5786,150.5368 201.4936,150.6148 L217.2546,136.2228 L189.0746,120.3268 L188.8176,120.5618 Z M220.6226,174.1458 L223.5976,171.8998 L225.0286,172.9508 L206.6966,189.6888 L205.7806,188.1688 L207.6876,185.9568 C211.3446,181.3608 211.4976,179.8068 204.0926,162.0638 L186.3886,119.7938 L183.4356,117.9358 C184.5866,116.0998 185.1226,111.5258 184.6346,109.6158 L185.5776,108.7548 L244.2096,141.1428 C250.5456,144.4678 251.9976,145.0268 255.4876,142.3118 L258.9766,139.5958 L260.4076,140.6468 L238.3056,160.8268 L237.3896,159.3068 L240.6676,155.8428 C243.4306,152.8488 243.1926,150.8678 238.2316,148.0138 L219.4196,137.3888 L202.5436,152.7968 L208.8416,167.7808 C213.0536,178.0718 214.1716,178.7798 220.6226,174.1458 L220.6226,174.1458 Z",
                                      id: "Fill-28",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M270.2726,131.8665 C272.9556,129.8065 273.5406,128.7655 273.1156,128.0745 L239.0816,72.7465 C238.6556,72.0545 238.1386,72.0995 233.8446,74.1965 L230.0066,76.1485 L228.7986,74.8495 L255.3766,58.5005 L255.9916,60.1655 L250.0096,64.3885 C247.3276,66.4485 246.6426,67.5505 247.0686,68.2415 L281.7106,124.5575 L290.6856,119.4455 C305.1116,110.5715 304.6406,104.0525 302.8796,94.1035 L301.3686,88.7685 L303.1466,87.6755 L311.6386,109.0085 L266.6846,136.6605 L266.0696,134.9965 L270.2726,131.8665 Z",
                                      id: "Fill-30",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M350.2316,89.4818 C353.5876,88.1448 354.3166,87.3288 354.0896,86.5488 L335.9666,24.1708 C335.7406,23.3908 335.2296,23.2978 330.5336,24.1788 L326.2036,25.0748 L325.3836,23.5018 L377.5136,8.3548 L384.9316,28.0628 L383.0376,28.6138 L380.7726,24.5608 C375.0796,16.1888 371.1586,12.2548 354.8956,16.9798 L344.3136,20.0548 L353.4076,51.3548 L360.5036,49.1718 C370.6546,45.8608 372.0986,44.5958 372.1986,38.2858 L372.4066,34.8428 L373.9656,34.3898 L378.2756,57.5378 L376.6836,57.8788 L375.5546,54.8258 C373.4406,48.7958 371.4636,48.6458 360.5616,51.4508 L353.9586,53.2488 L362.9216,84.1038 C363.1816,84.9948 363.8676,85.2788 368.5316,84.2858 L375.0896,82.7428 L375.9096,84.3168 L345.5006,93.1518 L345.3486,91.3838 L350.2316,89.4818 Z",
                                      id: "Fill-32",
                                      fill: "#000000",
                                    },
                                  }),
                                  n(
                                    "g",
                                    {
                                      attrs: {
                                        id: "Group-36",
                                        transform:
                                          "translate(405.000000, 0.680000)",
                                      },
                                    },
                                    [
                                      n(
                                        "mask",
                                        {
                                          attrs: {
                                            id: "mask-4",
                                            fill: "white",
                                          },
                                        },
                                        [
                                          n("use", {
                                            attrs: {
                                              "xlink:href": "#path-3",
                                            },
                                          }),
                                        ]
                                      ),
                                      n("g", {
                                        attrs: {
                                          id: "Clip-35",
                                        },
                                      }),
                                      n("path", {
                                        attrs: {
                                          d: "M48.3097,18.2129 C47.7107,7.5579 40.5077,1.3399 26.7577,2.6919 C26.6427,2.6989 25.2527,2.7769 23.5227,2.9899 C22.6017,3.1579 21.5597,3.2169 20.4017,3.2809 L22.1757,34.8999 C24.6607,35.6899 30.1427,36.0779 32.8067,35.9289 C42.8627,35.0159 48.9007,28.7519 48.3097,18.2129 L48.3097,18.2129 Z M10.4287,71.9219 C12.6097,71.4509 15.0097,70.7359 14.9637,69.9249 L11.3237,5.0689 C11.2787,4.2589 10.8017,4.0529 5.7957,3.8689 L0.5647,3.8149 L0.1207,2.0969 L29.8837,0.4259 C47.8347,-0.5821 57.3617,5.6219 58.0117,17.2029 C58.5317,26.4689 51.5297,34.1809 41.1267,37.2039 L59.2407,61.8629 C63.6247,67.5429 65.5427,68.5969 69.8477,68.7029 L72.5317,68.9009 L73.0917,70.6119 L55.0257,71.6269 L52.8617,68.2629 L31.8877,38.1879 C29.7837,37.9569 24.4367,37.9089 22.2997,37.0999 L24.1327,69.7589 C24.1777,70.5689 27.0927,70.7549 29.5447,70.9659 L34.8917,71.0139 L35.3367,72.7319 L4.0667,74.4869 L4.3167,72.7309 L10.4287,71.9219 Z",
                                          id: "Fill-34",
                                          fill: "#000000",
                                          mask: "url(#mask-4)",
                                        },
                                      }),
                                    ]
                                  ),
                                  n("path", {
                                    attrs: {
                                      d: "M492.2238,72.1575 C495.6048,72.2585 496.8308,71.8375 496.9388,71.0325 L505.5518,6.6465 C505.6608,5.8415 505.1158,5.5345 500.4628,4.4435 L495.7948,3.4695 L495.6808,1.6975 L525.5748,5.6965 L524.9988,7.3755 L519.4188,7.0965 C515.8088,6.9655 514.6968,7.4015 514.5888,8.2065 L505.9758,72.5925 C505.8688,73.3965 506.5278,73.7195 511.1798,74.8095 L515.7328,75.7695 L515.8468,77.5415 L485.9528,73.5415 L486.5288,71.8635 L492.2238,72.1575 Z",
                                      id: "Fill-37",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M533.4162,78.4027 C536.5022,79.0407 538.2262,78.9907 538.4772,78.2187 L558.5662,16.4387 C558.8172,15.6667 558.3372,15.2667 553.9582,13.3547 L549.9832,11.6967 L550.1902,9.9337 L601.6012,26.6507 L596.6482,46.7547 L594.8842,46.1797 L595.2512,41.2987 C595.0292,31.1017 593.5992,26.1227 578.9262,21.3517 L567.7832,17.7277 L557.9182,48.0667 L565.5662,50.4317 C575.7132,53.3657 577.7292,53.1667 581.3602,48.0047 L583.5042,45.1637 L585.0132,45.7757 L575.5602,67.3427 L574.0512,66.7307 L574.8352,63.5707 C576.4772,57.3947 574.8162,56.1237 564.3332,52.3477 L557.3092,49.9417 L546.7262,82.4867 L558.6442,86.7277 C574.9712,92.0377 580.5842,87.1537 586.4022,80.1397 L589.1252,76.2677 L590.9652,76.9887 L580.6132,96.0677 L527.6592,78.8487 L528.5282,77.3007 L533.4162,78.4027 Z",
                                      id: "Fill-39",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M639.3498,52.7152 L610.3208,100.9082 C607.9778,104.7782 608.9648,106.7332 613.5278,110.1712 L616.7218,112.5112 L616.1198,114.1802 L594.5828,101.1472 L595.7818,99.8392 L599.4358,101.6432 C603.2478,103.4082 605.8728,103.7772 608.4548,99.5092 L637.6228,51.5352 C637.7328,48.8902 637.2068,45.7252 633.4198,42.3482 L630.8818,40.2702 L631.4848,38.6012 L648.1588,48.6912 L655.8378,121.8112 L656.0368,121.9312 L681.9438,78.8992 C684.2858,75.0272 683.1178,73.3722 678.6748,69.7352 L675.4818,67.3952 L676.0838,65.7262 L697.5208,78.6982 L696.3228,80.0072 L692.7678,78.2632 C688.8558,76.4382 686.2698,76.2292 683.8078,80.2982 L650.2558,135.5172 L648.3708,134.3752 L639.5478,52.8352 L639.3498,52.7152 Z",
                                      id: "Fill-41",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M733.7717,161.9857 C747.4307,147.6037 750.3137,128.9047 733.7447,113.1687 L728.8667,108.5357 L683.0147,156.8117 L688.2967,164.3887 C703.7517,178.2667 721.7907,174.6017 733.7717,161.9857 L733.7717,161.9857 Z M671.8087,148.5677 C674.4877,150.6317 675.8117,151.0897 676.3707,150.5017 L721.1037,103.4017 C721.6627,102.8137 721.4857,102.3257 718.1887,98.5537 L715.4847,95.5067 L716.4307,94.0057 L736.6877,112.7637 C756.3557,130.9647 757.0567,151.7867 742.7577,166.8407 C728.0607,182.3167 705.3637,183.4757 684.8407,163.9857 L666.4217,146.4917 L667.8727,145.4697 L671.8087,148.5677 Z",
                                      id: "Fill-43",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M727.0295,204.3226 L740.3215,192.6656 L741.4415,194.1456 L736.3005,201.9636 C732.7815,209.4266 734.1335,216.4036 738.8925,222.6946 C745.2615,231.1136 754.8725,232.2796 762.0885,226.8206 C769.6755,221.0816 769.2705,213.2406 765.4175,201.6096 C761.8685,190.7656 760.5265,180.7246 769.5925,173.8656 C778.0115,167.4966 790.3595,168.6286 798.8295,179.8226 C802.3985,184.5406 804.3875,188.7086 805.5405,192.3466 L809.7295,195.5776 L794.8845,207.6806 L793.8355,206.2936 L801.3425,192.7586 C801.1405,188.8386 799.8935,184.6906 796.8835,180.7126 C791.3545,173.4036 783.2765,172.5326 777.3555,177.0116 C770.1395,182.4706 771.8665,190.3296 774.8545,200.4326 C778.9185,212.3426 780.0775,223.1026 770.1775,230.5926 C760.9265,237.5916 746.0465,235.6116 736.8775,223.4926 C732.5855,218.0106 728.9805,210.5546 727.0295,204.3226",
                                      id: "Fill-45",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M834.3351,316.8421 C853.8811,307.4351 861.6641,290.1711 854.6711,275.6421 C847.6281,261.0091 830.3141,256.5981 810.3501,266.2061 C790.8041,275.6141 783.3351,292.7271 790.3271,307.2551 C797.4211,321.9931 814.4761,326.4011 834.3351,316.8421 M805.7101,257.3681 C823.7921,248.6651 847.5941,256.3911 856.6491,275.2051 C866.0071,294.6471 859.0941,316.1251 839.0251,325.7841 C820.8391,334.5381 797.3511,326.6611 788.2451,307.7421 C778.7871,288.0931 785.6421,267.0281 805.7101,257.3681",
                                      id: "Fill-47",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M876.1144,346.8118 L822.2264,362.9748 C817.8904,364.2648 817.3084,366.3748 818.3754,371.9888 L819.1664,375.8678 L817.5974,376.6978 L810.4164,352.5728 L812.1824,352.4098 L813.6754,356.2018 C815.3114,360.0718 817.0074,362.1078 821.7874,360.6848 L875.6294,344.7778 C877.4884,342.8928 879.2274,340.1968 878.6944,335.1508 L878.2124,331.9058 L879.7804,331.0758 L885.3414,349.7528 L841.8734,409.0468 L841.9384,409.2698 L890.0454,394.8278 C894.3814,393.5368 894.6294,391.5268 893.7864,385.8468 L892.9944,381.9678 L894.5624,381.1378 L901.7114,405.1518 L899.9444,405.3148 L898.4854,401.6338 C896.8164,397.6528 895.0434,395.7598 890.4844,397.1178 L828.5264,415.4408 L827.8974,413.3288 L876.1794,347.0348 L876.1144,346.8118 Z",
                                      id: "Fill-49",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M840.6486,465.2464 C841.4576,468.5304 842.1916,469.6004 842.9956,469.4874 L907.3386,460.5324 C908.1426,460.4214 908.2936,459.8144 908.0966,455.0394 L907.7866,450.2804 L909.4616,449.6964 L913.6196,479.5694 L911.8476,479.4654 L910.6206,474.0144 C909.7806,470.5004 909.0616,469.5464 908.2566,469.6574 L843.9146,478.6124 C843.1096,478.7254 842.9756,479.4464 843.1716,484.2214 L843.4666,488.8644 L841.7916,489.4494 L837.6336,459.5754 L839.4056,459.6814 L840.6486,465.2464 Z",
                                      id: "Fill-51",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M909.051,513.4671 L852.825,515.3591 C848.304,515.5011 847.203,517.3931 846.803,523.0921 L846.58,527.0451 L844.852,527.4471 L844.057,502.2891 L845.807,502.5821 L846.283,506.6291 C846.879,510.7881 847.999,513.1901 852.985,513.0321 L909.101,511.3761 C911.379,510.0281 913.748,507.8631 914.519,502.8491 L914.879,499.5881 L916.608,499.1861 L917.223,518.6631 L860.077,564.9161 L860.085,565.1481 L910.281,563.4461 C914.803,563.3031 915.556,561.4231 916.189,555.7161 L916.411,551.7631 L918.14,551.3611 L918.931,576.4031 L917.181,576.1101 L916.708,572.1791 C916.109,567.9051 914.876,565.6221 910.123,565.7721 L845.543,567.6961 L845.473,565.4931 L909.059,513.6981 L909.051,513.4671 Z",
                                      id: "Fill-53",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M847.2082,591.5052 L864.8852,591.7872 L864.7222,593.6372 L855.6632,595.9802 C848.0462,599.1492 844.3642,605.2292 843.6692,613.0862 C842.7382,623.6022 849.0792,630.9172 858.0922,631.7152 C867.5672,632.5532 872.5312,626.4702 877.4822,615.2632 C882.1312,604.8432 887.8772,596.5002 899.2012,597.5022 C909.7172,598.4332 918.1092,607.5612 916.8722,621.5422 C916.3512,627.4362 915.0272,631.8602 913.4392,635.3302 L914.3772,640.5372 L895.2492,639.5442 L895.4022,637.8112 L910.0522,632.8182 C912.5332,629.7762 914.3942,625.8652 914.8332,620.8962 C915.6412,611.7682 910.2382,605.6992 902.8432,605.0452 C893.8292,604.2472 889.8342,611.2312 885.2682,620.7262 C880.2852,632.2802 873.9212,641.0342 861.5572,639.9412 C850.0012,638.9182 840.3012,627.4622 841.6402,612.3242 C842.1382,605.3812 844.4712,597.4352 847.2082,591.5052",
                                      id: "Fill-55",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M836.9582,674.5003 C836.5652,678.0923 836.9202,679.2323 837.7152,679.3973 L902.9052,692.9743 L904.0172,687.6363 C905.9462,676.6623 901.2622,670.7103 895.1092,666.1103 L890.5332,663.6173 L890.9592,661.5723 L909.8032,669.5253 L897.8122,727.1063 L877.6672,725.3993 L878.0452,723.5833 L883.0522,723.4403 C892.7812,721.6753 897.2672,718.3433 899.9252,707.2833 L901.0372,701.9453 L835.5062,688.2993 C834.7112,688.1343 834.3652,688.6543 832.9412,693.2163 L831.4182,698.8233 L829.6442,698.8103 L836.1952,667.3513 L837.8282,668.0463 L836.9582,674.5003 Z",
                                      id: "Fill-57",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M872.7521,775.3782 C868.0921,771.5132 863.8991,768.0592 860.1391,765.1272 C853.8131,760.0572 847.3781,754.9462 847.4161,754.8372 L840.2901,774.9572 L872.6361,775.7062 L872.7521,775.3782 Z M819.4581,743.0892 L817.8911,746.4732 L816.1351,746.2202 L824.4221,722.8192 L825.9461,723.7292 L825.3061,726.5782 C824.2441,732.3562 824.8411,733.7972 839.7311,745.9622 L875.2561,774.9112 L878.7361,775.1592 C878.5861,777.3212 880.2681,781.6082 881.5981,783.0652 L881.1721,784.2672 L814.1951,783.3132 C807.0411,783.3622 805.4961,783.5542 803.6961,787.5932 L801.8971,791.6322 L800.1411,791.3792 L810.1321,763.1682 L811.6561,764.0762 L810.3971,768.6762 C809.3691,772.6192 810.5131,774.2552 816.2321,774.4352 L837.8321,774.9472 L845.4621,753.4062 L832.8491,743.1552 C824.2851,736.0622 822.9671,735.9642 819.4581,743.0892 L819.4581,743.0892 Z",
                                      id: "Fill-59",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M827.9601,815.9446 C847.9981,826.6836 854.2081,849.2266 842.9761,870.1846 C839.4141,876.8306 834.3411,881.8756 830.6861,884.5216 L829.7561,891.6576 L810.3721,881.6626 L811.2481,880.0266 L829.3131,881.6806 C832.9391,880.0716 837.4641,876.0486 841.4091,868.6876 C850.1221,852.4316 841.7521,834.6546 824.2711,825.2846 C805.3561,815.1476 786.5031,819.3866 778.0101,835.2336 C775.2151,840.4476 773.6881,846.7346 774.0411,850.7406 L794.9991,861.9736 C795.7151,862.3576 796.4601,861.7036 799.0131,857.6766 L802.0521,852.9886 L803.6481,853.4496 L789.3471,880.1326 L788.0791,879.0576 L789.9241,874.3876 C791.2071,871.2586 791.2431,869.9636 790.5271,869.5796 L771.3081,859.2776 L768.4431,865.1126 L767.2651,864.6116 C768.7241,855.2606 772.0601,841.9146 776.6071,833.4286 C787.4021,813.2896 808.0241,805.2596 827.9601,815.9446",
                                      id: "Fill-61",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M770.05,942.5335 C778.495,949.0585 787.912,947.6855 795.893,936.4065 C795.964,936.3145 796.815,935.2135 797.787,933.7655 C798.263,932.9605 798.901,932.1345 799.611,931.2165 L774.55,911.8535 C772.326,913.2155 768.442,917.1035 766.811,919.2155 C760.916,927.4145 761.696,936.0805 770.05,942.5335 L770.05,942.5335 Z M754.257,878.7315 C753.185,880.6885 752.154,882.9705 752.797,883.4675 L804.203,923.1835 C804.846,923.6805 805.314,923.4555 808.731,919.7915 L812.197,915.8735 L813.787,916.6625 L795.56,940.2545 C784.567,954.4825 773.64,957.6205 764.46,950.5285 C757.116,944.8545 755.872,934.5125 760.399,924.6685 L729.899,922.2135 C722.735,921.8085 720.682,922.5685 717.781,925.7525 L715.875,927.6505 L714.215,926.9545 L725.279,912.6345 L729.238,913.2015 L765.705,917.0415 C767.257,915.6015 770.795,911.5925 772.806,910.5075 L746.919,890.5065 C746.277,890.0105 744.228,892.0925 742.463,893.8065 L738.925,897.8165 L737.335,897.0285 L756.485,872.2435 L757.649,873.5825 L754.257,878.7315 Z",
                                      id: "Fill-63",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M729.7678,998.7484 C727.4938,993.1374 725.4368,988.1104 723.5118,983.7484 C720.3408,976.2874 717.0918,968.7394 717.1778,968.6624 L701.3768,983.0094 L729.5108,998.9824 L729.7678,998.7484 Z M698.1158,945.0784 L695.1328,947.3154 L693.7058,946.2624 L712.0838,929.5754 L712.9958,931.0984 L711.0818,933.3054 C707.4128,937.8904 707.2558,939.4424 714.6108,957.2064 L732.1948,999.5224 L735.1428,1001.3894 C733.9868,1003.2224 733.4378,1007.7944 733.9198,1009.7064 L732.9758,1010.5634 L674.4378,978.0144 C668.1118,974.6704 666.6608,974.1074 663.1638,976.8134 L659.6658,979.5194 L658.2398,978.4644 L680.3958,958.3484 L681.3068,959.8694 L678.0198,963.3244 C675.2478,966.3104 675.4818,968.2934 680.4338,971.1594 L699.2158,981.8374 L716.1338,966.4774 L709.8768,951.4764 C705.6938,941.1744 704.5788,940.4634 698.1158,945.0784 L698.1158,945.0784 Z",
                                      id: "Fill-65",
                                      fill: "#000000",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M651.382,986.9017 C647.186,989.6217 646.428,991.3667 648.426,995.6967 L675.437,1048.1017 C676.934,1049.1307 679.165,1049.0897 682.074,1047.8767 L686.562,1045.9237 L687.709,1047.2777 L669.032,1057.6127 L620.752,1016.8447 L620.549,1016.9577 L629.041,1079.7427 L610.669,1089.9087 L610.131,1088.2177 L615.85,1084.6557 C618.422,1082.8347 618.739,1081.7307 618.447,1080.9647 L587.506,1027.6847 C585.663,1024.5937 584.763,1024.1647 580.636,1026.0507 L576.306,1028.0487 L575.158,1026.6957 L601.143,1012.3167 L601.681,1014.0067 L596.424,1017.4467 C593.853,1019.2667 593.703,1020.6747 595.434,1023.5627 L625.746,1075.4667 L626.051,1075.2977 L616.807,1004.4437 L618.431,1003.5457 L670.536,1047.6317 C671.448,1048.3207 672.867,1048.7287 673.452,1049.0677 L646.137,996.8317 C644.251,992.7047 641.604,992.7117 636.055,995.3827 L632.537,996.9317 L631.39,995.5787 L654.532,982.7717 L655.07,984.4637 L651.382,986.9017 Z",
                                      id: "Fill-67",
                                      fill: "#000000",
                                    },
                                  }),
                                ]
                              ),
                              n(
                                "g",
                                {
                                  attrs: {
                                    id: "star_icon",
                                    transform:
                                      "translate(413.000000, 1021.000000)",
                                  },
                                },
                                [
                                  n(
                                    "mask",
                                    {
                                      attrs: {
                                        id: "mask-6",
                                        fill: "white",
                                      },
                                    },
                                    [
                                      n("use", {
                                        attrs: {
                                          "xlink:href": "#path-5",
                                        },
                                      }),
                                    ]
                                  ),
                                  n("g", {
                                    attrs: {
                                      id: "Clip-2",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M51.5,0 C51.5,45 45,51.5 0,51.5 C45,51.5 51.5,58 51.5,103 C51.5,58 58.0836874,51.5 103,51.5 C58,51.5 51.5,45 51.5,0 Z",
                                      id: "Fill-1",
                                      fill: "#000000",
                                      mask: "url(#mask-6)",
                                    },
                                  }),
                                ]
                              ),
                            ]
                          ),
                        ]
                      ),
                    ]
                  ),
                ]
              ),
            ])
          );
        },
      };
    },
    ,
    function (t, e, n) {
      n(23), n(19), n(17), n(14), n(20);
      var r = n(96),
        o = n(97);

      function l(object, t) {
        var e = Object.keys(object);
        if (Object.getOwnPropertySymbols) {
          var n = Object.getOwnPropertySymbols(object);
          t &&
            (n = n.filter(function (t) {
              return Object.getOwnPropertyDescriptor(object, t).enumerable;
            })),
            e.push.apply(e, n);
        }
        return e;
      }
      t.exports = {
        functional: !0,
        render: function (t, e) {
          var n = e._c,
            c = e._v,
            data = e.data,
            d = e.children,
            h = void 0 === d ? [] : d,
            f = data.class,
            m = data.staticClass,
            style = data.style,
            v = data.staticStyle,
            y = data.attrs,
            w = void 0 === y ? {} : y,
            x = o(data, [
              "class",
              "staticClass",
              "style",
              "staticStyle",
              "attrs",
            ]);
          return n(
            "svg",
            (function (t) {
              for (var i = 1; i < arguments.length; i++) {
                var source = null != arguments[i] ? arguments[i] : {};
                i % 2
                  ? l(Object(source), !0).forEach(function (e) {
                      r(t, e, source[e]);
                    })
                  : Object.getOwnPropertyDescriptors
                  ? Object.defineProperties(
                      t,
                      Object.getOwnPropertyDescriptors(source)
                    )
                  : l(Object(source)).forEach(function (e) {
                      Object.defineProperty(
                        t,
                        e,
                        Object.getOwnPropertyDescriptor(source, e)
                      );
                    });
              }
              return t;
            })(
              {
                class: [f, m],
                style: [style, v],
                attrs: Object.assign(
                  {
                    viewBox: "0 0 41 8",
                    version: "1.1",
                    xmlns: "http://www.w3.org/2000/svg",
                    "xmlns:xlink": "http://www.w3.org/1999/xlink",
                  },
                  w
                ),
              },
              x
            ),
            h.concat([
              n("title", [c("arrow_right")]),
              n("desc", [c("Created with Sketch.")]),
              n(
                "g",
                {
                  attrs: {
                    id: "Page-1",
                    stroke: "none",
                    "stroke-width": "1",
                    fill: "none",
                    "fill-rule": "evenodd",
                  },
                },
                [
                  n(
                    "g",
                    {
                      attrs: {
                        id: "Homepage_mob",
                        transform: "translate(-266.000000, -6498.000000)",
                        stroke: "#2E2E2E",
                      },
                    },
                    [
                      n(
                        "g",
                        {
                          attrs: {
                            id: "10",
                            transform: "translate(0.000000, 5816.000000)",
                          },
                        },
                        [
                          n(
                            "g",
                            {
                              attrs: {
                                id: "arrow_right",
                                transform:
                                  "translate(286.000000, 686.000000) scale(1, -1) translate(-286.000000, -686.000000) translate(266.000000, 682.000000)",
                              },
                            },
                            [
                              n("polyline", {
                                attrs: {
                                  id: "Path-2",
                                  transform:
                                    "translate(35.867248, 4.000000) rotate(-45.000000) translate(-35.867248, -4.000000) ",
                                  points:
                                    "33.2005814 6.66666667 38.5294997 6.66225166 38.5339147 1.33333333",
                                },
                              }),
                              n("path", {
                                attrs: {
                                  d: "M40,4 L0,4",
                                  id: "Path",
                                },
                              }),
                            ]
                          ),
                        ]
                      ),
                    ]
                  ),
                ]
              ),
            ])
          );
        },
      };
    },
    function (t, e, n) {
      n(23), n(19), n(17), n(14), n(20);
      var r = n(96),
        o = n(97);

      function l(object, t) {
        var e = Object.keys(object);
        if (Object.getOwnPropertySymbols) {
          var n = Object.getOwnPropertySymbols(object);
          t &&
            (n = n.filter(function (t) {
              return Object.getOwnPropertyDescriptor(object, t).enumerable;
            })),
            e.push.apply(e, n);
        }
        return e;
      }
      t.exports = {
        functional: !0,
        render: function (t, e) {
          var n = e._c,
            c = e._v,
            data = e.data,
            d = e.children,
            h = void 0 === d ? [] : d,
            f = data.class,
            m = data.staticClass,
            style = data.style,
            v = data.staticStyle,
            y = data.attrs,
            w = void 0 === y ? {} : y,
            x = o(data, [
              "class",
              "staticClass",
              "style",
              "staticStyle",
              "attrs",
            ]);
          return n(
            "svg",
            (function (t) {
              for (var i = 1; i < arguments.length; i++) {
                var source = null != arguments[i] ? arguments[i] : {};
                i % 2
                  ? l(Object(source), !0).forEach(function (e) {
                      r(t, e, source[e]);
                    })
                  : Object.getOwnPropertyDescriptors
                  ? Object.defineProperties(
                      t,
                      Object.getOwnPropertyDescriptors(source)
                    )
                  : l(Object(source)).forEach(function (e) {
                      Object.defineProperty(
                        t,
                        e,
                        Object.getOwnPropertyDescriptor(source, e)
                      );
                    });
              }
              return t;
            })(
              {
                class: [f, m],
                style: [style, v],
                attrs: Object.assign(
                  {
                    viewBox: "0 0 33 33",
                    version: "1.1",
                    xmlns: "http://www.w3.org/2000/svg",
                    "xmlns:xlink": "http://www.w3.org/1999/xlink",
                  },
                  w
                ),
              },
              x
            ),
            h.concat([
              n("title", [c("star_icon")]),
              n("desc", [c("Created with Sketch.")]),
              n(
                "g",
                {
                  attrs: {
                    id: "Page-1",
                    stroke: "none",
                    "stroke-width": "1",
                    fill: "none",
                    "fill-rule": "evenodd",
                  },
                },
                [
                  n(
                    "g",
                    {
                      attrs: {
                        id: "Homepage",
                        transform: "translate(-943.000000, -5743.000000)",
                      },
                    },
                    [
                      n(
                        "g",
                        {
                          attrs: {
                            id: "all_projects",
                            transform: "translate(0.000000, 5216.000000)",
                          },
                        },
                        [
                          n(
                            "g",
                            {
                              attrs: {
                                id: "star_icon",
                                transform: "translate(935.000000, 519.000000)",
                              },
                            },
                            [
                              n("rect", {
                                attrs: {
                                  id: "Rectangle",
                                  x: "0",
                                  y: "0",
                                  width: "50",
                                  height: "50",
                                },
                              }),
                              n("path", {
                                attrs: {
                                  d: "M24.5,8 C24.5,22.4174757 22.4174757,24.5 8,24.5 C22.4174757,24.5 24.5,26.5825243 24.5,41 C24.5,26.5825243 26.6093367,24.5 41,24.5 C26.5825243,24.5 24.5,22.4174757 24.5,8 Z",
                                  id: "Fill-1",
                                  stroke: "#FFFFFF",
                                },
                              }),
                            ]
                          ),
                        ]
                      ),
                    ]
                  ),
                ]
              ),
            ])
          );
        },
      };
    },
    ,
    ,
    function (t, e, n) {
      n(23), n(19), n(17), n(14), n(20);
      var r = n(96),
        o = n(97);

      function l(object, t) {
        var e = Object.keys(object);
        if (Object.getOwnPropertySymbols) {
          var n = Object.getOwnPropertySymbols(object);
          t &&
            (n = n.filter(function (t) {
              return Object.getOwnPropertyDescriptor(object, t).enumerable;
            })),
            e.push.apply(e, n);
        }
        return e;
      }
      t.exports = {
        functional: !0,
        render: function (t, e) {
          var n = e._c,
            c = e._v,
            data = e.data,
            d = e.children,
            h = void 0 === d ? [] : d,
            f = data.class,
            m = data.staticClass,
            style = data.style,
            v = data.staticStyle,
            y = data.attrs,
            w = void 0 === y ? {} : y,
            x = o(data, [
              "class",
              "staticClass",
              "style",
              "staticStyle",
              "attrs",
            ]);
          return n(
            "svg",
            (function (t) {
              for (var i = 1; i < arguments.length; i++) {
                var source = null != arguments[i] ? arguments[i] : {};
                i % 2
                  ? l(Object(source), !0).forEach(function (e) {
                      r(t, e, source[e]);
                    })
                  : Object.getOwnPropertyDescriptors
                  ? Object.defineProperties(
                      t,
                      Object.getOwnPropertyDescriptors(source)
                    )
                  : l(Object(source)).forEach(function (e) {
                      Object.defineProperty(
                        t,
                        e,
                        Object.getOwnPropertyDescriptor(source, e)
                      );
                    });
              }
              return t;
            })(
              {
                class: [f, m],
                style: [style, v],
                attrs: Object.assign(
                  {
                    viewBox: "0 0 151 18",
                    version: "1.1",
                    xmlns: "http://www.w3.org/2000/svg",
                    "xmlns:xlink": "http://www.w3.org/1999/xlink",
                  },
                  w
                ),
              },
              x
            ),
            h.concat([
              n("title", [c("arrow_left")]),
              n("desc", [c("Created with Sketch.")]),
              n(
                "g",
                {
                  attrs: {
                    id: "tab",
                    "stroke-width": "1",
                    "fill-rule": "evenodd",
                  },
                },
                [
                  n(
                    "g",
                    {
                      attrs: {
                        id: "folio-inner",
                        transform: "translate(-1392.000000, -12479.000000)",
                      },
                    },
                    [
                      n(
                        "g",
                        {
                          attrs: {
                            id: "gallery",
                            transform: "translate(173.000000, 12458.000000)",
                          },
                        },
                        [
                          n(
                            "g",
                            {
                              attrs: {
                                id: "slider",
                              },
                            },
                            [
                              n(
                                "g",
                                {
                                  attrs: {
                                    id: "arrow_left",
                                    transform:
                                      "translate(1294.500000, 30.000000) rotate(-180.000000) translate(-1294.500000, -30.000000) translate(1219.000000, 21.000000)",
                                  },
                                },
                                [
                                  n("polyline", {
                                    attrs: {
                                      id: "Path-2",
                                      transform:
                                        "translate(141.685887, 9.000000) rotate(-45.000000) translate(-141.685887, -9.000000) ",
                                      points:
                                        "135.685887 15 147.685887 15 147.685887 3",
                                    },
                                  }),
                                  n("path", {
                                    attrs: {
                                      d: "M150.171169,9 L0.171168738,9",
                                      id: "path",
                                    },
                                  }),
                                ]
                              ),
                            ]
                          ),
                        ]
                      ),
                    ]
                  ),
                ]
              ),
            ])
          );
        },
      };
    },
    ,
    ,
    ,
    function (t) {
      t.exports = JSON.parse(
        '{"title":"","meta":[{"hid":"charset","charset":"utf-8"},{"hid":"viewport","name":"viewport","content":"width=device-width, initial-scale=1"},{"hid":"mobile-web-app-capable","name":"mobile-web-app-capable","content":"yes"},{"hid":"author","name":"author","content":"synchronized.studio"},{"hid":"theme-color","name":"theme-color","content":"#000000"}],"link":[{"rel":"shortcut icon","href":"../_nuxt/icons/icon_64x64.01fe1d.png"},{"rel":"apple-touch-icon","href":"../_nuxt/icons/icon_512x512.01fe1d.png","sizes":"512x512"},{"rel":"manifest","href":"../_nuxt/manifest.60fe2001.json","hid":"manifest"}],"htmlAttrs":{"lang":"en"}}'
      );
    },
    function (t, e, n) {
      n(17), n(91), n(19), n(54), n(83), n(71), n(72), n(14), n(92);
      var r = n(786);

      function o(t, e) {
        var n;
        if ("undefined" == typeof Symbol || null == t[Symbol.iterator]) {
          if (
            Array.isArray(t) ||
            (n = (function (t, e) {
              if (!t) return;
              if ("string" == typeof t) return l(t, e);
              var n = Object.prototype.toString.call(t).slice(8, -1);
              "Object" === n && t.constructor && (n = t.constructor.name);
              if ("Map" === n || "Set" === n) return Array.from(t);
              if (
                "Arguments" === n ||
                /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)
              )
                return l(t, e);
            })(t)) ||
            (e && t && "number" == typeof t.length)
          ) {
            n && (t = n);
            var i = 0,
              r = function () {};
            return {
              s: r,
              n: function () {
                return i >= t.length
                  ? {
                      done: !0,
                    }
                  : {
                      done: !1,
                      value: t[i++],
                    };
              },
              e: function (t) {
                throw t;
              },
              f: r,
            };
          }
          throw new TypeError(
            "Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
          );
        }
        var o,
          c = !0,
          d = !1;
        return {
          s: function () {
            n = t[Symbol.iterator]();
          },
          n: function () {
            var t = n.next();
            return (c = t.done), t;
          },
          e: function (t) {
            (d = !0), (o = t);
          },
          f: function () {
            try {
              c || null == n.return || n.return();
            } finally {
              if (d) throw o;
            }
          },
        };
      }

      function l(t, e) {
        (null == e || e > t.length) && (e = t.length);
        for (var i = 0, n = new Array(e); i < e; i++) n[i] = t[i];
        return n;
      }

      function c(t, e, n) {
        return t.find(function (t) {
          return n ? t[e] === n : t[e];
        });
      }
      n(38),
        (t.exports = function (t, e) {
          if ("function" != typeof t)
            for (var n in e) {
              var l = e[n];
              if (Array.isArray(l)) {
                t[n] = t[n] || [];
                var d,
                  h = o(l);
                try {
                  for (h.s(); !(d = h.n()).done; ) {
                    var f = d.value;
                    (f.hid && c(t[n], "hid", f.hid)) ||
                      (f.name && c(t[n], "name", f.name)) ||
                      t[n].push(f);
                  }
                } catch (t) {
                  h.e(t);
                } finally {
                  h.f();
                }
              } else if ("object" === r(l))
                for (var m in ((t[n] = t[n] || {}), l)) t[n][m] = l[m];
              else void 0 === t[n] && (t[n] = l);
            }
          else
            console.warn("Cannot merge meta. Avoid using head as a function!");
        });
    },
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    function (t, e, n) {
      (function (r) {
        var o, l, c;
        n(40), n(55);
        var d = t.exports && void 0 !== r ? r : this || window;
        !(function (t) {
          "use strict";
          var e = t.GreenSockGlobals || t,
            n = (function (t) {
              var i,
                n = t.split("."),
                s = e;
              for (i = 0; i < n.length; i++) s[n[i]] = s = s[n[i]] || {};
              return s;
            })("com.greensock.utils"),
            r = document,
            o = r.defaultView ? r.defaultView.getComputedStyle : function () {},
            l = /([A-Z])/g,
            c = function (t, e, i, n) {
              var s;
              return (
                (i = i || o(t, null))
                  ? (s =
                      (t = i.getPropertyValue(
                        e.replace(l, "-$1").toLowerCase()
                      )) || i.length
                        ? t
                        : i[e])
                  : t.currentStyle && (s = (i = t.currentStyle)[e]),
                n ? s : parseInt(s, 10) || 0
              );
            },
            p = function (t) {
              return !!(
                t.length &&
                t[0] &&
                ((t[0].nodeType && t[0].style && !t.nodeType) ||
                  (t[0].length && t[0][0]))
              );
            },
            d = /(?:\r|\n|\t\t)/g,
            u = /(?:\s\s+)/g,
            h = 127462,
            f = 127487,
            b = function (t) {
              return (
                ((t.charCodeAt(0) - 55296) << 10) +
                (t.charCodeAt(1) - 56320) +
                65536
              );
            },
            m =
              " style='position:relative;display:inline-block;" +
              (r.all && !r.addEventListener
                ? "*display:inline;*zoom:1;'"
                : "'"),
            v = function (t, e) {
              var i = -1 !== (t = t || "").indexOf("++"),
                n = 1;
              return (
                i && (t = t.split("++").join("")),
                function () {
                  return (
                    "<" +
                    e +
                    m +
                    (t ? " class='" + t + (i ? n++ : "") + "'>" : ">")
                  );
                }
              );
            },
            y =
              (n.SplitText =
              e.SplitText =
                function (t, e) {
                  if (("string" == typeof t && (t = y.selector(t)), !t))
                    throw "cannot split a null element.";
                  (this.elements = p(t)
                    ? (function (t) {
                        var e,
                          i,
                          n,
                          s = [],
                          r = t.length;
                        for (e = 0; r > e; e++)
                          if (((i = t[e]), p(i)))
                            for (n = i.length, n = 0; n < i.length; n++)
                              s.push(i[n]);
                          else s.push(i);
                        return s;
                      })(t)
                    : [t]),
                    (this.chars = []),
                    (this.words = []),
                    (this.lines = []),
                    (this._originals = []),
                    (this.vars = e || {}),
                    this.split(e);
                }),
            w = function t(e, n, i) {
              var r = e.nodeType;
              if (1 === r || 9 === r || 11 === r)
                for (e = e.firstChild; e; e = e.nextSibling) t(e, n, i);
              else
                (3 === r || 4 === r) &&
                  (e.nodeValue = e.nodeValue.split(n).join(i));
            },
            x = function (t, e) {
              for (var i = e.length; --i > -1; ) t.push(e[i]);
            },
            C = function (t) {
              var e,
                i = [],
                n = t.length;
              for (e = 0; e !== n; i.push(t[e++]));
              return i;
            },
            _ = function (t, e, i) {
              for (var n; t && t !== e; ) {
                if ((n = t._next || t.nextSibling))
                  return n.textContent.charAt(0) === i;
                t = t.parentNode || t._parent;
              }
              return !1;
            },
            O = function t(e) {
              var n,
                i,
                r = C(e.childNodes),
                s = r.length;
              for (n = 0; s > n; n++)
                (i = r[n])._isSplit
                  ? t(i)
                  : (n && 3 === i.previousSibling.nodeType
                      ? (i.previousSibling.nodeValue +=
                          3 === i.nodeType
                            ? i.nodeValue
                            : i.firstChild.nodeValue)
                      : 3 !== i.nodeType && e.insertBefore(i.firstChild, i),
                    e.removeChild(i));
            },
            P = function (t, e, i, n, s, l, p) {
              var a,
                d,
                u,
                h,
                f,
                g,
                m,
                v,
                y,
                C,
                b,
                P,
                L = o(t),
                k = c(t, "paddingLeft", L),
                S = -999,
                I = c(t, "borderBottomWidth", L) + c(t, "borderTopWidth", L),
                j = c(t, "borderLeftWidth", L) + c(t, "borderRightWidth", L),
                T = c(t, "paddingTop", L) + c(t, "paddingBottom", L),
                $ = c(t, "paddingLeft", L) + c(t, "paddingRight", L),
                E = 0.2 * c(t, "fontSize"),
                A = c(t, "textAlign", L, !0),
                M = [],
                D = [],
                W = [],
                H = e.wordDelimiter || " ",
                R = e.span ? "span" : "div",
                z = e.type || e.split || "chars,words,lines",
                q = s && -1 !== z.indexOf("lines") ? [] : null,
                F = -1 !== z.indexOf("words"),
                X = -1 !== z.indexOf("chars"),
                B = "absolute" === e.position || !0 === e.absolute,
                N = e.linesClass,
                Y = -1 !== (N || "").indexOf("++"),
                V = [];
              for (
                q &&
                  1 === t.children.length &&
                  t.children[0]._isSplit &&
                  (t = t.children[0]),
                  Y && (N = N.split("++").join("")),
                  u = (d = t.getElementsByTagName("*")).length,
                  f = [],
                  a = 0;
                u > a;
                a++
              )
                f[a] = d[a];
              if (q || B)
                for (a = 0; u > a; a++)
                  ((g = (h = f[a]).parentNode === t) || B || (X && !F)) &&
                    ((P = h.offsetTop),
                    q &&
                      g &&
                      Math.abs(P - S) > E &&
                      ("BR" !== h.nodeName || 0 === a) &&
                      ((m = []), q.push(m), (S = P)),
                    B &&
                      ((h._x = h.offsetLeft),
                      (h._y = P),
                      (h._w = h.offsetWidth),
                      (h._h = h.offsetHeight)),
                    q &&
                      (((h._isSplit && g) ||
                        (!X && g) ||
                        (F && g) ||
                        (!F &&
                          h.parentNode.parentNode === t &&
                          !h.parentNode._isSplit)) &&
                        (m.push(h),
                        (h._x -= k),
                        _(h, t, H) && (h._wordEnd = !0)),
                      "BR" === h.nodeName &&
                        ((h.nextSibling && "BR" === h.nextSibling.nodeName) ||
                          0 === a) &&
                        q.push([])));
              for (a = 0; u > a; a++)
                (g = (h = f[a]).parentNode === t),
                  "BR" !== h.nodeName
                    ? (B &&
                        ((y = h.style),
                        F ||
                          g ||
                          ((h._x += h.parentNode._x),
                          (h._y += h.parentNode._y)),
                        (y.left = h._x + "px"),
                        (y.top = h._y + "px"),
                        (y.position = "absolute"),
                        (y.display = "block"),
                        (y.width = h._w + 1 + "px"),
                        (y.height = h._h + "px")),
                      !F && X
                        ? h._isSplit
                          ? ((h._next = h.nextSibling),
                            h.parentNode.appendChild(h))
                          : h.parentNode._isSplit
                          ? ((h._parent = h.parentNode),
                            !h.previousSibling &&
                              h.firstChild &&
                              (h.firstChild._isFirst = !0),
                            h.nextSibling &&
                              " " === h.nextSibling.textContent &&
                              !h.nextSibling.nextSibling &&
                              V.push(h.nextSibling),
                            (h._next =
                              h.nextSibling && h.nextSibling._isFirst
                                ? null
                                : h.nextSibling),
                            h.parentNode.removeChild(h),
                            f.splice(a--, 1),
                            u--)
                          : g ||
                            ((P = !h.nextSibling && _(h.parentNode, t, H)),
                            h.parentNode._parent &&
                              h.parentNode._parent.appendChild(h),
                            P &&
                              h.parentNode.appendChild(r.createTextNode(" ")),
                            e.span && (h.style.display = "inline"),
                            M.push(h))
                        : h.parentNode._isSplit &&
                          !h._isSplit &&
                          "" !== h.innerHTML
                        ? D.push(h)
                        : X &&
                          !h._isSplit &&
                          (e.span && (h.style.display = "inline"), M.push(h)))
                    : q || B
                    ? (h.parentNode && h.parentNode.removeChild(h),
                      f.splice(a--, 1),
                      u--)
                    : F || t.appendChild(h);
              for (a = V.length; --a > -1; ) V[a].parentNode.removeChild(V[a]);
              if (q) {
                for (
                  B &&
                    ((C = r.createElement(R)),
                    t.appendChild(C),
                    (b = C.offsetWidth + "px"),
                    (P = C.offsetParent === t ? 0 : t.offsetLeft),
                    t.removeChild(C)),
                    y = t.style.cssText,
                    t.style.cssText = "display:none;";
                  t.firstChild;

                )
                  t.removeChild(t.firstChild);
                for (
                  v = " " === H && (!B || (!F && !X)), a = 0;
                  a < q.length;
                  a++
                ) {
                  for (
                    m = q[a],
                      (C = r.createElement(R)).style.cssText =
                        "display:block;text-align:" +
                        A +
                        ";position:" +
                        (B ? "absolute;" : "relative;"),
                      N && (C.className = N + (Y ? a + 1 : "")),
                      W.push(C),
                      u = m.length,
                      d = 0;
                    u > d;
                    d++
                  )
                    "BR" !== m[d].nodeName &&
                      ((h = m[d]),
                      C.appendChild(h),
                      v && h._wordEnd && C.appendChild(r.createTextNode(" ")),
                      B &&
                        (0 === d &&
                          ((C.style.top = h._y + "px"),
                          (C.style.left = k + P + "px")),
                        (h.style.top = "0px"),
                        P && (h.style.left = h._x - P + "px")));
                  0 === u
                    ? (C.innerHTML = "&nbsp;")
                    : F || X || (O(C), w(C, String.fromCharCode(160), " ")),
                    B && ((C.style.width = b), (C.style.height = h._h + "px")),
                    t.appendChild(C);
                }
                t.style.cssText = y;
              }
              B &&
                (p > t.clientHeight &&
                  ((t.style.height = p - T + "px"),
                  t.clientHeight < p && (t.style.height = p + I + "px")),
                l > t.clientWidth &&
                  ((t.style.width = l - $ + "px"),
                  t.clientWidth < l && (t.style.width = l + j + "px"))),
                x(i, M),
                x(n, D),
                x(s, W);
            },
            L = function (t, e, i, n) {
              var o,
                l,
                c,
                p,
                a,
                g,
                m,
                v,
                y,
                x = e.span ? "span" : "div",
                C =
                  -1 !==
                  (e.type || e.split || "chars,words,lines").indexOf("chars"),
                _ = "absolute" === e.position || !0 === e.absolute,
                O = e.wordDelimiter || " ",
                P = " " !== O ? "" : _ ? "&#173; " : " ",
                L = e.span ? "</span>" : "</div>",
                k = !0,
                S = r.createElement("div"),
                I = t.parentNode;
              for (
                I.insertBefore(S, t),
                  S.textContent = t.nodeValue,
                  I.removeChild(t),
                  m =
                    -1 !==
                    (o = (function s(t) {
                      var e = t.nodeType,
                        i = "";
                      if (1 === e || 9 === e || 11 === e) {
                        if ("string" == typeof t.textContent)
                          return t.textContent;
                        for (t = t.firstChild; t; t = t.nextSibling) i += s(t);
                      } else if (3 === e || 4 === e) return t.nodeValue;
                      return i;
                    })((t = S))).indexOf("<"),
                  !1 !== e.reduceWhiteSpace &&
                    (o = o.replace(u, " ").replace(d, "")),
                  m && (o = o.split("<").join("{{LT}}")),
                  a = o.length,
                  l = (" " === o.charAt(0) ? P : "") + i(),
                  c = 0;
                a > c;
                c++
              )
                if ((g = o.charAt(c)) === O && o.charAt(c - 1) !== O && c) {
                  for (l += k ? L : "", k = !1; o.charAt(c + 1) === O; )
                    (l += P), c++;
                  c === a - 1
                    ? (l += P)
                    : ")" !== o.charAt(c + 1) && ((l += P + i()), (k = !0));
                } else
                  "{" === g && "{{LT}}" === o.substr(c, 6)
                    ? ((l += C ? n() + "{{LT}}</" + x + ">" : "{{LT}}"),
                      (c += 5))
                    : (g.charCodeAt(0) >= 55296 && g.charCodeAt(0) <= 56319) ||
                      (o.charCodeAt(c + 1) >= 65024 &&
                        o.charCodeAt(c + 1) <= 65039)
                    ? ((v = b(o.substr(c, 2))),
                      (y = b(o.substr(c + 2, 2))),
                      (p =
                        (h > v || v > f || h > y || y > f) &&
                        (127995 > y || y > 127999)
                          ? 2
                          : 4),
                      (l +=
                        C && " " !== g
                          ? n() + o.substr(c, p) + "</" + x + ">"
                          : o.substr(c, p)),
                      (c += p - 1))
                    : (l += C && " " !== g ? n() + g + "</" + x + ">" : g);
              (t.outerHTML = l + (k ? L : "")), m && w(I, "{{LT}}", "<");
            },
            k = function t(e, n, i, r) {
              var s,
                o,
                l = C(e.childNodes),
                d = l.length,
                p = "absolute" === n.position || !0 === n.absolute;
              if (3 !== e.nodeType || d > 1) {
                for (n.absolute = !1, s = 0; d > s; s++)
                  (3 !== (o = l[s]).nodeType || /\S+/.test(o.nodeValue)) &&
                    (p &&
                      3 !== o.nodeType &&
                      "inline" === c(o, "display", null, !0) &&
                      ((o.style.display = "inline-block"),
                      (o.style.position = "relative")),
                    (o._isSplit = !0),
                    t(o, n, i, r));
                return (n.absolute = p), void (e._isSplit = !0);
              }
              L(e, n, i, r);
            },
            S = y.prototype;
          (S.split = function (t) {
            this.isSplit && this.revert(),
              (this.vars = t = t || this.vars),
              (this._originals.length =
                this.chars.length =
                this.words.length =
                this.lines.length =
                  0);
            for (
              var e,
                i,
                n,
                s = this.elements.length,
                r = t.span ? "span" : "div",
                o = v(t.wordsClass, r),
                l = v(t.charsClass, r);
              --s > -1;

            )
              (n = this.elements[s]),
                (this._originals[s] = n.innerHTML),
                (e = n.clientHeight),
                (i = n.clientWidth),
                k(n, t, o, l),
                P(n, t, this.chars, this.words, this.lines, i, e);
            return (
              this.chars.reverse(),
              this.words.reverse(),
              this.lines.reverse(),
              (this.isSplit = !0),
              this
            );
          }),
            (S.revert = function () {
              if (!this._originals)
                throw "revert() call wasn't scoped properly.";
              for (var t = this._originals.length; --t > -1; )
                this.elements[t].innerHTML = this._originals[t];
              return (
                (this.chars = []),
                (this.words = []),
                (this.lines = []),
                (this.isSplit = !1),
                this
              );
            }),
            (y.selector =
              t.$ ||
              t.jQuery ||
              function (e) {
                var i = t.$ || t.jQuery;
                return i
                  ? ((y.selector = i), i(e))
                  : "undefined" == typeof document
                  ? e
                  : document.querySelectorAll
                  ? document.querySelectorAll(e)
                  : document.getElementById(
                      "#" === e.charAt(0) ? e.substr(1) : e
                    );
              }),
            (y.version = "0.5.8");
        })(d),
          (function (n) {
            "use strict";
            var r = function () {
              return (d.GreenSockGlobals || d).SplitText;
            };
            t.exports
              ? (t.exports = r())
              : ((l = []),
                void 0 ===
                  (c = "function" == typeof (o = r) ? o.apply(e, l) : o) ||
                  (t.exports = c));
          })();
      }.call(this, n(25)));
    },
    function (t, e, n) {
      "use strict";
      n(40),
        Object.defineProperty(e, "__esModule", {
          value: !0,
        }),
        (e.install = e.swiper = e.Swiper = void 0);
      var r = l(n(805)),
        o = l(n(275));

      function l(t) {
        return t && t.__esModule
          ? t
          : {
              default: t,
            };
      }
      var c = window.Swiper || r.default,
        d = [
          "beforeDestroy",
          "slideChange",
          "slideChangeTransitionStart",
          "slideChangeTransitionEnd",
          "slideNextTransitionStart",
          "slideNextTransitionEnd",
          "slidePrevTransitionStart",
          "slidePrevTransitionEnd",
          "transitionStart",
          "transitionEnd",
          "touchStart",
          "touchMove",
          "touchMoveOpposite",
          "sliderMove",
          "touchEnd",
          "click",
          "tap",
          "doubleTap",
          "imagesReady",
          "progress",
          "reachBeginning",
          "reachEnd",
          "fromEdge",
          "setTranslate",
          "setTransition",
          "resize",
        ],
        h = function (t) {
          var e = function (t, e, n) {
            var r = null;
            return (
              e.arg
                ? (r = e.arg)
                : n.data.attrs &&
                  (n.data.attrs.instanceName || n.data.attrs["instance-name"])
                ? (r =
                    n.data.attrs.instanceName || n.data.attrs["instance-name"])
                : t.id && (r = t.id),
              r || "swiper"
            );
          };
          return {
            bind: function (t, e, n) {
              n.context;
              -1 === t.className.indexOf("swiper-container") &&
                (t.className += (t.className ? " " : "") + "swiper-container");
            },
            inserted: function (n, r, l) {
              var h = l.context,
                f = r.value,
                m = e(n, r, l),
                v = h[m],
                y = function (t, e, data) {
                  var n =
                    (t.data && t.data.on) ||
                    (t.componentOptions && t.componentOptions.listeners);
                  n && n[e] && n[e].fns(data);
                };
              if (!v) {
                var w = (0, o.default)({}, t, f);
                (v = h[m] = new c(n, w)),
                  d.forEach(function (t) {
                    v.on(t, function () {
                      y.apply(
                        void 0,
                        [l, t].concat(Array.prototype.slice.call(arguments))
                      ),
                        y.apply(
                          void 0,
                          [l, t.replace(/([A-Z])/g, "-$1")].concat(
                            Array.prototype.slice.call(arguments)
                          )
                        );
                    });
                  });
              }
              y(l, "ready", v);
            },
            componentUpdated: function (t, n, r) {
              var o = e(t, n, r),
                l = r.context[o];
              l &&
                (l.update && l.update(),
                l.navigation && l.navigation.update(),
                l.pagination && l.pagination.render(),
                l.pagination && l.pagination.update());
            },
            unbind: function (t, n, r) {
              var o = e(t, n, r),
                l = r.context[o];
              l &&
                (setTimeout(function () {
                  l.destroy && l.destroy(!0, !1);
                }, 800),
                delete r.context[o]);
            },
          };
        },
        f = h({}),
        m = function (t) {
          var e =
            arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
          t.directive("swiper", h(e));
        },
        v = {
          Swiper: c,
          swiper: f,
          install: m,
        };
      (e.Swiper = c), (e.swiper = f), (e.install = m), (e.default = v);
    },
    ,
    function (t, e, n) {
      "use strict";
      n(14), n(30);
      var r = n(11),
        o = n(7),
        l = n(12),
        c = window.__NUXT__;

      function d() {
        if (!this._hydrated) return this.$fetch();
      }

      function h() {
        if (
          (t = this).$vnode &&
          t.$vnode.elm &&
          t.$vnode.elm.dataset &&
          t.$vnode.elm.dataset.fetchKey
        ) {
          var t;
          (this._hydrated = !0),
            (this._fetchKey = +this.$vnode.elm.dataset.fetchKey);
          var data = c.fetch[this._fetchKey];
          if (data && data._error) this.$fetchState.error = data._error;
          else for (var e in data) o.default.set(this.$data, e, data[e]);
        }
      }

      function f() {
        var t = this;
        return (
          this._fetchPromise ||
            (this._fetchPromise = m.call(this).then(function () {
              delete t._fetchPromise;
            })),
          this._fetchPromise
        );
      }

      function m() {
        return v.apply(this, arguments);
      }

      function v() {
        return (v = Object(r.a)(
          regeneratorRuntime.mark(function t() {
            var e,
              n,
              r,
              o = this;
            return regeneratorRuntime.wrap(
              function (t) {
                for (;;)
                  switch ((t.prev = t.next)) {
                    case 0:
                      return (
                        this.$nuxt.nbFetching++,
                        (this.$fetchState.pending = !0),
                        (this.$fetchState.error = null),
                        (this._hydrated = !1),
                        (e = null),
                        (n = Date.now()),
                        (t.prev = 6),
                        (t.next = 9),
                        this.$options.fetch.call(this)
                      );
                    case 9:
                      t.next = 15;
                      break;
                    case 11:
                      (t.prev = 11),
                        (t.t0 = t.catch(6)),
                        (e = Object(l.n)(t.t0));
                    case 15:
                      if (!((r = this._fetchDelay - (Date.now() - n)) > 0)) {
                        t.next = 19;
                        break;
                      }
                      return (
                        (t.next = 19),
                        new Promise(function (t) {
                          return setTimeout(t, r);
                        })
                      );
                    case 19:
                      (this.$fetchState.error = e),
                        (this.$fetchState.pending = !1),
                        (this.$fetchState.timestamp = Date.now()),
                        this.$nextTick(function () {
                          return o.$nuxt.nbFetching--;
                        });
                    case 23:
                    case "end":
                      return t.stop();
                  }
              },
              t,
              this,
              [[6, 11]]
            );
          })
        )).apply(this, arguments);
      }
      e.a = {
        beforeCreate: function () {
          Object(l.l)(this) &&
            ((this._fetchDelay =
              "number" == typeof this.$options.fetchDelay
                ? this.$options.fetchDelay
                : 200),
            o.default.util.defineReactive(this, "$fetchState", {
              pending: !1,
              error: null,
              timestamp: Date.now(),
            }),
            (this.$fetch = f.bind(this)),
            Object(l.a)(this, "created", h),
            Object(l.a)(this, "beforeMount", d));
        },
      };
    },
    ,
    function (t, e, n) {
      t.exports = n(450);
    },
    function (t, e, n) {
      "use strict";
      n.r(e),
        function (t) {
          n(91), n(19), n(83), n(71), n(72), n(40), n(54);
          var e = n(107),
            r = (n(30), n(301), n(11)),
            o =
              (n(117),
              n(142),
              n(17),
              n(14),
              n(20),
              n(38),
              n(302),
              n(462),
              n(470),
              n(472),
              n(7)),
            l = n(418),
            c = n(277),
            d = n(12),
            h = n(64),
            f = n(447),
            m = n(221);

          function v(t, e) {
            var n;
            if ("undefined" == typeof Symbol || null == t[Symbol.iterator]) {
              if (
                Array.isArray(t) ||
                (n = (function (t, e) {
                  if (!t) return;
                  if ("string" == typeof t) return y(t, e);
                  var n = Object.prototype.toString.call(t).slice(8, -1);
                  "Object" === n && t.constructor && (n = t.constructor.name);
                  if ("Map" === n || "Set" === n) return Array.from(t);
                  if (
                    "Arguments" === n ||
                    /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)
                  )
                    return y(t, e);
                })(t)) ||
                (e && t && "number" == typeof t.length)
              ) {
                n && (t = n);
                var i = 0,
                  r = function () {};
                return {
                  s: r,
                  n: function () {
                    return i >= t.length
                      ? {
                          done: !0,
                        }
                      : {
                          done: !1,
                          value: t[i++],
                        };
                  },
                  e: function (t) {
                    throw t;
                  },
                  f: r,
                };
              }
              throw new TypeError(
                "Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
              );
            }
            var o,
              l = !0,
              c = !1;
            return {
              s: function () {
                n = t[Symbol.iterator]();
              },
              n: function () {
                var t = n.next();
                return (l = t.done), t;
              },
              e: function (t) {
                (c = !0), (o = t);
              },
              f: function () {
                try {
                  l || null == n.return || n.return();
                } finally {
                  if (c) throw o;
                }
              },
            };
          }

          function y(t, e) {
            (null == e || e > t.length) && (e = t.length);
            for (var i = 0, n = new Array(e); i < e; i++) n[i] = t[i];
            return n;
          }
          o.default.__nuxt__fetch__mixin__ ||
            (o.default.mixin(f.a), (o.default.__nuxt__fetch__mixin__ = !0)),
            o.default.component(m.a.name, m.a),
            o.default.component("NLink", m.a),
            t.fetch || (t.fetch = l.a);
          var w,
            x,
            C = [],
            _ = window.__NUXT__ || {};
          Object.assign(o.default.config, {
            silent: !0,
            performance: !1,
          });
          var O = o.default.config.errorHandler || console.error;

          function P(t, e, n) {
            for (
              var r = function (component) {
                  var t =
                    (function (component, t) {
                      if (
                        !component ||
                        !component.options ||
                        !component.options[t]
                      )
                        return {};
                      var option = component.options[t];
                      if ("function" == typeof option) {
                        for (
                          var e = arguments.length,
                            n = new Array(e > 2 ? e - 2 : 0),
                            r = 2;
                          r < e;
                          r++
                        )
                          n[r - 2] = arguments[r];
                        return option.apply(void 0, n);
                      }
                      return option;
                    })(component, "transition", e, n) || {};
                  return "string" == typeof t
                    ? {
                        name: t,
                      }
                    : t;
                },
                o = n ? Object(d.g)(n) : [],
                l = Math.max(t.length, o.length),
                c = [],
                h = function (i) {
                  var e = Object.assign({}, r(t[i])),
                    n = Object.assign({}, r(o[i]));
                  Object.keys(e)
                    .filter(function (t) {
                      return (
                        void 0 !== e[t] && !t.toLowerCase().includes("leave")
                      );
                    })
                    .forEach(function (t) {
                      n[t] = e[t];
                    }),
                    c.push(n);
                },
                i = 0;
              i < l;
              i++
            )
              h(i);
            return c;
          }

          function L(t, e, n) {
            return k.apply(this, arguments);
          }

          function k() {
            return (k = Object(r.a)(
              regeneratorRuntime.mark(function t(e, n, r) {
                var o,
                  l,
                  c,
                  h,
                  f = this;
                return regeneratorRuntime.wrap(
                  function (t) {
                    for (;;)
                      switch ((t.prev = t.next)) {
                        case 0:
                          if (
                            ((this._routeChanged =
                              Boolean(w.nuxt.err) || n.name !== e.name),
                            (this._paramChanged =
                              !this._routeChanged && n.path !== e.path),
                            (this._queryChanged =
                              !this._paramChanged && n.fullPath !== e.fullPath),
                            (this._diffQuery = this._queryChanged
                              ? Object(d.i)(e.query, n.query)
                              : []),
                            (t.prev = 4),
                            !this._queryChanged)
                          ) {
                            t.next = 10;
                            break;
                          }
                          return (
                            (t.next = 8),
                            Object(d.p)(e, function (t, e) {
                              return {
                                Component: t,
                                instance: e,
                              };
                            })
                          );
                        case 8:
                          (o = t.sent),
                            o.some(function (t) {
                              var r = t.Component,
                                o = t.instance,
                                l = r.options.watchQuery;
                              return (
                                !0 === l ||
                                (Array.isArray(l)
                                  ? l.some(function (t) {
                                      return f._diffQuery[t];
                                    })
                                  : "function" == typeof l &&
                                    l.apply(o, [e.query, n.query]))
                              );
                            });
                        case 10:
                          r(), (t.next = 24);
                          break;
                        case 13:
                          if (
                            ((t.prev = 13),
                            (t.t0 = t.catch(4)),
                            (l = t.t0 || {}),
                            (c =
                              l.statusCode ||
                              l.status ||
                              (l.response && l.response.status) ||
                              500),
                            (h = l.message || ""),
                            !/^Loading( CSS)? chunk (\d)+ failed\./.test(h))
                          ) {
                            t.next = 21;
                            break;
                          }
                          return window.location.reload(!0), t.abrupt("return");
                        case 21:
                          this.error({
                            statusCode: c,
                            message: h,
                          }),
                            this.$nuxt.$emit("routeChanged", e, n, l),
                            r();
                        case 24:
                        case "end":
                          return t.stop();
                      }
                  },
                  t,
                  this,
                  [[4, 13]]
                );
              })
            )).apply(this, arguments);
          }

          function S(t, e) {
            return _.serverRendered && e && Object(d.b)(t, e), (t._Ctor = t), t;
          }

          function I(t) {
            var path = Object(d.f)(t.options.base, t.options.mode);
            return Object(d.d)(
              t.match(path),
              (function () {
                var t = Object(r.a)(
                  regeneratorRuntime.mark(function t(e, n, r, o, l) {
                    var c;
                    return regeneratorRuntime.wrap(function (t) {
                      for (;;)
                        switch ((t.prev = t.next)) {
                          case 0:
                            if ("function" != typeof e || e.options) {
                              t.next = 4;
                              break;
                            }
                            return (t.next = 3), e();
                          case 3:
                            e = t.sent;
                          case 4:
                            return (
                              (c = S(
                                Object(d.q)(e),
                                _.data ? _.data[l] : null
                              )),
                              (r.components[o] = c),
                              t.abrupt("return", c)
                            );
                          case 7:
                          case "end":
                            return t.stop();
                        }
                    }, t);
                  })
                );
                return function (e, n, r, o, l) {
                  return t.apply(this, arguments);
                };
              })()
            );
          }

          function j(t, e, n) {
            var r = this,
              o = ["i18n"],
              l = !1;
            if (
              (void 0 !== n &&
                ((o = []),
                (n = Object(d.q)(n)).options.middleware &&
                  (o = o.concat(n.options.middleware)),
                t.forEach(function (t) {
                  t.options.middleware && (o = o.concat(t.options.middleware));
                })),
              (o = o.map(function (t) {
                return "function" == typeof t
                  ? t
                  : ("function" != typeof c.a[t] &&
                      ((l = !0),
                      r.error({
                        statusCode: 500,
                        message: "Unknown middleware " + t,
                      })),
                    c.a[t]);
              })),
              !l)
            )
              return Object(d.m)(o, e);
          }

          function T(t, e, n) {
            return $.apply(this, arguments);
          }

          function $() {
            return ($ = Object(r.a)(
              regeneratorRuntime.mark(function t(e, n, o) {
                var l,
                  c,
                  f,
                  m,
                  y,
                  x,
                  _,
                  O,
                  L,
                  k,
                  S,
                  I,
                  T,
                  $,
                  E,
                  A = this;
                return regeneratorRuntime.wrap(
                  function (t) {
                    for (;;)
                      switch ((t.prev = t.next)) {
                        case 0:
                          if (
                            !1 !== this._routeChanged ||
                            !1 !== this._paramChanged ||
                            !1 !== this._queryChanged
                          ) {
                            t.next = 2;
                            break;
                          }
                          return t.abrupt("return", o());
                        case 2:
                          return (
                            !1,
                            e === n
                              ? ((C = []), !0)
                              : ((l = []),
                                (C = Object(d.g)(n, l).map(function (t, i) {
                                  return Object(d.c)(n.matched[l[i]].path)(
                                    n.params
                                  );
                                }))),
                            (c = !1),
                            (f = function (path) {
                              c || ((c = !0), o(path));
                            }),
                            (t.next = 8),
                            Object(d.r)(w, {
                              route: e,
                              from: n,
                              next: f.bind(this),
                            })
                          );
                        case 8:
                          if (
                            ((this._dateLastError = w.nuxt.dateErr),
                            (this._hadError = Boolean(w.nuxt.err)),
                            (m = []),
                            (y = Object(d.g)(e, m)).length)
                          ) {
                            t.next = 27;
                            break;
                          }
                          return (t.next = 15), j.call(this, y, w.context);
                        case 15:
                          if (!c) {
                            t.next = 17;
                            break;
                          }
                          return t.abrupt("return");
                        case 17:
                          return (
                            (x = (h.a.options || h.a).layout),
                            (t.next = 20),
                            this.loadLayout(
                              "function" == typeof x
                                ? x.call(h.a, w.context)
                                : x
                            )
                          );
                        case 20:
                          return (
                            (_ = t.sent),
                            (t.next = 23),
                            j.call(this, y, w.context, _)
                          );
                        case 23:
                          if (!c) {
                            t.next = 25;
                            break;
                          }
                          return t.abrupt("return");
                        case 25:
                          return (
                            w.context.error({
                              statusCode: 404,
                              message: "This page could not be found",
                            }),
                            t.abrupt("return", o())
                          );
                        case 27:
                          return (
                            y.forEach(function (t) {
                              t._Ctor &&
                                t._Ctor.options &&
                                ((t.options.asyncData =
                                  t._Ctor.options.asyncData),
                                (t.options.fetch = t._Ctor.options.fetch));
                            }),
                            this.setTransitions(P(y, e, n)),
                            (t.prev = 29),
                            (t.next = 32),
                            j.call(this, y, w.context)
                          );
                        case 32:
                          if (!c) {
                            t.next = 34;
                            break;
                          }
                          return t.abrupt("return");
                        case 34:
                          if (!w.context._errored) {
                            t.next = 36;
                            break;
                          }
                          return t.abrupt("return", o());
                        case 36:
                          return (
                            "function" == typeof (O = y[0].options.layout) &&
                              (O = O(w.context)),
                            (t.next = 40),
                            this.loadLayout(O)
                          );
                        case 40:
                          return (
                            (O = t.sent),
                            (t.next = 43),
                            j.call(this, y, w.context, O)
                          );
                        case 43:
                          if (!c) {
                            t.next = 45;
                            break;
                          }
                          return t.abrupt("return");
                        case 45:
                          if (!w.context._errored) {
                            t.next = 47;
                            break;
                          }
                          return t.abrupt("return", o());
                        case 47:
                          (L = !0),
                            (t.prev = 48),
                            (k = v(y)),
                            (t.prev = 50),
                            k.s();
                        case 52:
                          if ((S = k.n()).done) {
                            t.next = 63;
                            break;
                          }
                          if (
                            "function" == typeof (I = S.value).options.validate
                          ) {
                            t.next = 56;
                            break;
                          }
                          return t.abrupt("continue", 61);
                        case 56:
                          return (t.next = 58), I.options.validate(w.context);
                        case 58:
                          if ((L = t.sent)) {
                            t.next = 61;
                            break;
                          }
                          return t.abrupt("break", 63);
                        case 61:
                          t.next = 52;
                          break;
                        case 63:
                          t.next = 68;
                          break;
                        case 65:
                          (t.prev = 65), (t.t0 = t.catch(50)), k.e(t.t0);
                        case 68:
                          return (t.prev = 68), k.f(), t.finish(68);
                        case 71:
                          t.next = 77;
                          break;
                        case 73:
                          return (
                            (t.prev = 73),
                            (t.t1 = t.catch(48)),
                            this.error({
                              statusCode: t.t1.statusCode || "500",
                              message: t.t1.message,
                            }),
                            t.abrupt("return", o())
                          );
                        case 77:
                          if (L) {
                            t.next = 80;
                            break;
                          }
                          return (
                            this.error({
                              statusCode: 404,
                              message: "This page could not be found",
                            }),
                            t.abrupt("return", o())
                          );
                        case 80:
                          return (
                            (t.next = 82),
                            Promise.all(
                              y.map(
                                (function () {
                                  var t = Object(r.a)(
                                    regeneratorRuntime.mark(function t(r, i) {
                                      var o, l, c, h, f, v, y, p;
                                      return regeneratorRuntime.wrap(function (
                                        t
                                      ) {
                                        for (;;)
                                          switch ((t.prev = t.next)) {
                                            case 0:
                                              if (
                                                ((r._path = Object(d.c)(
                                                  e.matched[m[i]].path
                                                )(e.params)),
                                                (r._dataRefresh = !1),
                                                (o = r._path !== C[i]),
                                                A._routeChanged && o
                                                  ? (r._dataRefresh = !0)
                                                  : A._paramChanged && o
                                                  ? ((l = r.options.watchParam),
                                                    (r._dataRefresh = !1 !== l))
                                                  : A._queryChanged &&
                                                    (!0 ===
                                                    (c = r.options.watchQuery)
                                                      ? (r._dataRefresh = !0)
                                                      : Array.isArray(c)
                                                      ? (r._dataRefresh =
                                                          c.some(function (t) {
                                                            return A
                                                              ._diffQuery[t];
                                                          }))
                                                      : "function" ==
                                                          typeof c &&
                                                        (T ||
                                                          (T = Object(d.h)(e)),
                                                        (r._dataRefresh =
                                                          c.apply(T[i], [
                                                            e.query,
                                                            n.query,
                                                          ])))),
                                                A._hadError ||
                                                  !A._isMounted ||
                                                  r._dataRefresh)
                                              ) {
                                                t.next = 6;
                                                break;
                                              }
                                              return t.abrupt("return");
                                            case 6:
                                              return (
                                                (h = []),
                                                (f =
                                                  r.options.asyncData &&
                                                  "function" ==
                                                    typeof r.options.asyncData),
                                                (v =
                                                  Boolean(r.options.fetch) &&
                                                  r.options.fetch.length),
                                                f &&
                                                  ((y = Object(d.o)(
                                                    r.options.asyncData,
                                                    w.context
                                                  )).then(function (t) {
                                                    Object(d.b)(r, t);
                                                  }),
                                                  h.push(y)),
                                                (A.$loading.manual =
                                                  !1 === r.options.loading),
                                                v &&
                                                  (((p = r.options.fetch(
                                                    w.context
                                                  )) &&
                                                    (p instanceof Promise ||
                                                      "function" ==
                                                        typeof p.then)) ||
                                                    (p = Promise.resolve(p)),
                                                  p.then(function (t) {}),
                                                  h.push(p)),
                                                t.abrupt(
                                                  "return",
                                                  Promise.all(h)
                                                )
                                              );
                                            case 13:
                                            case "end":
                                              return t.stop();
                                          }
                                      },
                                      t);
                                    })
                                  );
                                  return function (e, n) {
                                    return t.apply(this, arguments);
                                  };
                                })()
                              )
                            )
                          );
                        case 82:
                          c || o(), (t.next = 99);
                          break;
                        case 85:
                          if (
                            ((t.prev = 85),
                            (t.t2 = t.catch(29)),
                            "ERR_REDIRECT" !== ($ = t.t2 || {}).message)
                          ) {
                            t.next = 90;
                            break;
                          }
                          return t.abrupt(
                            "return",
                            this.$nuxt.$emit("routeChanged", e, n, $)
                          );
                        case 90:
                          return (
                            (C = []),
                            Object(d.k)($),
                            "function" ==
                              typeof (E = (h.a.options || h.a).layout) &&
                              (E = E(w.context)),
                            (t.next = 96),
                            this.loadLayout(E)
                          );
                        case 96:
                          this.error($),
                            this.$nuxt.$emit("routeChanged", e, n, $),
                            o();
                        case 99:
                        case "end":
                          return t.stop();
                      }
                  },
                  t,
                  this,
                  [
                    [29, 85],
                    [48, 73],
                    [50, 65, 68, 71],
                  ]
                );
              })
            )).apply(this, arguments);
          }

          function E(t, n) {
            Object(d.d)(t, function (t, n, r, l) {
              return (
                "object" !== Object(e.a)(t) ||
                  t.options ||
                  (((t = o.default.extend(t))._Ctor = t),
                  (r.components[l] = t)),
                t
              );
            });
          }

          function A(t) {
            var e = Boolean(this.$options.nuxt.err);
            this._hadError &&
              this._dateLastError === this.$options.nuxt.dateErr &&
              (e = !1);
            var n = e
              ? (h.a.options || h.a).layout
              : t.matched[0].components.default.options.layout;
            "function" == typeof n && (n = n(w.context)), this.setLayout(n);
          }

          function M(t) {
            t._hadError &&
              t._dateLastError === t.$options.nuxt.dateErr &&
              t.error();
          }

          function D(t, e) {
            var n = this;
            if (
              !1 !== this._routeChanged ||
              !1 !== this._paramChanged ||
              !1 !== this._queryChanged
            ) {
              var r = Object(d.h)(t),
                l = Object(d.g)(t);
              o.default.nextTick(function () {
                r.forEach(function (t, i) {
                  if (
                    t &&
                    !t._isDestroyed &&
                    t.constructor._dataRefresh &&
                    l[i] === t.constructor &&
                    !0 !== t.$vnode.data.keepAlive &&
                    "function" == typeof t.constructor.options.data
                  ) {
                    var e = t.constructor.options.data.call(t);
                    for (var n in e) o.default.set(t.$data, n, e[n]);
                    window.$nuxt.$nextTick(function () {
                      window.$nuxt.$emit("triggerScroll");
                    });
                  }
                }),
                  M(n);
              });
            }
          }

          function W(t) {
            window.onNuxtReadyCbs.forEach(function (e) {
              "function" == typeof e && e(t);
            }),
              "function" == typeof window._onNuxtLoaded &&
                window._onNuxtLoaded(t),
              x.afterEach(function (e, n) {
                o.default.nextTick(function () {
                  return t.$nuxt.$emit("routeChanged", e, n);
                });
              });
          }

          function H() {
            return (H = Object(r.a)(
              regeneratorRuntime.mark(function t(e) {
                var n, r, l, c, h;
                return regeneratorRuntime.wrap(function (t) {
                  for (;;)
                    switch ((t.prev = t.next)) {
                      case 0:
                        return (
                          (w = e.app),
                          (x = e.router),
                          e.store,
                          (n = new o.default(w)),
                          (r = _.layout || "default"),
                          (t.next = 7),
                          n.loadLayout(r)
                        );
                      case 7:
                        return (
                          n.setLayout(r),
                          (l = function () {
                            n.$mount("#__nuxt"),
                              x.afterEach(E),
                              x.afterEach(A.bind(n)),
                              x.afterEach(D.bind(n)),
                              o.default.nextTick(function () {
                                W(n);
                              });
                          }),
                          (t.next = 11),
                          Promise.all(I(x))
                        );
                      case 11:
                        if (
                          ((c = t.sent),
                          (n.setTransitions =
                            n.$options.nuxt.setTransitions.bind(n)),
                          c.length &&
                            (n.setTransitions(P(c, x.currentRoute)),
                            (C = x.currentRoute.matched.map(function (t) {
                              return Object(d.c)(t.path)(x.currentRoute.params);
                            }))),
                          (n.$loading = {}),
                          _.error && n.error(_.error),
                          x.beforeEach(L.bind(n)),
                          x.beforeEach(T.bind(n)),
                          !_.serverRendered ||
                            _.routePath !== n.context.route.path)
                        ) {
                          t.next = 22;
                          break;
                        }
                        return l(), t.abrupt("return");
                      case 22:
                        return (
                          (h = function () {
                            E(x.currentRoute, x.currentRoute),
                              A.call(n, x.currentRoute),
                              M(n),
                              l();
                          }),
                          (t.next = 25),
                          new Promise(function (t) {
                            return setTimeout(t, 0);
                          })
                        );
                      case 25:
                        T.call(
                          n,
                          x.currentRoute,
                          x.currentRoute,
                          function (path) {
                            if (path) {
                              var t = x.afterEach(function (e, n) {
                                t(), h();
                              });
                              x.push(path, void 0, function (t) {
                                t && O(t);
                              });
                            } else h();
                          }
                        );
                      case 26:
                      case "end":
                        return t.stop();
                    }
                }, t);
              })
            )).apply(this, arguments);
          }
          Object(h.b)(null, _.config)
            .then(function (t) {
              return H.apply(this, arguments);
            })
            .catch(O);
        }.call(this, n(25));
    },
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    function (t, e, n) {
      n(92);
      var r = n(477);
      t.exports = function (t, e, n) {
        var o = r.find(function (e) {
          return e.from === t.url;
        });
        o
          ? (console.log("redirect: ".concat(o.from, " => ").concat(o.to)),
            e.writeHead(301, {
              Location: o.to,
            }),
            e.end())
          : n();
      };
    },
    ,
    ,
    ,
    function (t) {
      t.exports = JSON.parse(
        '[{"from":"/about/","to":"/about"},{"from":"/blog/","to":"/portfolio"},{"from":"/blog/page/2/","to":"/portfolio"},{"from":"/blog/page/3/","to":"/portfolio"},{"from":"/faq/","to":"/contact"},{"from":"/contact/","to":"/contact"},{"from":"/overnight-pacific-city-engagement-session-oregon-coast/","to":"/portfolio/overnight-pacific-city-engagement-session"},{"from":"/intimate-sayulita-wedding-villa-del-oso-mexico/","to":"/portfolio/intimate-sayulita-wedding-villa-del-oso"},{"from":"/cozy-columbia-river-gorge-engagement-session-washington/","to":"/portfolio"},{"from":"/romantic-southern-oregon-coast-elopement-brookings-oregon/","to":"/portfolio/romantic-southern-oregon-coast-elopement"},{"from":"/castaway-portland-wedding/","to":"/portfolio/castaway-portland-wedding"},{"from":"/mt-hood-oregon-wedding/","to":"/portfolio/foggy-mt-hood-wedding"},{"from":"/portland-anniversary-photographer/","to":"/portfolio/fox-and-the-wolf"},{"from":"/a-spice-palette-oregon-coast-wedding-inspiration-shoot/","to":"/portfolio"},{"from":"/portland-engagement-photographer/","to":"/portfolio"},{"from":"/cape-kiwanda-elopement-pacific-city-oregon/","to":"/portfolio/cape-kiwanda-elopement"},{"from":"/witchs-castle-elopement-portland-oregon/","to":"/portfolio/witch-castle-elopement"},{"from":"/wvictoria-beach-engagement-laguna-beach-california/","to":"/portfolio"},{"from":"/moody-woodland-wedding-in-washington/","to":"/portfolio/a-moody-woodland-wedding"},{"from":"/intimate-cozy-fall-in-home-session-camas-washington/","to":"/portfolio"}]'
      );
    },
    function (t, e, n) {
      "use strict";
      n.r(e);
      n(40), n(143);
      var r = n(78);
      e.default = function (t) {
        var e = t.isHMR,
          n = t.app,
          o = t.store,
          l = t.route,
          c = (t.params, t.error, t.redirect),
          d = n.i18n.fallbackLocale;
        if (!e) {
          var h = r.defaultLocale;
          if (
            ((n.i18n.locale = o.state.locale),
            h === d && 0 === l.fullPath.indexOf("/" + d))
          ) {
            var f = new RegExp("^/" + d);
            return c(l.fullPath.replace(f, "/"));
          }
        }
      };
    },
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    function (t) {
      t.exports = JSON.parse(
        '{"CTF_SPACE_ID":"tjd3v9sv4r3f","CTF_ACCESS_TOKEN":"Fl9_C1fse3ANCNrftu4cGXu4uLAtXiyhKHPdn0nnp-w","CTF_MANAGEMENT_TOKEN":"CFPAT-cauXeeLXShpBUgtkQPBv61UbLi3lAudFLSnt4y3LHks","CTF_PREVIEW_TOKEN":"Csh0UxoN3rOjmKVq6FCyoWYQZUJZ6wYzqLdoYYrK39c","CTF_PROD_HOST":"cdn.contentful.com","CTF_DEV_HOST":"preview.contentful.com"}'
      );
    },
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    function (t, e, n) {
      "use strict";
      var r = n(162);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(163);
      n.n(r).a;
    },
    ,
    ,
    function (t, e, n) {
      "use strict";
      var r = n(164);
      n.n(r).a;
    },
    ,
    function (t, e, n) {
      "use strict";
      var r = n(165);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(166);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(167);
      n.n(r).a;
    },
    ,
    ,
    function (t, e, n) {
      "use strict";
      var r = n(168);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(169);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(170);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(171);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(172);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(173);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(174);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(175);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(176);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(177);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(178);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(179);
      n.n(r).a;
    },
    ,
    ,
    ,
    function (t, e, n) {
      "use strict";
      var r = n(180);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(181);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(182);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(183);
      n.n(r).a;
    },
    ,
    ,
    ,
    function (t, e, n) {
      "use strict";
      var r = n(184);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(185);
      n.n(r).a;
    },
    ,
    ,
    ,
    ,
    ,
    function (t, e) {},
    ,
    function (t, e) {},
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    function (t, e) {},
    ,
    function (t, e) {},
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    function (t, e) {},
    ,
    function (t, e) {},
    function (t, e) {},
    ,
    ,
    ,
    ,
    function (t, e) {},
    ,
    function (t, e) {},
    ,
    ,
    ,
    ,
    ,
    ,
    function (t, e) {},
    ,
    function (t, e) {},
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    function (t, e) {},
    ,
    ,
    function (t, e) {},
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    function (t, e) {},
    ,
    ,
    function (t, e) {},
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    function (t, e, n) {
      t.exports = n.p + "6786671e43ba7b8261e383bf0032adc9.svg";
    },
    function (t, e, n) {
      "use strict";
      var r = n(186);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(187);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(188);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(189);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(190);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(191);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(192);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(193);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(194);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(195);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(196);
      n.n(r).a;
    },
    ,
    function (t, e, n) {
      "use strict";
      var r = n(197);
      n.n(r).a;
    },
    ,
    ,
    ,
    ,
    ,
    function (t, e, n) {
      "use strict";
      var r = n(198);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(199);
      n.n(r).a;
    },
    function (t, e, n) {},
    function (t, e, n) {
      "use strict";
      var r = n(200);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(201);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(202);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(203);
      n.n(r).a;
    },
    ,
    function (t, e, n) {
      "use strict";
      n.r(e),
        n.d(e, "actions", function () {
          return f;
        }),
        n.d(e, "modules", function () {
          return m;
        });
      n(17), n(14), n(54), n(30);
      var r = n(11),
        o = n(274),
        l = n(52),
        c = n(213),
        d = n(42),
        h = n.n(d),
        f = {
          nuxtServerInit: function (t, e) {
            var n = this;
            return Object(r.a)(
              regeneratorRuntime.mark(function r() {
                var o, d, f, m, v, y, w, x, footer;
                return regeneratorRuntime.wrap(function (r) {
                  for (;;)
                    switch ((r.prev = r.next)) {
                      case 0:
                        return (
                          (o = t.dispatch),
                          t.commit,
                          (d = t.state),
                          (f = e.req),
                          (m = f.headers.accept),
                          console.log("REQ", f.headers.accept),
                          o("app/SET_STATE", {
                            webp: h()(m, "webp"),
                            width: n.$device.isMobile ? 320 : 1920,
                            acceptHeader: m,
                          }),
                          o("pages/SET_STATE", {
                            urls: c,
                          }),
                          (v = d.route.path),
                          (y = l.a.getLocale(v)),
                          (w = o("pages/LOAD_TRANSLATIONS", y)),
                          (x = o("pages/LOAD_NAVIGATION", y)),
                          (footer = o("pages/LOAD_FOOTER", y)),
                          r.abrupt(
                            "return",
                            Promise.all([w, x, footer]).then(function (t) {
                              return t;
                            })
                          )
                        );
                      case 12:
                      case "end":
                        return r.stop();
                    }
                }, r);
              })
            )();
          },
        },
        m = o.default;
    },
    function (t, e, n) {
      var map = {
        "./app.store.js": 412,
        "./pages.store.js": 413,
        "./stage.store.js": 414,
      };

      function r(t) {
        var e = o(t);
        return n(e);
      }

      function o(t) {
        if (!n.o(map, t)) {
          var e = new Error("Cannot find module '" + t + "'");
          throw ((e.code = "MODULE_NOT_FOUND"), e);
        }
        return map[t];
      }
      (r.keys = function () {
        return Object.keys(map);
      }),
        (r.resolve = o),
        (t.exports = r),
        (r.id = 784);
    },
    ,
    ,
    ,
    ,
    function (t, e, n) {
      var map = {
        "./CustomParallax.vue": 807,
        "./CustomTransition.vue": 808,
        "./ImageDiv.vue": 809,
        "./LiquidButton.vue": 810,
        "./MouseParallax.vue": 811,
        "./Parallax.vue": 812,
        "./ProportionDiv.vue": 813,
        "./SmoothScroll.vue": 814,
        "./SplitText.vue": 815,
        "./Swiper.vue": 816,
        "./Waypoint.vue": 817,
      };

      function r(t) {
        var e = o(t);
        return n(e);
      }

      function o(t) {
        if (!n.o(map, t)) {
          var e = new Error("Cannot find module '" + t + "'");
          throw ((e.code = "MODULE_NOT_FOUND"), e);
        }
        return map[t];
      }
      (r.keys = function () {
        return Object.keys(map);
      }),
        (r.resolve = o),
        (t.exports = r),
        (r.id = 789);
    },
    function (t, e, n) {
      "use strict";
      var r = n(204);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(205);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(206);
      n.n(r).a;
    },
    ,
    function (t, e, n) {
      "use strict";
      var r = n(207);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(208);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(209);
      n.n(r).a;
    },
    function (t, e, n) {
      "use strict";
      var r = n(210);
      n.n(r).a;
    },
    ,
    function (t, e, n) {
      "use strict";
      var r = n(211);
      n.n(r).a;
    },
    ,
    ,
    ,
    ,
    ,
    ,
    ,
    function (t, e, n) {
      "use strict";
      n.r(e);
      var r = n(106),
        o = n(9),
        l = {
          name: "custom-parallax",
          data: function () {
            return {
              timeline: null,
            };
          },
          mixins: [r.a],
          watch: {
            currentProgress: function (t) {
              this.timeline && this.timeline.progress(t);
            },
            pageWidth: function () {
              this.resizeHandler(),
                this.debug &&
                  console.log(
                    "RATIO",
                    this.easeRatio,
                    this.pageHeight,
                    this.height
                  );
            },
          },
          computed: {
            noScale: function () {
              return !1;
            },
          },
          methods: {
            initParallaxTween: function () {
              this.timeline && this.timeline.seek(0).clear();
              var t = this.scaleFactor < 0 ? 1 - this.scaleFactor : 1,
                e = this.scaleFactor < 0 ? 1 : 1 + this.scaleFactor;
              (this.timeline = o.b.timeline({
                paused: !0,
              })),
                this.timeline.fromTo(
                  this.$refs.parallax,
                  1,
                  {
                    y: 0,
                    scale: t,
                  },
                  {
                    y: this.move,
                    scale: e,
                    ease: "Power1.easeOut",
                  }
                ),
                this.timeline.progress(this.currentProgress);
            },
            resizeHandler: function () {
              this.$el &&
                ((this.bounds = this.$el.getBoundingClientRect()),
                (this.topBound =
                  0 != this.y || this.$device.isMobileOrTablet
                    ? this.scrollTop + this.bounds.top
                    : this.bounds.top),
                (this.height = this.$el.offsetHeight),
                this.initParallaxTween());
            },
          },
        },
        c = (n(790), n(3)),
        component = Object(c.a)(
          l,
          function () {
            var t = this.$createElement,
              e = this._self._c || t;
            return e(
              "div",
              {
                ref: "holder",
                staticClass: "custom-parallax parallax-wrapper",
                class: [
                  this.className,
                  {
                    wrapped: this.wrapped,
                    "not-wrapped": !this.wrapped,
                    "position-absolute": this.absolute,
                  },
                ],
              },
              [
                e(
                  "div",
                  {
                    ref: "parallax",
                    staticClass: "parallax",
                    style: this.elStyle,
                  },
                  [this._t("default")],
                  2
                ),
              ]
            );
          },
          [],
          !1,
          null,
          "45e57f73",
          null
        );
      e.default = component.exports;
    },
    function (t, e, n) {
      "use strict";
      n.r(e);
      var r = n(6),
        o = n(9),
        l = {
          name: "customTransition",
          computed: {
            dur: function () {
              return this.$store.getters["app/getState"]("instantTransition") &&
                this.pageTransition
                ? 0
                : this.duration;
            },
            delay: function () {
              return this.pageTransition ? this.dl : 0;
            },
          },
          props: {
            dl: {
              default: 0.2,
            },
            crossTransition: !1,
            noAdditionalClasses: !1,
            pageTransition: !1,
            mode: !1,
            duration: {
              default: 1.6,
            },
          },
          methods: {
            afterEnter: function (t, e) {},
            beforeEnter: function (t, e) {},
            enter: function (t, e) {
              var n = this;
              if (
                ((this.tl = new r.c({
                  onComplete: function () {
                    n.pageTransition &&
                      setTimeout(function () {
                        n.$emit("update:loading", !1);
                      }, 400),
                      n.$bus.$emit("resize"),
                      e();
                  },
                })),
                this.pageTransition)
              ) {
                this.$emit("update:loading", !0);
                var l = t.querySelectorAll(".transition-in-image");
                l &&
                  l.length &&
                  o.b.from(l, this.dur, {
                    scale: 1.5,
                    delay: 0.1,
                    ease: "Power4.easeOut",
                  });
              }
              this.tl.fromTo(
                t,
                this.dur,
                {
                  opacity: 0,
                },
                {
                  opacity: 1,
                  delay: this.delay,
                }
              );
            },
            leave: function (t, e) {
              var n = this;
              if (this.pageTransition) {
                this.$emit("update:loading", !0);
                var r = t.querySelectorAll(".transition-out-image");
                r &&
                  r.length &&
                  o.b.to(r, this.dur, {
                    scale: 1.5,
                    delay: 0.1,
                    ease: "Power4.easeOut",
                  });
              }
              if (!this.noAdditionalClasses && "out-in" != this.mode) {
                var l,
                  c =
                    this.pageTransition || this.crossTransition
                      ? ["transitioning-out"]
                      : [];
                (l = t.classList).add.apply(l, c);
              }
              var d = this.pageTransition ? this.dur / 2 : this.dur;
              o.b.to(t, d, {
                opacity: 0,
                delay: 0.1,
                onComplete: function () {
                  n.pageTransition &&
                    (window.scrollTo(0, 0),
                    n.$store.dispatch("app/SET_STATE", {
                      scrollTop: 0,
                      instantTransition: !1,
                      menuOpen: !1,
                    })),
                    n.$nextTick(e);
                },
              });
            },
          },
        },
        c = n(3),
        component = Object(c.a)(
          l,
          function () {
            var t = this.$createElement;
            return (this._self._c || t)(
              "transition",
              {
                attrs: {
                  mode: this.mode,
                  css: !1,
                },
                on: {
                  enter: this.enter,
                  "before-enter": this.beforeEnter,
                  "after-enter": this.afterEnter,
                  leave: this.leave,
                },
              },
              [this._t("default")],
              2
            );
          },
          [],
          !1,
          null,
          "8250e94c",
          null
        );
      e.default = component.exports;
    },
    function (t, e, n) {
      "use strict";
      n.r(e);
      var r = {
          name: "image-div",
          data: function () {
            return {
              naturalWidth: null,
              naturalHeight: null,
              ratio: 0,
            };
          },
          props: [
            "innerClasses",
            "src",
            "contain",
            "keepProportion",
            "proportion",
            "bgTop",
            "bgBottom",
            "alt",
          ],
          watch: {
            proportion: function (p) {
              this.ratio = p;
            },
          },
          created: function () {
            this.proportion && (this.ratio = this.proportion);
          },
          mounted: function () {
            var t = this;
            if (!this.proportion && this.keepProportion) {
              var image = new Image();
              image.addEventListener(
                "load",
                function () {
                  (t.naturalHeight = image.naturalHeight),
                    (t.naturalWidth = image.naturalWidth),
                    (t.ratio = t.naturalHeight / t.naturalWidth),
                    setTimeout(function () {
                      t.$bus.$emit("resize");
                    }, 100);
                },
                !1
              ),
                (image.src = this.src);
            }
          },
          computed: {
            isIE: function () {
              return this.$store.getters["app/getState"]("isIE");
            },
            classes: function () {
              return {
                cover: !this.contain,
                contain: this.contain,
                "bg-top": this.bgTop,
                "bg-center": !this.bgTop,
                "bg-bottom": this.bgBottom,
              };
            },
            paddingBottom: function () {
              return this.ratio ? 100 * this.ratio + "%" : null;
            },
          },
        },
        o = (n(791), n(3)),
        component = Object(o.a)(
          r,
          function () {
            var t = this,
              e = t.$createElement,
              n = t._self._c || e;
            return n(
              "div",
              {
                staticClass: "image-div position-relative overflow-hidden",
                style: {
                  paddingBottom: t.paddingBottom,
                },
              },
              [
                t.isIE
                  ? n("div", {
                      staticClass:
                        "img h-100 w-100 no-repeat position-absolute t-0 l-0",
                      class: [t.innerClasses, t.classes],
                      style: {
                        backgroundImage: "url('" + t.src + "')",
                      },
                      attrs: {
                        draggable: "false",
                      },
                    })
                  : t._e(),
                t._v(" "),
                n("img", {
                  staticClass: "w-100 h-100 cover position-absolute t-0 l-0",
                  class: [t.innerClasses, t.classes],
                  attrs: {
                    draggable: "false",
                    alt: t.alt || "Naba Zabih Photography",
                    src: t.src,
                    loading: "lazy",
                  },
                }),
                t._v(" "),
                t._t("default"),
              ],
              2
            );
          },
          [],
          !1,
          null,
          "0e2740ec",
          null
        );
      e.default = component.exports;
    },
    function (t, e, n) {
      "use strict";
      n.r(e);
      var r = {
          name: "liquid-btn",
          props: ["text", "black", "blackBg", "pinkBg"],
        },
        o = (n(792), n(3)),
        component = Object(o.a)(
          r,
          function () {
            var t = this,
              e = t.$createElement,
              n = t._self._c || e;
            return n(
              "button",
              {
                staticClass: "liquid-btn",
                class: {
                  "black-btn": t.black,
                  "black-bg-btn": t.blackBg,
                  "pink-bg-btn": t.pinkBg,
                },
              },
              [
                n(
                  "svg",
                  {
                    staticClass: "d-none",
                    attrs: {
                      xmlns: "http://www.w3.org/2000/svg",
                      version: "1.1",
                    },
                  },
                  [
                    n("defs", [
                      n(
                        "filter",
                        {
                          attrs: {
                            id: "goo",
                          },
                        },
                        [
                          n("feGaussianBlur", {
                            attrs: {
                              in: "SourceGraphic",
                              stdDeviation: "10",
                              result: "blur",
                            },
                          }),
                          t._v(" "),
                          n("feColorMatrix", {
                            attrs: {
                              in: "blur",
                              mode: "matrix",
                              values:
                                "1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9",
                              result: "goo",
                            },
                          }),
                          t._v(" "),
                          n("feBlend", {
                            attrs: {
                              in: "SourceGraphic",
                              in2: "goo",
                            },
                          }),
                        ],
                        1
                      ),
                    ]),
                  ]
                ),
                t._v(" "),
                n(
                  "span",
                  {
                    staticClass: "btn-container d-flex flex-center",
                  },
                  [
                    t._m(0),
                    t._v(" "),
                    n("span", {
                      staticClass: "text color-black no-wrap",
                      domProps: {
                        innerHTML: t._s(t.text),
                      },
                    }),
                  ]
                ),
              ]
            );
          },
          [
            function () {
              var t = this,
                e = t.$createElement,
                n = t._self._c || e;
              return n(
                "span",
                {
                  staticClass: "blobs",
                },
                [
                  n("span", {
                    staticClass: "liquid",
                  }),
                  t._v(" "),
                  n("span", {
                    staticClass: "blob",
                  }),
                  t._v(" "),
                  n("span", {
                    staticClass: "blob",
                  }),
                  t._v(" "),
                  n("span", {
                    staticClass: "blob",
                  }),
                  t._v(" "),
                  n("span", {
                    staticClass: "blob",
                  }),
                  t._v(" "),
                  n("span", {
                    staticClass: "blob",
                  }),
                  t._v(" "),
                  n("span", {
                    staticClass: "blob",
                  }),
                  t._v(" "),
                  n("span", {
                    staticClass: "blob",
                  }),
                  t._v(" "),
                  n("span", {
                    staticClass: "blob",
                  }),
                ]
              );
            },
          ],
          !1,
          null,
          "4b4fd526",
          null
        );
      e.default = component.exports;
    },
    function (t, e, n) {
      "use strict";
      n.r(e);
      var r = n(9),
        o = n(284),
        l = n.n(o),
        c = {
          name: "mouseParallax",
          data: function () {
            return {
              totalWidth: null,
              totalHeight: null,
              newPageX: 0,
              newPageY: 0,
              bounds: [],
              hoveringX: 0,
              hoveringY: 0,
              topBound: 0,
            };
          },
          created: function () {},
          mounted: function () {
            this.$nextTick(this.resizeHandler);
          },
          props: [
            "ratioX",
            "ratioY",
            "notWrapped",
            "hoverOnly",
            "debug",
            "model",
            "global",
            "dur",
            "y",
          ],
          watch: {
            position: {
              handler: function (t, e) {
                this.animate();
              },
              deep: !0,
            },
            pageSize: {
              handler: function (t, e) {
                this.resizeHandler();
              },
              deep: !0,
            },
          },
          computed: {
            elStyle: function () {
              return {
                transform: this.elTransform,
                left: -this.totalMoveX + "px",
              };
            },
            scrollTop: function () {
              return this.$store.getters["app/getState"]("scrollTop");
            },
            isHovered: function () {
              var t = this.mouseX - this.bounds.x,
                e = t > 0 && t < this.bounds.width,
                n = this.mouseY - this.topBound + this.scrollTop,
                r = n > 0 && n < this.bounds.height;
              return e && r;
            },
            classObject: function () {
              return {};
            },
            hoverX: function () {
              var t = this.mouseX - this.bounds.x;
              return l()(t, 0, this.bounds.width);
            },
            hoverY: function () {
              var t = this.mouseY - this.topBound + this.scrollTop;
              return l()(t, 0, this.bounds.height);
            },
            mouseX: function () {
              return this.$store.getters["app/getState"]("mouseX");
            },
            mouseY: function () {
              return this.$store.getters["app/getState"]("mouseY");
            },
            position: function () {
              return {
                x: this.hoverOnly ? this.hoverX : this.mouseX,
                y: this.hoverOnly ? this.hoverY : this.mouseY,
              };
            },
            elTransform: function () {
              return (
                "translate3d(" +
                this.moveX +
                "px," +
                this.moveY +
                "px, 0px) rotateX( 0deg )"
              );
            },
            totalMoveX: function () {
              return this.$device.isMobileOrTablet
                ? 0
                : (this.ratioX * this.totalWidth) / 10;
            },
            totalMoveY: function () {
              return this.$device.isMobileOrTablet
                ? 0
                : (this.ratioY * this.totalWidth) / 10;
            },
            currentX: function () {
              return this.newPageX / this.totalWidth;
            },
            currentY: function () {
              return this.newPageY / this.totalHeight;
            },
            moveY: function () {
              return this.currentY * this.totalMoveY;
            },
            moveX: function () {
              return this.currentX * this.totalMoveX;
            },
            pageSize: function () {
              return {
                height: this.$store.getters["app/getState"]("height"),
                width: this.$store.getters["app/getState"]("width"),
              };
            },
          },
          methods: {
            animate: function () {
              (this.hoverOnly && !this.isHovered) ||
                this.$device.isMobileOrTablet ||
                (this.mouseParallaxTween = r.b.to(this, this.dur || 3, {
                  newPageX: this.position.x,
                  newPageY: this.position.y,
                  overwrite: !0,
                  ease: "Power4.easeOut",
                  onUpdate: function () {},
                }));
            },
            resizeHandler: function () {
              this.$el &&
                ((this.bounds = this.$el.getBoundingClientRect()),
                (this.topBound =
                  0 != this.y
                    ? this.scrollTop + this.bounds.top
                    : this.bounds.top),
                (this.totalWidth = this.global
                  ? this.pageSize.width
                  : this.$el.clientWidth),
                (this.totalHeight = this.global
                  ? this.pageSize.height
                  : this.$el.clientHeight));
            },
          },
        },
        d = (n(794), n(3)),
        component = Object(d.a)(
          c,
          function () {
            var t = this.$createElement,
              e = this._self._c || t;
            return e(
              "div",
              {
                staticClass: "outer-wrapper t-0",
                class: {
                  "w-100 h-100 perspective": !this.notWrapped,
                },
              },
              [
                e(
                  "div",
                  {
                    ref: "mouse-parallax",
                    staticClass: "mouse-parallax w-100 h-100 position-relative",
                    style: this.elStyle,
                  },
                  [
                    e(
                      "div",
                      {
                        staticClass: "inner-wrapper",
                      },
                      [this._t("default")],
                      2
                    ),
                  ]
                ),
              ]
            );
          },
          [],
          !1,
          null,
          "a8268e16",
          null
        );
      e.default = component.exports;
    },
    function (t, e, n) {
      "use strict";
      n.r(e);
      var r = {
          name: "parallax",
          mixins: [n(106).a],
          computed: {
            elStyle: function () {
              return {
                transform: this.elTransform,
                left: this.elLeft + "px",
                top: this.elTop + "px",
                height: this.elHeight + "px",
                width: this.elWidth + "px",
              };
            },
            elTransform: function () {
              var t = this.horizontal ? this.currentMove : 0,
                e = this.horizontal ? 0 : this.currentMove,
                n = "translate3d(".concat(t, "px, ").concat(e, "px, 0px)");
              if (this.scaleFactor) {
                var r =
                  (this.scaleFactor < 0 ? 1 - this.scaleFactor : 1) +
                  this.currentProgress * this.scaleFactor;
                n += "scale(".concat(r, ")");
              }
              return n;
            },
          },
        },
        o = (n(795), n(3)),
        component = Object(o.a)(
          r,
          function () {
            var t = this.$createElement,
              e = this._self._c || t;
            return e(
              "div",
              {
                ref: "holder",
                staticClass: "parallax-wrapper",
                class: {
                  wrapped: this.wrapped,
                  "not-wrapped": !this.wrapped,
                  "position-absolute": this.absolute,
                },
              },
              [
                e(
                  "div",
                  {
                    ref: "parallax",
                    staticClass: "parallax",
                    class: this.className,
                    style: this.elStyle,
                  },
                  [this._t("default")],
                  2
                ),
              ]
            );
          },
          [],
          !1,
          null,
          "1cc82325",
          null
        );
      e.default = component.exports;
    },
    function (t, e, n) {
      "use strict";
      n.r(e);
      var r = {
          data: function () {
            return {
              ratio: null,
            };
          },
          name: "proportion-div",
          props: ["proportion", "innerClasses", "src"],
          mounted: function () {
            if (this.src) {
              var t = this,
                image = new Image();
              image.addEventListener(
                "load",
                function () {
                  (t.naturalHeight = image.naturalHeight),
                    (t.naturalWidth = image.naturalWidth),
                    (t.ratio = t.naturalHeight / t.naturalWidth),
                    setTimeout(function () {
                      t.$bus.$emit("resize");
                    }, 100);
                },
                !1
              ),
                (image.src = this.src);
            }
          },
          computed: {
            paddingBottom: function () {
              return 100 * (this.ratio || this.proportion || 1) + "%";
            },
          },
        },
        o = n(3),
        component = Object(o.a)(
          r,
          function () {
            var t = this.$createElement,
              e = this._self._c || t;
            return e(
              "div",
              {
                staticClass: "proportion-div position-relative",
                style: {
                  paddingBottom: this.paddingBottom,
                },
              },
              [
                e(
                  "div",
                  {
                    staticClass: "position-absolute w-100 h-100 t-0 l-0",
                    class: this.innerClasses,
                  },
                  [this._t("default")],
                  2
                ),
              ]
            );
          },
          [],
          !1,
          null,
          null,
          null
        );
      e.default = component.exports;
    },
    function (t, e, n) {
      "use strict";
      n.r(e);
      var r = n(89),
        o = n.n(r),
        l = {
          beforeDestroy: function () {
            this.lmS.destroy(),
              this.$bus.$off("resize", this.resizeSmooth),
              this.$bus.$off("enableScrollbar", this.enable),
              this.$bus.$off("disableScrollbar", this.disable);
          },
          mounted: function () {
            var t = this;
            this.$bus.$on("resize", this.resizeSmooth),
              this.$bus.$on("enableScrollbar", this.enable),
              this.$bus.$on("disableScrollbar", this.disable),
              (this.lmS = new this.locomotiveScroll({
                el: this.$el,
                smooth: !0,
                scrollFromAnywhere: !0,
                firefoxMultiplier: 64,
              })),
              this.lmS.on("scroll", this.onScroll),
              o()(this.$el, function () {
                t.resizeSmooth();
              });
          },
          watch: {},
          methods: {
            onScroll: function (t) {
              this.$store.dispatch("app/SET_STATE", {
                scrollTop: Math.round(t.scroll.y),
              });
            },
            enable: function () {
              this.lmS.start();
            },
            disable: function () {
              this.lmS.stop();
            },
            resizeSmooth: function () {
              var t = this;
              this.lmS.update(),
                this.t && clearTimeout(this.t),
                (this.t = setTimeout(function () {
                  t.lmS.update(), console.log("resizeSmooth");
                }, 200));
            },
          },
        },
        c = (n(796), n(3)),
        component = Object(c.a)(
          l,
          function () {
            var t = this.$createElement;
            return (this._self._c || t)(
              "div",
              {
                attrs: {
                  "data-scroll-container": "",
                },
              },
              [this._t("default")],
              2
            );
          },
          [],
          !1,
          null,
          null,
          null
        );
      e.default = component.exports;
    },
    function (t, e, n) {
      "use strict";
      n.r(e);
      n(23), n(19), n(17), n(14), n(20);
      var r = n(13),
        o = n(18);

      function l(object, t) {
        var e = Object.keys(object);
        if (Object.getOwnPropertySymbols) {
          var n = Object.getOwnPropertySymbols(object);
          t &&
            (n = n.filter(function (t) {
              return Object.getOwnPropertyDescriptor(object, t).enumerable;
            })),
            e.push.apply(e, n);
        }
        return e;
      }
      var c = {
          name: "waypoint",
          data: function () {
            return {
              elements: null,
            };
          },
          props: {
            type: {
              default: "lines",
            },
            linesClass: !1,
            charClass: !1,
            wordsClass: !1,
            wrap: !1,
            tag: {
              default: "div",
            },
          },
          watch: {
            fontsLoaded: function () {
              this.splitLines();
            },
          },
          computed: (function (t) {
            for (var i = 1; i < arguments.length; i++) {
              var source = null != arguments[i] ? arguments[i] : {};
              i % 2
                ? l(Object(source), !0).forEach(function (e) {
                    Object(r.a)(t, e, source[e]);
                  })
                : Object.getOwnPropertyDescriptors
                ? Object.defineProperties(
                    t,
                    Object.getOwnPropertyDescriptors(source)
                  )
                : l(Object(source)).forEach(function (e) {
                    Object.defineProperty(
                      t,
                      e,
                      Object.getOwnPropertyDescriptor(source, e)
                    );
                  });
            }
            return t;
          })({}, Object(o.c)("app", ["fontsLoaded"])),
          mounted: function () {
            var t = this;
            setTimeout(function () {
              t.fontsLoaded && t.splitLines();
            }, 50);
          },
          methods: {
            splitLines: function (t) {
              (this.mySplitText = new SplitText(this.$el, {
                type: this.type,
                linesClass: this.linesClass,
                charClass: this.charClass,
                wordsClass: this.wordsClass,
              })),
                (this.elements = this.mySplitText[this.type]),
                this.$emit("splited", this.elements);
            },
          },
        },
        d = (n(797), n(3)),
        component = Object(d.a)(
          c,
          function () {
            var t = this.$createElement;
            return (this._self._c || t)(
              "div",
              {
                staticClass: "split-text",
              },
              [this._t("default")],
              2
            );
          },
          [],
          !1,
          null,
          "5ef187a6",
          null
        );
      e.default = component.exports;
    },
    function (t, e, n) {
      "use strict";
      n.r(e);
      var r = n(5),
        o = n.n(r),
        l = n(439),
        c = n.n(l),
        d = n(216),
        h = n.n(d),
        f = {
          name: "swiper",
          data: function () {
            return {
              dragging: !1,
              roundedWidth: "auto",
            };
          },
          props: [
            "customOptions",
            "update",
            "parallaxSpeed",
            "currentIndex",
            "overflowVisible",
            "noSwiping",
            "numOfSlides",
            "isFullSize",
            "realCurrentIndex",
            "autoplay",
          ],
          mixins: [],
          watch: {
            currentIndex: function (i) {
              var t = this,
                e = this;
              i !== o()(this, "mySwiper.realIndex") &&
                e.$nextTick(function () {
                  e.mySwiper.slideToLoop(i, t.swiperOptions.speed, !1);
                });
            },
            autoplay: function (p) {
              p
                ? this.mySwiper.autoplay.start()
                : this.mySwiper.autoplay.stop();
            },
            update: function (t) {
              this.$nextTick(this.updateSwiper);
            },
          },
          methods: {
            onTouchStart: function () {
              var t = this;
              this.dragTo && clearTimeout(this.dragTo),
                (this.dragTo = setTimeout(function () {
                  t.dragging = !0;
                }, 120));
            },
            onTouchEnd: function () {
              this.dragTo && clearTimeout(this.dragTo),
                (this.dragging = !1),
                this.$emit(
                  "update:realCurrentIndex",
                  o()(this, "mySwiper.realIndex")
                ),
                console.log("update swiper1");
            },
            resizeHandler: function () {
              var t = this,
                e = this.$refs.swiperWrapper.getBoundingClientRect();
              (this.roundedWidth = Math.round(e.width) + "px"),
                console.log("BOUNDS!!!", e),
                setTimeout(function () {
                  t.updateSwiper();
                }, 100);
            },
            emitStartChange: function () {
              var t = this;
              this.mySwiper &&
                (clearTimeout(this.ts),
                (this.ts = setTimeout(function () {
                  t.$emit(
                    "update:realCurrentIndex",
                    o()(t, "mySwiper.realIndex")
                  ),
                    console.log("update swiper2", o()(t, "mySwiper.realIndex"));
                }, 100)));
            },
            emitChange: function () {
              var t = this,
                e = this;
              clearTimeout(this.t),
                (this.t = setTimeout(function () {
                  e.$emit("update:currentIndex", o()(t, "mySwiper.realIndex")),
                    console.log("update swiper3", o()(t, "mySwiper.realIndex"));
                }, 100));
            },
            updateSwiper: function () {
              this.mySwiper &&
                (this.mySwiper.update(),
                this.mySwiper.pagination.render(),
                this.mySwiper.pagination.update());
            },
            nextSlide: function () {
              this.mySwiper.slideNext();
            },
            prevSlide: function () {
              this.mySwiper.slidePrev();
            },
          },
          beforeDestroy: function () {
            this.$bus.$off("updateSwiper", this.updateSwiper),
              this.$bus.$off("resize", this.resizeHandler);
          },
          mounted: function () {
            var t = this;
            this.mySwiper &&
              (this.mySwiper.autoplay.start(),
              this.autoplay || this.mySwiper.autoplay.stop(),
              this.mySwiper.slideToLoop(this.currentIndex, 0, !1),
              setTimeout(function () {
                t.updateSwiper();
              }, 400),
              this.$bus.$on("resize", this.resizeHandler),
              this.$nextTick(this.resizeHandler));
          },
          computed: {
            oldPhone: function () {
              return this.$store.getters["app/getState"]("oldPhone");
            },
            isIE: function () {
              return this.$store.getters["app/getState"]("isIE");
            },
            isEdge: function () {
              return this.$store.getters["app/getState"]("isEdge");
            },
            defaultOptions: function () {
              var t = {
                  loop: !1,
                  effect: this.$device.isMobileOrTablet ? "drag" : "fade",
                  threshold: 5,
                  touchRatio: this.$device.isMobileOrTablet ? 1 : 1.25,
                  speed: 1e3,
                  fadeEffect: {
                    crossFade: !1,
                  },
                  slidesPerView: "auto",
                  roundLengths: !0,
                  slideToClickedSlide: !1,
                  centeredSlides: !1,
                  mousewheelControl: !1,
                  grabCursor: !1,
                  spaceBetween: 0,
                },
                e = "vertical" == this.customOptions.direction ? "Y" : "X",
                n = {
                  watchSlidesProgress: !0,
                  on: {
                    init: function () {
                      this.autoplay.stop();
                    },
                    progress: function () {
                      for (var i = 0; i < this.slides.length; i++) {
                        var t = this.slides[i].progress * (0.65 * this.width);
                        this.slides[i].querySelector(
                          ".slide-bgimg"
                        ).style.transform = "translate"
                          .concat(e, "(")
                          .concat(t, "px)");
                      }
                    },
                    setTransition: function (t) {
                      for (var i = 0; i < this.slides.length; i++)
                        (this.slides[i].style.transition = t + "ms"),
                          (this.slides[i].querySelector(
                            ".slide-bgimg"
                          ).style.transition = t + "ms");
                    },
                  },
                };
              return (
                !this.customOptions.parallax ||
                  this.isIE ||
                  this.oldPhone ||
                  this.isEdge ||
                  c()(t, n),
                t
              );
            },
            swiperOptions: function () {
              return h()({}, this.defaultOptions, this.customOptions);
            },
          },
          components: {},
        },
        m = (n(799), n(3)),
        component = Object(m.a)(
          f,
          function () {
            var t = this,
              e = t.$createElement,
              n = t._self._c || e;
            return n(
              "div",
              {
                staticClass: "swiper position-relative",
                class: {
                  visible: t.overflowVisible,
                  "no-swiping": t.noSwiping,
                  dragging: t.dragging && !t.noSwiping,
                },
              },
              [
                n(
                  "div",
                  {
                    ref: "swiperWrapper",
                    staticClass: "swiper-parent w-100 h-100",
                  },
                  [
                    n(
                      "div",
                      {
                        directives: [
                          {
                            name: "swiper",
                            rawName: "v-swiper:mySwiper",
                            value: t.swiperOptions,
                            expression: "swiperOptions",
                            arg: "mySwiper",
                          },
                        ],
                        ref: "swiper",
                        staticClass: "swiper-container position-relative",
                        class: {
                          "h-100 w-100": t.isFullSize,
                        },
                        style: {
                          width: t.roundedWidth,
                        },
                        on: {
                          transitionEnd: t.emitChange,
                          touchStart: t.onTouchStart,
                          touchEnd: t.onTouchEnd,
                          transitionStart: t.emitStartChange,
                        },
                      },
                      [
                        n(
                          "div",
                          {
                            staticClass: "swiper-wrapper",
                          },
                          [t._t("default"), t._v(" "), t._t("bullets")],
                          2
                        ),
                      ]
                    ),
                  ]
                ),
              ]
            );
          },
          [],
          !1,
          null,
          null,
          null
        );
      e.default = component.exports;
    },
    function (t, e, n) {
      "use strict";
      n.r(e);
      n(71), n(72), n(14);
      var r = n(35),
        o = n.n(r),
        l = {
          name: "waypoint",
          data: function () {
            return {
              intersected: !1,
              observer: null,
              visibleOneDirection: !1,
              visible: !1,
              previousY: null,
              previousRatio: 0,
              scrollingEl: !1,
              scrollingDown: !0,
            };
          },
          mixins: [],
          props: {
            debug: !1,
            elementId: null,
            startOffset: {
              default: 0,
            },
            threshold: {
              default: function () {
                return null;
              },
            },
            open: {
              default: !1,
            },
          },
          beforeDestroy: function () {
            this.observer.disconnect(),
              this.$bus.$off("resize", this.resizeHandler);
          },
          mounted: function () {
            this.$bus.$on("resize", this.resizeHandler),
              (this.scrollingEl =
                (this.elementId && document.getElementById(this.elementId)) ||
                this.$el),
              "IntersectionObserver" in window &&
              "IntersectionObserverEntry" in window &&
              "intersectionRatio" in window.IntersectionObserverEntry.prototype
                ? ((this.observer = new IntersectionObserver(this.callback, {
                    threshold: this.threshold,
                  })),
                  this.observer.observe(this.$el))
                : (this.visibleOneDirection = this.visible = !0);
          },
          watch: {
            visibleOneDirection: function (t) {
              this.$emit("toggleOneDirectionVisible", {
                visible: t,
                el: this.$el,
              });
            },
            visible: function (t) {
              this.$emit("toggleVisible", {
                visible: t,
                el: this.$el,
              });
            },
            open: (function (t) {
              function e() {
                return t.apply(this, arguments);
              }
              return (
                (e.toString = function () {
                  return t.toString();
                }),
                e
              );
            })(function () {
              open && this.resizeHandler(!0);
            }),
            scrollTop: function (t, e) {
              this.scrollingDown = t > e || t < 3;
            },
          },
          computed: {
            pageHeight: function () {
              return this.$store.getters["app/getState"]("height");
            },
            scrollTop: function () {
              return this.$store.getters["app/getState"]("scrollTop");
            },
          },
          methods: {
            resizeHandler: function (t) {
              var e = this;
              t &&
                !this.visibleOneDirection &&
                setTimeout(function () {
                  e.scrollingDown = !0;
                  var t = e.$el.getBoundingClientRect();
                  (e.previousY = t.top),
                    (e.visible = e.visibleOneDirection = t.top < e.pageHeight);
                });
            },
            callback: function (t) {
              var e = t[0],
                n = e.boundingClientRect.y,
                r = this.scrollingEl.style.opacity;
              (r = r && r.length > 0 ? 1 * r : null),
                (!this.threshold || 0 == this.threshold[0] || n >= 0) &&
                  (this.visible = o()(r)
                    ? e.isIntersecting && 0 != r
                    : e.isIntersecting),
                this.scrollingDown && e.isIntersecting
                  ? (this.visibleOneDirection = !0)
                  : !this.scrollingDown &&
                    !e.isIntersecting &&
                    -n < this.pageHeight &&
                    0 != r &&
                    (this.visibleOneDirection = !1);
            },
          },
        },
        c = n(3),
        component = Object(c.a)(
          l,
          function () {
            var t = this.$createElement;
            return (this._self._c || t)(
              "div",
              {
                ref: "waypoint",
                staticClass: "waypoint",
              },
              [this._t("default")],
              2
            );
          },
          [],
          !1,
          null,
          null,
          null
        );
      e.default = component.exports;
    },
  ],
  [[449, 2, 1, 3]],
]);
